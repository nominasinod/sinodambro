package com.mbn.suite;

import com.mbn.suite.util.Encryption;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@ManagedBean
@SessionScoped
public class LoginMB implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userName;
    private String password;

    public void iniciarSesion() {
        System.out.println("Iniciando sesion===>");

        if (userName == null || userName.trim().equals("") || password == null || password.trim().equals("")) {
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Ingrese su usuario y contraseña."));
            return;
        }

        try {
            System.out.println("entra try");
            InputStream conPropFile = this.getClass().getResourceAsStream("/connection.properties");
            Properties connectionProp = new Properties();
            connectionProp.load(conPropFile);

            String url = "jdbc:postgresql://" + connectionProp.getProperty("dbHost") + ":" + connectionProp.getProperty("dbPort") + "/" + connectionProp.getProperty("dbName");
            Properties props = new Properties();
            props.setProperty("user", connectionProp.getProperty("dbUser"));
            props.setProperty("password", connectionProp.getProperty("dbPassword"));
//            props.setProperty("sslmode", connectionProp.getProperty("sslmode"));

            Connection connection = DriverManager.getConnection(url, props);

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM sgco.tsgcousuarios WHERE cod_usuariosistema=? AND des_contrasenasistema=?");
            stmt.setString(1, userName);
            stmt.setString(2, password);
            stmt.setMaxRows(1);
            ResultSet rs = stmt.executeQuery();

            String cod_usuario = "0";
            String cod_empleado = "0";

            while (rs.next()) {
                System.out.println("cod_usuario: " + rs.getString("cod_usuario"));
                System.out.println("cod_empleado: " + rs.getString("cod_empleado"));
                cod_usuario = rs.getString("cod_usuario");
                cod_empleado = rs.getString("cod_empleado");
                break;
            }

            rs.close();
            stmt.close();
            connection.close();

            if (!cod_usuario.equals("0") && !cod_empleado.equals("0")) {
                System.out.println("Login correcto");
                //Login correcto
                crearCookie(cod_usuario, cod_empleado);
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                context.redirect("index.xhtml");
            } else {
                //Error con credenciales
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "!Usuario o contraseña incorrectos!"));
                return;
            }

        } catch (SQLException | FileNotFoundException ex) {
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Ocurrio un error con:" + ex));
        } catch (IOException ex) {
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Ocurrio un error con:" + ex));
        } finally {
            return;
        }
    }

    public void crearCookie(String cod_usuario, String cod_empleado) throws Exception {
        try {
            //Encript data
            Encryption encrypt = new Encryption();
            cod_usuario = encrypt.encrypt(cod_usuario);
            cod_empleado = encrypt.encrypt(cod_empleado);

            // create cookies
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            Cookie usuarioCookie = new Cookie("mbnUmbn", cod_usuario);
            usuarioCookie.setPath("/");
            Cookie empleadoCookie = new Cookie("mbnEmbn", cod_empleado);
            empleadoCookie.setPath("/");
            httpServletResponse.addCookie(usuarioCookie);
            httpServletResponse.addCookie(empleadoCookie);
        } catch (Exception e) {
            System.out.println("Error con: " + e);
        }

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
