/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.DPruebaDTO;
import com.mbn.sinod.model.negocio.PruebaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ambrosio
 */
@RestController
@RequestMapping("/ws")
public class PruebaWS {
    @Autowired
    PruebaService PruebaService;

    @RequestMapping(method = RequestMethod.GET, value = "/Prueba",
//    @RequestMapping(method = RequestMethod.GET, value = "/productosProveedor/{proveedorId}/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DPruebaDTO listaPrueba() {
        return PruebaService.prueba();
    }
}
