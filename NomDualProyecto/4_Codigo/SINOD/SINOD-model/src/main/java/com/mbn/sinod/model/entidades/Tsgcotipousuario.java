/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ambrosio
 */
@Entity
@Table(name = "tsgcotipousuario", catalog = "suite", schema = "sgco")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgcotipousuario.findAll", query = "SELECT t FROM Tsgcotipousuario t")})
public class Tsgcotipousuario implements Serializable {

    private static final long serialVersionUID = 1L;
    	@Id
    @Basic(optional = false)
    @Column(name = "cod_tipousuario")
    private Integer codTipousuario;
    @Basic(optional = false)
    @Column(name = "cod_usuario")
    private int codUsuario;
    @Basic(optional = false)
    @Column(name = "cod_sistema")
    private int codSistema;
    @Basic(optional = false)
    @Column(name = "cod_rol")
    private String codRol;

    public Tsgcotipousuario() {
    }

    public Tsgcotipousuario(Integer codTipousuario) {
        this.codTipousuario = codTipousuario;
    }

    public Tsgcotipousuario(Integer codTipousuario, int codUsuario, int codSistema, String codRol) {
        this.codTipousuario = codTipousuario;
        this.codUsuario = codUsuario;
        this.codSistema = codSistema;
        this.codRol = codRol;
    }

    public Integer getCodTipousuario() {
        return codTipousuario;
    }

    public void setCodTipousuario(Integer codTipousuario) {
        this.codTipousuario = codTipousuario;
    }

    public int getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
    }

    public int getCodSistema() {
        return codSistema;
    }

    public void setCodSistema(int codSistema) {
        this.codSistema = codSistema;
    }

    public String getCodRol() {
        return codRol;
    }

    public void setCodRol(String codRol) {
        this.codRol = codRol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipousuario != null ? codTipousuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgcotipousuario)) {
            return false;
        }
        Tsgcotipousuario other = (Tsgcotipousuario) object;
        if ((this.codTipousuario == null && other.codTipousuario != null) || (this.codTipousuario != null && !this.codTipousuario.equals(other.codTipousuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgcotipousuario[ codTipousuario=" + codTipousuario + " ]";
    }
    
}
