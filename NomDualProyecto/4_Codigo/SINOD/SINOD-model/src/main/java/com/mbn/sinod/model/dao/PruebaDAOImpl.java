/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.dto.PruebaDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;
import org.hibernate.transform.Transformers;


public class PruebaDAOImpl extends GenericDAOImpl<Tsgrhempleados, Integer> implements PruebaDAO {

    @Override
    public List<PruebaDTO> prueba() {
        List<PruebaDTO> resultado = (List<PruebaDTO>) 
                getSession().createSQLQuery("SELECT * FROM sgnom.prueba();")
                .addScalar("desPuesto")
                .addScalar("nomCompleto")
                .addScalar("desNbArea")
                .setResultTransformer(Transformers.aliasToBean(PruebaDTO.class))
                .list();
        
        return resultado;
    }

}
