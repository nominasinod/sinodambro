/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.PruebaDAO;
import com.mbn.sinod.model.dto.DPruebaDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;


public class PruebaServiceImpl extends BaseServiceImpl<Tsgrhempleados, Integer> implements PruebaService {
    private static final Logger logger = Logger.getLogger(PruebaService.class.getName());
    
    @Autowired
    private PruebaDAO PruebaDAO;
    
    @Override
    public DPruebaDTO prueba() {
        DPruebaDTO dto = new DPruebaDTO();
        dto.setListaprueba(getPruebaDAO().prueba());
        return dto;
    }    

    /**
     * @return the PruebaDAO
     */
    public PruebaDAO getPruebaDAO() {
        return PruebaDAO;
    }

    /**
     * @param PruebaDAO the PruebaDAO to set
     */
    public void setPruebaDAO(PruebaDAO PruebaDAO) {
        this.PruebaDAO = PruebaDAO;
    }
}
