/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dto;

/**
 *
 * @author ambrosio
 */
public class PruebaDTO {
    //despuesto character varying, nomcompleto text, desnbarea  character varying
    private String desPuesto;
    private String nomCompleto;
    private String desNbArea;

    /**
     * @return the desPuesto
     */
    public String getDesPuesto() {
        return desPuesto;
    }

    /**
     * @param desPuesto the desPuesto to set
     */
    public void setDesPuesto(String desPuesto) {
        this.desPuesto = desPuesto;
    }

    /**
     * @return the nomCompleto
     */
    public String getNomCompleto() {
        return nomCompleto;
    }

    /**
     * @param nomCompleto the nomCompleto to set
     */
    public void setNomCompleto(String nomCompleto) {
        this.nomCompleto = nomCompleto;
    }

    /**
     * @return the desNbArea
     */
    public String getDesNbArea() {
        return desNbArea;
    }

    /**
     * @param desNbArea the desNbArea to set
     */
    public void setDesNbArea(String desNbArea) {
        this.desNbArea = desNbArea;
    }
}
