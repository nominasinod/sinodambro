/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomcabecera;
import java.util.List;


public class CabeceraDAOImpl extends GenericDAOImpl<Tsgnomcabecera, Integer> implements CabeceraDAO {

    @Override
    public List<Tsgnomcabecera> listarCabeceras() {
        return findAll();
    }

    @Override
    public Boolean guardarAtualizarCabecera(Tsgnomcabecera cabecera) {
        try {
            _saveOrUpdate(cabecera);
//            save(catIncidencia);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
}
