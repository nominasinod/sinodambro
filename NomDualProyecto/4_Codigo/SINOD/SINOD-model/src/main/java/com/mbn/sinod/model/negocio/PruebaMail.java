/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.negocio;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author ambrosio
 */
public class PruebaMail {
    private final Properties properties = new Properties();
    private String pass;
    private Session session;
    
    private void init(){
        properties.put("mail.stmp.host", "mail.gmail.com");
        properties.put("mail.stmp.starttls.enable", "true");
        properties.put("mail.stmp.port", 25);
        properties.put("mail.stmp.mail.sender", "mnflambr1@gmail.com");
        properties.put("mail.stmp.user", "usuario");
        properties.put("mail.stmp.auth", "true");
        
        session = Session.getDefaultInstance(properties);
    }
    
    public void sendEmail(){
        init();
        
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress((String)properties.get("mail.smtp.mail.sender")));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("mnflambr@gmail.com"));
            message.setSubject("prueba");
            message.setText("texto");
            Transport t = session.getTransport("smtp");
            t.connect((String)properties.getProperty("mail.smtp.user"), "allwiner.1");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        } catch (MessagingException e) {
        }
    }
}
