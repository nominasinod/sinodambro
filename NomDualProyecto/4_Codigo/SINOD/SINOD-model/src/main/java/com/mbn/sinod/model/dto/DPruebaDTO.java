/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author ambrosio
 */
public class DPruebaDTO extends GenericDTO{
    private DPruebaDTO dprueba;
    private List<PruebaDTO> listaprueba;

    /**
     * @return the listaprueba
     */
    public List<PruebaDTO> getListaprueba() {
        return listaprueba;
    }

    /**
     * @param listaprueba the listaprueba to set
     */
    public void setListaprueba(List<PruebaDTO> listaprueba) {
        this.listaprueba = listaprueba;
    }

    /**
     * @return the dprueba
     */
    public DPruebaDTO getDprueba() {
        return dprueba;
    }

    /**
     * @param dprueba the dprueba to set
     */
    public void setDprueba(DPruebaDTO dprueba) {
        this.dprueba = dprueba;
    }
}
