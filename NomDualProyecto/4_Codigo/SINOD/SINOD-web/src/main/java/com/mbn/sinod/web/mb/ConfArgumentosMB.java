/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbn.sinod.model.dto.ArgumentoDTO;
import com.mbn.sinod.model.entidades.Tsgnomargumento;
import com.mbn.sinod.model.entidades.Tsgnomfuncion;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import com.mbn.sinod.web.client.ArgumentoWSClient;
import com.mbn.sinod.web.client.FuncionWSClient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mariana
 */
@Named(value = "confArgumentosMB")
@ViewAccessScoped
public class ConfArgumentosMB implements Serializable {

    private String clave;
    private String descripcion;
    private String titulo;
    private Tsgnomargumento argumentoSeleccionado;
    private List<Tsgnomargumento> listaArgumentos;
    private List<ArgumentoDTO> listaArgumentosDTO;
    private List<Tsgnomfuncion> listaFunciones;
    private String mensaje;
    private Boolean boton;
    private Integer tipoArgumento;
    private Boolean tipoArgu;
    private Tsgrhempleados empleado;

    @PostConstruct
    public void iniciarVariables() {
        this.setEmpleado((Tsgrhempleados) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("empleado"));
        listaFunciones = new ArrayList<>();
        setListaArgumentos(ArgumentoWSClient.listarArgumentos());
        setListaFunciones(FuncionWSClient.listarFunciones());

    }

    public void eliminarArgumento() throws JsonProcessingException {
        ArgumentoWSClient.eliminarArgumento(getArgumentoSeleccionado().getCodArgumentoid());
        setListaArgumentos(ArgumentoWSClient.listarArgumentos());
        setMensaje("Los datos han sido eliminados exitosamente");
        generarMensaje();
    }

    public void guardar() throws JsonProcessingException {
        ArgumentoDTO dtoRespuesta = new ArgumentoDTO();
        if (getBoton().equals(true)) {
            //si va a crear un nuevo argumento
            getArgumentoSeleccionado().setAudCodcreadopor(getEmpleado().getCodEmpleado());
            getArgumentoSeleccionado().setAudFeccreacion(new Date());

        } else {
            
            if(getArgumentoSeleccionado().getCodTipoargumento().equals('0'))
            {
                System.out.println("El argumento es variable");
                getArgumentoSeleccionado().setImpValorconst(null);
            }
             if(getArgumentoSeleccionado().getCodTipoargumento().equals('1'))
             {
                 System.out.println("El argumento es constante"); 
                 getArgumentoSeleccionado().setDesFuncionbd(null);
             }
            
            getArgumentoSeleccionado().setAudCodmodificadopor(getEmpleado().getCodEmpleado());
            getArgumentoSeleccionado().setAudFecmodificacion(new Date());
        }

        getArgumentoSeleccionado().setBolEstatus(true);
        getArgumentoSeleccionado().setCodNbargumento(getArgumentoSeleccionado().getCodNbargumento().toUpperCase());
        getArgumentoSeleccionado().setCodNbargumento(getArgumentoSeleccionado().getCodNbargumento().replace(" ", ""));
        
        getArgumentoSeleccionado().setCodClavearg(getArgumentoSeleccionado().getCodClavearg().toUpperCase());
        getArgumentoSeleccionado().setCodClavearg(getArgumentoSeleccionado().getCodClavearg().replace(" ", ""));
        
        dtoRespuesta.setArgumento(getArgumentoSeleccionado());
        ArgumentoWSClient.guardarArgumento(dtoRespuesta);
        getListaArgumentos().clear();
        setListaArgumentos(ArgumentoWSClient.listarArgumentos());
        
        setMensaje("Los datos han sido guardados exitosamente");
        generarMensaje();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ArgumentoDialogo').hide();");
        
    }
    
    public void comparar() {
        for (int i = 0; i < getListaArgumentos().size(); i++) {
            if (getListaArgumentos().get(0).getCodClavearg().equals(getArgumentoSeleccionado().getCodClavearg())) {
                System.out.println("Entre al if de comparar");
                generarMensaje("Clave repetida", FacesMessage.SEVERITY_WARN);
                break;
            }
           
        }
        System.out.println("fuera del for");
        
    }
    
     public void compararNombre() {
        for (int i = 0; i < getListaArgumentos().size(); i++) {
            if (getListaArgumentos().get(0).getCodNbargumento().toUpperCase().equals(getArgumentoSeleccionado().getCodNbargumento().toUpperCase())) {
                System.out.println("Entre al if de comparar");
                generarMensaje("Clave repetida", FacesMessage.SEVERITY_WARN);
                break;
            }
           
        }
        System.out.println("fuera del for");
        
    }
     
    public void generarMensaje(String mensaje, FacesMessage.Severity sever) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(sever, "Mensaje: ", mensaje));
    }

    public void hola()
    {
        System.out.println("ambrosio: " + 
                getArgumentoSeleccionado().getCodTipoargumento());
    }

    public void generarMensaje() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje: ", getMensaje()));
    } 

    public void dialogoCrear() {
        argumentoSeleccionado = new Tsgnomargumento();
        setBoton(true);
        setTitulo("Nuevo argumento");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ArgumentoDialogo').show();");
    }

    public void dialogoEliminar(Tsgnomargumento argumento) {
        
        setArgumentoSeleccionado(argumento);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogEliminar').show();");
    }

    public void dialogoEditar(Tsgnomargumento argumento) {
        setBoton(false);
        setTitulo("Modificar argumento");
        setArgumentoSeleccionado(argumento);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('ArgumentoDialogo').show();");
    }

    public void limpiarDialogo() {
        setClave("");
        setDescripcion("");
        setTitulo("");

    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Tsgnomargumento getArgumentoSeleccionado() {
        return argumentoSeleccionado;
    }

    public void setArgumentoSeleccionado(Tsgnomargumento argumentoSeleccionado) {
        this.argumentoSeleccionado = argumentoSeleccionado;
    }

    public List<Tsgnomargumento> getListaArgumentos() {
        return listaArgumentos;
    }

    public void setListaArgumentos(List<Tsgnomargumento> listaArgumentos) {
        this.listaArgumentos = listaArgumentos;
    }

    public List<ArgumentoDTO> getListaArgumentosDTO() {
        return listaArgumentosDTO;
    }

    public void setListaArgumentosDTO(List<ArgumentoDTO> listaArgumentosDTO) {
        this.listaArgumentosDTO = listaArgumentosDTO;
    }

    public List<Tsgnomfuncion> getListaFunciones() {
        return listaFunciones;
    }

    public void setListaFunciones(List<Tsgnomfuncion> listaFunciones) {
        this.listaFunciones = listaFunciones;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getBoton() {
        return boton;
    }

    public void setBoton(Boolean boton) {
        this.boton = boton;
    }

    public Integer getTipoArgumento() {
        return tipoArgumento;
    }

    public void setTipoArgumento(Integer tipoArgumento) {
        this.tipoArgumento = tipoArgumento;
    }

    public Boolean getTipoArgu() {
        return tipoArgu;
    }

    public void setTipoArgu(Boolean tipoArgu) {
        this.tipoArgu = tipoArgu;
    }

    /**
     * @return the empleado
     */
    public Tsgrhempleados getEmpleado() {
        return empleado;
    }

    /**
     * @param empleado the empleado to set
     */
    public void setEmpleado(Tsgrhempleados empleado) {
        this.empleado = empleado;
    }
    
    
}
