/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.web.mb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbn.sinod.model.dto.CatIncidenciaDTO;
import com.mbn.sinod.model.entidades.Tsgnomcatincidencia;
import com.mbn.sinod.web.client.CatIncidenciaWSClient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Ambrosio Muñoz Flores
 */
@Named(value = "confIncidenciaMB")
@ViewAccessScoped
public class ConfIncidenciaMB implements Serializable {

    private String titulo;

    private List<Tsgnomcatincidencia> listaCatIncidencias;
    private Tsgnomcatincidencia catIncidenciaSeleccionado;

    @PostConstruct
    public void iniciarVariables() {
        listaCatIncidencias = new ArrayList();
        setCatIncidenciaSeleccionado(new Tsgnomcatincidencia());
        titulo = "";
        setListaCatIncidencias(CatIncidenciaWSClient.listarCatIncidencias());
    }

    public void dialogoCrearConfig() {
        setTitulo("Nueva");
        setCatIncidenciaSeleccionado(new Tsgnomcatincidencia());
        catIncidenciaSeleccionado.setBolEstatus(true);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleConfIncidencia').show();");
    }

    public void dialogoEditarConfig(Tsgnomcatincidencia catIncidencia) {
        setTitulo("Modificar");
        setCatIncidenciaSeleccionado(catIncidencia);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleConfIncidencia').show();");
    }

    public void dialogoEliminar(Tsgnomcatincidencia catIncidencia) {
        setCatIncidenciaSeleccionado(catIncidencia);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('eliminarConfIncidencia').show();");
    }

    public void comparar() {
        for (int i = 0; i < getListaCatIncidencias().size(); i++) {
            if (getListaCatIncidencias().get(i).getCodClaveincidencia().toUpperCase().equals(getCatIncidenciaSeleccionado().getCodClaveincidencia().toUpperCase())) {
                generarMensaje("Clave repetida", FacesMessage.SEVERITY_WARN);
                break;
            }
        }
    }

    public void guardar() {
        try {
            CatIncidenciaDTO dtoRespuesta = new CatIncidenciaDTO();
            getCatIncidenciaSeleccionado().setCodClaveincidencia(getCatIncidenciaSeleccionado().getCodClaveincidencia().toUpperCase());
            dtoRespuesta.setCatIncidencia(getCatIncidenciaSeleccionado());
            dtoRespuesta = CatIncidenciaWSClient.guardarActualizarCatInci(dtoRespuesta);
            getListaCatIncidencias().clear();
            setListaCatIncidencias(CatIncidenciaWSClient.listarCatIncidencias());
            generarMensaje("Incidencia Guardada", FacesMessage.SEVERITY_INFO);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ConfIncidenciaMB.class.getName()).log(Level.SEVERE, null, ex);
            generarMensaje("Error", FacesMessage.SEVERITY_ERROR);
        } catch (Exception e) {
            generarMensaje("Campos Vacios", FacesMessage.SEVERITY_WARN);
        }
    }

    public void eliminar() {
        try {
            CatIncidenciaWSClient.eliminarCatIncidenciasId(getCatIncidenciaSeleccionado().getCodCatincidenciaid());
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ConfIncidenciaMB.class.getName()).log(Level.SEVERE, null, ex);
            generarMensaje("Error", FacesMessage.SEVERITY_ERROR);
        }
    }

    public void generarMensaje(String mensaje, FacesMessage.Severity sever) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(sever, "Mensaje: ", mensaje));
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the listaCatIncidencias
     */
    public List<Tsgnomcatincidencia> getListaCatIncidencias() {
        return listaCatIncidencias;
    }

    /**
     * @param listaCatIncidencias the listaCatIncidencias to set
     */
    public void setListaCatIncidencias(List<Tsgnomcatincidencia> listaCatIncidencias) {
        this.listaCatIncidencias = listaCatIncidencias;
    }

    /**
     * @return the catIncidenciaSeleccionado
     */
    public Tsgnomcatincidencia getCatIncidenciaSeleccionado() {
        return catIncidenciaSeleccionado;
    }

    /**
     * @param catIncidenciaSeleccionado the catIncidenciaSeleccionado to set
     */
    public void setCatIncidenciaSeleccionado(Tsgnomcatincidencia catIncidenciaSeleccionado) {
        this.catIncidenciaSeleccionado = catIncidenciaSeleccionado;
    }
}
