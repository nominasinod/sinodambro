package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.ListaAltaDePersonalDTO;
import com.mbn.sinod.model.negocio.ListaAltaDePersonalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ivette
 */
@RestController //Incluye las anotaciones @Controller y @ResponseBody,simplifica la implementación del controlador
@RequestMapping("/ws")
public class ListaAltaDePersonalWS {

    @Autowired
    ListaAltaDePersonalService listaAltaDePersonalService;

    /**
     *
     * @return
     */
    @RequestMapping(value = "altaNom/listarEmpleados",
            method = RequestMethod.GET)
    @ResponseBody
    public ListaAltaDePersonalDTO obtenerListaParaAlta() {
        return listaAltaDePersonalService.listaInformacionParaAltaNom();
    }

}
