package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.ConceptoDTO;
import com.mbn.sinod.model.negocio.ConceptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ambrosio
 */
@RestController
@RequestMapping("/ws/concepto")
public class ConceptoWS {

    @Autowired
    ConceptoService tsgnomconceptoService;

    /**
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/listarConceptos",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody// anotacion que nos permite retornar un json
    public ConceptoDTO listarConceptos() {
        return tsgnomconceptoService.listarConceptos();
    }

    /**
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/listarConceptos/percepcion",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody// anotacion que nos permite retornar un json
    public ConceptoDTO listarConceptosPercepcion() {
        return tsgnomconceptoService.listarConceptosPercepcion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/listarConceptos/deduccion",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody// anotacion que nos permite retornar un json
    public ConceptoDTO listarConceptosDeduccion() {
        return tsgnomconceptoService.listarConceptosDeduccion();
    }

    /**
     *
     * @param conceptoid
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/eliminar/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ConceptoDTO eliminarConcepto(@RequestParam Integer conceptoid) {
        return tsgnomconceptoService.eliminarConcepto(conceptoid);
    }

    /**
     *
     * @param concepto
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/guardar/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ConceptoDTO guardar(@RequestBody ConceptoDTO concepto) {
        return tsgnomconceptoService.guardarConcepto(concepto);
    }

    /**
     *
     * @param concepto
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/guardarPrioridad/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ConceptoDTO guardarPrioridad(@RequestBody ConceptoDTO concepto) {
        return tsgnomconceptoService.guardarPrioridad(concepto);
    }

}
