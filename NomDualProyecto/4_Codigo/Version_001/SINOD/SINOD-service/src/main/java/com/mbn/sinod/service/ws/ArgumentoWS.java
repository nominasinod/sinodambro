package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.ArgumentoDTO;
import com.mbn.sinod.model.negocio.ArgumentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mariana
 */
@RestController //Incluye las anotaciones @Controller y @ResponseBody,simplifica la implementación del controlador
@RequestMapping("/ws/argumento")
public class ArgumentoWS {

    @Autowired
    ArgumentoService argumento;

    /**
     *
     * @return
     */
    @RequestMapping(value = "/obtenerArgumento/", method = RequestMethod.GET)
    @ResponseBody
    public ArgumentoDTO obtenerArgumento() {
        return argumento.listarArgumentos();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/obtenerArgumentoConstante/", method = RequestMethod.GET)
    @ResponseBody
    public ArgumentoDTO obtenerArgumentoConstante() {
        return argumento.listarArgumentosConstantes();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/obtenerArgumentoVariable/", method = RequestMethod.GET)
    @ResponseBody
    public ArgumentoDTO obtenerArgumentoVariable() {
        return argumento.listarArgumentosVariables();
    }

    /**
     *
     * @param argu
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/guardar/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ArgumentoDTO guardar(@RequestBody ArgumentoDTO argu) {
        return argumento.guardar(argu);
    }

    /**
     *
     * @param argumentoid
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/eliminar/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ArgumentoDTO eliminar(@RequestParam Integer argumentoid) {
        return argumento.eliminarArgumento(argumentoid);
    }

}
