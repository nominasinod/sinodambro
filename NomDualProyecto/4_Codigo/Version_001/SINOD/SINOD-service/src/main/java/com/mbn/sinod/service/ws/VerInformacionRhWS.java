package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.VerInformacionRhDTO;
import com.mbn.sinod.model.negocio.VerInformacionRhService;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ivette
 */
@RestController
@RequestMapping("/ws")
public class VerInformacionRhWS implements Serializable {

    @Autowired
    VerInformacionRhService verInformacionRh;

    /**
     *
     * @param empleadoRh
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "idRh/listaInformacionRh", produces
            = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VerInformacionRhDTO listaInformacionRh(@RequestParam Integer empleadoRh) {

        return verInformacionRh.listaInformacionRh(empleadoRh);

    }

}
