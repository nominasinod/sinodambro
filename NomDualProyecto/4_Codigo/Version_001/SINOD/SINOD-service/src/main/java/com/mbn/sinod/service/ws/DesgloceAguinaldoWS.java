/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.DesgloceAguinaldoDTO;
import com.mbn.sinod.model.negocio.DesgloceAguinaldoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ivette
 */
@RestController
@RequestMapping("/ws")
public class DesgloceAguinaldoWS {
    
    @Autowired
    DesgloceAguinaldoService desgloceAguinaldoService;
    /**
     * 
     * @param empQuinc
     * @return 
     */
    
    @RequestMapping(method = RequestMethod.POST, value = "/desgloceAguinaldoEmp",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody// anotacion que nos permite retornar un json
    public DesgloceAguinaldoDTO listarDesgloceAguinaldo(@RequestParam Integer empQuinc) {
        return desgloceAguinaldoService.listaDesgloceAguinaldoEmp(empQuinc);
                
     
    }
    
}
