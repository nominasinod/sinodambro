package com.mbn.sinod.service.ws;

import com.mbn.sinod.model.dto.DetalleDesgloPercepDTO;
import com.mbn.sinod.model.negocio.DetalleDesglosePercepcionService;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author torre
 */
@RestController
@RequestMapping("/ws")

public class DetalleDesglosePercepcionWS implements Serializable {

    @Autowired
    DetalleDesglosePercepcionService detalleDesglosePercepcionService;

    /**
     *
     * @param cod_empleado
     * @param cod_cabecera
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/detalleDesglocePercecion/",
            produces = MediaType.APPLICATION_JSON_VALUE)

    @ResponseBody
    public DetalleDesgloPercepDTO detalleDesglosePercepcion(@RequestParam(value = "cod_empleado") Integer cod_empleado,
            @RequestParam(value = "cod_cabecera") Integer cod_cabecera) {
        return detalleDesglosePercepcionService.detalleDesglocePercepDTO(cod_empleado, cod_cabecera);

    }
}
