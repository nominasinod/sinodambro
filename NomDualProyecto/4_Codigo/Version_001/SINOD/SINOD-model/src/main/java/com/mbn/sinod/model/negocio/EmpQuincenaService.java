package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.EmpQuincenaDTO;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;

/**
 *
 * @author mipe
 */
public interface EmpQuincenaService extends BaseService<Tsgnomempquincena, Integer> {

    /**
     *
     * @return
     */
    EmpQuincenaDTO listarEmpQuincena();

}
