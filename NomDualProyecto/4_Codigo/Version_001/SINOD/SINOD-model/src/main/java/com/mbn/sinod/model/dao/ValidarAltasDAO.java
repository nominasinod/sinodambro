package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.ValidacionAltasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author mariana
 */
public interface ValidarAltasDAO extends GenericDAO<Tsgnomempleados, Integer> {

    /**
     * Obtiene una lista de las altas a validar por Finanzas en el sistema de nómina
     * 
     * @return
     */
    List<ValidacionAltasDTO> listarAltasAValidar();

    /**
     * Ayuda a la validación y rechazo de altas realizandolo una a la vez
     * 
     * @param listaInformacionValidar
     * @return
     */
    boolean validarCadaAlta(List<ValidacionAltasDTO> listaInformacionValidar);

    /**
     * Ayuda a validar todas las altas en conjunto
     * 
     * @param listaInformacionValidar
     * @return
     */
    boolean validarAltasAceptadas(List<ValidacionAltasDTO> listaInformacionValidar);

    /**
     * Ayuda a rechazar todas las altas en conjunto
     * 
     * @param listaInformacionValidar
     * @return
     */
    boolean validarAltasRechazadas(List<ValidacionAltasDTO> listaInformacionValidar);
}
