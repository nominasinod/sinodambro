package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.dto.DetalleDesglosePercepcionDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import java.util.List;
import org.hibernate.transform.Transformers;

/**
 *
 * @author mipe
 */
public class DetalleDesglosePercepcionDAOImpl extends GenericDAOImpl<Tsgnomcncptoquinc, Integer>
        implements DetalleDesglosePercepcionDAO {

    @Override
    public List<DetalleDesglosePercepcionDTO> percepcionesPosEmp(Integer cod_empleado, Integer cod_cabecera) {
        List<DetalleDesglosePercepcionDTO> detalleDesglosePercepcion = (List<DetalleDesglosePercepcionDTO>) getSession().createSQLQuery("SELECT * FROM sgnom.detalle_desglose_percepcion(?,?);")
                .addScalar("imp_concepto")
                .addScalar("imp_exento")
                .addScalar("imp_gravado")
                .addScalar("nombre")
                .addScalar("clave")
                .addScalar("cantidad")
                .addScalar("cod_conceptoid_fk")
                .setResultTransformer(Transformers.aliasToBean(DetalleDesglosePercepcionDTO.class))
                .setInteger(0, cod_empleado)
                .setInteger(1, cod_cabecera)
                .list();
        System.out.println("SQL Desglose percepcion model");
        return detalleDesglosePercepcion;
    }

}
