package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.dto.ListaAltaDePersonalDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Ivette
 */
public class ListaAltaDePersonalDAOImpl extends GenericDAOImpl<Tsgrhempleados, Integer>
        implements ListaAltaDePersonalDAO {

    @Override
    public List<ListaAltaDePersonalDTO> listaInformacionParaAltaNom() {
        List<ListaAltaDePersonalDTO> listaInformacionParaAltaNom = (List<ListaAltaDePersonalDTO>) getSession().createSQLQuery("select * from sgnom.empledossinaltanom();")
                .addScalar("cod_empleado")
                .addScalar("nomempleado")
                .setResultTransformer(Transformers.aliasToBean(ListaAltaDePersonalDTO.class))
                .list();

        return listaInformacionParaAltaNom;
    }

}
