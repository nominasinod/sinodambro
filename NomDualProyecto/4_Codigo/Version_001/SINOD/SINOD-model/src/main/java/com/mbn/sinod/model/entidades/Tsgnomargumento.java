package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author ambrosio
 */
@Entity
@Table(name = "tsgnomargumento", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomargumento.findAll", query = "SELECT t FROM Tsgnomargumento t")})
public class Tsgnomargumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "argId")
    @Basic(optional = false)
    @Column(name = "cod_argumentoid")
    private Integer codArgumentoid;
    @Basic(optional = false)
    @Column(name = "cod_nbargumento")
    private String codNbargumento;
    @Basic(optional = false)
    @Column(name = "cod_clavearg")
    private String codClavearg;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "imp_valorconst")
    private BigDecimal impValorconst;
    @Column(name = "des_funcionbd")
    private String desFuncionbd;
    @Column(name = "cod_tipoargumento")
    private Character codTipoargumento;
    @Basic(optional = false)
    @Column(name = "bol_estatus")
    private boolean bolEstatus;
    @Basic(optional = false)
    @Column(name = "txt_descripcion")
    private String txtDescripcion;
    @Basic(optional = false)
    @Column(name = "aud_codcreadopor")
    private int audCodcreadopor;
    @Column(name = "aud_codmodificadopor")
    private Integer audCodmodificadopor;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;

    /**
     *
     */
    public Tsgnomargumento() {
    }

    /**
     *
     * @param codArgumentoid
     */
    public Tsgnomargumento(Integer codArgumentoid) {
        this.codArgumentoid = codArgumentoid;
    }

    /**
     *
     * @param codArgumentoid
     * @param codNbargumento
     * @param codClavearg
     * @param bolEstatus
     * @param txtDescripcion
     * @param audCodcreadopor
     * @param audFeccreacion
     */
    public Tsgnomargumento(Integer codArgumentoid, String codNbargumento, String codClavearg, boolean bolEstatus, String txtDescripcion, int audCodcreadopor, Date audFeccreacion) {
        this.codArgumentoid = codArgumentoid;
        this.codNbargumento = codNbargumento;
        this.codClavearg = codClavearg;
        this.bolEstatus = bolEstatus;
        this.txtDescripcion = txtDescripcion;
        this.audCodcreadopor = audCodcreadopor;
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Integer getCodArgumentoid() {
        return codArgumentoid;
    }

    /**
     *
     * @param codArgumentoid
     */
    public void setCodArgumentoid(Integer codArgumentoid) {
        this.codArgumentoid = codArgumentoid;
    }

    /**
     *
     * @return
     */
    public String getCodNbargumento() {
        return codNbargumento;
    }

    /**
     *
     * @param codNbargumento
     */
    public void setCodNbargumento(String codNbargumento) {
        this.codNbargumento = codNbargumento;
    }

    /**
     *
     * @return
     */
    public String getCodClavearg() {
        return codClavearg;
    }

    /**
     *
     * @param codClavearg
     */
    public void setCodClavearg(String codClavearg) {
        this.codClavearg = codClavearg;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpValorconst() {
        return impValorconst;
    }

    /**
     *
     * @param impValorconst
     */
    public void setImpValorconst(BigDecimal impValorconst) {
        this.impValorconst = impValorconst;
    }

    /**
     *
     * @return
     */
    public String getDesFuncionbd() {
        return desFuncionbd;
    }

    /**
     *
     * @param desFuncionbd
     */
    public void setDesFuncionbd(String desFuncionbd) {
        this.desFuncionbd = desFuncionbd;
    }

    /**
     *
     * @return
     */
    public Character getCodTipoargumento() {
        return codTipoargumento;
    }

    /**
     *
     * @param codTipoargumento
     */
    public void setCodTipoargumento(Character codTipoargumento) {
        this.codTipoargumento = codTipoargumento;
    }

    /**
     *
     * @return
     */
    public boolean getBolEstatus() {
        return bolEstatus;
    }

    /**
     *
     * @param bolEstatus
     */
    public void setBolEstatus(boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    public String getTxtDescripcion() {
        return txtDescripcion;
    }

    /**
     *
     * @param txtDescripcion
     */
    public void setTxtDescripcion(String txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    /**
     *
     * @return
     */
    public int getAudCodcreadopor() {
        return audCodcreadopor;
    }

    /**
     *
     * @param audCodcreadopor
     */
    public void setAudCodcreadopor(int audCodcreadopor) {
        this.audCodcreadopor = audCodcreadopor;
    }

    /**
     *
     * @return
     */
    public Integer getAudCodmodificadopor() {
        return audCodmodificadopor;
    }

    /**
     *
     * @param audCodmodificadopor
     */
    public void setAudCodmodificadopor(Integer audCodmodificadopor) {
        this.audCodmodificadopor = audCodmodificadopor;
    }

    /**
     *
     * @return
     */
    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    /**
     *
     * @param audFeccreacion
     */
    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    /**
     *
     * @param audFecmodificacion
     */
    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codArgumentoid != null ? codArgumentoid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomargumento)) {
            return false;
        }
        Tsgnomargumento other = (Tsgnomargumento) object;
        if ((this.codArgumentoid == null && other.codArgumentoid != null) || (this.codArgumentoid != null && !this.codArgumentoid.equals(other.codArgumentoid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomargumento[ codArgumentoid=" + codArgumentoid + " ]";
    }

}
