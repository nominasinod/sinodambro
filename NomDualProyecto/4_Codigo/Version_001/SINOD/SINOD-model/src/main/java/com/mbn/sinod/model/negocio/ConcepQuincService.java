package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.ConcepQuincDTO;
import com.mbn.sinod.model.dto.ConcepQuincHTDTO;
import com.mbn.sinod.model.dto.ConceptosEmpleadoDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;

/**
 *
 * @author User
 */
public interface ConcepQuincService extends BaseService<Tsgnomcncptoquinc, Integer> {

    /**
     *
     * @param cabecera
     * @return
     */
    ConcepQuincDTO listaConcepQuincByCabecera(Integer cabecera);

    /**
     *
     * @param empQuinc
     * @return
     */
    ConcepQuincDTO listaConcepQuincByEmpQuin(Integer empQuinc);

    /**
     *
     * @param empleado
     * @return
     */
    ConcepQuincDTO listaConceptosEmpl(Integer empleado);

    /**
     *
     * @param empQuinc
     * @return
     */
    ConcepQuincHTDTO listaConcepQuincByEmpQuinHT(Integer empQuinc);

    /**
     *
     * @param idEmpleado
     * @param idCabecera
     * @return
     */
    ConceptosEmpleadoDTO listaConceptosEmpleado(Integer idEmpleado, Integer idCabecera);

    /**
     *
     * @return
     */
    ConcepQuincDTO listaConcepQuinc();
}
