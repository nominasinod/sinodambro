package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomclasificador;
import java.util.List;

/**
 *
 * @author mipe
 */
public class CatClasificadorDAOImpl extends GenericDAOImpl<Tsgnomclasificador, Integer> implements CatClasificadorDAO {

    @Override
    public List<Tsgnomclasificador> listarCatClasificador() {
        return findAll();
    }
}
