package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mi Pe
 */
@Entity
@Table(name = "tsgnomconfpagoht", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomconfpagoht.findAll", query = "SELECT t FROM Tsgnomconfpagoht t")})
public class Tsgnomconfpagoht implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_confpagohtid")
    private Integer codConfpagohtid;
    @Column(name = "bol_pagoempleado")
    private Boolean bolPagoempleado;
    @Column(name = "bol_pagorh")
    private Boolean bolPagorh;
    @Column(name = "bol_pagofinanzas")
    private Boolean bolPagofinanzas;
    @JoinColumn(name = "cod_empquincenaid_fk", referencedColumnName = "cod_empquincenahtid")
    @ManyToOne
    private Tsgnomempquincenaht codEmpquincenaidFk;

    /**
     *
     */
    public Tsgnomconfpagoht() {
    }

    /**
     *
     * @param codConfpagohtid
     */
    public Tsgnomconfpagoht(Integer codConfpagohtid) {
        this.codConfpagohtid = codConfpagohtid;
    }

    /**
     *
     * @return
     */
    public Integer getCodConfpagohtid() {
        return codConfpagohtid;
    }

    /**
     *
     * @param codConfpagohtid
     */
    public void setCodConfpagohtid(Integer codConfpagohtid) {
        this.codConfpagohtid = codConfpagohtid;
    }

    /**
     *
     * @return
     */
    public Boolean getBolPagoempleado() {
        return bolPagoempleado;
    }

    /**
     *
     * @param bolPagoempleado
     */
    public void setBolPagoempleado(Boolean bolPagoempleado) {
        this.bolPagoempleado = bolPagoempleado;
    }

    /**
     *
     * @return
     */
    public Boolean getBolPagorh() {
        return bolPagorh;
    }

    /**
     *
     * @param bolPagorh
     */
    public void setBolPagorh(Boolean bolPagorh) {
        this.bolPagorh = bolPagorh;
    }

    /**
     *
     * @return
     */
    public Boolean getBolPagofinanzas() {
        return bolPagofinanzas;
    }

    /**
     *
     * @param bolPagofinanzas
     */
    public void setBolPagofinanzas(Boolean bolPagofinanzas) {
        this.bolPagofinanzas = bolPagofinanzas;
    }

    /**
     *
     * @return
     */
    public Tsgnomempquincenaht getCodEmpquincenaidFk() {
        return codEmpquincenaidFk;
    }

    /**
     *
     * @param codEmpquincenaidFk
     */
    public void setCodEmpquincenaidFk(Tsgnomempquincenaht codEmpquincenaidFk) {
        this.codEmpquincenaidFk = codEmpquincenaidFk;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codConfpagohtid != null ? codConfpagohtid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomconfpagoht)) {
            return false;
        }
        Tsgnomconfpagoht other = (Tsgnomconfpagoht) object;
        if ((this.codConfpagohtid == null && other.codConfpagohtid != null) || (this.codConfpagohtid != null && !this.codConfpagohtid.equals(other.codConfpagohtid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomconfpagoht[ codConfpagohtid=" + codConfpagohtid + " ]";
    }

}
