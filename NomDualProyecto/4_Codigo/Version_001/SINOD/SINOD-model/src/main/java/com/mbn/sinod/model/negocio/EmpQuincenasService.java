package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.EmpQuincenasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;

/**
 *
 * @author ambrosio
 */
public interface EmpQuincenasService
        extends BaseService<Tsgnomempquincena, Integer> {

    /**
     *
     * @return
     */
    EmpQuincenasDTO listaEmpQuincenas();

    /**
     *
     * @param cabeceraid_fk
     * @return
     */
    EmpQuincenasDTO listaEmpQuincenas(Integer cabeceraid_fk);

    /**
     *
     * @param cabeceraid_fk
     * @return
     */
    EmpQuincenasDTO insertaEmpleadosQuincena(Integer cabeceraid_fk);
}
