package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.InfoValidacionesNominaDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;

/**
 *
 * @author Ivette
 */
public interface ValidacionesNominaService extends BaseService<Tsgnomempleados, Integer> {

    /**
     *
     * @return
     */
    InfoValidacionesNominaDTO detallesValidacionesNomina();

}
