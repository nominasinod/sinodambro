package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.EmpDTO;
import com.mbn.sinod.model.entidades.Tsgnomconfpago;

/**
 *
 * @author eduardotorres
 */
public interface ConfirmaEmpleadoService extends BaseService<Tsgnomconfpago, Integer> {

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    EmpDTO empDTO(Integer cabecera, Integer empleado);
}
