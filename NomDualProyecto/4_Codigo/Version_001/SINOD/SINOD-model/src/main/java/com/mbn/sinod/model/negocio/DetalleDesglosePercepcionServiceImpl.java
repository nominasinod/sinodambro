package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.DetalleDesglosePercepcionDAO;
import com.mbn.sinod.model.dto.DetalleDesgloPercepDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author mipe
 */
public class DetalleDesglosePercepcionServiceImpl extends BaseServiceImpl<Tsgnomcncptoquinc, Integer>
        implements DetalleDesglosePercepcionService {

    @Autowired
    private DetalleDesglosePercepcionDAO detalleDesglosePercepcionDAO;

    /**
     *
     * @param cod_empleado
     * @param cod_cabecera
     * @return
     */
    @Override
    public DetalleDesgloPercepDTO detalleDesglocePercepDTO(Integer cod_empleado, Integer cod_cabecera) {
        DetalleDesgloPercepDTO dto = new DetalleDesgloPercepDTO();
        dto.setGetListaPercepcion(getDetalleDesglosePercepcionDAO().percepcionesPosEmp(cod_empleado, cod_cabecera));
        return dto;
    }

    /**
     * @return the detalleDesglosePercepcionDAO
     */
    public DetalleDesglosePercepcionDAO getDetalleDesglosePercepcionDAO() {
        return detalleDesglosePercepcionDAO;
    }

    /**
     * @param detalleDesglosePercepcionDAO the detalleDesglosePercepcionDAO to
     * set
     */
    public void setDetalleDesglosePercepcionDAO(DetalleDesglosePercepcionDAO detalleDesglosePercepcionDAO) {
        this.detalleDesglosePercepcionDAO = detalleDesglosePercepcionDAO;
    }
}
