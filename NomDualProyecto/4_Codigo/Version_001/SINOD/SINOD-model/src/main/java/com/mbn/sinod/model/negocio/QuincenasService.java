package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.QuincenasDTO;
import com.mbn.sinod.model.entidades.Tsgnomquincena;

/**
 *
 * @author mipe
 */
public interface QuincenasService extends BaseService<Tsgnomquincena, Integer> {

    /**
     *
     * @return
     */
    QuincenasDTO listQuincenas();

    /**
     *
     * @return
     */
    QuincenasDTO listQuinFuturas();

    /**
     *
     * @param quincena
     * @return
     */
    QuincenasDTO guardar(QuincenasDTO quincena);

    /**
     * Obtiene la quincena actual,
     *
     * @return
     */
    QuincenasDTO quincenaActual();
}
