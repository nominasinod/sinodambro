package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.ValidacionesNomBajasDAO;
import com.mbn.sinod.model.dto.InfoValidacionesNomBajasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Ivette
 */
public class ValidacionesNomBajasServiceImpl extends BaseServiceImpl<Tsgnomempleados, Integer>
        implements ValidacionesNomBajasService {

    @Autowired
    private ValidacionesNomBajasDAO validacionesNomBajasDAO;

    /**
     *
     * @return
     */
    public ValidacionesNomBajasDAO getValidacionesNomBajasDAO() {
        return validacionesNomBajasDAO;
    }

    /**
     *
     * @param validacionesNomBajasDAO
     */
    public void setValidacionesNomBajasDAO(ValidacionesNomBajasDAO validacionesNomBajasDAO) {
        this.validacionesNomBajasDAO = validacionesNomBajasDAO;
    }

    /**
     *
     * @return
     */
    @Override
    public InfoValidacionesNomBajasDTO detallesValidacionesNominaBajas() {
        InfoValidacionesNomBajasDTO dto = new InfoValidacionesNomBajasDTO();
        dto.setListaValidacionesNomBajas(getValidacionesNomBajasDAO().detallesValidacionesNominaBajas());
        return dto;
    }

}
