package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.EstatusNominaDTO;
import com.mbn.sinod.model.entidades.Tsgnomestatusnom;

/**
 *
 * @author User
 */
public interface EstatusNominaService extends BaseService<Tsgnomestatusnom, Integer> {

    /**
     *
     * @return
     */
    EstatusNominaDTO listarEstatusNominas();
}
