package com.mbn.sinod.model.comun;

import java.util.ResourceBundle;

/**
 * Clase en donde se encuentran las variables estaticas, mensajes y tipos de
 * mensajes mostrados en pantalla
 *
 * @author Francisco Rolando Muñoz
 */
public class StaticConstantes {

    private static final ResourceBundle CONF = ResourceBundle.getBundle("com.mbn.sinod.modelo.recurso.configuration");

    public static int MENSAJE_ERROR = 1;
    public static int MENSAJE_ADVERTENCIA = 2;
    public static int MENSAJE_CORRECTO = 3;

    //Sistema
    public static int CODIGO_SISTEMA = 3;

    //Mensajes
//    ARGUMENTOS
    public static String EXITO_ELIMINAR_ARGUMENTO = "IOO1";
    public static String ERROR_ELIMINAR_ARGUMENTO = "EOO1";

    public static String EXITO_GUARDAR_ARGUMENTO = "IOO2";
    public static String ERROR_GUARDAR_ARGUMENTO = "EOO2";

    public static String EXITO_OBTENER_LISTA_ARGUMENTOS_CONSTANTES = "IOO3";
    public static String ERROR_OBTENER_LISTA_ARGUMENTOS_CONSTANTES = "EOO3";

    public static String EXITO_OBTENER_LISTA_ARGUMENTOS_VARIABLES = "IOO4";
    public static String ERROR_OBTENER_LISTA_ARGUMENTOS_VARIABLES = "EOO4";

    public static String EXITO_OBTENER_LISTA_ARGUMENTOS = "IOO5";
    public static String ERROR_OBTENER_LISTA_ARGUMENTOS = "EOO5";

//    CABECERA
    public static String EXITO_CALCULO_NOMINA = "IOO6";
    public static String ERROR_CALCULO_NOMINA = "EOO6";

    public static String EXITO_CARGAR_IMSS = "IOO7";
    public static String ERROR_CARGAR_IMSS = "EOO7";

    public static String EXITO_CERRAR_NOMINA = "IOO8";
    public static String ERROR_CERRAR_NOMINA = "EOO8";

    public static String EXITO_ELIMINAR_CABECERA = "I009";
    public static String ERROR_ELIMINAR_CABECERA = "E009";

    public static String EXITO_GUARDAR_ACTUALIZAR = "I010";
    public static String ERROR_GUARDAR_ACTUALIZAR = "E010";

    public static String EXITO_OBTENER_LISTA_CABECERA = "I011";
    public static String ERROR_OBTENER_LISTA_CABECERA = "E011";

    public static String EXITO_VALIDAR_PAGOS = "I012";
    public static String ERROR_VALIDAR_PAGOS = "E012";

    public static String EXITO_LISTAR_EMPLEADOS_CABECERA = "I013";
    public static String ERROR_LISTAR_EMPLEADOS_CABECERA = "E013";

//    CAT CLASIFICADOR
    public static String EXITO_LISTAR_CAT_CLASIFICADOR = "I014";
    public static String ERROR_LISTAR_CAT_CLASIFICADOR = "E014";

//    CONCEPTOS SAT
    public static String EXITO_LISTAR_SAT = "I015";
    public static String ERROR_LISTAR_SAT = "E015";

//    CAT INCIDENCIAS
    public static String EXITO_LISTAR_CAT_INCIDENCIAS = "I016";
    public static String ERROR_LISTAR_CAT_INCIDENCIAS = "E016";

//    CONCEPTO QUIN
    public static String EXITO_LISTAR_CONCEPTO_QUIN_CABECERA = "I017";
    public static String ERROR_LISTAR_CONCEPTO_QUIN_CABECERA = "E017";

    public static String EXITO_LISTAR_CONCEPTO_QUIN_EMP = "I018";
    public static String ERROR_LISTAR_CONCEPTO_QUIN_EMP = "E018";

    public static String EXITO_LISTAR_CONCEPTOS_EMP = "I019";
    public static String ERROR_LISTAR_CONCEPTOS_EMP = "E019";

    public static String EXITO_LISTAR_CONCEPTO_QUIN_EMP_HT = "I020";
    public static String ERROR_LISTAR_CONCEPTO_QUIN_EMP_HT = "E020";

    public static String EXITO_CONCEPTO_EMP = "I021";
    public static String ERROR_CONCEPTO_EMP = "E021";

    public static String EXITO_LISTAR_CONCEPTO_QUIN = "I022";
    public static String ERROR_LISTAR_CONCEPTO_QUIN = "E022";

//    CONCEPTO
    public static String EXITO_ELIMINAR_CONCEPTO = "IO23";
    public static String ERROR_ELIMINAR_CONCEPTO = "EO23";

    public static String EXITO_GUARDAR_CONCEPTO = "IO24";
    public static String ERROR_GUARDAR_CONCEPTO = "EO24";

    public static String EXITO_GUARDAR_PRIORIDAD = "IO25";
    public static String ERROR_GUARDAR_PRIORIDAD = "EO25";

    public static String EXITO_LISTAR_CONCEPTOS_DEDUCCION = "I026";
    public static String ERROR_LISTAR_CONCEPTOS_DEDUCCION = "E026";

    public static String EXITO_LISTAR_CONCEPTOS_PERCEPCION = "I027";
    public static String ERROR_LISTAR_CONCEPTOS_PERCEPCION = "E027";

    public static String EXITO_OBTENER_LISTA_CONCEPTO = "I028";
    public static String ERROR_OBTENER_LISTA_CONCEPTO = "E028";

//    CONF PAGO 
    public static String EXITO_AUTORIZACION_EMP = "I029";
    public static String ERROR_AUTORIZACION_EMP = "E029";

    public static String EXITO_AUTORIZACION_RH = "I030";
    public static String ERROR_AUTORIZACION_RH = "E030";

    public static String EXITO_RECHAZAR_TODO_RF = "I031";
    public static String ERROR_RECHAZAR_TODO_RF = "E031";

    public static String EXITO_RECHAZAR_TODO_RH = "I032";
    public static String ERROR_RECHAZAR_TODO_RH = "E032";

    public static String EXITO_VALIDA_TODAS = "I033";
    public static String ERROR_VALIDA_TODAS = "E033";

    public static String EXITO_VALIDAR_TODAS_RH = "I034";
    public static String ERROR_VALIDAR_TODAS_RH = "E034";

    public static String EXITO_GUARDAR_AUTORIZACION_RF = "I035";
    public static String ERROR_GUARDAR_AUTORIZACION_RF = "E035";

//    EJERCICIO FISCAL
    public static String EXITO_LISTA_EJER_FISC = "I036";
    public static String ERROR_LISTA_EJER_FISC = "E036";

//    EMP QUINCENA
    public static String EXITO_LISTA_EMP_QUINC = "I037";
    public static String ERROR_LISTA_EMP_QUINC = "E037";

//    EMP QUINCENAS
    public static String EXITO_LISTA_EMP_QUINCs = "I038";
    public static String ERROR_LISTA_EMP_QUINCs = "E038";

    public static String EXITO_INSERTAR_EMP_QUINC = "I039";
    public static String ERROR_INSERTAR_EMP_QUINC = "E039";

//    EMPLEADOS NOM
    public static String EXITO_OBTENER_EMP_NOM_HT = "I040";
    public static String ERROR_OBTENER_EMP_NOM_HT = "E040";

    public static String EXITO_DETELLE_EMP_RH = "I041";
    public static String ERROR_DETELLE_EMP_RH = "E041";

    public static String EXITO_GUARDAR_EMPLEADOS = "I042";
    public static String ERROR_GUARDAR_EMPLEADOS = "E042";

//    ENVIAR CORREO
    public static String EXITO_ENVIAR_CORREO = "I043";
    public static String ERROR_ENVIAR_CORREO = "E043";

    public static String EXITO_ENVIAR_CORREO_VALIDAR_ALTA = "I044";
    public static String ERROR_ENVIAR_CORREO_VALIDAR_ALTA = "E044";

    public static String EXITO_ENVIAR_CORREO_VALIDAR_BAJA = "I045";
    public static String ERROR_ENVIAR_CORREO_VALIDAR_BAJA = "E045";

//    ESTATUS NOMINA
    public static String EXITO_LISTA_ESTATUS_NOM = "I046";
    public static String ERROR_LISTA_ESTATUS_NOM = "E046";

//    FORMULA
    public static String EXITO_GUARDAR_FORMULA = "I047";
    public static String ERROR_GUARDAR_FORMULA = "E047";

    public static String EXITO_OBTENER_LISTA_FORMULA = "I048";
    public static String ERROR_OBTENER_LISTA_FORMULA = "E048";

//    FUNCION
    public static String EXITO_OBTENER_LISTA_FUNCIONES = "IO49";
    public static String ERROR_OBTENER_LISTA_FUNCIONES = "EO49";

//     INCIDENCIAS QUINCENA
    public static String EXITO_LISTAR_INCIDENCIAS_QUINCENA = "I050";
    public static String ERROR_LISTAR_INCIDENCIAS_QUINCENA = "E050";

    public static String EXITO_VALIDAR_TODAS_INCIDCENCIAS = "I051";
    public static String ERROR_VALIDAR_TODAS_INCIDCENCIAS = "E051";

    public static String EXITO_VALIDAR_INCIDENCIA = "I052";
    public static String ERROR_VALIDAR_INCIDENCIA = "E052";

    public static String EXITO_RECHAZAR_TODAS_INCIDENCIAS = "I053";
    public static String ERROR_RECHAZAR_TODAS_INCIDENCIAS = "E053";

    public static String EXITO_AUTORIZAR_INCIDENCIA = "I054";
    public static String ERROR_AUTORIZAR_INCIDENCIA = "E054";

    public static String EXITO_AUTORIZAR_TODAS_INCIDENCIAS = "I055";
    public static String ERROR_AUTORIZAR_TODAS_INCIDENCIAS = "E055";

    public static String EXITO_DENEGAR_TODAS_INCIDENCIAS = "I056";
    public static String ERROR_DENEGAR_TODAS_INCIDENCIAS = "E056";

    public static String EXITO_LISTAR_INCIDENCIAS_QUINCENA_AREA = "I057";
    public static String ERROR_LISTAR_INCIDENCIAS_QUINCENA_AREA = "E057";

    public static String EXITO_AUTORIZAR_PAGO_INCIDENCIAS = "I058";
    public static String ERROR_AUTORIZAR_PAGO_INCIDENCIAS = "E058";

    public static String EXITO_DENEGAR_PAGO_INCIDENCIAS = "I059";
    public static String ERROR_DENEGAR_PAGO_INCIDENCIAS = "E059";

    public static String EXITO_AUTORIZAR_PAGOS_INCIDENCIAS = "I060";
    public static String ERROR_AUTORIZAR_PAGOS_INCIDENCIAS = "E060";

//    LISTA ALTA DE PERSONAL
    public static String EXITO_LISTAR_ALTAS_PARA_ALTA = "I061";
    public static String ERROR_LISTAR_ALTAS_PARA_ALTA = "E061";

//    QUINCENAS
    public static String EXITO_GUARDAR_QUINCs = "I062";
    public static String ERROR_GUARDAR_QUINCs = "E062";

//    SUMA DESGLOCE
    public static String EXITO_LISTAR_SUMA_DESGLOSE = "I063";
    public static String ERROR_LISTAR_SUMA_DESGLOSE = "E063";

    public static String EXITO_LISTAR_SUMA_DESGLOSE_HT = "I064";
    public static String ERROR_LISTAR_SUMA_DESGLOSE_HT = "E064";

//    TIPO CALCULO
    public static String EXITO_LISTAR_TIPOS_CALCULO = "I065";
    public static String ERROR_LISTAR_TIPOS_CALCULO = "E065";

//    TIPO CONCEPTOS
    public static String EXITO_LISTAR_TIPO_CONCEPTOS = "I066";
    public static String ERROR_LISTAR_TIPO_CONCEPTOS = "E066";

//    TIPO NOMINA
    public static String EXITO_LISTA_TIPO_NOMINA = "I067";
    public static String ERROR_LISTA_TIPO_NOMINA = "E067";

//    VALIDAR ALTAS
    public static String EXITO_LISTAR_ALTAS_A_VALIDAR = "I068";
    public static String ERROR_LISTAR_ALTAS_A_VALIDAR = "E068";

    public static String EXITO_VALIDAR_CADA_ALTA = "I069";
    public static String ERROR_VALIDAR_CADA_ALTA = "E069";

    public static String EXITO_VALIDAR_ALTAS_ACEPTADAS = "I070";
    public static String ERROR_VALIDAR_ALTAS_ACEPTADAS = "E070";

    public static String EXITO_VALIDAR_ALTAS_RECHAZADAS = "I071";
    public static String ERROR_VALIDAR_ALTAS_RECHAZADAS = "E071";

//    VALIDAR BAJAS
    public static String EXITO_LISTAR_BAJAS_A_VALIDAR = "I072";
    public static String ERROR_LISTAR_BAJAS_A_VALIDAR = "E072";

    public static String EXITO_VALIDAR_CADA_BAJA = "I073";
    public static String ERROR_VALIDAR_CADA_BAJA = "E073";

    public static String EXITO_VALIDAR_BAJAS_ACEPTADAS = "I074";
    public static String ERROR_VALIDAR_BAJAS_ACEPTADAS = "E074";

    public static String EXITO_VALIDAR_BAJAS_RECHAZADAS = "I075";
    public static String ERROR_VALIDAR_BAJAS_RECHAZADAS = "E075";

//////////////////////////////////////////////////////////////////////////////////////////////
    public static String EXITO_OBTENER_LISTAEMPLEADOS = "LISTA DE EMPLEADOS OBTENIDA";
    public static String ERROR_OBTENER_LISTAEMPLEADOS = "LISTA DE EMPLEADOS NO OBTENIDA";

    //Banderas
    //Correo 
    //Nombre sistema
    public static String NOMBRE_SISTEMA = CONF.getString("sistema");

    //Formatos
    public static final String FORMATO_FECHA = CONF.getString("formato.fecha");

    /**
     * @param conf
     * @return the CONF
     */
    public static String getCONF(String conf) {
        return CONF.getString(conf);
    }

}
