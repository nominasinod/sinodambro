package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.DetalleEmpDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;

/**
 * Interfaz DAO para el metodo empleadosPorArea donde trae informacion del
 * empleado de la BD SUITE en el esquema SGNOM.
 *
 * Esta interfaz contiene el metodo para el modulo de confirmación de pago del
 * empelado donde manda a traer los datos del empeado en conjunto con las
 * aprobaciones de pago de los departamentos de Recursos Humanos y Financieros.
 *
 *
 * @author Eduardo Torres C. MBN
 */
public interface DetalleEmpDAO extends GenericDAO<Tsgrhempleados, Integer> {

    /**
     * Este metodo retorna una lista com los datos del empleado y los estatus de
     * pagos conforme a la quincena correspondiente.
     *
     * @param cabecera
     * @return
     */
    List<DetalleEmpDTO> empleadosPorArea(Integer cabecera);
//    boolean validarPagosRF(List<DetalleEmpDTO> lista);

//    void aumentarEmpleado(DetalleEmpDTO detalleEmp);
}
