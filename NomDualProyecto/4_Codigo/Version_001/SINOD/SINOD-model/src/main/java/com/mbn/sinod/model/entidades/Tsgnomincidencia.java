package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author ambrosio
 */
@Entity
@Table(name = "tsgnomincidencia", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomincidencia.findAll", query = "SELECT t FROM Tsgnomincidencia t")})
public class Tsgnomincidencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "incId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "incId")
    @Basic(optional = false)
    @Column(name = "cod_incidenciaid")
    private Integer codIncidenciaid;
    @Column(name = "cnu_cantidad")
    private BigDecimal cnuCantidad;
    @Column(name = "des_actividad")
    private String desActividad;
    @Column(name = "txt_comentarios")
    private String txtComentarios;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "imp_monto")
    private BigDecimal impMonto;
    @Column(name = "xml_detcantidad", columnDefinition = "XMLType")
    private String xmlDetcantidad;
    @Column(name = "bol_estatus")
    private Boolean bolEstatus;
    @Column(name = "bol_validacion")
    private Boolean bolValidacion;
    @Column(name = "fec_validacion")
    @Temporal(TemporalType.DATE)
    private Date fecValidacion;
    @Basic(optional = false)
    @Column(name = "aud_codcreadopor")
    private int audCodcreadopor;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_codmodificadopor")
    private Integer audCodmodificadopor;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
//    @JoinColumn(name = "cod_catincidenciaid_fk", referencedColumnName = "cod_catincidenciaid")
//    @ManyToOne(optional = false)
    @Column(name = "cod_catincidenciaid_fk")
    private Integer codCatincidenciaidFk;
//    @JoinColumn(name = "cod_empautoriza_fk", referencedColumnName = "cod_empleadoid")
//    @ManyToOne()
    @Column(name = "cod_empautoriza_fk")
    private Integer codEmpautorizaFk;
//    @JoinColumn(name = "cod_empreporta_fk", referencedColumnName = "cod_empleadoid")
//    @ManyToOne()
    @Column(name = "cod_empreporta_fk")
    private Integer codEmpreportaFk;
//    @JoinColumn(name = "cod_quincenaid_fk", referencedColumnName = "cod_quincenaid")
//    @ManyToOne(optional = false)
    @Basic(optional = false)
    @Column(name = "cod_quincenaid_fk")
    private Integer codQuincenaidFk;
    @Column(name = "bol_aceptacion")
    private Boolean bolAceptacion;
    @Column(name = "bol_pago")
    private Boolean bolPago;

    /**
     *
     */
    public Tsgnomincidencia() {
    }

    /**
     *
     * @param codIncidenciaid
     */
    public Tsgnomincidencia(Integer codIncidenciaid) {
        this.codIncidenciaid = codIncidenciaid;
    }

    /**
     *
     * @param codIncidenciaid
     * @param audCodcreadopor
     * @param audFeccreacion
     */
    public Tsgnomincidencia(Integer codIncidenciaid, int audCodcreadopor, Date audFeccreacion) {
        this.codIncidenciaid = codIncidenciaid;
        this.audCodcreadopor = audCodcreadopor;
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Integer getCodIncidenciaid() {
        return codIncidenciaid;
    }

    /**
     *
     * @param codIncidenciaid
     */
    public void setCodIncidenciaid(Integer codIncidenciaid) {
        this.codIncidenciaid = codIncidenciaid;
    }

    /**
     *
     * @return
     */
    public BigDecimal getCnuCantidad() {
        return cnuCantidad;
    }

    /**
     *
     * @param cnuCantidad
     */
    public void setCnuCantidad(BigDecimal cnuCantidad) {
        this.cnuCantidad = cnuCantidad;
    }

    /**
     *
     * @return
     */
    public String getDesActividad() {
        return desActividad;
    }

    /**
     *
     * @param desActividad
     */
    public void setDesActividad(String desActividad) {
        this.desActividad = desActividad;
    }

    /**
     *
     * @return
     */
    public String getTxtComentarios() {
        return txtComentarios;
    }

    /**
     *
     * @param txtComentarios
     */
    public void setTxtComentarios(String txtComentarios) {
        this.txtComentarios = txtComentarios;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpMonto() {
        return impMonto;
    }

    /**
     *
     * @param impMonto
     */
    public void setImpMonto(BigDecimal impMonto) {
        this.impMonto = impMonto;
    }

    /**
     *
     * @return
     */
    public String getXmlDetcantidad() {
        return xmlDetcantidad;
    }

    /**
     *
     * @param xmlDetcantidad
     */
    public void setXmlDetcantidad(String xmlDetcantidad) {
        this.xmlDetcantidad = xmlDetcantidad;
    }

    /**
     *
     * @return
     */
    public Boolean getBolEstatus() {
        return bolEstatus;
    }

    /**
     *
     * @param bolEstatus
     */
    public void setBolEstatus(Boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    public Boolean getBolValidacion() {
        return bolValidacion;
    }

    /**
     *
     * @param bolValidacion
     */
    public void setBolValidacion(Boolean bolValidacion) {
        this.bolValidacion = bolValidacion;
    }

    /**
     *
     * @return
     */
    public Date getFecValidacion() {
        return fecValidacion;
    }

    /**
     *
     * @param fecValidacion
     */
    public void setFecValidacion(Date fecValidacion) {
        this.fecValidacion = fecValidacion;
    }

    /**
     *
     * @return
     */
    public int getAudCodcreadopor() {
        return audCodcreadopor;
    }

    /**
     *
     * @param audCodcreadopor
     */
    public void setAudCodcreadopor(int audCodcreadopor) {
        this.audCodcreadopor = audCodcreadopor;
    }

    /**
     *
     * @return
     */
    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    /**
     *
     * @param audFeccreacion
     */
    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Integer getAudCodmodificadopor() {
        return audCodmodificadopor;
    }

    /**
     *
     * @param audCodmodificadopor
     */
    public void setAudCodmodificadopor(Integer audCodmodificadopor) {
        this.audCodmodificadopor = audCodmodificadopor;
    }

    /**
     *
     * @return
     */
    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    /**
     *
     * @param audFecmodificacion
     */
    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

//    public Tsgnomcatincidencia getCodCatincidenciaidFk() {
//        return codCatincidenciaidFk;
//    }
//
//    public void setCodCatincidenciaidFk(Tsgnomcatincidencia codCatincidenciaidFk) {
//        this.codCatincidenciaidFk = codCatincidenciaidFk;
//    }
//
//    public Tsgnomempleados getCodEmpautorizaFk() {
//        return codEmpautorizaFk;
//    }
//
//    public void setCodEmpautorizaFk(Tsgnomempleados codEmpautorizaFk) {
//        this.codEmpautorizaFk = codEmpautorizaFk;
//    }
//
//    public Tsgnomempleados getCodEmpreportaFk() {
//        return codEmpreportaFk;
//    }
//
//    public void setCodEmpreportaFk(Tsgnomempleados codEmpreportaFk) {
//        this.codEmpreportaFk = codEmpreportaFk;
//    }
//
//    public Tsgnomquincena getCodQuincenaidFk() {
//        return codQuincenaidFk;
//    }
//
//    public void setCodQuincenaidFk(Tsgnomquincena codQuincenaidFk) {
//        this.codQuincenaidFk = codQuincenaidFk;
//    }
    /**
     *
     * @return
     */
    public Integer getCodCatincidenciaidFk() {
        return codCatincidenciaidFk;
    }

    /**
     *
     * @param codCatincidenciaidFk
     */
    public void setCodCatincidenciaidFk(Integer codCatincidenciaidFk) {
        this.codCatincidenciaidFk = codCatincidenciaidFk;
    }

    /**
     *
     * @return
     */
    public Integer getCodEmpautorizaFk() {
        return codEmpautorizaFk;
    }

    /**
     *
     * @param codEmpautorizaFk
     */
    public void setCodEmpautorizaFk(Integer codEmpautorizaFk) {
        this.codEmpautorizaFk = codEmpautorizaFk;
    }

    /**
     *
     * @return
     */
    public Integer getCodEmpreportaFk() {
        return codEmpreportaFk;
    }

    /**
     *
     * @param codEmpreportaFk
     */
    public void setCodEmpreportaFk(Integer codEmpreportaFk) {
        this.codEmpreportaFk = codEmpreportaFk;
    }

    /**
     *
     * @return
     */
    public Integer getCodQuincenaidFk() {
        return codQuincenaidFk;
    }

    /**
     *
     * @param codQuincenaidFk
     */
    public void setCodQuincenaidFk(Integer codQuincenaidFk) {
        this.codQuincenaidFk = codQuincenaidFk;
    }

    /**
     *
     * @return
     */
    public Boolean getBolAceptacion() {
        return bolAceptacion;
    }

    /**
     *
     * @param bolAceptacion
     */
    public void setBolAceptacion(Boolean bolAceptacion) {
        this.bolAceptacion = bolAceptacion;
    }

    /**
     *
     * @return
     */
    public Boolean getBolPago() {
        return bolPago;
    }

    /**
     *
     * @param bolPago
     */
    public void setBolPago(Boolean bolPago) {
        this.bolPago = bolPago;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codIncidenciaid != null ? codIncidenciaid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomincidencia)) {
            return false;
        }
        Tsgnomincidencia other = (Tsgnomincidencia) object;
        if ((this.codIncidenciaid == null && other.codIncidenciaid != null) || (this.codIncidenciaid != null && !this.codIncidenciaid.equals(other.codIncidenciaid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomincidencia[ codIncidenciaid=" + codIncidenciaid + " ]";
    }

}
