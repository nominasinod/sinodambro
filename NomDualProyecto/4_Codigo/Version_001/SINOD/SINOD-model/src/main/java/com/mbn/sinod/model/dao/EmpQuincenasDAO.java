package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import java.util.List;

/**
 *
 * @author ambrosio
 */
public interface EmpQuincenasDAO extends GenericDAO<Tsgnomempquincena, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomempquincena> listaEmpQuincenas();

    /**
     *
     * @param cabeceraid_fk
     * @return
     */
    List<Tsgnomempquincena> listaEmpQuincenas(Integer cabeceraid_fk);

    /**
     *
     * @param cabeceraid_fk
     * @return
     */
    Boolean insertaEmpleadosQuincena(Integer cabeceraid_fk);
}
