package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.DetalleDesglosePercepcionDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import java.util.List;

/**
 * Interfaz DAO para la función detalle_desglose_percepcion dentro del esquema
 * SGNOM de la BD Suite.
 *
 * Esta interfaz contiene el metodo percepcionesPosEmp del modulo de Desglose de
 * Nómina y consulta todas las percepciones de los trabajadores segun la
 * quincena seleccionada y del numero de control del trabajador.
 *
 * @author Eduardo Torres C. MBN
 */
public interface DetalleDesglosePercepcionDAO extends GenericDAO<Tsgnomcncptoquinc, Integer> {

    /**
     * Metodo que retorna una lista con las perciones del trabajador en la
     * quincena
     *
     * @param cod_empleado
     * @param cod_cabecera
     * @return Retorna una lista de los conceptos del trabajador, su nombre,
     * clave, cantidad y importe.
     */
    List<DetalleDesglosePercepcionDTO> percepcionesPosEmp(Integer cod_empleado, Integer cod_cabecera);
}
