package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomformula;
import java.util.List;

/**
 *
 * @author ambrosio
 */
public interface FormulaDAO extends GenericDAO<Tsgnomformula, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomformula> listarFormulas();

    /**
     *
     * @param formula
     * @return
     */
    boolean guardarFormula(Tsgnomformula formula);
}
