package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ambrosio
 */
@Entity
@Table(name = "tsgnommanterceros", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnommanterceros.findAll", query = "SELECT t FROM Tsgnommanterceros t")})
public class Tsgnommanterceros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "argId")
    @Basic(optional = false)
    @Column(name = "cod_mantercerosid")
    private Integer codMantercerosid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "imp_monto")
    private BigDecimal impMonto;
    @Column(name = "cod_frecuenciapago")
    private String codFrecuenciapago;
    @Column(name = "bol_estatus")
    private Boolean bolEstatus;
    @Basic(optional = false)
    @Column(name = "aud_codcreadopor")
    private int audCodcreadopor;
    @Column(name = "aud_codmodificadopor")
    private Integer audCodmodificadopor;
    @Basic(optional = false)
    @Column(name = "aud_fecreacion")
    @Temporal(TemporalType.DATE)
    private Date audFecreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @JoinColumn(name = "cod_conceptoid_fk", referencedColumnName = "cod_conceptoid")
    @ManyToOne()
    private Tsgnomconcepto codConceptoidFk;
    @JoinColumn(name = "cod_empleadoid_fk", referencedColumnName = "cod_empleadoid")
    @ManyToOne()
    private Tsgnomempleados codEmpleadoidFk;
    @JoinColumn(name = "cod_quincenafin_fk", referencedColumnName = "cod_quincenaid")
    @ManyToOne()
    private Tsgnomquincena codQuincenafinFk;
    @JoinColumn(name = "cod_quincenainicio_fk", referencedColumnName = "cod_quincenaid")
    @ManyToOne()
    private Tsgnomquincena codQuincenainicioFk;

    /**
     *
     */
    public Tsgnommanterceros() {
    }

    /**
     *
     * @param codMantercerosid
     */
    public Tsgnommanterceros(Integer codMantercerosid) {
        this.codMantercerosid = codMantercerosid;
    }

    /**
     *
     * @param codMantercerosid
     * @param audCodcreadopor
     * @param audFecreacion
     */
    public Tsgnommanterceros(Integer codMantercerosid, int audCodcreadopor, Date audFecreacion) {
        this.codMantercerosid = codMantercerosid;
        this.audCodcreadopor = audCodcreadopor;
        this.audFecreacion = audFecreacion;
    }

    /**
     *
     * @return
     */
    public Integer getCodMantercerosid() {
        return codMantercerosid;
    }

    /**
     *
     * @param codMantercerosid
     */
    public void setCodMantercerosid(Integer codMantercerosid) {
        this.codMantercerosid = codMantercerosid;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpMonto() {
        return impMonto;
    }

    /**
     *
     * @param impMonto
     */
    public void setImpMonto(BigDecimal impMonto) {
        this.impMonto = impMonto;
    }

    /**
     *
     * @return
     */
    public String getCodFrecuenciapago() {
        return codFrecuenciapago;
    }

    /**
     *
     * @param codFrecuenciapago
     */
    public void setCodFrecuenciapago(String codFrecuenciapago) {
        this.codFrecuenciapago = codFrecuenciapago;
    }

    /**
     *
     * @return
     */
    public Boolean getBolEstatus() {
        return bolEstatus;
    }

    /**
     *
     * @param bolEstatus
     */
    public void setBolEstatus(Boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    public int getAudCodcreadopor() {
        return audCodcreadopor;
    }

    /**
     *
     * @param audCodcreadopor
     */
    public void setAudCodcreadopor(int audCodcreadopor) {
        this.audCodcreadopor = audCodcreadopor;
    }

    /**
     *
     * @return
     */
    public Integer getAudCodmodificadopor() {
        return audCodmodificadopor;
    }

    /**
     *
     * @param audCodmodificadopor
     */
    public void setAudCodmodificadopor(Integer audCodmodificadopor) {
        this.audCodmodificadopor = audCodmodificadopor;
    }

    /**
     *
     * @return
     */
    public Date getAudFecreacion() {
        return audFecreacion;
    }

    /**
     *
     * @param audFecreacion
     */
    public void setAudFecreacion(Date audFecreacion) {
        this.audFecreacion = audFecreacion;
    }

    /**
     *
     * @return
     */
    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    /**
     *
     * @param audFecmodificacion
     */
    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    /**
     *
     * @return
     */
    public Tsgnomconcepto getCodConceptoidFk() {
        return codConceptoidFk;
    }

    /**
     *
     * @param codConceptoidFk
     */
    public void setCodConceptoidFk(Tsgnomconcepto codConceptoidFk) {
        this.codConceptoidFk = codConceptoidFk;
    }

    /**
     *
     * @return
     */
    public Tsgnomempleados getCodEmpleadoidFk() {
        return codEmpleadoidFk;
    }

    /**
     *
     * @param codEmpleadoidFk
     */
    public void setCodEmpleadoidFk(Tsgnomempleados codEmpleadoidFk) {
        this.codEmpleadoidFk = codEmpleadoidFk;
    }

    /**
     *
     * @return
     */
    public Tsgnomquincena getCodQuincenafinFk() {
        return codQuincenafinFk;
    }

    /**
     *
     * @param codQuincenafinFk
     */
    public void setCodQuincenafinFk(Tsgnomquincena codQuincenafinFk) {
        this.codQuincenafinFk = codQuincenafinFk;
    }

    /**
     *
     * @return
     */
    public Tsgnomquincena getCodQuincenainicioFk() {
        return codQuincenainicioFk;
    }

    /**
     *
     * @param codQuincenainicioFk
     */
    public void setCodQuincenainicioFk(Tsgnomquincena codQuincenainicioFk) {
        this.codQuincenainicioFk = codQuincenainicioFk;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codMantercerosid != null ? codMantercerosid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnommanterceros)) {
            return false;
        }
        Tsgnommanterceros other = (Tsgnommanterceros) object;
        if ((this.codMantercerosid == null && other.codMantercerosid != null) || (this.codMantercerosid != null && !this.codMantercerosid.equals(other.codMantercerosid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnommanterceros[ codMantercerosid=" + codMantercerosid + " ]";
    }

}
