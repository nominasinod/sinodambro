package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public class EmpleadosNomDTO extends GenericDTO {

    private Tsgnomempleados empleado;
    private List<Tsgnomempleados> listarEmpleadosNom;

    /**
     *
     * @return the listarEmpleadosNom
     */
    public List<Tsgnomempleados> getListarEmpleadosNom() {
        return listarEmpleadosNom;
    }

    /**
     *
     * @param listarEmpleadosNom the listarEmpleadosNom to set
     */
    public void setListarEmpleadosNom(List<Tsgnomempleados> listarEmpleadosNom) {
        this.listarEmpleadosNom = listarEmpleadosNom;
    }

    /**
     *
     * @return the empleado
     */
    public Tsgnomempleados getEmpleado() {
        return empleado;
    }

    /**
     *
     * @param empleado the empleado to set
     */
    public void setEmpleado(Tsgnomempleados empleado) {
        this.empleado = empleado;
    }

}
