package com.mbn.sinod.model.dto;

/**
 *
 * @author ambrosio
 */
public class PrioridadCalculoDTO extends GenericDTO {

    private Integer id;
    private Integer indexOrigen;
    private Integer indexDestino;
    private Integer prioridadCalculo;

    /**
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Integer getIndexOrigen() {
        return indexOrigen;
    }

    /**
     *
     * @param indexOrigen
     */
    public void setIndexOrigen(Integer indexOrigen) {
        this.indexOrigen = indexOrigen;
    }

    /**
     *
     * @return
     */
    public Integer getIndexDestino() {
        return indexDestino;
    }

    /**
     *
     * @param indexDestino
     */
    public void setIndexDestino(Integer indexDestino) {
        this.indexDestino = indexDestino;
    }

    /**
     *
     * @return
     */
    public Integer getPrioridadCalculo() {
        return prioridadCalculo;
    }

    /**
     *
     * @param prioridadCalculo
     */
    public void setPrioridadCalculo(Integer prioridadCalculo) {
        this.prioridadCalculo = prioridadCalculo;
    }

}
