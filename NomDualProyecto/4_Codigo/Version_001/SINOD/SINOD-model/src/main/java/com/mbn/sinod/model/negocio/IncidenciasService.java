package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.entidades.Tsgnomincidencia;
import java.util.List;

/**
 *
 * @author Karla
 */
public interface IncidenciasService extends BaseService<Tsgnomincidencia, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomincidencia> listarIncidencias();
}
