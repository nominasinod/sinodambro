package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomargumento;
import java.util.List;

/**
 *
 * @author mariana
 */
public class ArgumentoDTO extends GenericDTO {

    private List<Tsgnomargumento> listaArgumentos;
    private Tsgnomargumento argumento;

    /**
     *
     * @return
     */
    public List<Tsgnomargumento> getListaArgumentos() {
        return listaArgumentos;
    }

    /**
     *
     * @param listaArgumentos
     */
    public void setListaArgumentos(List<Tsgnomargumento> listaArgumentos) {
        this.listaArgumentos = listaArgumentos;
    }

    /**
     *
     * @return
     */
    public Tsgnomargumento getArgumento() {
        return argumento;
    }

    /**
     *
     * @param argumento
     */
    public void setArgumento(Tsgnomargumento argumento) {
        this.argumento = argumento;
    }

}
