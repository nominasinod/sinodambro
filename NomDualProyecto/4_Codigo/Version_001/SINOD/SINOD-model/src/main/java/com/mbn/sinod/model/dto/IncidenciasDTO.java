package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomincidencia;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Karla
 */
public class IncidenciasDTO extends GenericDTO {

    private Tsgnomincidencia incidencia;
    private List<Tsgnomincidencia> listaIncidencias;

    /**
     *
     */
    public IncidenciasDTO() {
        this.incidencia = new Tsgnomincidencia();
        this.listaIncidencias = new ArrayList<Tsgnomincidencia>();
    }

    /**
     *
     * @return
     */
    public Tsgnomincidencia getIncidencia() {
        return incidencia;
    }

    /**
     *
     * @param incidencia
     */
    public void setIncidencia(Tsgnomincidencia incidencia) {
        this.incidencia = incidencia;
    }

    /**
     *
     * @return
     */
    public List<Tsgnomincidencia> getListaIncidencias() {
        return listaIncidencias;
    }

    /**
     *
     * @param listaIncidencias
     */
    public void setListaIncidencias(List<Tsgnomincidencia> listaIncidencias) {
        this.listaIncidencias = listaIncidencias;
    }

}
