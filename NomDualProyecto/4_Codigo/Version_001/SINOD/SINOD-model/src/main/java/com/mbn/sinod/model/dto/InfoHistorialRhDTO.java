package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Ivette
 */
public class InfoHistorialRhDTO extends GenericDTO {

    private List<HistorialEmpleadosRhDTO> listaHistorialEmpleadosRh;

    /**
     *
     * @return the listaHistorialEmpleadosRh
     */
    public List<HistorialEmpleadosRhDTO> getListaHistorialEmpleadosRh() {
        return listaHistorialEmpleadosRh;
    }

    /**
     *
     * @param listaHistorialEmpleadosRh the listaHistorialEmpleadosRh to set
     */
    public void setListaHistorialEmpleadosRh(List<HistorialEmpleadosRhDTO> listaHistorialEmpleadosRh) {
        this.listaHistorialEmpleadosRh = listaHistorialEmpleadosRh;
    }

}
