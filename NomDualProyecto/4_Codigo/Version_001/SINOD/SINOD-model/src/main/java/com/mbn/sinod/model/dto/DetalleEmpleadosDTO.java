package com.mbn.sinod.model.dto;

/**
 *
 * @author Ivette
 */
public class DetalleEmpleadosDTO {

    private Integer codempleado;
    private String nomcompleto;
    private String desrfc;
    private String descurp;
    private String desnbarea;
    private String despuesto;

    /**
     *
     * @return the codempleado
     */
    public Integer getCodempleado() {
        return codempleado;
    }

    /**
     *
     * @param codempleado the codempleado to set
     */
    public void setCodempleado(Integer codempleado) {
        this.codempleado = codempleado;
    }

    /**
     *
     * @return the nomcompleto
     */
    public String getNomcompleto() {
        return nomcompleto;
    }

    /**
     *
     * @param nomcompleto the nomcompleto to set
     */
    public void setNomcompleto(String nomcompleto) {
        this.nomcompleto = nomcompleto;
    }

    /**
     *
     * @return the desrfc
     */
    public String getDesrfc() {
        return desrfc;
    }

    /**
     *
     * @param desrfc the desrfc to set
     */
    public void setDesrfc(String desrfc) {
        this.desrfc = desrfc;
    }

    /**
     *
     * @return the desnbarea
     */
    public String getDesnbarea() {
        return desnbarea;
    }

    /**
     *
     * @param desnbarea the desnbarea to set
     */
    public void setDesnbarea(String desnbarea) {
        this.desnbarea = desnbarea;
    }

    /**
     *
     * // * @return the despuesto
     */
    public String getDespuesto() {
        return despuesto;
    }

    /**
     *
     * @param despuesto the despuesto to set
     */
    public void setDespuesto(String despuesto) {
        this.despuesto = despuesto;
    }

    /**
     *
     * @return the descurp
     */
    public String getDescurp() {
        return descurp;
    }

    /**
     *
     * @param descurp the descurp to set
     */
    public void setDescurp(String descurp) {
        this.descurp = descurp;
    }

}
