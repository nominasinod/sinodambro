package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.dto.ConceptosEmpleadoDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquincht;
import java.util.List;
import org.hibernate.transform.Transformers;

/**
 *
 * @author mipe
 */
public class ConcepQuincDAOImpl extends GenericDAOImpl<Tsgnomcncptoquinc, Integer> implements ConcepQuincDAO {

    @Override
    public List<Tsgnomcncptoquinc> listaConcepQuinc() {
        return findAll();
    }

    @Override
    public List<Tsgnomcncptoquinc> listaConcepQuincByEmpQuin(Integer empQuin) {
        List<Tsgnomcncptoquinc> concepQuincena = (List<Tsgnomcncptoquinc>) getSession().createQuery("FROM Tsgnomcncptoquinc "
                + "WHERE cod_empquincenaid_fk = :parametro ")
                .setParameter("parametro", empQuin).list();
        return concepQuincena;
    }

    @Override
    public List<Tsgnomcncptoquincht> listaConcepQuincByEmpQuinHT(Integer empQuin) {
        List<Tsgnomcncptoquincht> concepQuincena = (List<Tsgnomcncptoquincht>) getSession().createQuery("FROM Tsgnomcncptoquincht "
                + "WHERE codEmpquincenaidFk.codEmpquincenahtid = :parametro ")
                .setParameter("parametro", empQuin).list();
        return concepQuincena;
    }

    @Override
    public List<Tsgnomcncptoquinc> listaConceptosEmpl(Integer empleado) {
        List<Tsgnomcncptoquinc> concepQuincena = (List<Tsgnomcncptoquinc>) getSession().createQuery("FROM Tsgnomcncptoquinc "
                + "WHERE codEmpquincenaidFk.codEmpleadoidFk.codEmpleadoid = :parametro ")
                .setParameter("parametro", empleado).list();
        return concepQuincena;
    }

    @Override
    public List<Tsgnomcncptoquinc> listaConcepQuincByCabecera(Integer cabecera) {
        List<Tsgnomcncptoquinc> concepQuincena = (List<Tsgnomcncptoquinc>) getSession().createQuery("FROM Tsgnomcncptoquinc "
                + "WHERE codEmpquincenaidFk.codCabeceraidFk.codCabeceraid = :parametro ")
                .setParameter("parametro", cabecera).list();
        return concepQuincena;
    }

    @Override
    public List<ConceptosEmpleadoDTO> listaConceptosEmpleados(Integer idEmpleado, Integer idCabecera) {
        List<ConceptosEmpleadoDTO> datos = (List<ConceptosEmpleadoDTO>) getSession().createSQLQuery("SELECT * FROM sgnom.fn_buscar_conceptos_del_empleado(?, ?);")
                .addScalar("tipo")
                .addScalar("clave")
                .addScalar("nombre")
                .addScalar("importe")
                .addScalar("cantidad")
                .setResultTransformer(Transformers.aliasToBean(ConceptosEmpleadoDTO.class))
                .setInteger(0, idEmpleado)
                .setInteger(1, idCabecera)
                .list();
        return datos;

    }
}
