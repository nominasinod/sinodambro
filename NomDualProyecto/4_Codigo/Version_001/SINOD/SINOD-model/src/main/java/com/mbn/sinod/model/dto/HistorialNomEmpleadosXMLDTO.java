package com.mbn.sinod.model.dto;

/**
 *
 * @author Ivette
 */
public class HistorialNomEmpleadosXMLDTO extends GenericDTO {

    private String fecha;
    private String sueldoImss;
    private String sueldoHonorarios;
    private String estatusFnzs;

    /**
     *
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     *
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     *
     * @return the sueldoImss
     */
    public String getSueldoImss() {
        return sueldoImss;
    }

    /**
     *
     * @param sueldoImss the sueldoImss to set
     */
    public void setSueldoImss(String sueldoImss) {
        this.sueldoImss = sueldoImss;
    }

    /**
     *
     * @return the sueldoHonorarios
     */
    public String getSueldoHonorarios() {
        return sueldoHonorarios;
    }

    /**
     *
     * @param sueldoHonorarios the sueldoHonorarios to set
     */
    public void setSueldoHonorarios(String sueldoHonorarios) {
        this.sueldoHonorarios = sueldoHonorarios;
    }

    /**
     *
     * @return the estatusFnzs
     */
    public String getEstatusFnzs() {
        return estatusFnzs;
    }

    /**
     *
     * @param estatusFnzs the estatusFnzs to set
     */
    public void setEstatusFnzs(String estatusFnzs) {
        this.estatusFnzs = estatusFnzs;
    }

}
