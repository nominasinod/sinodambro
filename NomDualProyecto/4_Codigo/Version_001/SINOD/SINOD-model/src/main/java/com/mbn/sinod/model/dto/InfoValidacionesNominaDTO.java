package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Ivette
 */
public class InfoValidacionesNominaDTO extends GenericDTO {

    private List<ValidacionesNominaDTO> listaValidacionesNomina;

    /**
     *
     * @return the listaValidacionesNomina
     */
    public List<ValidacionesNominaDTO> getListaValidacionesNomina() {
        return listaValidacionesNomina;
    }

    /**
     *
     * @param listaValidacionesNomina the listaValidacionesNomina to set
     */
    public void setListaValidacionesNomina(List<ValidacionesNominaDTO> listaValidacionesNomina) {
        this.listaValidacionesNomina = listaValidacionesNomina;
    }

}
