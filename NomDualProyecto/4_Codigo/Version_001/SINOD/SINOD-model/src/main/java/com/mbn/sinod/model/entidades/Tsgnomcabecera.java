package com.mbn.sinod.model.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author ambrosio
 */
@Entity
@Table(name = "tsgnomcabecera", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomcabecera.findAll", query = "SELECT t FROM Tsgnomcabecera t")})
public class Tsgnomcabecera implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "argId")
    @Basic(optional = false)
    @Column(name = "cod_cabeceraid")
    private Integer codCabeceraid;
    @Basic(optional = false)
    @Column(name = "cod_nbnomina")
    private String codNbnomina;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Column(name = "fec_ejecucion")
    @Temporal(TemporalType.DATE)
    private Date fecEjecucion;
    @Column(name = "fec_cierre")
    @Temporal(TemporalType.DATE)
    private Date fecCierre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "imp_totpercepcion")
    private BigDecimal impTotpercepcion;
    @Column(name = "imp_totdeduccion")
    private BigDecimal impTotdeduccion;
    @Column(name = "imp_totalemp")
    private BigDecimal impTotalemp;
    @Column(name = "cnu_totalemp")
    private Integer cnuTotalemp;
    @Basic(optional = false)
    @Column(name = "aud_codcreadopor")
    private int audCodcreadopor;
    @Column(name = "aud_codmodificadopor")
    private Integer audCodmodificadopor;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCabeceraidFk", fetch = FetchType.LAZY)
    private List<Tsgnomempquincenaht> tsgnomempquincenahtList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCabeceraidFk", fetch = FetchType.LAZY)
    private List<Tsgnomempquincena> tsgnomempquincenaList;
    @JoinColumn(name = "cod_estatusnomid_fk", referencedColumnName = "cod_estatusnomid")
    @ManyToOne(optional = false)
    private Tsgnomestatusnom codEstatusnomidFk;
    @JoinColumn(name = "cod_quincenaid_fk", referencedColumnName = "cod_quincenaid")
    @ManyToOne(optional = false)
    private Tsgnomquincena codQuincenaidFk;
    @JoinColumn(name = "cod_tiponominaid_fk", referencedColumnName = "cod_tiponominaid")
    @ManyToOne(optional = false)
    private Tsgnomtiponomina codTiponominaidFk;

    /**
     *
     */
    public Tsgnomcabecera() {
    }

    /**
     *
     * @param codCabeceraid
     */
    public Tsgnomcabecera(Integer codCabeceraid) {
        this.codCabeceraid = codCabeceraid;
    }

    /**
     *
     * @param codCabeceraid
     * @param codNbnomina
     * @param fecCreacion
     * @param audCodcreadopor
     * @param audFeccreacion
     */
    public Tsgnomcabecera(Integer codCabeceraid, String codNbnomina, Date fecCreacion, int audCodcreadopor, Date audFeccreacion) {
        this.codCabeceraid = codCabeceraid;
        this.codNbnomina = codNbnomina;
        this.fecCreacion = fecCreacion;
        this.audCodcreadopor = audCodcreadopor;
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Integer getCodCabeceraid() {
        return codCabeceraid;
    }

    /**
     *
     * @param codCabeceraid
     */
    public void setCodCabeceraid(Integer codCabeceraid) {
        this.codCabeceraid = codCabeceraid;
    }

    /**
     *
     * @return
     */
    public String getCodNbnomina() {
        return codNbnomina;
    }

    /**
     *
     * @param codNbnomina
     */
    public void setCodNbnomina(String codNbnomina) {
        this.codNbnomina = codNbnomina;
    }

    /**
     *
     * @return
     */
    public Date getFecCreacion() {
        return fecCreacion;
    }

    /**
     *
     * @param fecCreacion
     */
    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    /**
     *
     * @return
     */
    public Date getFecEjecucion() {
        return fecEjecucion;
    }

    /**
     *
     * @param fecEjecucion
     */
    public void setFecEjecucion(Date fecEjecucion) {
        this.fecEjecucion = fecEjecucion;
    }

    /**
     *
     * @return
     */
    public Date getFecCierre() {
        return fecCierre;
    }

    /**
     *
     * @param fecCierre
     */
    public void setFecCierre(Date fecCierre) {
        this.fecCierre = fecCierre;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpTotpercepcion() {
        return impTotpercepcion;
    }

    /**
     *
     * @param impTotpercepcion
     */
    public void setImpTotpercepcion(BigDecimal impTotpercepcion) {
        this.impTotpercepcion = impTotpercepcion;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpTotdeduccion() {
        return impTotdeduccion;
    }

    /**
     *
     * @param impTotdeduccion
     */
    public void setImpTotdeduccion(BigDecimal impTotdeduccion) {
        this.impTotdeduccion = impTotdeduccion;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpTotalemp() {
        return impTotalemp;
    }

    /**
     *
     * @param impTotalemp
     */
    public void setImpTotalemp(BigDecimal impTotalemp) {
        this.impTotalemp = impTotalemp;
    }

    /**
     *
     * @return
     */
    public Integer getCnuTotalemp() {
        return cnuTotalemp;
    }

    /**
     *
     * @param cnuTotalemp
     */
    public void setCnuTotalemp(Integer cnuTotalemp) {
        this.cnuTotalemp = cnuTotalemp;
    }

    /**
     *
     * @return
     */
    public int getAudCodcreadopor() {
        return audCodcreadopor;
    }

    /**
     *
     * @param audCodcreadopor
     */
    public void setAudCodcreadopor(int audCodcreadopor) {
        this.audCodcreadopor = audCodcreadopor;
    }

    /**
     *
     * @return
     */
    public Integer getAudCodmodificadopor() {
        return audCodmodificadopor;
    }

    /**
     *
     * @param audCodmodificadopor
     */
    public void setAudCodmodificadopor(Integer audCodmodificadopor) {
        this.audCodmodificadopor = audCodmodificadopor;
    }

    /**
     *
     * @return
     */
    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    /**
     *
     * @param audFeccreacion
     */
    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    /**
     *
     * @param audFecmodificacion
     */
    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgnomempquincenaht> getTsgnomempquincenahtList() {
        return tsgnomempquincenahtList;
    }

    /**
     *
     * @param tsgnomempquincenahtList
     */
    public void setTsgnomempquincenahtList(List<Tsgnomempquincenaht> tsgnomempquincenahtList) {
        this.tsgnomempquincenahtList = tsgnomempquincenahtList;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgnomempquincena> getTsgnomempquincenaList() {
        return tsgnomempquincenaList;
    }

    /**
     *
     * @param tsgnomempquincenaList
     */
    public void setTsgnomempquincenaList(List<Tsgnomempquincena> tsgnomempquincenaList) {
        this.tsgnomempquincenaList = tsgnomempquincenaList;
    }

    /**
     *
     * @return
     */
    public Tsgnomestatusnom getCodEstatusnomidFk() {
        return codEstatusnomidFk;
    }

    /**
     *
     * @param codEstatusnomidFk
     */
    public void setCodEstatusnomidFk(Tsgnomestatusnom codEstatusnomidFk) {
        this.codEstatusnomidFk = codEstatusnomidFk;
    }

    /**
     *
     * @return
     */
    public Tsgnomquincena getCodQuincenaidFk() {
        return codQuincenaidFk;
    }

    /**
     *
     * @param codQuincenaidFk
     */
    public void setCodQuincenaidFk(Tsgnomquincena codQuincenaidFk) {
        this.codQuincenaidFk = codQuincenaidFk;
    }

    /**
     *
     * @return
     */
    public Tsgnomtiponomina getCodTiponominaidFk() {
        return codTiponominaidFk;
    }

    /**
     *
     * @param codTiponominaidFk
     */
    public void setCodTiponominaidFk(Tsgnomtiponomina codTiponominaidFk) {
        this.codTiponominaidFk = codTiponominaidFk;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCabeceraid != null ? codCabeceraid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomcabecera)) {
            return false;
        }
        Tsgnomcabecera other = (Tsgnomcabecera) object;
        if ((this.codCabeceraid == null && other.codCabeceraid != null) || (this.codCabeceraid != null && !this.codCabeceraid.equals(other.codCabeceraid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomcabecera[ codCabeceraid=" + codCabeceraid + " ]";
    }

}
