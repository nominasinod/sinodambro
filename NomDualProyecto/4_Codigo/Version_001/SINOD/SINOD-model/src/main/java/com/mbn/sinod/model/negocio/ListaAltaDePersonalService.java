package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.ListaAltaDePersonalDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;

/**
 *
 * @author Ivette
 */
public interface ListaAltaDePersonalService extends BaseService<Tsgrhempleados, Integer> {

    /**
     *
     * @return
     */
    ListaAltaDePersonalDTO listaInformacionParaAltaNom();

}
