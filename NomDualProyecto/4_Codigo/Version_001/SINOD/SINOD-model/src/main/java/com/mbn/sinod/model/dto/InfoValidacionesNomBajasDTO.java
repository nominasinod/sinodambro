package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Ivette
 */
public class InfoValidacionesNomBajasDTO extends GenericDTO {

    private List<ValidacionesNomBajasDTO> listaValidacionesNomBajas;

    /**
     *
     * @return the listaValidacionesNomBajas
     */
    public List<ValidacionesNomBajasDTO> getListaValidacionesNomBajas() {
        return listaValidacionesNomBajas;
    }

    /**
     *
     * @param listaValidacionesNomBajas the listaValidacionesNomBajas to set
     */
    public void setListaValidacionesNomBajas(List<ValidacionesNomBajasDTO> listaValidacionesNomBajas) {
        this.listaValidacionesNomBajas = listaValidacionesNomBajas;
    }

}
