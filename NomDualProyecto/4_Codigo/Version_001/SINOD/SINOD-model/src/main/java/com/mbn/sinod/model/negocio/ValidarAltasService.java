package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.ValidacionAltasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;

/**
 *
 * @author mariana
 */
public interface ValidarAltasService extends BaseService<Tsgnomempleados, Integer> {

    /**
     *
     * @return
     */
    ValidacionAltasDTO listarAltasAValidar();

    /**
     *
     * @param altas
     * @return
     */
    ValidacionAltasDTO validarCadaAlta(ValidacionAltasDTO altas);

    /**
     *
     * @param altas
     * @return
     */
    ValidacionAltasDTO validarAltasAceptadas(ValidacionAltasDTO altas);

    /**
     *
     * @param altas
     * @return
     */
    ValidacionAltasDTO validarAltasRechazadas(ValidacionAltasDTO altas);
}
