package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.ValidacionBajasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface ValidarBajasDAO extends GenericDAO<Tsgnomempleados, Integer> {

    /**
     * Obtiene una lista de las bajas a validar por Finanzas en el sistema de nómina
     * 
     * @return
     */
    List<ValidacionBajasDTO> listarBajasAValidar();

    /**
     * Ayuda a la validación y rechazo de bajas realizandolo una a la vez
     * 
     * @param listaInformacionValidar
     * @return
     */
    boolean validarCadaBaja(List<ValidacionBajasDTO> listaInformacionValidar);

    /**
     * Ayuda a validar todas las bajas en conjunto
     * 
     * @param listaInformacionValidar
     * @return
     */
    boolean validarBajasAceptadas(List<ValidacionBajasDTO> listaInformacionValidar);

    /**
     * Ayuda a rechazar todas las bajas en conjunto
     * 
     * @param listaInformacionValidar
     * @return
     */
    boolean validarBajasRechazadas(List<ValidacionBajasDTO> listaInformacionValidar);
}
