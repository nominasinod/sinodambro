/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomaguinaldo;
import java.util.List;

/**
 *
 * @author Ivette
 */
public class DesgloceAguinaldoDAOImpl extends GenericDAOImpl<Tsgnomaguinaldo, Integer> implements DesgloceAguinaldoDAO{

    @Override
    public List<Tsgnomaguinaldo> listaDesgloceAguinaldoEmp(Integer empQuin) {
       List<Tsgnomaguinaldo> desgloceAguinaldo = (List<Tsgnomaguinaldo>) getSession().createQuery("FROM Tsgnomaguinaldo "
                + "WHERE cod_empquincenaid_fk = :parametro ")
                .setParameter("parametro", empQuin).list();
        return desgloceAguinaldo;
    }
    
}
