package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.VerInformacionDePersonalDAO;
import com.mbn.sinod.model.dto.InformacionPorPersonalDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Ivette
 */
public class VerInformacionDePersonalServiceImpl extends BaseServiceImpl<Tsgrhempleados, Integer>
        implements VerInformacionDePersonalService {

    @Autowired
    private VerInformacionDePersonalDAO verInformacionDePersonalDAO;

    /**
     *
     * @return
     */
    public VerInformacionDePersonalDAO getVerInformacionDePersonalDAO() {
        return verInformacionDePersonalDAO;
    }

    /**
     *
     * @param verInformacionDePersonalDAO
     */
    public void setVerInformacionDePersonalDAO(VerInformacionDePersonalDAO verInformacionDePersonalDAO) {
        this.verInformacionDePersonalDAO = verInformacionDePersonalDAO;
    }

    /**
     *
     * @param cod_empleadoid
     * @return
     */
    @Override
    public InformacionPorPersonalDTO informacionPorPersonal(Integer cod_empleadoid) {

        InformacionPorPersonalDTO dto = new InformacionPorPersonalDTO();
        dto.setListaInformacionPorPersonal(getVerInformacionDePersonalDAO().informacionPorPersonal(cod_empleadoid));
        return dto;

    }

}
