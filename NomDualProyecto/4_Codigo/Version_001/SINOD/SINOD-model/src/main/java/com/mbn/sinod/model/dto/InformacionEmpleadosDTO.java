package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Ivette
 */
public class InformacionEmpleadosDTO extends GenericDTO {

    private List<DetalleEmpleadosDTO> listaInformacionEmpleados;

    /**
     *
     * @return the listaInformacionEmpleados
     */
    public List<DetalleEmpleadosDTO> getListaInformacionEmpleados() {
        return listaInformacionEmpleados;
    }

    /**
     *
     * @param listaInformacionEmpleados the listaInformacionEmpleados to set
     */
    public void setListaInformacionEmpleados(List<DetalleEmpleadosDTO> listaInformacionEmpleados) {
        this.listaInformacionEmpleados = listaInformacionEmpleados;
    }

}
