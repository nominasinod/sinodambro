/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomaguinaldo;
import java.util.List;

/**
 * Interfaz DAO para listar la entidad Tsgnomaguinaldo de 
 * SGNOM de la BD Suite.
 *
 * Esta interfaz contiene el metodo para listar la entidad Tsgnomaguinaldo y obtener el XML 
 * correspondiente a la quincena por empleado del aguinaldo en la nómina,
 * por medio del id del empleado de la quincena.
 *
 * @author Ivette
 */
public interface DesgloceAguinaldoDAO extends GenericDAO<Tsgnomaguinaldo, Integer>{
    /**
     * 
     * @param empQuin
     * @return 
     */
    List<Tsgnomaguinaldo> listaDesgloceAguinaldoEmp(Integer empQuin);
}
