package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Karla
 */
public class IncidenciasPorEmpleadoDTO {

    private List<DetalleIncidenciasEmpleadoDTO> listaIncidenciasEmpleado;

    /**
     *
     * @return
     */
    public List<DetalleIncidenciasEmpleadoDTO> getListaIncidenciasEmpleado() {
        return listaIncidenciasEmpleado;
    }

    /**
     *
     * @param listaIncidenciasEmpleado
     */
    public void setListaIncidenciasEmpleado(List<DetalleIncidenciasEmpleadoDTO> listaIncidenciasEmpleado) {
        this.listaIncidenciasEmpleado = listaIncidenciasEmpleado;
    }

}
