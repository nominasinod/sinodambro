package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.EmpDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;

/**
 *
 * @author eduardotorres
 */
public interface DetalleEmpService extends BaseService<Tsgrhempleados, Integer> {

    /**
     *
     * @param cabecera
     * @return
     */
    EmpDTO empDTO(Integer cabecera);
//DetalleEmpDTO detalleEmpDTO (int empleadoId);

}
