package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgrhpuestos;
import java.util.List;

/**
 *
 * @author eduardotorres
 */
public interface PuestosDAO extends GenericDAO<Tsgrhpuestos, Integer> {

    /**
     *
     * @return
     */
    List<Tsgrhpuestos> listaPuestos();

    /**
     *
     * @param puestosId
     * @return
     */
    Tsgrhpuestos obtenerPuestospoId(Integer puestosId);
}
