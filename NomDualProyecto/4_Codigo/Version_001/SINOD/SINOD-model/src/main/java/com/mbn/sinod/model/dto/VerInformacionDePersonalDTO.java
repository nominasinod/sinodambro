package com.mbn.sinod.model.dto;

import java.math.BigDecimal;

/**
 *
 * @author Ivette
 */
public class VerInformacionDePersonalDTO {

    private Integer cod_empleado;
    private String fec_ingreso;
    private String fec_modificacion;
    private String des_nombre;
    private String des_apepaterno;
    private String des_apematerno;
    private String des_nbarea;
    private String des_puesto;
    private String des_rol;
    private String cod_rfc;
    private String cod_curp;
    private String cod_nss;
    private String fec_nacimiento;
    private String des_direccion;
    private String des_correo;
    private String correo_personal;
    private Character cod_tipoaguinaldo;
    private BigDecimal imp_aguinaldo;
    private Integer quincena;

    /**
     *
     * @return the des_apepaterno
     */
    public String getDes_apepaterno() {
        return des_apepaterno;
    }

    /**
     *
     * @param des_apepaterno the des_apepaterno to set
     */
    public void setDes_apepaterno(String des_apepaterno) {
        this.des_apepaterno = des_apepaterno;
    }

    /**
     *
     * @return the des_apematerno
     */
    public String getDes_apematerno() {
        return des_apematerno;
    }

    /**
     *
     * @param des_apematerno the des_apematerno to set
     */
    public void setDes_apematerno(String des_apematerno) {
        this.des_apematerno = des_apematerno;
    }

    /**
     *
     * @return the des_puesto
     */
    public String getDes_puesto() {
        return des_puesto;
    }

    /**
     *
     * @param des_puesto the des_puesto to set
     */
    public void setDes_puesto(String des_puesto) {
        this.des_puesto = des_puesto;
    }

    /**
     *
     * @return the cod_tipoaguinaldo 
     */
    public Character getCod_tipoaguinaldo() {
        return cod_tipoaguinaldo;
    }

    /**
     *
     * @param cod_tipoaguinaldo the cod_tipoaguinaldo to set
     */
    public void setCod_tipoaguinaldo(Character cod_tipoaguinaldo) {
        this.cod_tipoaguinaldo = cod_tipoaguinaldo;
    }

    /**
     *
     * @return the imp_aguinaldo
     */
    public BigDecimal getImp_aguinaldo() {
        return imp_aguinaldo;
    }

    /**
     *
     * @param imp_aguinaldo the imp_aguinaldo to set
     */
    public void setImp_aguinaldo(BigDecimal imp_aguinaldo) {
        this.imp_aguinaldo = imp_aguinaldo;
    }

    /**
     *
     * @return the quincena
     */
    public Integer getQuincena() {
        return quincena;
    }

    /**
     *
     * @param quincena the quincena to set
     */
    public void setQuincena(Integer quincena) {
        this.quincena = quincena;
    }

    /**
     *
     * @return the des_nombre
     */
    public String getDes_nombre() {
        return des_nombre;
    }

    /**
     *
     * @param des_nombre the des_nombre to set
     */
    public void setDes_nombre(String des_nombre) {
        this.des_nombre = des_nombre;
    }

    /**
     *
     * @return the des_nbarea
     */
    public String getDes_nbarea() {
        return des_nbarea;
    }

    /**
     *
     * @param des_nbarea the des_nbarea to set
     */
    public void setDes_nbarea(String des_nbarea) {
        this.des_nbarea = des_nbarea;
    }

    /**
     *
     * @return the cod_rfc
     */
    public String getCod_rfc() {
        return cod_rfc;
    }

    /**
     *
     * @param cod_rfc the cod_rfc to set
     */
    public void setCod_rfc(String cod_rfc) {
        this.cod_rfc = cod_rfc;
    }

    /**
     *
     * @return the cod_nss
     */
    public String getCod_nss() {
        return cod_nss;
    }

    /**
     *
     * @param cod_nss the cod_nss to set
     */
    public void setCod_nss(String cod_nss) {
        this.cod_nss = cod_nss;
    }

    /**
     *
     * @return the  des_direccion
     */
    public String getDes_direccion() {
        return des_direccion;
    }

    /**
     *
     * @param des_direccion the  des_direccion to set
     */
    public void setDes_direccion(String des_direccion) {
        this.des_direccion = des_direccion;
    }

    /**
     *
     * @return the des_correo
     */
    public String getDes_correo() {
        return des_correo;
    }

    /**
     *
     * @param des_correo the des_correo to set
     */
    public void setDes_correo(String des_correo) {
        this.des_correo = des_correo;
    }

    /**
     *
     * @return the cod_empleado
     */
    public Integer getCod_empleado() {
        return cod_empleado;
    }

    /**
     *
     * @param cod_empleado the cod_empleado to set
     */
    public void setCod_empleado(Integer cod_empleado) {
        this.cod_empleado = cod_empleado;
    }

    /**
     *
     * @return the cod_curp
     */
    public String getCod_curp() {
        return cod_curp;
    }

    /**
     *
     * @param cod_curp the cod_curp to set
     */
    public void setCod_curp(String cod_curp) {
        this.cod_curp = cod_curp;
    }

    /**
     *
     * @return the correo_personal
     */
    public String getCorreo_personal() {
        return correo_personal;
    }

    /**
     *
     * @param correo_personal the correo_personal to set
     */
    public void setCorreo_personal(String correo_personal) {
        this.correo_personal = correo_personal;
    }

    /**
     * 
     * @return the des_rol
     */
    public String getDes_rol() {
        return des_rol;
    }

    /**
     *
     * @param des_rol the des_rol to set
     */
    public void setDes_rol(String des_rol) {
        this.des_rol = des_rol;
    }

    /**
     *
     * @return the fec_ingreso
     */
    public String getFec_ingreso() {
        return fec_ingreso;
    }

    /**
     *
     * @param fec_ingreso the fec_ingreso to set
     */
    public void setFec_ingreso(String fec_ingreso) {
        this.fec_ingreso = fec_ingreso;
    }

    /**
     *
     * @return the fec_modificacion
     */
    public String getFec_modificacion() {
        return fec_modificacion;
    }

    /**
     *
     * @param fec_modificacion  the fec_modificacion to set
     */
    public void setFec_modificacion(String fec_modificacion) {
        this.fec_modificacion = fec_modificacion;
    }

    /**
     *
     * @return the fec_nacimiento
     */
    public String getFec_nacimiento() {
        return fec_nacimiento;
    }

    /**
     *
     * @param fec_nacimiento the fec_nacimiento to set
     */
    public void setFec_nacimiento(String fec_nacimiento) {
        this.fec_nacimiento = fec_nacimiento;
    }

}
