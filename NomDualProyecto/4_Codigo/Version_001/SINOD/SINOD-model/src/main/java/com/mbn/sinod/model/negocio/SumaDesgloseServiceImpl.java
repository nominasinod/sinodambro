package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.SumaDesgloseDAO;
import com.mbn.sinod.model.dto.SumaDesgloseDTO;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import com.mbn.sinod.model.entidades.Tsgnomempquincenaht;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mipe
 */
public class SumaDesgloseServiceImpl extends BaseServiceImpl<Tsgnomempquincena, Integer>
        implements SumaDesgloseService {

    private static final Logger logger = Logger.getLogger(SumaDesgloseService.class.getName());

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    @Override
    public SumaDesgloseDTO sumaDesglose(Integer cabecera, Integer empleado) {
        SumaDesgloseDTO respuesta = new SumaDesgloseDTO();
        try {

            List<Tsgnomempquincena> sumaDesglose = ((SumaDesgloseDAO) getGenericDAO()).sumaDesglose(cabecera, empleado);

            if (sumaDesglose != null) {
                respuesta.setListConceptosSum(sumaDesglose);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_SUMA_DESGLOSE);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);

            } else {
                respuesta.setListConceptosSum(sumaDesglose);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_SUMA_DESGLOSE);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new SumaDesgloseDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ConceptoServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    @Override
    public SumaDesgloseDTO sumaDesgloseht(Integer cabecera, Integer empleado) {
        SumaDesgloseDTO respuesta = new SumaDesgloseDTO();
        try {

            List<Tsgnomempquincenaht> sumaDesglose = ((SumaDesgloseDAO) getGenericDAO()).sumaDesgloseht(cabecera, empleado);

            if (sumaDesglose != null) {
                respuesta.setListConceptosSumht(sumaDesglose);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_SUMA_DESGLOSE_HT);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);

            } else {
                respuesta.setListConceptosSumht(sumaDesglose);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_SUMA_DESGLOSE_HT);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new SumaDesgloseDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ConceptoServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }
}
