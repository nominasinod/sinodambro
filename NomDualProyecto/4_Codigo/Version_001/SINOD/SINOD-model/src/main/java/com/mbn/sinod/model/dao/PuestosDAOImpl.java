package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.Search;
import com.mbn.sinod.model.entidades.Tsgrhpuestos;
import java.util.List;

/**
 *
 * @author mipe
 */
public class PuestosDAOImpl extends GenericDAOImpl<Tsgrhpuestos, Integer> implements PuestosDAO {

    @Override
    public List<Tsgrhpuestos> listaPuestos() {
        return findAll();
    }

    @Override
    public Tsgrhpuestos obtenerPuestospoId(Integer puestosId) {
        Search search = new Search();
        search.addFilterEqual("codPuesto", puestosId);
        return searchUnique(search);
    }

}
