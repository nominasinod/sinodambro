package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomquincena;
import java.util.List;

/**
 *
 * @author mipe
 */
public interface QuincenasDAO extends GenericDAO<Tsgnomquincena, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomquincena> listarQuincena();

    /**
     *
     * @return
     */
    List<Tsgnomquincena> listarQuinFuturas();

    /**
     *
     * @param quincena
     * @return
     */
    boolean guardarActualizarQuincena(Tsgnomquincena quincena);

    /**
     * Obtiene la quincena actual,
     *
     * @return
     */
    Tsgnomquincena quincenaActual();
}
