package com.mbn.sinod.model.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author ambrosio
 */
@Entity
@Table(name = "tsgnomejercicio", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomejercicio.findAll", query = "SELECT t FROM Tsgnomejercicio t")})
public class Tsgnomejercicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "argId")
    @Basic(optional = false)
    @Column(name = "cod_ejercicioid")
    private Integer codEjercicioid;
    @Basic(optional = false)
    @Column(name = "cnu_valorejercicio")
    private int cnuValorejercicio;
    @Basic(optional = false)
    @Column(name = "bol_estatus")
    private boolean bolEstatus;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codEjercicioidFk")
    private List<Tsgnomquincena> tsgnomquincenaList;

    /**
     *
     */
    public Tsgnomejercicio() {
    }

    /**
     *
     * @param codEjercicioid
     */
    public Tsgnomejercicio(Integer codEjercicioid) {
        this.codEjercicioid = codEjercicioid;
    }

    /**
     *
     * @param codEjercicioid
     * @param cnuValorejercicio
     * @param bolEstatus
     */
    public Tsgnomejercicio(Integer codEjercicioid, int cnuValorejercicio, boolean bolEstatus) {
        this.codEjercicioid = codEjercicioid;
        this.cnuValorejercicio = cnuValorejercicio;
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    public Integer getCodEjercicioid() {
        return codEjercicioid;
    }

    /**
     *
     * @param codEjercicioid
     */
    public void setCodEjercicioid(Integer codEjercicioid) {
        this.codEjercicioid = codEjercicioid;
    }

    /**
     *
     * @return
     */
    public int getCnuValorejercicio() {
        return cnuValorejercicio;
    }

    /**
     *
     * @param cnuValorejercicio
     */
    public void setCnuValorejercicio(int cnuValorejercicio) {
        this.cnuValorejercicio = cnuValorejercicio;
    }

    /**
     *
     * @return
     */
    public boolean getBolEstatus() {
        return bolEstatus;
    }

    /**
     *
     * @param bolEstatus
     */
    public void setBolEstatus(boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgnomquincena> getTsgnomquincenaList() {
        return tsgnomquincenaList;
    }

    /**
     *
     * @param tsgnomquincenaList
     */
    public void setTsgnomquincenaList(List<Tsgnomquincena> tsgnomquincenaList) {
        this.tsgnomquincenaList = tsgnomquincenaList;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEjercicioid != null ? codEjercicioid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomejercicio)) {
            return false;
        }
        Tsgnomejercicio other = (Tsgnomejercicio) object;
        if ((this.codEjercicioid == null && other.codEjercicioid != null) || (this.codEjercicioid != null && !this.codEjercicioid.equals(other.codEjercicioid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomejercicio[ codEjercicioid=" + codEjercicioid + " ]";
    }

}
