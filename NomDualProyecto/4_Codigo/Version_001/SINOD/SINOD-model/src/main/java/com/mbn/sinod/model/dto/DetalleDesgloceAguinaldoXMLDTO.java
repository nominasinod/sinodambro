/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dto;

/**
 *
 * @author Ivette
 */
public class DetalleDesgloceAguinaldoXMLDTO extends GenericDTO {
    
    private String salarioDiario;
    private String fSalarioD;
    private String vSalarioD;
    private String diasLaborados;
    private String fDiasLab;
    private String vDiasLab;
    private String diasPropAguinaldo;
    private String propAguinaldo;

    /**
     *
     * @return the salarioDiario
     */
    public String getSalarioDiario() {
        return salarioDiario;
    }

    /**
     *
     * @param salarioDiario the salarioDiario to set
     */
    public void setSalarioDiario(String salarioDiario) {
        this.salarioDiario = salarioDiario;
    }

    /**
     *
     * @return the diasLaborados
     */
    public String getDiasLaborados() {
        return diasLaborados;
    }

    /**
     *
     * @param diasLaborados the diasLaborados to set
     */
    public void setDiasLaborados(String diasLaborados) {
        this.diasLaborados = diasLaborados;
    }

    /**
     *
     * @return the diasPropAguinaldo to get
     */
    public String getDiasPropAguinaldo() {
        return diasPropAguinaldo;
    }

    /**
     *
     * @param diasPropAguinaldo the diasPropAguinaldo to set
     */
    public void setDiasPropAguinaldo(String diasPropAguinaldo) {
        this.diasPropAguinaldo = diasPropAguinaldo;
    }

    /**
     *
     * @return the propAguinaldo
     */
    public String getPropAguinaldo() {
        return propAguinaldo;
    }

    /**
     *
     * @param propAguinaldo the propAguinaldo to set
     */
    public void setPropAguinaldo(String propAguinaldo) {
        this.propAguinaldo = propAguinaldo;
    }

    /**
     *
     * @return the fSalarioD
     */
    public String getfSalarioD() {
        return fSalarioD;
    }

    /**
     *
     * @param fSalarioD the fSalarioD to set
     */
    public void setfSalarioD(String fSalarioD) {
        this.fSalarioD = fSalarioD;
    }

    /**
     *
     * @return the vSalarioD
     */
    public String getvSalarioD() {
        return vSalarioD;
    }

    /**
     *
     * @param vSalarioD the vSalarioD to set
     */
    public void setvSalarioD(String vSalarioD) {
        this.vSalarioD = vSalarioD;
    }

    /**
     *
     * @return the fDiasLab
     */
    public String getfDiasLab() {
        return fDiasLab;
    }

    /**
     *
     * @param fDiasLab to set
     */
    public void setfDiasLab(String fDiasLab) {
        this.fDiasLab = fDiasLab;
    }

    /**
     *
     * @return the vDiasLab
     */
    public String getvDiasLab() {
        return vDiasLab;
    }

    /**
     *
     * @param vDiasLab the vDiasLab to set
     */
    public void setvDiasLab(String vDiasLab) {
        this.vDiasLab = vDiasLab;
    }

}
