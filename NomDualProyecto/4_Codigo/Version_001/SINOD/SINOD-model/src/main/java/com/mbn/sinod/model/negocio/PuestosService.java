package com.mbn.sinod.model.negocio;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgrhpuestos;
import java.util.List;

/**
 *
 * @author eduardotorres
 */
public interface PuestosService extends GenericDAO<Tsgrhpuestos, Integer> {

    /**
     *
     * @return
     */
    List<Tsgrhpuestos> listaPuestos();

    /**
     *
     * @param puestosId
     * @return
     */
    Tsgrhpuestos obtenerPuestosPorId(Integer puestosId);

}
