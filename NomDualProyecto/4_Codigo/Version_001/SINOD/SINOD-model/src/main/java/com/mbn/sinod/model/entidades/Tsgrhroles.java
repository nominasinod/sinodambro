package com.mbn.sinod.model.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Ivette
 */
@Entity
@Table(name = "tsgrhroles", catalog = "suite", schema = "sgrh")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgrhroles.findAll", query = "SELECT t FROM Tsgrhroles t")})
public class Tsgrhroles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_rol")
    private Integer codRol;
    @Basic(optional = false)
    @Column(name = "des_nbrol")
    private String desNbrol;
    @OneToMany(mappedBy = "codRol", fetch = FetchType.LAZY)
    private List<Tsgrhempleados> tsgrhempleadosList;

    /**
     *
     */
    public Tsgrhroles() {
    }

    /**
     *
     * @param codRol
     */
    public Tsgrhroles(Integer codRol) {
        this.codRol = codRol;
    }

    /**
     *
     * @param codRol
     * @param desNbrol
     */
    public Tsgrhroles(Integer codRol, String desNbrol) {
        this.codRol = codRol;
        this.desNbrol = desNbrol;
    }

    /**
     *
     * @return
     */
    public Integer getCodRol() {
        return codRol;
    }

    /**
     *
     * @param codRol
     */
    public void setCodRol(Integer codRol) {
        this.codRol = codRol;
    }

    /**
     *
     * @return
     */
    public String getDesNbrol() {
        return desNbrol;
    }

    /**
     *
     * @param desNbrol
     */
    public void setDesNbrol(String desNbrol) {
        this.desNbrol = desNbrol;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgrhempleados> getTsgrhempleadosList() {
        return tsgrhempleadosList;
    }

    /**
     *
     * @param tsgrhempleadosList
     */
    public void setTsgrhempleadosList(List<Tsgrhempleados> tsgrhempleadosList) {
        this.tsgrhempleadosList = tsgrhempleadosList;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRol != null ? codRol.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhroles)) {
            return false;
        }
        Tsgrhroles other = (Tsgrhroles) object;
        if ((this.codRol == null && other.codRol != null) || (this.codRol != null && !this.codRol.equals(other.codRol))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhroles[ codRol=" + codRol + " ]";
    }

}
