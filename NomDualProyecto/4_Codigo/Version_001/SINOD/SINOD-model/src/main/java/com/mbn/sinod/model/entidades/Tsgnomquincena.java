package com.mbn.sinod.model.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author ambrosio
 */
@Entity
@Table(name = "tsgnomquincena", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomquincena.findAll", query = "SELECT t FROM Tsgnomquincena t")})
public class Tsgnomquincena implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "argId")
    @Basic(optional = false)
    @Column(name = "cod_quincenaid")
    private Integer codQuincenaid;
    @Basic(optional = false)
    @Column(name = "des_quincena")
    private String desQuincena;
    @Basic(optional = false)
    @Column(name = "fec_inicio")
    @Temporal(TemporalType.DATE)
    private Date fecInicio;
    @Basic(optional = false)
    @Column(name = "fec_fin")
    @Temporal(TemporalType.DATE)
    private Date fecFin;
    @Basic(optional = false)
    @Column(name = "fec_pago")
    @Temporal(TemporalType.DATE)
    private Date fecPago;
    @Basic(optional = false)
    @Column(name = "fec_dispersion")
    @Temporal(TemporalType.DATE)
    private Date fecDispersion;
    @Basic(optional = false)
    @Column(name = "cnu_numquincena")
    private int cnuNumquincena;
    @Basic(optional = false)
    @Column(name = "bol_estatus")
    private boolean bolEstatus;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codQuincenaidFk", fetch = FetchType.LAZY)
//    private List<Tsgnomincidencia> tsgnomincidenciaList;
    @OneToMany(mappedBy = "codQuincenafinFk", fetch = FetchType.LAZY)
    private List<Tsgnommanterceros> tsgnommantercerosList;
    @OneToMany(mappedBy = "codQuincenainicioFk", fetch = FetchType.LAZY)
    private List<Tsgnommanterceros> tsgnommantercerosList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codQuincenaidFk", fetch = FetchType.LAZY)
    private List<Tsgnomcabecera> tsgnomcabeceraList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codQuincenaidFk", fetch = FetchType.LAZY)
    private List<Tsgnomcabeceraht> tsgnomcabecerahtList;
    @JoinColumn(name = "cod_ejercicioid_fk", referencedColumnName = "cod_ejercicioid")
    @ManyToOne(optional = false)
    private Tsgnomejercicio codEjercicioidFk;

    /**
     *
     */
    public Tsgnomquincena() {
    }

    /**
     *
     * @param codQuincenaid
     */
    public Tsgnomquincena(Integer codQuincenaid) {
        this.codQuincenaid = codQuincenaid;
    }

    /**
     *
     * @param codQuincenaid
     * @param desQuincena
     * @param fecInicio
     * @param fecFin
     * @param fecPago
     * @param fecDispersion
     * @param cnuNumquincena
     * @param bolEstatus
     */
    public Tsgnomquincena(Integer codQuincenaid, String desQuincena, Date fecInicio, Date fecFin, Date fecPago, Date fecDispersion, int cnuNumquincena, boolean bolEstatus) {
        this.codQuincenaid = codQuincenaid;
        this.desQuincena = desQuincena;
        this.fecInicio = fecInicio;
        this.fecFin = fecFin;
        this.fecPago = fecPago;
        this.fecDispersion = fecDispersion;
        this.cnuNumquincena = cnuNumquincena;
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    public Integer getCodQuincenaid() {
        return codQuincenaid;
    }

    /**
     *
     * @param codQuincenaid
     */
    public void setCodQuincenaid(Integer codQuincenaid) {
        this.codQuincenaid = codQuincenaid;
    }

    /**
     *
     * @return
     */
    public String getDesQuincena() {
        return desQuincena;
    }

    /**
     *
     * @param desQuincena
     */
    public void setDesQuincena(String desQuincena) {
        this.desQuincena = desQuincena;
    }

    /**
     *
     * @return
     */
    public Date getFecInicio() {
        return fecInicio;
    }

    /**
     *
     * @param fecInicio
     */
    public void setFecInicio(Date fecInicio) {
        this.fecInicio = fecInicio;
    }

    /**
     *
     * @return
     */
    public Date getFecFin() {
        return fecFin;
    }

    /**
     *
     * @param fecFin
     */
    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    /**
     *
     * @return
     */
    public Date getFecPago() {
        return fecPago;
    }

    /**
     *
     * @param fecPago
     */
    public void setFecPago(Date fecPago) {
        this.fecPago = fecPago;
    }

    /**
     *
     * @return
     */
    public Date getFecDispersion() {
        return fecDispersion;
    }

    /**
     *
     * @param fecDispersion
     */
    public void setFecDispersion(Date fecDispersion) {
        this.fecDispersion = fecDispersion;
    }

    /**
     *
     * @return
     */
    public int getCnuNumquincena() {
        return cnuNumquincena;
    }

    /**
     *
     * @param cnuNumquincena
     */
    public void setCnuNumquincena(int cnuNumquincena) {
        this.cnuNumquincena = cnuNumquincena;
    }

    /**
     *
     * @return
     */
    public boolean getBolEstatus() {
        return bolEstatus;
    }

    /**
     *
     * @param bolEstatus
     */
    public void setBolEstatus(boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

//    @XmlTransient @JsonIgnore
//    public List<Tsgnomincidencia> getTsgnomincidenciaList() {
//        return tsgnomincidenciaList;
//    }
//
//    public void setTsgnomincidenciaList(List<Tsgnomincidencia> tsgnomincidenciaList) {
//        this.tsgnomincidenciaList = tsgnomincidenciaList;
//    }
    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgnommanterceros> getTsgnommantercerosList() {
        return tsgnommantercerosList;
    }

    /**
     *
     * @param tsgnommantercerosList
     */
    public void setTsgnommantercerosList(List<Tsgnommanterceros> tsgnommantercerosList) {
        this.tsgnommantercerosList = tsgnommantercerosList;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgnommanterceros> getTsgnommantercerosList1() {
        return tsgnommantercerosList1;
    }

    /**
     *
     * @param tsgnommantercerosList1
     */
    public void setTsgnommantercerosList1(List<Tsgnommanterceros> tsgnommantercerosList1) {
        this.tsgnommantercerosList1 = tsgnommantercerosList1;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgnomcabecera> getTsgnomcabeceraList() {
        return tsgnomcabeceraList;
    }

    /**
     *
     * @param tsgnomcabeceraList
     */
    public void setTsgnomcabeceraList(List<Tsgnomcabecera> tsgnomcabeceraList) {
        this.tsgnomcabeceraList = tsgnomcabeceraList;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    @JsonIgnore
    public List<Tsgnomcabeceraht> getTsgnomcabecerahtList() {
        return tsgnomcabecerahtList;
    }

    /**
     *
     * @param tsgnomcabecerahtList
     */
    public void setTsgnomcabecerahtList(List<Tsgnomcabeceraht> tsgnomcabecerahtList) {
        this.tsgnomcabecerahtList = tsgnomcabecerahtList;
    }

    /**
     *
     * @return
     */
    public Tsgnomejercicio getCodEjercicioidFk() {
        return codEjercicioidFk;
    }

    /**
     *
     * @param codEjercicioidFk
     */
    public void setCodEjercicioidFk(Tsgnomejercicio codEjercicioidFk) {
        this.codEjercicioidFk = codEjercicioidFk;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codQuincenaid != null ? codQuincenaid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomquincena)) {
            return false;
        }
        Tsgnomquincena other = (Tsgnomquincena) object;
        if ((this.codQuincenaid == null && other.codQuincenaid != null) || (this.codQuincenaid != null && !this.codQuincenaid.equals(other.codQuincenaid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomquincena[ codQuincenaid=" + codQuincenaid + " ]";
    }

}
