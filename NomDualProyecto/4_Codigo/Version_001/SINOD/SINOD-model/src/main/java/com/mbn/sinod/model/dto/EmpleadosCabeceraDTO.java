package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author mariana
 */
public class EmpleadosCabeceraDTO extends GenericDTO {

    private String nom_empleado;
    private String rol;
    private String area;
    private Integer cod_empleado;
    private Integer cod_empquincenaid;
    private List<EmpleadosCabeceraDTO> listaEmpleadosCabecera;

    /**
     *
     * @return
     */
    public String getNom_empleado() {
        return nom_empleado;
    }

    /**
     *
     * @param nom_empleado
     */
    public void setNom_empleado(String nom_empleado) {
        this.nom_empleado = nom_empleado;
    }

    /**
     *
     * @return
     */
    public String getRol() {
        return rol;
    }

    /**
     *
     * @param rol
     */
    public void setRol(String rol) {
        this.rol = rol;
    }

    /**
     *
     * @return
     */
    public String getArea() {
        return area;
    }

    /**
     *
     * @param area
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     *
     * @return
     */
    public Integer getCod_empleado() {
        return cod_empleado;
    }

    /**
     *
     * @param cod_empleado
     */
    public void setCod_empleado(Integer cod_empleado) {
        this.cod_empleado = cod_empleado;
    }

    /**
     *
     * @return
     */
    public Integer getCod_empquincenaid() {
        return cod_empquincenaid;
    }

    /**
     *
     * @param cod_empquincenaid
     */
    public void setCod_empquincenaid(Integer cod_empquincenaid) {
        this.cod_empquincenaid = cod_empquincenaid;
    }

    /**
     *
     * @return
     */
    public List<EmpleadosCabeceraDTO> getListaEmpleadosCabecera() {
        return listaEmpleadosCabecera;
    }

    /**
     *
     * @param listaEmpleadosCabecera
     */
    public void setListaEmpleadosCabecera(List<EmpleadosCabeceraDTO> listaEmpleadosCabecera) {
        this.listaEmpleadosCabecera = listaEmpleadosCabecera;
    }

}
