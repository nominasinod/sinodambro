package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomejercicio;
import java.util.List;

/**
 *
 * @author mipe
 */
public interface EjercicioFiscalDAO extends GenericDAO<Tsgnomejercicio, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomejercicio> listaEjercicios();
}
