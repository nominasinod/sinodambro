package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.DetalleDesgloseDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import java.util.List;

/**
 * Interfaz DAO para la función detalle_desglose_deduccion dentro del esquema
 * SGNOM de la BD Suite.
 *
 * Esta interfaz contiene el metodo conceptosPorEmp del modulo de Desglose de
 * Nómina y consulta todas las deducciones de los trabajadores dependiendo de la
 * quincena seleccionada y del numero de control del trabajador.
 *
 * @author Eduardo Torres C. MBN
 */
public interface DetalleDesgloseDAO extends GenericDAO<Tsgnomcncptoquinc, Integer> {

    /**
     * Metodo que retorna una lista con los conceptos de deduccion del
     * trabajador en la quincena
     *
     * @param cod_empleado
     * @param cod_cabecera
     * @return retorna una lista de las percepsiones del trabajador, su nombre,
     * clave, cantidad y importe.
     */
    List<DetalleDesgloseDTO> conceptosPorEmp(Integer cod_empleado, Integer cod_cabecera);

}
