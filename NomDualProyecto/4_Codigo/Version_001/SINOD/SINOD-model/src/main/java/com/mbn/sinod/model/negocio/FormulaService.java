package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.FormulaDTO;
import com.mbn.sinod.model.entidades.Tsgnomformula;

/**
 *
 * @author ambrosio
 */
public interface FormulaService extends BaseService<Tsgnomformula, Integer> {

    /**
     *
     * @param argumento
     * @return
     */
    FormulaDTO guardar(FormulaDTO argumento);

    /**
     *
     * @return
     */
    FormulaDTO listarFormulas();
}
