package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.ValidacionesNomBajasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface ValidacionesNomBajasDAO extends GenericDAO<Tsgnomempleados, Integer> {

    /**
     * Obtiene la lista de empleados dados de baja y validadados por RH
     * en el sistema de nómina, por medio de una función
     * 
     * @return
     */
    List<ValidacionesNomBajasDTO> detallesValidacionesNominaBajas();

}
