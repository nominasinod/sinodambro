package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomconcepto;
import java.util.List;

/**
 *
 * @author ambrosio
 */
public interface ConceptoDAO extends GenericDAO<Tsgnomconcepto, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomconcepto> listarConcepto();

    /**
     *
     * @return
     */
    List<Tsgnomconcepto> listarConceptosPercepcion();

    /**
     *
     * @return
     */
    List<Tsgnomconcepto> listarConceptosDeduccion();

    /**
     *
     * @param id
     * @return
     */
    boolean eliminarConcepto(Integer id);

    /**
     *
     * @param concepto
     * @return
     */
    boolean guardarConcepto(Tsgnomconcepto concepto);

    /**
     *
     * @param concepto
     * @return
     */
    boolean guardarPrioridad(List<Tsgnomconcepto> concepto);

}
