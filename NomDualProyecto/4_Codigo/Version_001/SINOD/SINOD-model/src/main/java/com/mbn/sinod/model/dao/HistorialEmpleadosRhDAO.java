package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.HistorialEmpleadosRhDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface HistorialEmpleadosRhDAO extends GenericDAO<Tsgrhempleados, Integer> {

    /**
     * Obtiene información personal del empleado en la nómina
     * por medio de una función
     * 
     * @return
     */
    List<HistorialEmpleadosRhDTO> detallesHistorialRh();

    /**
     * Obtiene el historial de las quincenas por empleado en la nómina
     * por medio de una función
     * 
     * @param cod_empleadoid
     * @return
     */
    List<HistorialEmpleadosRhDTO> informacionEmpleado(Integer cod_empleadoid);

}
