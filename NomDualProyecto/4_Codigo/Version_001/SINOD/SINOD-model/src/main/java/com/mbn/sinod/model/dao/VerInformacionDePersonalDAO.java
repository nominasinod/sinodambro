package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.VerInformacionDePersonalDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface VerInformacionDePersonalDAO extends GenericDAO<Tsgrhempleados, Integer> {

    /**
     * Obtiene la información personal por empleado de Nómina
     * por medio del id de Nómina
     * 
     * @param cod_empleadoid
     * @return
     */
    List<VerInformacionDePersonalDTO> informacionPorPersonal(Integer cod_empleadoid);

}
