package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.TipoNominaDTO;
import com.mbn.sinod.model.entidades.Tsgnomtiponomina;

/**
 *
 * @author ambrosio
 */
public interface TipoNominaService extends BaseService<Tsgnomtiponomina, Integer> {

    /**
     *
     * @return
     */
    TipoNominaDTO listaTipoNomina();
}
