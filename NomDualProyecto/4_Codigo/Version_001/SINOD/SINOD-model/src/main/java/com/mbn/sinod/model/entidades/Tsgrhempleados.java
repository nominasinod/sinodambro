package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author José Antonio Zarco Robles, MBN
 */
@Entity
@Table(name = "tsgrhempleados", schema = "sgrh")
@XmlRootElement
public class Tsgrhempleados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_empleado")
    private Integer codEmpleado;
    @Basic(optional = false)
    @Column(name = "des_nombre")
    private String desNombre;
    @Column(name = "des_nombres")
    private String desNombres;
    @Basic(optional = false)
    @Column(name = "des_apepaterno")
    private String desApepaterno;
    @Column(name = "des_apematerno")
    private String desApematerno;
    @Column(name = "des_correo")
    private String desCorreo;
    @JoinColumn(name = "cod_puesto", referencedColumnName = "cod_puesto")
    @ManyToOne(optional = false)
    private Tsgrhpuestos codPuesto;
    @Basic(optional = false)
    @Column(name = "des_direccion")
    private String desDireccion;
    @Basic(optional = false)
    @Column(name = "fec_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fecNacimiento;
    @Basic(optional = false)
    @Column(name = "des_lugarnacimiento")
    private String desLugarnacimiento;
    @Basic(optional = false)
    @Column(name = "cod_edad")
    private int codEdad;
    @Basic(optional = false)
    @Column(name = "cod_tiposangre")
    private String codTiposangre;
    @Column(name = "cod_telefonocasa")
    private String codTelefonocasa;
    @Column(name = "cod_telefonocelular")
    private String codTelefonocelular;
    @Column(name = "cod_telemergencia")
    private String codTelemergencia;
    @Lob
    @Column(name = "bin_identificacion")
    private byte[] binIdentificacion;
    @Lob
    @Column(name = "bin_pasaporte")
    private byte[] binPasaporte;
    @Lob
    @Column(name = "bin_visa")
    private byte[] binVisa;
    @Column(name = "cod_licenciamanejo")
    private String codLicenciamanejo;
    @Basic(optional = false)
    @Column(name = "fec_ingreso")
    @Temporal(TemporalType.DATE)
    private Date fecIngreso;
    @Column(name = "cod_rfc")
    private String codRfc;
    @Column(name = "cod_nss")
    private String codNss;
    @Basic(optional = false)
    @Column(name = "cod_curp")
    private String codCurp;
    @Lob
    @Column(name = "bin_foto")
    private byte[] binFoto;
    @Column(name = "cod_tipofoto")
    private String codTipofoto;
    @Column(name = "cod_extensionfoto")
    private String codExtensionfoto;
    @Column(name = "cod_empleadoactivo")
    private Boolean codEmpleadoactivo;
    @Basic(optional = false)
    @Column(name = "cod_estatusempleado")
    private int codEstatusempleado;
    @Basic(optional = false)
    @Column(name = "cod_estadocivil")
    private int codEstadocivil;
    @JoinColumn(name = "cod_rol", referencedColumnName = "cod_rol")
    @ManyToOne(optional = false)
    private Tsgrhroles codRol;
    @Column(name = "cod_diasvacaciones")
    private Integer codDiasvacaciones;
    @Column(name = "cod_sistemasuite")
    private Integer codSistemasuite;
    @Basic(optional = false)
    @Column(name = "fec_creacion")
    @Temporal(TemporalType.DATE)
    private Date fecCreacion;
    @Basic(optional = false)
    @Column(name = "fec_modificacion")
    @Temporal(TemporalType.DATE)
    private Date fecModificacion;
    @Column(name = "des_correopersonal")
    private String desCorreopersonal;
//    @OneToMany(mappedBy = "codCreadopor")
//    private List<Tsgrhempleados> tsgrhempleadosList;
//    @JoinColumn(name = "cod_creadopor", referencedColumnName = "cod_empleado")
//    @ManyToOne
//    private Tsgrhempleados codCreadopor;
//    @OneToMany(mappedBy = "codModificadopor")
//    private List<Tsgrhempleados> tsgrhempleadosList1;
//    @JoinColumn(name = "cod_modificadopor", referencedColumnName = "cod_empleado")
//    @ManyToOne
//    private Tsgrhempleados codModificadopor;

    /**
     *
     */
    public Tsgrhempleados() {
    }

    /**
     *
     * @param codEmpleado
     */
    public Tsgrhempleados(Integer codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    /**
     *
     * @param codEmpleado
     * @param desNombre
     * @param desApepaterno
     * @param desDireccion
     * @param fecNacimiento
     * @param desLugarnacimiento
     * @param codTiposangre
     * @param fecIngreso
     * @param codCurp
     * @param binFoto
     * @param codTipofoto
     * @param codExtensionfoto
     * @param codEstatusempleado
     * @param codEstadocivil
     * @param codRol
     * @param codDiasvacaciones
     * @param codSistemasuite
     * @param fecCreacion
     * @param fecModificacion
     * @param codCreadopor
     * @param codModificadopor
     * @param desCorreo
     */
    public Tsgrhempleados(Integer codEmpleado, String desNombre, String desApepaterno, String desDireccion, Date fecNacimiento, String desLugarnacimiento, String codTiposangre, Date fecIngreso, String codCurp, byte[] binFoto, String codTipofoto, String codExtensionfoto, int codEstatusempleado, int codEstadocivil, int codRol, int codDiasvacaciones, int codSistemasuite, Date fecCreacion, Date fecModificacion, int codCreadopor, int codModificadopor, String desCorreo) {
        this.codEmpleado = codEmpleado;
        this.desNombre = desNombre;
        this.desApepaterno = desApepaterno;
        this.desDireccion = desDireccion;
        this.fecNacimiento = fecNacimiento;
        this.desLugarnacimiento = desLugarnacimiento;
        this.codTiposangre = codTiposangre;
        this.fecIngreso = fecIngreso;
        this.codCurp = codCurp;
        this.binFoto = binFoto;
        this.codTipofoto = codTipofoto;
        this.codExtensionfoto = codExtensionfoto;
        this.codEstatusempleado = codEstatusempleado;
        this.codEstadocivil = codEstadocivil;
        //this.codRol = codRol;
        this.codDiasvacaciones = codDiasvacaciones;
        this.codSistemasuite = codSistemasuite;
        this.fecCreacion = fecCreacion;
//        this.fecModificacion = fecModificacion;
//        this.codCreadopor = codCreadopor;
//        this.codModificadopor = codModificadopor;
        this.desCorreo = desCorreo;
    }

    /**
     *
     * @return
     */
    public Integer getCodEmpleado() {
        return codEmpleado;
    }

    /**
     *
     * @param codEmpleado
     */
    public void setCodEmpleado(Integer codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    /**
     *
     * @return
     */
    public String getDesNombre() {
        return desNombre;
    }

    /**
     *
     * @param desNombre
     */
    public void setDesNombre(String desNombre) {
        this.desNombre = desNombre;
    }

    /**
     *
     * @return
     */
    public String getDesNombres() {
        return desNombres;
    }

    /**
     *
     * @param desNombres
     */
    public void setDesNombres(String desNombres) {
        this.desNombres = desNombres;
    }

    /**
     *
     * @return
     */
    public String getDesApepaterno() {
        return desApepaterno;
    }

    /**
     *
     * @param desApepaterno
     */
    public void setDesApepaterno(String desApepaterno) {
        this.desApepaterno = desApepaterno;
    }

    /**
     *
     * @return
     */
    public String getDesApematerno() {
        return desApematerno;
    }

    /**
     *
     * @param desApematerno
     */
    public void setDesApematerno(String desApematerno) {
        this.desApematerno = desApematerno;
    }

    /**
     *
     * @return
     */
    public byte[] getBinFoto() {
        return binFoto;
    }

    /**
     *
     * @param binFoto
     */
    public void setBinFoto(byte[] binFoto) {
        this.binFoto = binFoto;
    }

//    public int getCodCreadopor() {
//        return codCreadopor;
//    }
//
//    public void setCodCreadopor(int codCreadopor) {
//        this.codCreadopor = codCreadopor;
//    }
//
//    public int getCodModificadopor() {
//        return codModificadopor;
//    }
//
//    public void setCodModificadopor(int codModificadopor) {
//        this.codModificadopor = codModificadopor;
//    }
    /**
     *
     * @return
     */
    public String getDesCorreo() {
        return desCorreo;
    }

    /**
     *
     * @param desCorreo
     */
    public void setDesCorreo(String desCorreo) {
        this.desCorreo = desCorreo;
    }

    /**
     *
     * @return
     */
    public Tsgrhpuestos getCodPuesto() {
        return codPuesto;
    }

    /**
     *
     * @param codPuesto
     */
    public void setCodPuesto(Tsgrhpuestos codPuesto) {
        this.codPuesto = codPuesto;
    }

    /**
     *
     * @return
     */
    public String getDesDireccion() {
        return desDireccion;
    }

    /**
     *
     * @param desDireccion
     */
    public void setDesDireccion(String desDireccion) {
        this.desDireccion = desDireccion;
    }

    /**
     *
     * @return
     */
    public Date getFecNacimiento() {
        return fecNacimiento;
    }

    /**
     *
     * @param fecNacimiento
     */
    public void setFecNacimiento(Date fecNacimiento) {
        this.fecNacimiento = fecNacimiento;
    }

    /**
     *
     * @return
     */
    public String getDesLugarnacimiento() {
        return desLugarnacimiento;
    }

    /**
     *
     * @param desLugarnacimiento
     */
    public void setDesLugarnacimiento(String desLugarnacimiento) {
        this.desLugarnacimiento = desLugarnacimiento;
    }

    /**
     *
     * @return
     */
    public int getCodEdad() {
        return codEdad;
    }

    /**
     *
     * @param codEdad
     */
    public void setCodEdad(int codEdad) {
        this.codEdad = codEdad;
    }

    /**
     *
     * @return
     */
    public String getCodTiposangre() {
        return codTiposangre;
    }

    /**
     *
     * @param codTiposangre
     */
    public void setCodTiposangre(String codTiposangre) {
        this.codTiposangre = codTiposangre;
    }

    /**
     *
     * @return
     */
    public String getCodTelefonocasa() {
        return codTelefonocasa;
    }

    /**
     *
     * @param codTelefonocasa
     */
    public void setCodTelefonocasa(String codTelefonocasa) {
        this.codTelefonocasa = codTelefonocasa;
    }

    /**
     *
     * @return
     */
    public String getCodTelefonocelular() {
        return codTelefonocelular;
    }

    /**
     *
     * @param codTelefonocelular
     */
    public void setCodTelefonocelular(String codTelefonocelular) {
        this.codTelefonocelular = codTelefonocelular;
    }

    /**
     *
     * @return
     */
    public String getCodTelemergencia() {
        return codTelemergencia;
    }

    /**
     *
     * @param codTelemergencia
     */
    public void setCodTelemergencia(String codTelemergencia) {
        this.codTelemergencia = codTelemergencia;
    }

    /**
     *
     * @return
     */
    public byte[] getBinIdentificacion() {
        return binIdentificacion;
    }

    /**
     *
     * @param binIdentificacion
     */
    public void setBinIdentificacion(byte[] binIdentificacion) {
        this.binIdentificacion = binIdentificacion;
    }

    /**
     *
     * @return
     */
    public byte[] getBinPasaporte() {
        return binPasaporte;
    }

    /**
     *
     * @param binPasaporte
     */
    public void setBinPasaporte(byte[] binPasaporte) {
        this.binPasaporte = binPasaporte;
    }

    /**
     *
     * @return
     */
    public byte[] getBinVisa() {
        return binVisa;
    }

    /**
     *
     * @param binVisa
     */
    public void setBinVisa(byte[] binVisa) {
        this.binVisa = binVisa;
    }

    /**
     *
     * @return
     */
    public String getCodLicenciamanejo() {
        return codLicenciamanejo;
    }

    /**
     *
     * @param codLicenciamanejo
     */
    public void setCodLicenciamanejo(String codLicenciamanejo) {
        this.codLicenciamanejo = codLicenciamanejo;
    }

    /**
     *
     * @return
     */
    public Date getFecIngreso() {
        return fecIngreso;
    }

    /**
     *
     * @param fecIngreso
     */
    public void setFecIngreso(Date fecIngreso) {
        this.fecIngreso = fecIngreso;
    }

    /**
     *
     * @return
     */
    public String getCodRfc() {
        return codRfc;
    }

    /**
     *
     * @param codRfc
     */
    public void setCodRfc(String codRfc) {
        this.codRfc = codRfc;
    }

    /**
     *
     * @return
     */
    public String getCodNss() {
        return codNss;
    }

    /**
     *
     * @param codNss
     */
    public void setCodNss(String codNss) {
        this.codNss = codNss;
    }

    /**
     *
     * @return
     */
    public String getCodCurp() {
        return codCurp;
    }

    /**
     *
     * @param codCurp
     */
    public void setCodCurp(String codCurp) {
        this.codCurp = codCurp;
    }

//    public byte[] getBinFoto() {
//        return binFoto;
//    }
//
//    public void setBinFoto(byte[] binFoto) {
//        this.binFoto = binFoto;
//    }
    /**
     *
     * @return
     */
    public String getCodTipofoto() {
        return codTipofoto;
    }

    /**
     *
     * @param codTipofoto
     */
    public void setCodTipofoto(String codTipofoto) {
        this.codTipofoto = codTipofoto;
    }

    /**
     *
     * @return
     */
    public String getCodExtensionfoto() {
        return codExtensionfoto;
    }

    /**
     *
     * @param codExtensionfoto
     */
    public void setCodExtensionfoto(String codExtensionfoto) {
        this.codExtensionfoto = codExtensionfoto;
    }

    /**
     *
     * @return
     */
    public Boolean getCodEmpleadoactivo() {
        return codEmpleadoactivo;
    }

    /**
     *
     * @param codEmpleadoactivo
     */
    public void setCodEmpleadoactivo(Boolean codEmpleadoactivo) {
        this.codEmpleadoactivo = codEmpleadoactivo;
    }

    /**
     *
     * @return
     */
    public int getCodEstatusempleado() {
        return codEstatusempleado;
    }

    /**
     *
     * @param codEstatusempleado
     */
    public void setCodEstatusempleado(int codEstatusempleado) {
        this.codEstatusempleado = codEstatusempleado;
    }

    /**
     *
     * @return
     */
    public int getCodEstadocivil() {
        return codEstadocivil;
    }

    /**
     *
     * @param codEstadocivil
     */
    public void setCodEstadocivil(int codEstadocivil) {
        this.codEstadocivil = codEstadocivil;
    }

    /**
     *
     * @return
     */
    public Tsgrhroles getCodRol() {
        return codRol;
    }

    /**
     *
     * @param codRol
     */
    public void setCodRol(Tsgrhroles codRol) {
        this.codRol = codRol;
    }

    /**
     *
     * @return
     */
    public Integer getCodDiasvacaciones() {
        return codDiasvacaciones;
    }

    /**
     *
     * @param codDiasvacaciones
     */
    public void setCodDiasvacaciones(Integer codDiasvacaciones) {
        this.codDiasvacaciones = codDiasvacaciones;
    }

    /**
     *
     * @return
     */
    public Integer getCodSistemasuite() {
        return codSistemasuite;
    }

    /**
     *
     * @param codSistemasuite
     */
    public void setCodSistemasuite(Integer codSistemasuite) {
        this.codSistemasuite = codSistemasuite;
    }

    /**
     *
     * @return
     */
    public Date getFecCreacion() {
        return fecCreacion;
    }

    /**
     *
     * @param fecCreacion
     */
    public void setFecCreacion(Date fecCreacion) {
        this.fecCreacion = fecCreacion;
    }

    /**
     *
     * @return
     */
    public Date getFecModificacion() {
        return fecModificacion;
    }

    /**
     *
     * @param fecModificacion
     */
    public void setFecModificacion(Date fecModificacion) {
        this.fecModificacion = fecModificacion;
    }

    /**
     *
     * @return
     */
    public String getDesCorreopersonal() {
        return desCorreopersonal;
    }

    /**
     *
     * @param desCorreopersonal
     */
    public void setDesCorreopersonal(String desCorreopersonal) {
        this.desCorreopersonal = desCorreopersonal;
    }

//    @XmlTransient @JsonIgnore
//    public List<Tsgrhempleados> getTsgrhempleadosList() {
//        return tsgrhempleadosList;
//    }
//
//    public void setTsgrhempleadosList(List<Tsgrhempleados> tsgrhempleadosList) {
//        this.tsgrhempleadosList = tsgrhempleadosList;
//    }
//
//    public Tsgrhempleados getCodCreadopor() {
//        return codCreadopor;
//    }
//
//    public void setCodCreadopor(Tsgrhempleados codCreadopor) {
//        this.codCreadopor = codCreadopor;
//    }
//
//    @XmlTransient @JsonIgnore
//    public List<Tsgrhempleados> getTsgrhempleadosList1() {
//        return tsgrhempleadosList1;
//    }
//
//    public void setTsgrhempleadosList1(List<Tsgrhempleados> tsgrhempleadosList1) {
//        this.tsgrhempleadosList1 = tsgrhempleadosList1;
//    }
//
//    public Tsgrhempleados getCodModificadopor() {
//        return codModificadopor;
//    }
//
//    public void setCodModificadopor(Tsgrhempleados codModificadopor) {
//        this.codModificadopor = codModificadopor;
//    }
//    
    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEmpleado != null ? codEmpleado.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgrhempleados)) {
            return false;
        }
        Tsgrhempleados other = (Tsgrhempleados) object;
        if ((this.codEmpleado == null && other.codEmpleado != null) || (this.codEmpleado != null && !this.codEmpleado.equals(other.codEmpleado))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgrhempleados[ codEmpleado=" + codEmpleado + " ]";
    }
}
