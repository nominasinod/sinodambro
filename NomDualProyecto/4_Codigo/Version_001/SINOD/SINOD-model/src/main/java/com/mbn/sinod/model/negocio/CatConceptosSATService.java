package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.CatConceptosSATDTO;
import com.mbn.sinod.model.entidades.Tsgnomconceptosat;

/**
 *
 * @author ambrosio
 */
public interface CatConceptosSATService extends BaseService<Tsgnomconceptosat, Integer> {

    /**
     *
     * @return
     */
    CatConceptosSATDTO listarConceptosSAT();
}
