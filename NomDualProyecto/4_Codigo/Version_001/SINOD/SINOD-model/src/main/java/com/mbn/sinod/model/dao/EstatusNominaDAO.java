package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomestatusnom;
import java.util.List;

/**
 *
 * @author User
 */
public interface EstatusNominaDAO extends GenericDAO<Tsgnomestatusnom, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomestatusnom> listarEstatusNomina();
}
