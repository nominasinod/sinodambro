package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.DetalleEmpDTO;
import com.mbn.sinod.model.entidades.Tsgnomconfpago;
import java.util.List;

/**
 *
 * @author eduardotorres
 */
public interface ConfirmaEmpleadoDAO extends GenericDAO<Tsgnomconfpago, Integer> {

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    List<DetalleEmpDTO> ConfiPAgo(Integer cabecera, Integer empleado);
}
