package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.VerInformacionRhDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;

/**
 *
 * @author Ivette
 */
public interface VerInformacionRhService extends BaseService<Tsgnomempleados, Integer> {

    /**
     *
     * @param empleadoRh
     * @return
     */
    VerInformacionRhDTO listaInformacionRh(Integer empleadoRh);

}
