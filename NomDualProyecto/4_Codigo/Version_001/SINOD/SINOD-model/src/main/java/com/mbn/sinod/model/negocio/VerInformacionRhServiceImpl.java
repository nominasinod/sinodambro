package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.VerInformacionRhDAO;
import com.mbn.sinod.model.dto.VerInformacionRhDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Ivette
 */
public class VerInformacionRhServiceImpl extends BaseServiceImpl<Tsgnomempleados, Integer>
        implements VerInformacionRhService {

    @Autowired
    private VerInformacionRhDAO verInformacionRhDAO;

    /**
     *
     * @return
     */
    public VerInformacionRhDAO getVerInformacionRhDAO() {
        return verInformacionRhDAO;
    }

    /**
     *
     * @param verInformacionRhDAO
     */
    public void setVerInformacionRhDAO(VerInformacionRhDAO verInformacionRhDAO) {
        this.verInformacionRhDAO = verInformacionRhDAO;
    }

    /**
     *
     * @param empleadoRh
     * @return
     */
    @Override
    public VerInformacionRhDTO listaInformacionRh(Integer empleadoRh) {
        VerInformacionRhDTO dto = new VerInformacionRhDTO();
        dto.setListaInformacionRh(getVerInformacionRhDAO().listaInformacionRh(empleadoRh));
        return dto;
    }

}
