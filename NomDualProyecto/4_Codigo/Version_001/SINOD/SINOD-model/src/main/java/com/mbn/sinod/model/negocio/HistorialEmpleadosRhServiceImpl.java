package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.HistorialEmpleadosRhDAO;
import com.mbn.sinod.model.dto.InfoHistorialRhDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Ivette
 */
public class HistorialEmpleadosRhServiceImpl extends BaseServiceImpl<Tsgrhempleados, Integer>
        implements HistorialEmpleadosRhService {

    @Autowired
    private HistorialEmpleadosRhDAO historialEmpleadosRhDAO;

    /**
     *
     * @return
     */
    public HistorialEmpleadosRhDAO getHistorialEmpleadosRhDAO() {
        return historialEmpleadosRhDAO;
    }

    /**
     *
     * @param historialEmpleadosRhDAO
     */
    public void setHistorialEmpleadosRhDAO(HistorialEmpleadosRhDAO historialEmpleadosRhDAO) {
        this.historialEmpleadosRhDAO = historialEmpleadosRhDAO;
    }

    /**
     *
     * @return
     */
    @Override
    public InfoHistorialRhDTO detallesHistorialRh() {

        InfoHistorialRhDTO dto = new InfoHistorialRhDTO();
        dto.setListaHistorialEmpleadosRh(getHistorialEmpleadosRhDAO().detallesHistorialRh());
        return dto;
    }

    /**
     *
     * @param cod_empleadoid
     * @return
     */
    @Override
    public InfoHistorialRhDTO informacionPersonal(Integer cod_empleadoid) {

        InfoHistorialRhDTO dto = new InfoHistorialRhDTO();
        dto.setListaHistorialEmpleadosRh(getHistorialEmpleadosRhDAO().informacionEmpleado(cod_empleadoid));
        return dto;
    }

}
