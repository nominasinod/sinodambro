package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.DetalleEmpDTO;
import com.mbn.sinod.model.entidades.Tsgnomconfpago;
import java.util.List;

/**
 * Interfaz DAO que declara TODOS los métodos de autorizacion y rechazo de pagos
 * a la BD SUITE en el esquema SGNOM.
 *
 * Esta interfaz contiene todos los metodos del modulo de CONFIRMACION DE PAGOS
 * en especifico para la autorizacion y rechazo de los pagos de los
 * departamentos de recursos humanod y financieros.
 *
 * @author Eduardo Torres C. MBN
 */
public interface ConfPagoDAO extends GenericDAO<Tsgnomconfpago, Integer> {

    /**
     * Metodo de autorizacion de pago del area de Recursos Financieros
     *
     * @param conf1
     * @return
     */
    boolean guardarAutorizacionRF(List<Tsgnomconfpago> conf1);

    /**
     * Metodo de autorizacion de pagos del area de Recursos Humanos
     *
     * @param conf2
     * @return
     */
    boolean guardarAutorizacionRH(List<Tsgnomconfpago> conf2);

    /**
     * Metodo de autorizacion de pagos del empleado
     *
     * @param empquincenaFK
     * @param conf3
     * @return
     */
    boolean guardarAutorizacionEMP(Integer empquincenaFK, Boolean conf3);

    /**
     * Metodo para autorizar todos los pagos de Recursos Humanos
     *
     * @param cabecera
     * @return
     */
    boolean validarTodas(Integer cabecera);

    /**
     * Metodo para rechazar todos los pagos de Recursos Humanos
     *
     * @param cabecera
     * @return
     */
    boolean validarTodasRH(Integer cabecera);

    /**
     * Metodo para rechazar todos los pagos de Recursos Financieros
     *
     * @param cabecera
     * @return
     */
    boolean rechazarTodasRF(Integer cabecera);

    /**
     * Metodo para autorizar todos los pagos de Recursos Financieros
     *
     * @param cabecera
     * @return
     */
    boolean rechazarTodasRH(Integer cabecera);
}
