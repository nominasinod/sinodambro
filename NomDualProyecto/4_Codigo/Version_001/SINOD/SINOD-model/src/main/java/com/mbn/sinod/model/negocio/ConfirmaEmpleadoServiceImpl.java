package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.ConfirmaEmpleadoDAO;
import com.mbn.sinod.model.dto.EmpDTO;
import com.mbn.sinod.model.entidades.Tsgnomconfpago;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author mipe
 */
public class ConfirmaEmpleadoServiceImpl extends BaseServiceImpl<Tsgnomconfpago, Integer>
        implements ConfirmaEmpleadoService {

    @Autowired
    private ConfirmaEmpleadoDAO confirmaEmpleadoDAO;

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    @Override
    public EmpDTO empDTO(Integer cabecera, Integer empleado) {
        EmpDTO dto = new EmpDTO();
        dto.setListaEmp(getConfirmaEmpleadoDAO().ConfiPAgo(cabecera, empleado));
        //System.err.println("ServiceEMP");
        return dto;
    }
//  public DetalleDesgloPercepDTO detalleDesglocePercepDTO(Integer cod_empleado, Integer cod_cabecera) {
//    DetalleDesgloPercepDTO dto = new DetalleDesgloPercepDTO();
//        dto.setGetListaPercepcion(getDetalleDesglosePercepcionDAO().percepcionesPosEmp(cod_empleado, cod_cabecera));
//        return dto;

    /**
     * @return the confirmaEmpleadoDAO
     */
    public ConfirmaEmpleadoDAO getConfirmaEmpleadoDAO() {
        return confirmaEmpleadoDAO;
    }

    /**
     * @param confirmaEmpleadoDAO the confirmaEmpleadoDAO to set
     */
    public void setConfirmaEmpleadoDAO(ConfirmaEmpleadoDAO confirmaEmpleadoDAO) {
        this.confirmaEmpleadoDAO = confirmaEmpleadoDAO;
    }
}
