package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Ivette
 */
public class InformacionPorPersonalDTO extends GenericDTO {

    private List<VerInformacionDePersonalDTO> listaInformacionPorPersonal;

    /**
     * 
     * @return the listaInformacionPorPersonal
     */
    public List<VerInformacionDePersonalDTO> getListaInformacionPorPersonal() {
        return listaInformacionPorPersonal;
    }

    /**
     *
     * @param listaInformacionPorPersonal the listaInformacionPorPersonal to set
     */
    public void setListaInformacionPorPersonal(List<VerInformacionDePersonalDTO> listaInformacionPorPersonal) {
        this.listaInformacionPorPersonal = listaInformacionPorPersonal;
    }

}
