package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.CatClasificadorDTO;
import com.mbn.sinod.model.entidades.Tsgnomclasificador;

/**
 *
 * @author ambrosio
 */
public interface CatClasificadorService extends BaseService<Tsgnomclasificador, Integer> {

    /**
     *
     * @return
     */
    CatClasificadorDTO listarCatClasificador();
}
