package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import java.util.List;

/**
 *
 * @author mipe
 */
public interface EmpQuincenaDAO extends GenericDAO<Tsgnomempquincena, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomempquincena> listarEmpQuincena();

}
