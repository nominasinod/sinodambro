package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.DesgloseEmpleadoDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface EmpleadosNomDAO extends GenericDAO<Tsgnomempleados, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomempleados> listarEmpleadosNom();

    /**
     * Obtiene el objeto/entidad de Tsgnomempleados por medio del id de RH
     *
     * @param idempleadorh id del empleado en RH (empleado logueado)
     * @return
     */
    Tsgnomempleados obtenerEmpleadonomPorIdrh(Integer idempleadorh);

    /**
     * Ayuda a editar / guardar un empleado en el esquema de SGNOM 
     * por medio de la entidad 
     * 
     * @param empleado
     * @return
     */
    boolean guardarActualizarEmpleados(Tsgnomempleados empleado);

    /**
     *Obtiene el objeto/entidad de Tsgnomempleados por medio del id de nómina
     * 
     * @param idempleadonom
     * @return
     */
    Tsgnomempleados obtenerEmpleadosnomPorIdNom(Integer idempleadonom);

    /**
     * Obtiene información de una tabla de Tsgnomempleados basada en un XML,
     * por medio de una función que tiene como prámetro el id del empleado de la nómina
     * 
     * @param idempleadonom
     * @return
     */
    List<DesgloseEmpleadoDTO> obtenerEmpleadosnomPorIdNomHT(Integer idempleadonom);

    /**
     *Obtiene el objeto/entidad de Tsgnomempleados por medio del id de RH
     * 
     * @param empleadoRh
     * @return
     */
    List<Tsgnomempleados> detallesEmpleadosPorIdRH(Integer empleadoRh);
}
