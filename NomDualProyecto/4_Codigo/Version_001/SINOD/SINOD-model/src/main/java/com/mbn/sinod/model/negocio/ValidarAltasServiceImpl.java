package com.mbn.sinod.model.negocio;

import com.ibm.wsdl.ServiceImpl;
import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.ValidarAltasDAO;
import com.mbn.sinod.model.dto.ValidacionAltasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mariana
 */
public class ValidarAltasServiceImpl extends BaseServiceImpl<Tsgnomempleados, Integer>
        implements ValidarAltasService {

    private static final Logger logger = Logger.getLogger(ServiceImpl.class.getName());

    /**
     *
     * @return
     */
    @Override
    public ValidacionAltasDTO listarAltasAValidar() {
        ValidacionAltasDTO respuesta = new ValidacionAltasDTO();
        try {

            List<ValidacionAltasDTO> listaIncidencias = ((ValidarAltasDAO) getGenericDAO()).listarAltasAValidar();

            if (listaIncidencias != null) {
                respuesta.setListaInformacionValidar(listaIncidencias);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_ALTAS_A_VALIDAR);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);

            } else {
                respuesta.setListaInformacionValidar(listaIncidencias);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_ALTAS_A_VALIDAR);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ValidacionAltasDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(IncidenciasQuincenaServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param altas
     * @return
     */
    @Transactional
    @Override
    public ValidacionAltasDTO validarCadaAlta(ValidacionAltasDTO altas) {
        ValidacionAltasDTO respuesta = new ValidacionAltasDTO();
        try {
            boolean respuestaGuardar
                    = ((ValidarAltasDAO) getGenericDAO()).validarCadaAlta(altas.getListaInformacionValidar());

            if (respuestaGuardar) {
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_VALIDAR_CADA_ALTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);

                respuesta.setListaInformacionValidar(altas.getListaInformacionValidar());

            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_VALIDAR_CADA_ALTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ValidacionAltasDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomempleados.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param altas
     * @return
     */
    @Transactional
    @Override
    public ValidacionAltasDTO validarAltasAceptadas(ValidacionAltasDTO altas) {
        ValidacionAltasDTO respuesta = new ValidacionAltasDTO();
        try {
            boolean respuestaGuardar
                    = ((ValidarAltasDAO) getGenericDAO()).validarAltasAceptadas(altas.getListaInformacionValidar());

            if (respuestaGuardar) {
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_VALIDAR_ALTAS_ACEPTADAS);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);

                respuesta.setListaInformacionValidar(altas.getListaInformacionValidar());

            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_VALIDAR_ALTAS_ACEPTADAS);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ValidacionAltasDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomempleados.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param altas
     * @return
     */
    @Transactional
    @Override
    public ValidacionAltasDTO validarAltasRechazadas(ValidacionAltasDTO altas) {
        ValidacionAltasDTO respuesta = new ValidacionAltasDTO();
        try {
            boolean respuestaGuardar
                    = ((ValidarAltasDAO) getGenericDAO()).validarAltasRechazadas(altas.getListaInformacionValidar());

            if (respuestaGuardar) {
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_VALIDAR_ALTAS_RECHAZADAS);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);

                respuesta.setListaInformacionValidar(altas.getListaInformacionValidar());

            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_VALIDAR_ALTAS_RECHAZADAS);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ValidacionAltasDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomempleados.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

}
