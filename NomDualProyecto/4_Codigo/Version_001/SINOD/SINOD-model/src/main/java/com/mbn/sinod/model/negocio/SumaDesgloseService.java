package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.SumaDesgloseDTO;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;

/**
 *
 * @author torre
 */
public interface SumaDesgloseService extends BaseService<Tsgnomempquincena, Integer> {

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    SumaDesgloseDTO sumaDesglose(Integer cabecera, Integer empleado);

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    SumaDesgloseDTO sumaDesgloseht(Integer cabecera, Integer empleado);
}
