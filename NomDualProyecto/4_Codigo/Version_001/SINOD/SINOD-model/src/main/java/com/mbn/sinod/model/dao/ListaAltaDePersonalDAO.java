package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.ListaAltaDePersonalDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface ListaAltaDePersonalDAO extends GenericDAO<Tsgrhempleados, Integer> {

    /**
     * Obtiene una lista de los empleados que aún no se dan de alta en el sistema de nómina
     * pero que están dados de alta en el sistema de RH por medio de una función
     * 
     * @return
     */
    List<ListaAltaDePersonalDTO> listaInformacionParaAltaNom();

}
