package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomtipoconcepto;
import java.util.List;

/**
 *
 * @author mipe
 */
public class TipoConceptoDAOImpl extends GenericDAOImpl<Tsgnomtipoconcepto, Integer> implements TipoConceptoDAO {

    @Override
    public List<Tsgnomtipoconcepto> listarTiposConceptos() {
        return findAll();
    }

}
