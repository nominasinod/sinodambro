package com.mbn.sinod.model.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author mariana
 */
public class IncidenciasQuincenaDTO extends GenericDTO {

    private Integer idincidencia;
    private String clave;
    private String incidencia;
    private Character idtipo;
    private String desctipo;
    private BigDecimal cantidad;
    private String actividad;
    private String detallefechas;
    private String comentarios;
    private BigDecimal importe;
    private Integer reporta;
    private Integer autoriza;
    private String nombre;
    private String perfil;
    private Boolean validacion;
    private Boolean aceptacion;
    private Boolean Correo;
    private Integer modifica;
    private Boolean autpago;
    private Integer quincenaid;
    private String desquincena;

    private List<IncidenciasQuincenaDTO> listaIncidencias;

    /**
     *
     * @return
     */
    public List<IncidenciasQuincenaDTO> getListaIncidencias() {
        return listaIncidencias;
    }

    /**
     *
     * @param listaIncidencias
     */
    public void setListaIncidencias(List<IncidenciasQuincenaDTO> listaIncidencias) {
        this.listaIncidencias = listaIncidencias;
    }

    /**
     *
     * @return
     */
    public Integer getIdincidencia() {
        return idincidencia;
    }

    /**
     *
     * @param idincidencia
     */
    public void setIdincidencia(Integer idincidencia) {
        this.idincidencia = idincidencia;
    }

    /**
     *
     * @return
     */
    public String getClave() {
        return clave;
    }

    /**
     *
     * @param clave
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     *
     * @return
     */
    public String getIncidencia() {
        return incidencia;
    }

    /**
     *
     * @param incidencia
     */
    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    /**
     *
     * @return
     */
    public Character getIdtipo() {
        return idtipo;
    }

    /**
     *
     * @param idtipo
     */
    public void setIdtipo(Character idtipo) {
        this.idtipo = idtipo;
    }

    /**
     *
     * @return
     */
    public BigDecimal getCantidad() {
        return cantidad;
    }

    /**
     *
     * @param cantidad
     */
    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    /**
     *
     * @return
     */
    public String getActividad() {
        return actividad;
    }

    /**
     *
     * @param actividad
     */
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    /**
     *
     * @return
     */
    public String getComentarios() {
        return comentarios;
    }

    /**
     *
     * @param comentarios
     */
    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImporte() {
        return importe;
    }

    /**
     *
     * @param importe
     */
    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    /**
     *
     * @return
     */
    public Integer getReporta() {
        return reporta;
    }

    /**
     *
     * @param reporta
     */
    public void setReporta(Integer reporta) {
        this.reporta = reporta;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getPerfil() {
        return perfil;
    }

    /**
     *
     * @param perfil
     */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    /**
     *
     * @return
     */
    public Boolean getCorreo() {
        return Correo;
    }

    /**
     *
     * @param Correo
     */
    public void setCorreo(Boolean Correo) {
        this.Correo = Correo;
    }

    /**
     *
     * @return
     */
    public String getDesctipo() {
        return desctipo;
    }

    /**
     *
     * @param desctipo
     */
    public void setDesctipo(String desctipo) {
        this.desctipo = desctipo;
    }

    /**
     *
     * @return
     */
    public String getDetallefechas() {
        return detallefechas;
    }

    /**
     *
     * @param detallefechas
     */
    public void setDetallefechas(String detallefechas) {
        this.detallefechas = detallefechas;
    }

    /**
     *
     * @return
     */
    public Boolean getValidacion() {
        return validacion;
    }

    /**
     *
     * @param validacion
     */
    public void setValidacion(Boolean validacion) {
        this.validacion = validacion;
    }

    /**
     *
     * @return
     */
    public Boolean getAceptacion() {
        return aceptacion;
    }

    /**
     *
     * @param aceptacion
     */
    public void setAceptacion(Boolean aceptacion) {
        this.aceptacion = aceptacion;
    }

    /**
     *
     * @return
     */
    public Integer getAutoriza() {
        return autoriza;
    }

    /**
     *
     * @param autoriza
     */
    public void setAutoriza(Integer autoriza) {
        this.autoriza = autoriza;
    }

    /**
     *
     * @return
     */
    public Integer getModifica() {
        return modifica;
    }

    /**
     *
     * @param modifica
     */
    public void setModifica(Integer modifica) {
        this.modifica = modifica;
    }

    /**
     *
     * @return
     */
    public Boolean getAutpago() {
        return autpago;
    }

    /**
     *
     * @param autpago
     */
    public void setAutpago(Boolean autpago) {
        this.autpago = autpago;
    }

    /**
     *
     * @return
     */
    public Integer getQuincenaid() {
        return quincenaid;
    }

    /**
     *
     * @param quincenaid
     */
    public void setQuincenaid(Integer quincenaid) {
        this.quincenaid = quincenaid;
    }

    /**
     *
     * @return
     */
    public String getDesquincena() {
        return desquincena;
    }

    /**
     *
     * @param desquincena
     */
    public void setDesquincena(String desquincena) {
        this.desquincena = desquincena;
    }

}
