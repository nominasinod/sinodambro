package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.InformacionEmpleadosDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;

/**
 *
 * @author Ivette
 */
public interface DetalleEmpleadosService extends BaseService<Tsgrhempleados, Integer> {

    /**
     *
     * @return
     */
    InformacionEmpleadosDTO detallesEmpleados();

    /**
     * Obtiene el detalle de los empleado en nómina de acuerdo al área/célula de
     * trabajo dado de alta en RH
     *
     * @param area id del área
     * @return
     */
    InformacionEmpleadosDTO empleadosPorArea(Integer area);
}
