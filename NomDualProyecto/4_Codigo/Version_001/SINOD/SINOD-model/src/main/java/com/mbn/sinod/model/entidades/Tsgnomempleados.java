package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author Ivette
 */
@Entity
@Table(name = "tsgnomempleados", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomempleados.findAll", query = "SELECT t FROM Tsgnomempleados t")})
public class Tsgnomempleados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "argId", strategy = "increment", parameters = {
        @Parameter(name = "schema", value = "sgnom")})
    @GeneratedValue(generator = "argId")
    @Basic(optional = false)
    @Column(name = "cod_empleadoid")
    private Integer codEmpleadoid;
    @Basic(optional = false)
    @Column(name = "fec_ingreso")
    @Temporal(TemporalType.DATE)
    private Date fecIngreso;
    @Column(name = "fec_salida")
    @Temporal(TemporalType.DATE)
    private Date fecSalida;
    @Basic(optional = false)
    @Column(name = "bol_estatus")
    private boolean bolEstatus;
    @Basic(optional = false)
    @Column(name = "cod_empleado_fk")
    private int codEmpleadoFk;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "imp_sueldoimss")
    private BigDecimal impSueldoimss;
    @Column(name = "imp_honorarios")
    private BigDecimal impHonorarios;
    @Column(name = "imp_finiquito")
    private BigDecimal impFiniquito;
    @Column(name = "cod_tipoimss")
    private Character codTipoimss;
    @Column(name = "cod_tipohonorarios")
    private Character codTipohonorarios;
    @Column(name = "cod_banco")
    private String codBanco;
    @Column(name = "cod_sucursal")
    private Integer codSucursal;
    @Column(name = "cod_cuenta")
    private String codCuenta;
    @Column(name = "cod_bancoh")
    private String codBancoh;
    @Column(name = "cod_sucursalh")
    private Integer codSucursalh;
    @Column(name = "cod_cuentah")
    private String codCuentah;
    @Column(name = "txt_descripcionbaja")
    private String txtDescripcionbaja;
    @Basic(optional = false)
    @Column(name = "aud_codcreadopor")
    private int audCodcreadopor;
    @Basic(optional = false)
    @Column(name = "aud_feccreacion")
    @Temporal(TemporalType.DATE)
    private Date audFeccreacion;
    @Column(name = "aud_codmodificadopor")
    private Integer audCodmodificadopor;
    @Column(name = "aud_fecmodificacion")
    @Temporal(TemporalType.DATE)
    private Date audFecmodificacion;
    @Column(name = "cod_clabe")
    private String codClabe;
    @Column(name = "cod_clabeh")
    private String codClabeh;
    @Column(name = "des_validacion")
    private Boolean desValidacion;
    @Column(name = "cod_validaciones")
    private Character codValidaciones;

    /**
     *
     */
    public Tsgnomempleados() {
    }

    /**
     *
     * @param codEmpleadoid
     */
    public Tsgnomempleados(Integer codEmpleadoid) {
        this.codEmpleadoid = codEmpleadoid;
    }

    /**
     *
     * @param codEmpleadoid
     * @param fecIngreso
     * @param bolEstatus
     * @param codEmpleadoFk
     * @param audCodcreadopor
     * @param audFeccreacion
     */
    public Tsgnomempleados(Integer codEmpleadoid, Date fecIngreso, boolean bolEstatus, int codEmpleadoFk, int audCodcreadopor, Date audFeccreacion) {
        this.codEmpleadoid = codEmpleadoid;
        this.fecIngreso = fecIngreso;
        this.bolEstatus = bolEstatus;
        this.codEmpleadoFk = codEmpleadoFk;
        this.audCodcreadopor = audCodcreadopor;
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Integer getCodEmpleadoid() {
        return codEmpleadoid;
    }

    /**
     *
     * @param codEmpleadoid
     */
    public void setCodEmpleadoid(Integer codEmpleadoid) {
        this.codEmpleadoid = codEmpleadoid;
    }

    /**
     *
     * @return
     */
    public Date getFecIngreso() {
        return fecIngreso;
    }

    /**
     *
     * @param fecIngreso
     */
    public void setFecIngreso(Date fecIngreso) {
        this.fecIngreso = fecIngreso;
    }

    /**
     *
     * @return
     */
    public Date getFecSalida() {
        return fecSalida;
    }

    /**
     *
     * @param fecSalida
     */
    public void setFecSalida(Date fecSalida) {
        this.fecSalida = fecSalida;
    }

    /**
     *
     * @return
     */
    public boolean getBolEstatus() {
        return bolEstatus;
    }

    /**
     *
     * @param bolEstatus
     */
    public void setBolEstatus(boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    public int getCodEmpleadoFk() {
        return codEmpleadoFk;
    }

    /**
     *
     * @param codEmpleadoFk
     */
    public void setCodEmpleadoFk(int codEmpleadoFk) {
        this.codEmpleadoFk = codEmpleadoFk;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpSueldoimss() {
        return impSueldoimss;
    }

    /**
     *
     * @param impSueldoimss
     */
    public void setImpSueldoimss(BigDecimal impSueldoimss) {
        this.impSueldoimss = impSueldoimss;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpHonorarios() {
        return impHonorarios;
    }

    /**
     *
     * @param impHonorarios
     */
    public void setImpHonorarios(BigDecimal impHonorarios) {
        this.impHonorarios = impHonorarios;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpFiniquito() {
        return impFiniquito;
    }

    /**
     *
     * @param impFiniquito
     */
    public void setImpFiniquito(BigDecimal impFiniquito) {
        this.impFiniquito = impFiniquito;
    }

    /**
     *
     * @return
     */
    public Character getCodTipoimss() {
        return codTipoimss;
    }

    /**
     *
     * @param codTipoimss
     */
    public void setCodTipoimss(Character codTipoimss) {
        this.codTipoimss = codTipoimss;
    }

    /**
     *
     * @return
     */
    public Character getCodTipohonorarios() {
        return codTipohonorarios;
    }

    /**
     *
     * @param codTipohonorarios
     */
    public void setCodTipohonorarios(Character codTipohonorarios) {
        this.codTipohonorarios = codTipohonorarios;
    }

    /**
     *
     * @return
     */
    public String getCodBanco() {
        return codBanco;
    }

    /**
     *
     * @param codBanco
     */
    public void setCodBanco(String codBanco) {
        this.codBanco = codBanco;
    }

    /**
     *
     * @return
     */
    public Integer getCodSucursal() {
        return codSucursal;
    }

    /**
     *
     * @param codSucursal
     */
    public void setCodSucursal(Integer codSucursal) {
        this.codSucursal = codSucursal;
    }

    /**
     *
     * @return
     */
    public String getCodCuenta() {
        return codCuenta;
    }

    /**
     *
     * @param codCuenta
     */
    public void setCodCuenta(String codCuenta) {
        this.codCuenta = codCuenta;
    }

    /**
     *
     * @return
     */
    public String getTxtDescripcionbaja() {
        return txtDescripcionbaja;
    }

    /**
     *
     * @param txtDescripcionbaja
     */
    public void setTxtDescripcionbaja(String txtDescripcionbaja) {
        this.txtDescripcionbaja = txtDescripcionbaja;
    }

    /**
     *
     * @return
     */
    public int getAudCodcreadopor() {
        return audCodcreadopor;
    }

    /**
     *
     * @param audCodcreadopor
     */
    public void setAudCodcreadopor(int audCodcreadopor) {
        this.audCodcreadopor = audCodcreadopor;
    }

    /**
     *
     * @return
     */
    public Date getAudFeccreacion() {
        return audFeccreacion;
    }

    /**
     *
     * @param audFeccreacion
     */
    public void setAudFeccreacion(Date audFeccreacion) {
        this.audFeccreacion = audFeccreacion;
    }

    /**
     *
     * @return
     */
    public Integer getAudCodmodificadopor() {
        return audCodmodificadopor;
    }

    /**
     *
     * @param audCodmodificadopor
     */
    public void setAudCodmodificadopor(Integer audCodmodificadopor) {
        this.audCodmodificadopor = audCodmodificadopor;
    }

    /**
     *
     * @return
     */
    public Date getAudFecmodificacion() {
        return audFecmodificacion;
    }

    /**
     *
     * @param audFecmodificacion
     */
    public void setAudFecmodificacion(Date audFecmodificacion) {
        this.audFecmodificacion = audFecmodificacion;
    }

    /**
     *
     * @return
     */
    public String getCodClabe() {
        return codClabe;
    }

    /**
     *
     * @param codClabe
     */
    public void setCodClabe(String codClabe) {
        this.codClabe = codClabe;
    }

    /**
     *
     * @return
     */
    public Boolean getDesValidacion() {
        return desValidacion;
    }

    /**
     *
     * @param desValidacion
     */
    public void setDesValidacion(Boolean desValidacion) {
        this.desValidacion = desValidacion;
    }

    /**
     *
     * @return
     */
    public Character getCodValidaciones() {
        return codValidaciones;
    }

    /**
     *
     * @param codValidaciones
     */
    public void setCodValidaciones(Character codValidaciones) {
        this.codValidaciones = codValidaciones;
    }

    /**
     *
     * @return
     */
    public String getCodBancoh() {
        return codBancoh;
    }

    /**
     *
     * @param codBancoh
     */
    public void setCodBancoh(String codBancoh) {
        this.codBancoh = codBancoh;
    }

    /**
     *
     * @return
     */
    public Integer getCodSucursalh() {
        return codSucursalh;
    }

    /**
     *
     * @param codSucursalh
     */
    public void setCodSucursalh(Integer codSucursalh) {
        this.codSucursalh = codSucursalh;
    }

    /**
     *
     * @return
     */
    public String getCodCuentah() {
        return codCuentah;
    }

    /**
     *
     * @param codCuentah
     */
    public void setCodCuentah(String codCuentah) {
        this.codCuentah = codCuentah;
    }

    /**
     *
     * @return
     */
    public String getCodClabeh() {
        return codClabeh;
    }

    /**
     *
     * @param codClabeh
     */
    public void setCodClabeh(String codClabeh) {
        this.codClabeh = codClabeh;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEmpleadoid != null ? codEmpleadoid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomempleados)) {
            return false;
        }
        Tsgnomempleados other = (Tsgnomempleados) object;
        if ((this.codEmpleadoid == null && other.codEmpleadoid != null) || (this.codEmpleadoid != null && !this.codEmpleadoid.equals(other.codEmpleadoid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomempleados[ codEmpleadoid=" + codEmpleadoid + " ]";
    }
}
