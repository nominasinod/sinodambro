package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.CatalogosDTO;

/**
 *
 * @author Francisco R M, MBN
 */
public interface CatalogosService {

    /**
     *
     * @param dto
     * @return
     */
    CatalogosDTO obtenerCatalogo(CatalogosDTO dto);
}
