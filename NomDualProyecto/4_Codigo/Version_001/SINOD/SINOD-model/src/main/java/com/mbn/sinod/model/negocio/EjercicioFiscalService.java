package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.EjercicioFiscalDTO;
import com.mbn.sinod.model.entidades.Tsgnomejercicio;

/**
 *
 * @author mipe
 */
public interface EjercicioFiscalService extends BaseService<Tsgnomejercicio, Integer> {

    /**
     *
     * @return
     */
    EjercicioFiscalDTO listaEjercicios();

}
