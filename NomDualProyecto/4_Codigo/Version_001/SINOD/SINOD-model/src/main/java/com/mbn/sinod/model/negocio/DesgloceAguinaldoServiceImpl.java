/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.DesgloceAguinaldoDAO;
import com.mbn.sinod.model.dto.DesgloceAguinaldoDTO;
import com.mbn.sinod.model.entidades.Tsgnomaguinaldo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ivette
 */
public class DesgloceAguinaldoServiceImpl extends BaseServiceImpl<Tsgnomaguinaldo, Integer> implements DesgloceAguinaldoService{

     private static final Logger logger = Logger.getLogger(DesgloceAguinaldoService.class.getName());
    
    @Override
    public DesgloceAguinaldoDTO listaDesgloceAguinaldoEmp(Integer empQuin) {
       DesgloceAguinaldoDTO respuesta = new DesgloceAguinaldoDTO();

        try {
            List<Tsgnomaguinaldo> listaDesgloceAguinaldoEmp = ((DesgloceAguinaldoDAO) getGenericDAO()).listaDesgloceAguinaldoEmp(empQuin);
            if (listaDesgloceAguinaldoEmp != null) {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setListAguinaldo(listaDesgloceAguinaldoEmp);
//                respuesta.setCodigoMensaje(StaticConstantes.ERROR_ELIMINAR_CONCEPTO);
            } else {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new DesgloceAguinaldoDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(DesgloceAguinaldoServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }
    
}
