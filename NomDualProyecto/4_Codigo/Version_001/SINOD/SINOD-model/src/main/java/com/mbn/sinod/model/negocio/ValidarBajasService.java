package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.ValidacionBajasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;

/**
 *
 * @author Ivette
 */
public interface ValidarBajasService extends BaseService<Tsgnomempleados, Integer> {

    /**
     *
     * @return
     */
    ValidacionBajasDTO listarBajasAValidar();

    /**
     *
     * @param bajas
     * @return
     */
    ValidacionBajasDTO validarCadaBaja(ValidacionBajasDTO bajas);

    /**
     *
     * @param bajas
     * @return
     */
    ValidacionBajasDTO validarBajasAceptadas(ValidacionBajasDTO bajas);

    /**
     *
     * @param bajas
     * @return
     */
    ValidacionBajasDTO validarBajasRechazadas(ValidacionBajasDTO bajas);

}
