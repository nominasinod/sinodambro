package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomcabecera;
import java.util.List;

/**
 *
 * @author Mi Pe
 */
public class CabeceraDTO extends GenericDTO {

    private Tsgnomcabecera cabecera;
    private List<Tsgnomcabecera> listarCabecera;
    private Boolean confirmacion;
    private Integer codError;

    /**
     * @return the CatCabecera
     */
    public Tsgnomcabecera getCabecera() {
        return cabecera;
    }

    /**
     * @param Cabecera
     */
    public void setCabecera(Tsgnomcabecera Cabecera) {
        this.cabecera = Cabecera;
    }

    /**
     * @return the listarCabecera
     */
    public List<Tsgnomcabecera> getListarCabecera() {
        return listarCabecera;
    }

    /**
     * @param listarCabecera the listarCabecera to set
     */
    public void setListarCabecera(List<Tsgnomcabecera> listarCabecera) {
        this.listarCabecera = listarCabecera;
    }

    /**
     * @return the confirmacion
     */
    public Boolean getConfirmacion() {
        return confirmacion;
    }

    /**
     * @param confirmacion the confirmacion to set
     */
    public void setConfirmacion(Boolean confirmacion) {
        this.confirmacion = confirmacion;
    }

    /**
     * @return the codError
     */
    public Integer getCodError() {
        return codError;
    }

    /**
     * @param codError the codError to set
     */
    public void setCodError(Integer codError) {
        this.codError = codError;
    }
}
