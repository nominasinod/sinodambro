package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.ConfPagoDTO;
import com.mbn.sinod.model.dto.DetalleEmpDTO;
import com.mbn.sinod.model.entidades.Tsgnomconfpago;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author eduardotorres
 */
public interface ConfPagoService extends BaseService<Tsgnomconfpago, Integer> {

    /**
     *
     * @param empquincenaFK
     * @param conf3
     * @return
     */
    ConfPagoDTO guardarAutorizacionEMP(Integer empquincenaFK, Boolean conf3);

    /**
     *
     * @param conf2
     * @return
     */
    ConfPagoDTO guardarAutorizacionRH(ConfPagoDTO conf2);

    /**
     *
     * @param cabecera
     * @return
     */
    ConfPagoDTO rechazarTodasRF(Integer cabecera);

    /**
     *
     * @param cabecera
     * @return
     */
    ConfPagoDTO rechazarTodasRH(Integer cabecera);

    /**
     *
     * @param cabecera
     * @return
     */
    ConfPagoDTO validarTodas(Integer cabecera);

    /**
     *
     * @param cabecera
     * @return
     */
    ConfPagoDTO validarTodasRH(Integer cabecera);

    /**
     *
     * @param conf1
     * @return
     */
    ConfPagoDTO guardarAutorizacionRF(ConfPagoDTO conf1);
}
