package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import java.util.List;

/**
 *
 * @author mipe
 */
public class EmpQuincenaDAOImpl extends GenericDAOImpl<Tsgnomempquincena, Integer>
        implements EmpQuincenaDAO {

    @Override
    public List<Tsgnomempquincena> listarEmpQuincena() {
        return findAll();
    }

}
