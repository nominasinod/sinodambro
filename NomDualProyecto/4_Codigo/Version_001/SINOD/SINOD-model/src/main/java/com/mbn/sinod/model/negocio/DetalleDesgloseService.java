package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.DesgloseDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;

/**
 *
 * @author eduardotorres
 */
public interface DetalleDesgloseService extends BaseService<Tsgnomcncptoquinc, Integer> {

    /**
     *
     * @param cod_empleado
     * @param cod_cabecera
     * @return
     */
    DesgloseDTO desgloseDTO(Integer cod_empleado, Integer cod_cabecera);

}
