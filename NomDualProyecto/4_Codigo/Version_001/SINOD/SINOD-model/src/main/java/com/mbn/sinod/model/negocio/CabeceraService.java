package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.CabeceraDTO;
import com.mbn.sinod.model.dto.EmpQuincenaPorCabeceraDTO;
import com.mbn.sinod.model.entidades.Tsgnomcabecera;

/**
 *
 * @author Mi Pe
 */
public interface CabeceraService extends BaseService<Tsgnomcabecera, Integer> {

    /**
     *
     * @param cabeceraid
     * @return
     */
    CabeceraDTO calculaNomina(Integer cabeceraid);

    /**
     *
     * @param archivo
     * @param cabecera
     * @return
     */
    CabeceraDTO cargarImss(String archivo, Integer cabecera);

    /**
     *
     * @param cabecera
     * @return
     */
    CabeceraDTO cerrarNomina(Integer cabecera);

    /**
     *
     * @param cabeceraId
     * @return
     */
    CabeceraDTO eliminarCabecera(Integer cabeceraId);

    /**
     *
     * @param cabecera
     * @return
     */
    CabeceraDTO guardarActualizarCabecera(CabeceraDTO cabecera);

    /**
     *
     * @return
     */
    CabeceraDTO listaCabeceras();

    /**
     *
     * @param cabecera
     * @return
     */
    CabeceraDTO validaPagosNomina(Integer cabecera);

    /**
     *
     * @param cabeceraId
     * @return
     */
    EmpQuincenaPorCabeceraDTO listarEmpleadosCabecera(Integer cabeceraId);

    /**
     *
     * @param cabeceraId
     * @return
     */
    Tsgnomcabecera obtenerCabeceraPorId(Integer cabeceraId);
}
