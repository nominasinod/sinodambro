package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.FuncionDTO;
import com.mbn.sinod.model.entidades.Tsgnomfuncion;

/**
 *
 * @author mariana
 */
public interface FuncionService extends BaseService<Tsgnomfuncion, Integer> {

    /**
     *
     * @return
     */
    FuncionDTO obtenerFunciones();
}
