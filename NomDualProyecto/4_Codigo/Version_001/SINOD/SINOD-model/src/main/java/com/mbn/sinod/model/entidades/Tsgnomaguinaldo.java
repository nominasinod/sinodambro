package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mi Pe
 */
@Entity
@Table(name = "tsgnomaguinaldo", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomaguinaldo.findAll", query = "SELECT t FROM Tsgnomaguinaldo t")})
public class Tsgnomaguinaldo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod_aguinaldoid")
    private Integer codAguinaldoid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "imp_aguinaldo")
    private BigDecimal impAguinaldo;
    @Basic(optional = false)
    @Column(name = "cod_tipoaguinaldo")
    private Character codTipoaguinaldo;
    @Basic(optional = false)
    @Column(name = "bol_estatus")
    private boolean bolEstatus;
    @Column(name = "xml_desgloce", columnDefinition = "XMLType")
    private String xmlDesgloce;
    @JoinColumn(name = "cod_empquincenaid_fk", referencedColumnName = "cod_empquincenaid")
    @ManyToOne(optional = false)
    private Tsgnomempquincena codEmpquincenaidFk;

    /**
     *
     */
    public Tsgnomaguinaldo() {
    }

    /**
     *
     * @param codAguinaldoid
     */
    public Tsgnomaguinaldo(Integer codAguinaldoid) {
        this.codAguinaldoid = codAguinaldoid;
    }

    /**
     *
     * @param codAguinaldoid
     * @param impAguinaldo
     * @param codTipoaguinaldo
     * @param bolEstatus
     */
    public Tsgnomaguinaldo(Integer codAguinaldoid, BigDecimal impAguinaldo, Character codTipoaguinaldo, boolean bolEstatus) {
        this.codAguinaldoid = codAguinaldoid;
        this.impAguinaldo = impAguinaldo;
        this.codTipoaguinaldo = codTipoaguinaldo;
        this.bolEstatus = bolEstatus;
    }

    /**
     *
     * @return
     */
    public Integer getCodAguinaldoid() {
        return codAguinaldoid;
    }

    /**
     *
     * @param codAguinaldoid
     */
    public void setCodAguinaldoid(Integer codAguinaldoid) {
        this.codAguinaldoid = codAguinaldoid;
    }

    /**
     *
     * @return
     */
    public BigDecimal getImpAguinaldo() {
        return impAguinaldo;
    }

    /**
     *
     * @param impAguinaldo
     */
    public void setImpAguinaldo(BigDecimal impAguinaldo) {
        this.impAguinaldo = impAguinaldo;
    }

    /**
     *
     * @return
     */
    public Character getCodTipoaguinaldo() {
        return codTipoaguinaldo;
    }

    /**
     *
     * @param codTipoaguinaldo
     */
    public void setCodTipoaguinaldo(Character codTipoaguinaldo) {
        this.codTipoaguinaldo = codTipoaguinaldo;
    }

    /**
     *
     * @return
     */
    public boolean getBolEstatus() {
        return bolEstatus;
    }

    /**
     *
     * @param bolEstatus
     */
    public void setBolEstatus(boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

   

    /**
     *
     * @return
     */
    public Tsgnomempquincena getCodEmpquincenaidFk() {
        return codEmpquincenaidFk;
    }

    /**
     *
     * @param codEmpquincenaidFk
     */
    public void setCodEmpquincenaidFk(Tsgnomempquincena codEmpquincenaidFk) {
        this.codEmpquincenaidFk = codEmpquincenaidFk;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codAguinaldoid != null ? codAguinaldoid.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomaguinaldo)) {
            return false;
        }
        Tsgnomaguinaldo other = (Tsgnomaguinaldo) object;
        if ((this.codAguinaldoid == null && other.codAguinaldoid != null) || (this.codAguinaldoid != null && !this.codAguinaldoid.equals(other.codAguinaldoid))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomaguinaldo[ codAguinaldoid=" + codAguinaldoid + " ]";
    }

    public String getXmlDesgloce() {
        return xmlDesgloce;
    }

    public void setXmlDesgloce(String xmlDesgloce) {
        this.xmlDesgloce = xmlDesgloce;
    }

}
