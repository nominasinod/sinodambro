package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomconceptosat;
import java.util.List;

/**
 *
 * @author mipe
 */
public class CatConceptosSATDAOImpl extends GenericDAOImpl<Tsgnomconceptosat, Integer> implements CatConceptosSATDAO {

    @Override
    public List<Tsgnomconceptosat> listarConceptosSAT() {
        return findAll();
    }

}
