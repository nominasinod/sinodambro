package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomtiponomina;
import java.util.List;

/**
 *
 * @author ambrosio
 */
public interface TipoNominaDAO extends GenericDAO<Tsgnomtiponomina, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomtiponomina> listarTiposConceptos();
}
