package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomfuncion;
import java.util.List;

/**
 *
 * @author mariana
 */
public interface FuncionDAO extends GenericDAO<Tsgnomfuncion, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomfuncion> obtenerFunciones();
}
