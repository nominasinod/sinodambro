package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.ConceptosEmpleadoDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquincht;
import java.util.List;

/**
 *
 * @author User
 */
public interface ConcepQuincDAO extends GenericDAO<Tsgnomcncptoquinc, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomcncptoquinc> listaConcepQuinc();

    /**
     *
     * @param empQuin
     * @return
     */
    List<Tsgnomcncptoquinc> listaConcepQuincByEmpQuin(Integer empQuin);

    /**
     *
     * @param empleado
     * @return
     */
    List<Tsgnomcncptoquinc> listaConceptosEmpl(Integer empleado);

    /**
     *
     * @param empQuin
     * @return
     */
    List<Tsgnomcncptoquincht> listaConcepQuincByEmpQuinHT(Integer empQuin);

    /**
     *
     * @param cabecera
     * @return
     */
    List<Tsgnomcncptoquinc> listaConcepQuincByCabecera(Integer cabecera);

    /**
     *
     * @param idEmpleado
     * @param idCabecera
     * @return
     */
    List<ConceptosEmpleadoDTO> listaConceptosEmpleados(Integer idEmpleado, Integer idCabecera);
}
