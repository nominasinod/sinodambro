package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Ivette
 */
public class ListaAltaDePersonalDTO extends GenericDTO {

    private String nomempleado;
    private Integer cod_empleado;
    private List<ListaAltaDePersonalDTO> listaEmpleadosSinAltaNom;

    /**
     *
     * @return the nomempleado
     */
    public String getNomempleado() {
        return nomempleado;
    }

    /**
     *
     * @param nomempleado the nomempleado to set
     */
    public void setNomempleado(String nomempleado) {
        this.nomempleado = nomempleado;
    }

    /**
     *
     * @return the listaEmpleadosSinAltaNom
     */
    public List<ListaAltaDePersonalDTO> getListaEmpleadosSinAltaNom() {
        return listaEmpleadosSinAltaNom;
    }

    /**
     *
     * @param listaEmpleadosSinAltaNom the listaEmpleadosSinAltaNom to set
     */
    public void setListaEmpleadosSinAltaNom(List<ListaAltaDePersonalDTO> listaEmpleadosSinAltaNom) {
        this.listaEmpleadosSinAltaNom = listaEmpleadosSinAltaNom;
    }

    /**
     *
     * @return the cod_empleado
     */
    public Integer getCod_empleado() {
        return cod_empleado;
    }

    /**
     *
     * @param cod_empleado the cod_empleado to set
     */
    public void setCod_empleado(Integer cod_empleado) {
        this.cod_empleado = cod_empleado;
    }
}
