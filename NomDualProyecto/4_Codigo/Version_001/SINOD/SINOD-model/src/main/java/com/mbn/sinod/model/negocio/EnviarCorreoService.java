package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.IncidenciasQuincenaDTO;
import com.mbn.sinod.model.dto.ValidacionAltasDTO;
import com.mbn.sinod.model.dto.ValidacionBajasDTO;
import com.mbn.sinod.model.entidades.Tsgnomincidencia;

/**
 *
 * @author mariana
 */
public interface EnviarCorreoService extends BaseService<Tsgnomincidencia, Integer> {

    /**
     *
     * @param incidencia
     * @return
     */
    IncidenciasQuincenaDTO enviarCorreos(IncidenciasQuincenaDTO incidencia);

    /**
     *
     * @param altas
     * @return
     */
    ValidacionAltasDTO enviarCorreosValidarAltas(ValidacionAltasDTO altas);

    /**
     *
     * @param bajas
     * @return
     */
    ValidacionBajasDTO enviarCorreosValidarBajas(ValidacionBajasDTO bajas);
}
