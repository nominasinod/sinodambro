package com.mbn.sinod.model.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Ambro
 */
public class ConceptosEmpleadoDTO extends GenericDTO {

    private String clave;
    private String nombre;
    private BigDecimal importe;
    private String cantidad;
    private Integer tipo;

    private List<ConceptosEmpleadoDTO> listaConceptosEmpleado;

    /**
     * @return the clave
     */
    public String getClave() {
        return clave;
    }

    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the importe
     */
    public BigDecimal getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    /**
     * @return the listaConceptosEmpleado
     */
    public List<ConceptosEmpleadoDTO> getListaConceptosEmpleado() {
        return listaConceptosEmpleado;
    }

    /**
     * @param listaConceptosEmpleado the listaConceptosEmpleado to set
     */
    public void setListaConceptosEmpleado(List<ConceptosEmpleadoDTO> listaConceptosEmpleado) {
        this.listaConceptosEmpleado = listaConceptosEmpleado;
    }

    /**
     * @return the id
     */
    public Integer getTipo() {
        return tipo;
    }

    /**
     * @param tipo
     */
    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the cantidad
     */
    public String getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

}
