package com.mbn.sinod.model.dto;

/**
 *
 * @author Ivette
 */
public class ValidacionesNomBajasDTO {

    private Integer cod_empleadoid;
    private String nom_empleado;
    private String cod_rfc;
    private String des_nbarea;
    private String des_puesto;
    private Boolean validar;
    private String fecha_validacion;

    /**
     *
     * @return the cod_empleadoid
     */
    public Integer getCod_empleadoid() {
        return cod_empleadoid;
    }

    /**
     *
     * @param cod_empleadoid the cod_empleadoid to set
     */
    public void setCod_empleadoid(Integer cod_empleadoid) {
        this.cod_empleadoid = cod_empleadoid;
    }

    /**
     *
     * @return the nom_empleado
     */
    public String getNom_empleado() {
        return nom_empleado;
    }

    /**
     *
     * @param nom_empleado the nom_empleado to set
     */
    public void setNom_empleado(String nom_empleado) {
        this.nom_empleado = nom_empleado;
    }

    /**
     *
     * @return the cod_rfc
     */
    public String getCod_rfc() {
        return cod_rfc;
    }

    /**
     *
     * @param cod_rfc the cod_rfc to set
     */
    public void setCod_rfc(String cod_rfc) {
        this.cod_rfc = cod_rfc;
    }

    /**
     *
     * @return the des_nbarea
     */
    public String getDes_nbarea() {
        return des_nbarea;
    }

    /**
     *
     * @param des_nbarea the des_nbarea to set
     */
    public void setDes_nbarea(String des_nbarea) {
        this.des_nbarea = des_nbarea;
    }

    /**
     *
     * @return the des_puesto
     */
    public String getDes_puesto() {
        return des_puesto;
    }

    /**
     *
     * @param des_puesto the des_puesto to set
     */
    public void setDes_puesto(String des_puesto) {
        this.des_puesto = des_puesto;
    }

    /**
     *
     * @return the validar
     */
    public Boolean getValidar() {
        return validar;
    }

    /**
     *
     * @param validar the validar to set
     */
    public void setValidar(Boolean validar) {
        this.validar = validar;
    }

    /**
     *
     * @return the fecha_validacion
     */
    public String getFecha_validacion() {
        return fecha_validacion;
    }

    /**
     *
     * @param fecha_validacion the fecha_validacion to set
     */
    public void setFecha_validacion(String fecha_validacion) {
        this.fecha_validacion = fecha_validacion;
    }

}
