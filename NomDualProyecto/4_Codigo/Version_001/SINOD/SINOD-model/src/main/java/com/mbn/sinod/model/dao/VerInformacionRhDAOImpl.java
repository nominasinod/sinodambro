package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.dto.VerInformacionRhDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Ivette
 */
public class VerInformacionRhDAOImpl extends GenericDAOImpl<Tsgnomempleados, Integer>
        implements VerInformacionRhDAO {

    @Override
    public List<VerInformacionRhDTO> listaInformacionRh(Integer empleadoRh) {
        List<VerInformacionRhDTO> listaInformacionRh = (List<VerInformacionRhDTO>) getSession().createSQLQuery("select * from sgnom.verinformacionrh(?);")
                .addScalar("cod_empleado")
                .addScalar("des_nombre")
                .addScalar("des_apepaterno")
                .addScalar("des_apematerno")
                .addScalar("des_nbarea")
                .addScalar("des_puesto")
                .addScalar("des_rol")
                .addScalar("cod_rfc")
                .addScalar("cod_curp")
                .addScalar("cod_nss")
                .addScalar("fec_nacimiento")
                .addScalar("des_direccion")
                .addScalar("des_correo")
                .addScalar("correo_personal")
                .setResultTransformer(Transformers.aliasToBean(VerInformacionRhDTO.class))
                .setInteger(0, empleadoRh)
                .list();

        return listaInformacionRh;
    }

}
