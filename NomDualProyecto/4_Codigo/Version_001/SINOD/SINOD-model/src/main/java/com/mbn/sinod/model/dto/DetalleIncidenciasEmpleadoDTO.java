package com.mbn.sinod.model.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Karla
 */
public class DetalleIncidenciasEmpleadoDTO {

    private Integer idincidencia;
    private Date fechaalta;
    private String clave;
    private String incidencia;
    private Character idtipo;
    private String desctipo;
    private BigDecimal cantidad;
    private String actividad;
    private String comentarios;
    private Integer reportaid;
    private String reportanb;
    private Integer autorizaid;
    private String autorizanb;
    private String perfil;
    private String detallefechas;
    private BigDecimal montoincidencia;
    private BigDecimal montopagado;
    private Boolean aceptacion; //validacion por parte del lider de celula
    private Boolean validacion; //de validacion por parte de RH
    private Boolean autorizacion; //de validacion de pago Finanzas
    private Integer quincenaid;
    private String desquincena;
    private Integer creaid;
    private String creanb;

    /**
     *
     * @return
     */
    public Integer getIdincidencia() {
        return idincidencia;
    }

    /**
     *
     * @param idincidencia
     */
    public void setIdincidencia(Integer idincidencia) {
        this.idincidencia = idincidencia;
    }

    /**
     *
     * @return
     */
    public String getClave() {
        return clave;
    }

    /**
     *
     * @param clave
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     *
     * @return
     */
    public String getIncidencia() {
        return incidencia;
    }

    /**
     *
     * @param incidencia
     */
    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    /**
     *
     * @return
     */
    public Character getIdtipo() {
        return idtipo;
    }

    /**
     *
     * @param idtipo
     */
    public void setIdtipo(Character idtipo) {
        this.idtipo = idtipo;
    }

    /**
     *
     * @return
     */
    public String getDesctipo() {
        return desctipo;
    }

    /**
     *
     * @param desctipo
     */
    public void setDesctipo(String desctipo) {
        this.desctipo = desctipo;
    }

    /**
     *
     * @return
     */
    public BigDecimal getCantidad() {
        return cantidad;
    }

    /**
     *
     * @param cantidad
     */
    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    /**
     *
     * @return
     */
    public String getActividad() {
        return actividad;
    }

    /**
     *
     * @param actividad
     */
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    /**
     *
     * @return
     */
    public String getComentarios() {
        return comentarios;
    }

    /**
     *
     * @param comentarios
     */
    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    /**
     *
     * @return
     */
    public Integer getReportaid() {
        return reportaid;
    }

    /**
     *
     * @param reportaid
     */
    public void setReportaid(Integer reportaid) {
        this.reportaid = reportaid;
    }

    /**
     *
     * @return
     */
    public String getReportanb() {
        return reportanb;
    }

    /**
     *
     * @param reportanb
     */
    public void setReportanb(String reportanb) {
        this.reportanb = reportanb;
    }

    /**
     *
     * @return
     */
    public Date getFechaalta() {
        return fechaalta;
    }

    /**
     *
     * @param fechaalta
     */
    public void setFechaalta(Date fechaalta) {
        this.fechaalta = fechaalta;
    }

    /**
     *
     * @return
     */
    public Integer getAutorizaid() {
        return autorizaid;
    }

    /**
     *
     * @param autorizaid
     */
    public void setAutorizaid(Integer autorizaid) {
        this.autorizaid = autorizaid;
    }

    /**
     *
     * @return
     */
    public String getAutorizanb() {
        return autorizanb;
    }

    /**
     *
     * @param autorizanb
     */
    public void setAutorizanb(String autorizanb) {
        this.autorizanb = autorizanb;
    }

    /**
     *
     * @return
     */
    public String getPerfil() {
        return perfil;
    }

    /**
     *
     * @param perfil
     */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    /**
     *
     * @return
     */
    public String getDetallefechas() {
        return detallefechas;
    }

    /**
     *
     * @param detallefechas
     */
    public void setDetallefechas(String detallefechas) {
        this.detallefechas = detallefechas;
    }

    /**
     *
     * @return
     */
    public BigDecimal getMontoincidencia() {
        return montoincidencia;
    }

    /**
     *
     * @param montoincidencia
     */
    public void setMontoincidencia(BigDecimal montoincidencia) {
        this.montoincidencia = montoincidencia;
    }

    /**
     *
     * @return
     */
    public BigDecimal getMontopagado() {
        return montopagado;
    }

    /**
     *
     * @param montopagado
     */
    public void setMontopagado(BigDecimal montopagado) {
        this.montopagado = montopagado;
    }

    /**
     *
     * @return
     */
    public Boolean getValidacion() {
        return validacion;
    }

    /**
     *
     * @param validacion
     */
    public void setValidacion(Boolean validacion) {
        this.validacion = validacion;
    }

    /**
     *
     * @return
     */
    public Boolean getAceptacion() {
        return aceptacion;
    }

    /**
     *
     * @param aceptacion
     */
    public void setAceptacion(Boolean aceptacion) {
        this.aceptacion = aceptacion;
    }

    /**
     *
     * @return
     */
    public Integer getQuincenaid() {
        return quincenaid;
    }

    /**
     *
     * @param quincenaid
     */
    public void setQuincenaid(Integer quincenaid) {
        this.quincenaid = quincenaid;
    }

    /**
     *
     * @return
     */
    public String getDesquincena() {
        return desquincena;
    }

    /**
     *
     * @param desquincena
     */
    public void setDesquincena(String desquincena) {
        this.desquincena = desquincena;
    }

    /**
     *
     * @return
     */
    public Integer getCreaid() {
        return creaid;
    }

    /**
     *
     * @param creaid
     */
    public void setCreaid(Integer creaid) {
        this.creaid = creaid;
    }

    /**
     *
     * @return
     */
    public String getCreanb() {
        return creanb;
    }

    /**
     *
     * @param creanb
     */
    public void setCreanb(String creanb) {
        this.creanb = creanb;
    }

    /**
     *
     * @return
     */
    public Boolean getAutorizacion() {
        return autorizacion;
    }

    /**
     *
     * @param autorizacion
     */
    public void setAutorizacion(Boolean autorizacion) {
        this.autorizacion = autorizacion;
    }

}
