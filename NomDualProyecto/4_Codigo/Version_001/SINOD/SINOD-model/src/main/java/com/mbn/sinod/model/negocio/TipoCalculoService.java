package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.TipoCalculoDTO;
import com.mbn.sinod.model.entidades.Tsgnomcalculo;

/**
 *
 * @author ambrosio
 */
public interface TipoCalculoService extends BaseService<Tsgnomcalculo, Integer> {

    /**
     *
     * @return
     */
    TipoCalculoDTO listarTiposCalculo();
}
