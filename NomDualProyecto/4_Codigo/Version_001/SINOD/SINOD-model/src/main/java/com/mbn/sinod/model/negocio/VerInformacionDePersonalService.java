package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.InformacionPorPersonalDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;

/**
 *
 * @author Ivette
 */
public interface VerInformacionDePersonalService extends BaseService<Tsgrhempleados, Integer> {

    /**
     *
     * @param cod_empleadoid
     * @return
     */
    InformacionPorPersonalDTO informacionPorPersonal(Integer cod_empleadoid);

}
