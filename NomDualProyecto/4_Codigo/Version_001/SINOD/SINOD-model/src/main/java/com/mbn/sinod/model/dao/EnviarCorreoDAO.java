package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.IncidenciasQuincenaDTO;
import com.mbn.sinod.model.dto.ValidacionAltasDTO;
import com.mbn.sinod.model.dto.ValidacionBajasDTO;
import com.mbn.sinod.model.entidades.Tsgnomincidencia;
import java.util.List;

/**
 *
 * @author mariana
 */
public interface EnviarCorreoDAO extends GenericDAO<Tsgnomincidencia, Integer> {

    /**
     *
     * @param incidencias
     * @return
     */
    boolean enviarCorreos(List<IncidenciasQuincenaDTO> incidencias);

    /**
     *
     * @param altas
     * @return
     */
    boolean enviarCorreosValidacionAltas(List<ValidacionAltasDTO> altas);

    /**
     *
     * @param bajas
     * @return
     */
    boolean enviarCorreosValidacionBajas(List<ValidacionBajasDTO> bajas);

}
