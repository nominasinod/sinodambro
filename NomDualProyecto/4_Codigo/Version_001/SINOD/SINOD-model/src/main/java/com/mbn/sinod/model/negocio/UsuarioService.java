package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.UsuarioDTO;
import com.mbn.sinod.model.entidades.Usuario;

/**
 *
 * @author mipe
 */
public interface UsuarioService extends BaseService<Usuario, Integer> {

    /**
     *
     * @param dto
     * @return
     */
    UsuarioDTO obtenerUsuarioPorId(UsuarioDTO dto);
}
