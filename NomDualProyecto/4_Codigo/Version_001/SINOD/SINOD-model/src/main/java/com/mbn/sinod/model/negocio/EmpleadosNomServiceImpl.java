package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.EmpleadosNomDAO;
import com.mbn.sinod.model.dto.DesgloseEmpleadoDTO;
import com.mbn.sinod.model.dto.EmpleadosNomDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ivette
 */
public class EmpleadosNomServiceImpl extends BaseServiceImpl<Tsgnomempleados, Integer> implements EmpleadosNomService {

    private static final Logger logger = Logger.getLogger(EmpleadosNomServiceImpl.class.getName());

    @Autowired
    private EmpleadosNomDAO empleadosDAO;

    /**
     *
     * @return
     */
    @Override
    public List<Tsgnomempleados> listarEmpleadosNom() {
        return empleadosDAO.listarEmpleadosNom();
    }

    /**
     *
     * @param idempleadorh
     * @return
     */
    @Override
    public Tsgnomempleados obtenerEmpleadonomPorIdrh(Integer idempleadorh) {
        return this.empleadosDAO.obtenerEmpleadonomPorIdrh(idempleadorh);
    }

    /**
     *
     * @return
     */
    public EmpleadosNomDAO getEmpleadosDAO() {
        return empleadosDAO;
    }

    /**
     *
     * @param empleadosDAO
     */
    public void setEmpleadosDAO(EmpleadosNomDAO empleadosDAO) {
        this.empleadosDAO = empleadosDAO;
    }

    /**
     *
     * @param empleado
     * @return
     */
    @Override
    @Transactional
    public EmpleadosNomDTO guardar(EmpleadosNomDTO empleado) {
        EmpleadosNomDTO respuesta = new EmpleadosNomDTO();
        System.out.println("EMPLEADO NOM SERVICE GUARDAR");
        try {
            boolean respuestaGuardar
                    = ((EmpleadosNomDAO) getGenericDAO()).guardarActualizarEmpleados(empleado.getEmpleado());

            if (respuestaGuardar) {
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_GUARDAR_EMPLEADOS);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setEmpleado(empleado.getEmpleado());
            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_GUARDAR_EMPLEADOS);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new EmpleadosNomDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomempleados.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param idempleadonom
     * @return
     */
    @Override
    public Tsgnomempleados obtenerEmpleadosnomPorIdNom(Integer idempleadonom) {
        return this.empleadosDAO.obtenerEmpleadosnomPorIdNom(idempleadonom);
    }

    @Override
    public DesgloseEmpleadoDTO obtenerEmpleadosnomPorIdNomHT(Integer idempleadonom) {
//        return this.empleadosDAO.obtenerEmpleadosnomPorIdNomHT(idempleadonom);
        DesgloseEmpleadoDTO respuesta = new DesgloseEmpleadoDTO();
        try {
            List<DesgloseEmpleadoDTO> listaDesgloce
                    = ((EmpleadosNomDAO) getGenericDAO()).obtenerEmpleadosnomPorIdNomHT(idempleadonom);

            if (listaDesgloce != null) {
                respuesta.setListaDesgloce(listaDesgloce);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_OBTENER_EMP_NOM_HT);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
            } else {
                listaDesgloce = new ArrayList();
                respuesta.setListaDesgloce(listaDesgloce);
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_OBTENER_EMP_NOM_HT);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new DesgloseEmpleadoDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomempleados.class.getName()).log(Level.SEVERE, null, e);
        }

        return respuesta;
    }

    /**
     *
     * @param empleadoRh
     * @return
     */
    @Override
    public EmpleadosNomDTO detallesEmpleadosPorIdRH(Integer empleadoRh) {
        EmpleadosNomDTO respuesta = new EmpleadosNomDTO();

        try {
            List<Tsgnomempleados> detalleEmpleadosNom = ((EmpleadosNomDAO) getGenericDAO()).detallesEmpleadosPorIdRH(empleadoRh);
            if (detalleEmpleadosNom != null) {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setListarEmpleadosNom(detalleEmpleadosNom);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_DETELLE_EMP_RH);
            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_DETELLE_EMP_RH);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new EmpleadosNomDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(EmpleadosNomServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }
}
