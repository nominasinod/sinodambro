/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomaguinaldo;
import java.util.List;

/**
 *
 * @author Ivette
 */
public class DesgloceAguinaldoDTO extends GenericDTO{
    
    private Tsgnomaguinaldo desgloceAguinaldo;
    private List<Tsgnomaguinaldo> listAguinaldo;

    /**
     *
     * @return the desgloceAguinaldo
     */
    public Tsgnomaguinaldo getDesgloceAguinaldo() {
        return desgloceAguinaldo;
    }

    /**
     *
     * @param desgloceAguinaldo the desgloceAguinaldo to set
     */
    public void setDesgloceAguinaldo(Tsgnomaguinaldo desgloceAguinaldo) {
        this.desgloceAguinaldo = desgloceAguinaldo;
    }

    /**
     *
     * @return the listAguinaldo
     */
    public List<Tsgnomaguinaldo> getListAguinaldo() {
        return listAguinaldo;
    }

    /**
     *
     * @param listAguinaldo the listAguinaldo to set
     */
    public void setListAguinaldo(List<Tsgnomaguinaldo> listAguinaldo) {
        this.listAguinaldo = listAguinaldo;
    }
    
    
}
