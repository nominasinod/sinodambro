package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomtiponomina;
import java.util.List;

/**
 *
 * @author mipe
 */
public class TipoNominaDAOImpl extends GenericDAOImpl<Tsgnomtiponomina, Integer> implements TipoNominaDAO {

    @Override
    public List<Tsgnomtiponomina> listarTiposConceptos() {
        return findAll();
    }
}
