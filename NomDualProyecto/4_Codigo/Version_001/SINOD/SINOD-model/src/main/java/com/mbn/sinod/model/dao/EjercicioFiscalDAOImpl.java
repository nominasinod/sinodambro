package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomejercicio;
import java.util.List;

/**
 *
 * @author mipe
 */
public class EjercicioFiscalDAOImpl extends GenericDAOImpl<Tsgnomejercicio, Integer>
        implements EjercicioFiscalDAO {

    @Override
    public List<Tsgnomejercicio> listaEjercicios() {
        List<Tsgnomejercicio> conceptos = (List<Tsgnomejercicio>) getSession()
                .createQuery("From Tsgnomejercicio "
                        + "WHERE cnuValorejercicio >= EXTRACT(year FROM current_date)")
                .list();
        return conceptos;
    }
}
