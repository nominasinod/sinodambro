package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.ConcepQuincDAO;
import com.mbn.sinod.model.dto.ConcepQuincDTO;
import com.mbn.sinod.model.dto.ConcepQuincHTDTO;
import com.mbn.sinod.model.dto.ConceptosEmpleadoDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquincht;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mipe
 */
public class ConcepQuincServiceImpl extends BaseServiceImpl<Tsgnomcncptoquinc, Integer> implements ConcepQuincService {

    private static final Logger logger = Logger.getLogger(ConcepQuincService.class.getName());

    /**
     *
     * @return
     */
    @Override
    public ConcepQuincDTO listaConcepQuinc() {
        ConcepQuincDTO respuesta = new ConcepQuincDTO();

        try {
            List<Tsgnomcncptoquinc> listConQui
                    = ((ConcepQuincDAO) getGenericDAO()).listaConcepQuinc();
            if (listConQui != null) {
                respuesta.setListCncptoquinc(listConQui);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_CONCEPTO_QUIN);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
            } else {
                listConQui = new ArrayList();
                respuesta.setListCncptoquinc(listConQui);
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_CONCEPTO_QUIN);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ConcepQuincDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(Tsgnomcncptoquinc.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param empQuinc
     * @return
     */
    @Override
    public ConcepQuincDTO listaConcepQuincByEmpQuin(Integer empQuinc) {
        ConcepQuincDTO respuesta = new ConcepQuincDTO();

        try {
            List<Tsgnomcncptoquinc> concepQuinc = ((ConcepQuincDAO) getGenericDAO()).listaConcepQuincByEmpQuin(empQuinc);
            if (concepQuinc != null) {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setListCncptoquinc(concepQuinc);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_CONCEPTO_QUIN_EMP);
            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_CONCEPTO_QUIN_EMP);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ConcepQuincDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ConcepQuincServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param empQuinc
     * @return
     */
    @Override
    public ConcepQuincHTDTO listaConcepQuincByEmpQuinHT(Integer empQuinc) {
        ConcepQuincHTDTO respuesta = new ConcepQuincHTDTO();
        try {
            List<Tsgnomcncptoquincht> concepQuinc = ((ConcepQuincDAO) getGenericDAO()).listaConcepQuincByEmpQuinHT(empQuinc);
            if (concepQuinc != null) {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setListCncptoquinc(concepQuinc);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_CONCEPTO_QUIN_EMP_HT);
            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_CONCEPTO_QUIN_EMP_HT);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ConcepQuincHTDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ConcepQuincServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param empleado
     * @return
     */
    @Override
    public ConcepQuincDTO listaConceptosEmpl(Integer empleado) {
        ConcepQuincDTO respuesta = new ConcepQuincDTO();

        try {
            List<Tsgnomcncptoquinc> concepQuinc = ((ConcepQuincDAO) getGenericDAO()).listaConceptosEmpl(empleado);
            if (concepQuinc != null) {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setListCncptoquinc(concepQuinc);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_CONCEPTOS_EMP);
            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_CONCEPTOS_EMP);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ConcepQuincDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ConcepQuincServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param cabecera
     * @return
     */
    @Override
    public ConcepQuincDTO listaConcepQuincByCabecera(Integer cabecera) {
        ConcepQuincDTO respuesta = new ConcepQuincDTO();

        try {
            List<Tsgnomcncptoquinc> concepQuinc = ((ConcepQuincDAO) getGenericDAO()).listaConcepQuincByCabecera(cabecera);
            if (concepQuinc != null) {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setListCncptoquinc(concepQuinc);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_CONCEPTO_QUIN_CABECERA);
            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_CONCEPTO_QUIN_CABECERA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ConcepQuincDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ConcepQuincServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    /**
     *
     * @param idEmpleado
     * @param idCabecera
     * @return
     */
    @Override
    public ConceptosEmpleadoDTO listaConceptosEmpleado(Integer idEmpleado, Integer idCabecera) {
        ConceptosEmpleadoDTO respuesta = new ConceptosEmpleadoDTO();

        try {
            List<ConceptosEmpleadoDTO> concepQuinc = ((ConcepQuincDAO) getGenericDAO()).listaConceptosEmpleados(idEmpleado, idCabecera);
            if (concepQuinc != null) {
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);
                respuesta.setListaConceptosEmpleado(concepQuinc);
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_CONCEPTO_EMP);
            } else {
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_CONCEPTO_EMP);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ConceptosEmpleadoDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ConcepQuincServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }
}
