package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomcalculo;
import java.util.List;

/**
 *
 * @author mipe
 */
public class TipoCalculoDAOImpl extends GenericDAOImpl<Tsgnomcalculo, Integer> implements TipoCalculoDAO {

    @Override
    public List<Tsgnomcalculo> listarTiposCalculo() {
        return findAll();
    }
}
