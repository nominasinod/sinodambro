package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author mariana
 */
public class CorreoDTO extends GenericDTO {

    private Integer nom_empleado;
    private String correo;
    private List<IncidenciasQuincenaDTO> incidenciasEmpleado;
    private List<CorreoDTO> listaCorreos;
    private String incidencia;
    private String validar;

    /**
     *
     * @return
     */
    public String getIncidencia() {
        return incidencia;
    }

    /**
     *
     * @param incidencia
     */
    public void setIncidencia(String incidencia) {
        this.incidencia = incidencia;
    }

    /**
     *
     * @return
     */
    public String getValidar() {
        return validar;
    }

    /**
     *
     * @param validar
     */
    public void setValidar(String validar) {
        this.validar = validar;
    }

    /**
     *
     * @return
     */
    public Integer getNom_empleado() {
        return nom_empleado;
    }

    /**
     *
     * @param nom_empleado
     */
    public void setNom_empleado(Integer nom_empleado) {
        this.nom_empleado = nom_empleado;
    }

    /**
     *
     * @return
     */
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param Correo
     */
    public void setCorreo(String Correo) {
        this.correo = Correo;
    }

    /**
     *
     * @return
     */
    public List<IncidenciasQuincenaDTO> getIncidenciasEmpleado() {
        return incidenciasEmpleado;
    }

    /**
     *
     * @param incidenciasEmpleado
     */
    public void setIncidenciasEmpleado(List<IncidenciasQuincenaDTO> incidenciasEmpleado) {
        this.incidenciasEmpleado = incidenciasEmpleado;
    }

    /**
     *
     * @return
     */
    public List<CorreoDTO> getListaCorreos() {
        return listaCorreos;
    }

    /**
     *
     * @param listaCorreos
     */
    public void setListaCorreos(List<CorreoDTO> listaCorreos) {
        this.listaCorreos = listaCorreos;
    }

}
