package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.DesgloseEmpleadoDTO;
import com.mbn.sinod.model.dto.EmpleadosNomDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface EmpleadosNomService extends BaseService<Tsgnomempleados, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomempleados> listarEmpleadosNom();

    /**
     * Obtiene el objeto del empleado de nómina por medio del id del empleado en
     * RH
     *
     * @param idempleadonom
     * @param idempleadorh id del empleado en RH (empleado logueado)
     * @return
     */
    DesgloseEmpleadoDTO obtenerEmpleadosnomPorIdNomHT(Integer idempleadonom);

    /**
     *
     * @param empleadoRh
     * @return
     */
    EmpleadosNomDTO detallesEmpleadosPorIdRH(Integer empleadoRh);

    /**
     *
     * @param empleado
     * @return
     */
    EmpleadosNomDTO guardar(EmpleadosNomDTO empleado);

    /**
     *
     * @param idempleadonom
     * @return
     */
    Tsgnomempleados obtenerEmpleadosnomPorIdNom(Integer idempleadonom);

    /**
     *
     * @param idempleadorh
     * @return
     */
    Tsgnomempleados obtenerEmpleadonomPorIdrh(Integer idempleadorh);
}
