package com.mbn.sinod.model.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Ivette
 */
public class HistorialEmpleadosRhDTO {

    private Integer cod_empleado;
    private String fecha_ingreso;
    private String nom_empleado;
    private String des_nbarea;
    private String des_puesto;
    private String des_quincena;
    private Date fec_inicio;
    private Date fec_fin;
    private Date fec_pago;
    private BigDecimal imp_concepto;
    private Integer quincena;

    /**
     *
     * @return the quincena
     */
    public Integer getQuincena() {
        return quincena;
    }

    /**
     *
     * @param quincena the quincena to set
     */
    public void setQuincena(Integer quincena) {
        this.quincena = quincena;
    }

    /**
     *
     * @return the imp_concepto
     */
    public BigDecimal getImp_concepto() {
        return imp_concepto;
    }

    /**
     *
     * @param imp_concepto the imp_concepto to set
     */
    public void setImp_concepto(BigDecimal imp_concepto) {
        this.imp_concepto = imp_concepto;
    }

    /**
     *
     * @return the cod_empleado
     */
    public Integer getCod_empleado() {
        return cod_empleado;
    }

    /**
     *
     * @param cod_empleado the cod_empleado to set
     */
    public void setCod_empleado(Integer cod_empleado) {
        this.cod_empleado = cod_empleado;
    }

    /**
     *
     * @return the fecha_ingreso
     */
    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    /**
     *
     * @param fecha_ingreso the fecha_ingreso to set
     */
    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    /**
     *
     * @return the nom_empleado
     */
    public String getNom_empleado() {
        return nom_empleado;
    }

    /**
     *
     * @param nom_empleado the nom_empleado to set
     */
    public void setNom_empleado(String nom_empleado) {
        this.nom_empleado = nom_empleado;
    }

    /**
     *
     * @return the des_nbarea
     */
    public String getDes_nbarea() {
        return des_nbarea;
    }

    /**
     *
     * @param des_nbarea the des_nbarea to set
     */
    public void setDes_nbarea(String des_nbarea) {
        this.des_nbarea = des_nbarea;
    }

    /**
     *
     * @return the des_puesto
     */
    public String getDes_puesto() {
        return des_puesto;
    }

    /**
     *
     * @param des_puesto the des_puesto to set
     */
    public void setDes_puesto(String des_puesto) {
        this.des_puesto = des_puesto;
    }

    /**
     *
     * @return the des_quincena
     */
    public String getDes_quincena() {
        return des_quincena;
    }

    /**
     *
     * @param des_quincena the des_quincena to set
     */
    public void setDes_quincena(String des_quincena) {
        this.des_quincena = des_quincena;
    }

    /**
     *
     * @return the fec_inicio
     */
    public Date getFec_inicio() {
        return fec_inicio;
    }

    /**
     *
     * @param fec_inicio the fec_inicio to set
     */
    public void setFec_inicio(Date fec_inicio) {
        this.fec_inicio = fec_inicio;
    }

    /**
     *
     * @return the fec_fin
     */
    public Date getFec_fin() {
        return fec_fin;
    }

    /**
     *
     * @param fec_fin the fec_fin to set
     */
    public void setFec_fin(Date fec_fin) {
        this.fec_fin = fec_fin;
    }

    /**
     *
     * @return the fec_pago
     */
    public Date getFec_pago() {
        return fec_pago;
    }

    /**
     *
     * @param fec_pago the fec_pago to set
     */
    public void setFec_pago(Date fec_pago) {
        this.fec_pago = fec_pago;
    }

}
