package com.mbn.sinod.model.dto;

import java.util.List;

/**
 *
 * @author Ivette
 */
public class ValidacionAltasDTO extends GenericDTO {

    private Integer cod_empleadoid;
    private String nom_empleado;
    private String cod_rfc;
    private String cod_curp;
    private String des_nbarea;
    private String des_puesto;
    private Boolean validar;
    private Character validacion;

    private List<ValidacionAltasDTO> listaInformacionValidar;

    /**
     *
     * @return the listaInformacionValidar
     */
    public List<ValidacionAltasDTO> getListaInformacionValidar() {
        return listaInformacionValidar;
    }

    /**
     *
     * @param listaInformacionValidar the listaInformacionValidar to set
     */
    public void setListaInformacionValidar(List<ValidacionAltasDTO> listaInformacionValidar) {
        this.listaInformacionValidar = listaInformacionValidar;
    }

    /**
     *
     * @return the cod_empleadoid
     */
    public Integer getCod_empleadoid() {
        return cod_empleadoid;
    }

    /**
     *
     * @param cod_empleadoid the cod_empleadoid to set
     */
    public void setCod_empleadoid(Integer cod_empleadoid) {
        this.cod_empleadoid = cod_empleadoid;
    }

    /**
     *
     * @return the nom_empleado
     */
    public String getNom_empleado() {
        return nom_empleado;
    }

    /**
     *
     * @param nom_empleado the nom_empleado to set
     */
    public void setNom_empleado(String nom_empleado) {
        this.nom_empleado = nom_empleado;
    }

    /**
     *
     * @return the cod_rfc
     */
    public String getCod_rfc() {
        return cod_rfc;
    }

    /**
     *
     * @param cod_rfc the cod_rfc to set
     */
    public void setCod_rfc(String cod_rfc) {
        this.cod_rfc = cod_rfc;
    }

    /**
     *
     * @return the des_nbarea
     */
    public String getDes_nbarea() {
        return des_nbarea;
    }

    /**
     *
     * @param des_nbarea the des_nbarea to set
     */
    public void setDes_nbarea(String des_nbarea) {
        this.des_nbarea = des_nbarea;
    }

    /**
     *
     * @return the des_puesto
     */
    public String getDes_puesto() {
        return des_puesto;
    }

    /**
     *
     * @param des_puesto the des_puesto to set
     */
    public void setDes_puesto(String des_puesto) {
        this.des_puesto = des_puesto;
    }

    /**
     *
     * @return the validar
     */
    public Boolean getValidar() {
        return validar;
    }

    /**
     *
     * @param validar the validar to set
     */
    public void setValidar(Boolean validar) {
        this.validar = validar;
    }

    /**
     *
     * @return the validacion
     */
    public Character getValidacion() {
        return validacion;
    }

    /**
     *
     * @param validacion the validacion to set
     */
    public void setValidacion(Character validacion) {
        this.validacion = validacion;
    }

    /**
     *
     * @return the cod_curp
     */
    public String getCod_curp() {
        return cod_curp;
    }

    /**
     *
     * @param cod_curp the cod_curp to set
     */
    public void setCod_curp(String cod_curp) {
        this.cod_curp = cod_curp;
    }

}
