package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomargumento;
import java.util.List;

/**
 *
 * @author mariana
 */
public interface ArgumentoDAO extends GenericDAO<Tsgnomargumento, Integer> {

    /**
     *
     * @return
     */
    List<Tsgnomargumento> obtenerListaArgumentos();

    /**
     *
     * @return
     */
    List<Tsgnomargumento> obtenerArgumentosConstantes();

    /**
     *
     * @return
     */
    List<Tsgnomargumento> obtenerArgumentosVariables();

    /**
     *
     * @param argumento
     * @return
     */
    boolean guardarActualizarArgumento(Tsgnomargumento argumento);

    /**
     *
     * @param id
     * @return
     */
    boolean eliminarArgumento(Integer id);

}
