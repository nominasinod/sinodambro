package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.InfoHistorialRhDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;

/**
 *
 * @author Ivette
 */
public interface HistorialEmpleadosRhService extends BaseService<Tsgrhempleados, Integer> {

    /**
     *
     * @return
     */
    InfoHistorialRhDTO detallesHistorialRh();

    /**
     *
     * @param cod_empleadoid
     * @return
     */
    InfoHistorialRhDTO informacionPersonal(Integer cod_empleadoid);

}
