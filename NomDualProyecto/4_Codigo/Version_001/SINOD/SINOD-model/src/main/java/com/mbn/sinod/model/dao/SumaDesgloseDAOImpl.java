package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import com.mbn.sinod.model.entidades.Tsgnomempquincenaht;
import java.util.List;

/**
 *
 * @author mipe
 */
public class SumaDesgloseDAOImpl extends GenericDAOImpl<Tsgnomempquincena, Integer>
        implements SumaDesgloseDAO {

    @Override
    public List<Tsgnomempquincena> sumaDesglose(Integer cabecera, Integer empleado) {
        List<Tsgnomempquincena> suma = (List<Tsgnomempquincena>) getSession().createQuery("FROM Tsgnomempquincena "
                + "WHERE codEmpleadoidFk.codEmpleadoid = :parametro1 "
                + "AND codCabeceraidFk.codCabeceraid = :parametro2 "
                + "")
                .setParameter("parametro1", empleado)
                .setParameter("parametro2", cabecera)
                .list();
        return suma;
    }

    @Override
    public List<Tsgnomempquincenaht> sumaDesgloseht(Integer cabecera, Integer empleado) {
        List<Tsgnomempquincenaht> suma = (List<Tsgnomempquincenaht>) getSession().createQuery("FROM Tsgnomempquincenaht "
                + "WHERE codEmpleadoidFk.codEmpleadoid = :parametro1 "
                + "AND codCabeceraidFk.codCabeceraid = :parametro2 "
                + "")
                .setParameter("parametro1", empleado)
                .setParameter("parametro2", cabecera)
                .list();
        return suma;
    }
}
