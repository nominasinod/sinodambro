package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomincidencia;
import java.util.List;

/**
 *
 * @author Karla
 */
public class IncidenciasDAOImpl extends GenericDAOImpl<Tsgnomincidencia, Integer> implements IncidenciasDAO {

    @Override
    public List<Tsgnomincidencia> listarIncidencias() {
        return findAll();
    }

}
