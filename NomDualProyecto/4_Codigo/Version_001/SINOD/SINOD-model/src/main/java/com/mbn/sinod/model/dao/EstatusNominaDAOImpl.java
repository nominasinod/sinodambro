package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.entidades.Tsgnomestatusnom;
import java.util.List;

/**
 *
 * @author mipe
 */
public class EstatusNominaDAOImpl extends GenericDAOImpl<Tsgnomestatusnom, Integer> implements EstatusNominaDAO {

    @Override
    public List<Tsgnomestatusnom> listarEstatusNomina() {
//        return findAll();
        List<Tsgnomestatusnom> estatus = (List<Tsgnomestatusnom>) getSession().createQuery("FROM Tsgnomestatusnom "
                + "WHERE codEstatusnomid = :parametro "
                + "")
                .setParameter("parametro", 1).list();
        return estatus;
    }

}
