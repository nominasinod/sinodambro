package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.ValidacionesNominaDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface ValidacionesNominaDAO extends GenericDAO<Tsgnomempleados, Integer> {

    /**
     * Obtiene la lista de empleados dados de alta y validadados por RH
     * en el sistema de nómina, por medio de una función
     * 
     * @return
     */
    List<ValidacionesNominaDTO> detallesValidacionesNomina();
}
