package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.dto.VerInformacionRhDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import java.util.List;

/**
 *
 * @author Ivette
 */
public interface VerInformacionRhDAO extends GenericDAO<Tsgnomempleados, Integer> {

    /**
     * Obtiene la información personal por empleado de Nómina
     * por medio del id de RH
     * 
     * @param empleadoRh
     * @return
     */
    List<VerInformacionRhDTO> listaInformacionRh(Integer empleadoRh);

}
