package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dao.DetalleEmpDAO;
import com.mbn.sinod.model.dto.EmpDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author mipe
 */
public class DetalleEmpServiceImpl extends BaseServiceImpl<Tsgrhempleados, Integer>
        implements DetalleEmpService {

    @Autowired
    private DetalleEmpDAO detalleEmpDAO;

    /**
     *
     * @param cabecera
     * @return
     */
    @Override
    public EmpDTO empDTO(Integer cabecera) {
        EmpDTO dto = new EmpDTO();
        dto.setListaEmp(getDetalleEmpDAO().empleadosPorArea(cabecera));
        return dto;

    }

    /**
     * @return the detalleEmpDAO
     */
    public DetalleEmpDAO getDetalleEmpDAO() {
        return detalleEmpDAO;
    }

    /**
     * @param detalleEmpDAO the detalleEmpDAO to set
     */
    public void setDetalleEmpDAO(DetalleEmpDAO detalleEmpDAO) {
        this.detalleEmpDAO = detalleEmpDAO;
    }
}
