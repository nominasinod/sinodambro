package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.TipoConceptoDTO;
import com.mbn.sinod.model.entidades.Tsgnomtipoconcepto;

/**
 *
 * @author ambrosio
 */
public interface TipoConceptoService extends BaseService<Tsgnomtipoconcepto, Integer> {

    /**
     *
     * @return
     */
    TipoConceptoDTO listarTiposConceptos();
}
