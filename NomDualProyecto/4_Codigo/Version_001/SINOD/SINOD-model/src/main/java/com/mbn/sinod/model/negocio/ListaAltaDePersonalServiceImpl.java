package com.mbn.sinod.model.negocio;

import com.ibm.wsdl.ServiceImpl;
import com.mbn.sinod.model.comun.StaticConstantes;
import com.mbn.sinod.model.dao.ListaAltaDePersonalDAO;
import com.mbn.sinod.model.dto.ListaAltaDePersonalDTO;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ivette
 */
public class ListaAltaDePersonalServiceImpl extends BaseServiceImpl<Tsgrhempleados, Integer>
        implements ListaAltaDePersonalService {

    private static final Logger logger = Logger.getLogger(ServiceImpl.class.getName());

    /**
     *
     * @return
     */
    @Override
    public ListaAltaDePersonalDTO listaInformacionParaAltaNom() {
        ListaAltaDePersonalDTO respuesta = new ListaAltaDePersonalDTO();
        try {

            List<ListaAltaDePersonalDTO> listaEmpleadosRh = ((ListaAltaDePersonalDAO) getGenericDAO()).listaInformacionParaAltaNom();

            if (listaEmpleadosRh != null) {
                respuesta.setListaEmpleadosSinAltaNom(listaEmpleadosRh);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.EXITO_LISTAR_ALTAS_PARA_ALTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_CORRECTO);

            } else {
                respuesta.setListaEmpleadosSinAltaNom(listaEmpleadosRh);
                //declara las constantes
                respuesta.setCodigoMensaje(StaticConstantes.ERROR_LISTAR_ALTAS_PARA_ALTA);
                respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            }
        } catch (Exception e) {
            respuesta = new ListaAltaDePersonalDTO();
            respuesta.setCodigoMensaje(e.getMessage());
            respuesta.setTipoMensaje(StaticConstantes.MENSAJE_ERROR);
            logger.getLogger(ListaAltaDePersonalServiceImpl.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

}
