package com.mbn.sinod.model.negocio;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.mbn.sinod.model.dao.PuestosDAO;
import com.mbn.sinod.model.entidades.Tsgrhpuestos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mipe
 */
@Transactional(readOnly = true)
public class PuestosServiceImpl extends GenericDAOImpl<Tsgrhpuestos, Integer> implements PuestosService {

    @Autowired
    private PuestosDAO puestosDAO;

    /**
     *
     * @return
     */
    @Override
    public List<Tsgrhpuestos> listaPuestos() {
        return puestosDAO.listaPuestos();
    }

    /**
     *
     * @param puestosId
     * @return
     */
    @Override
    public Tsgrhpuestos obtenerPuestosPorId(Integer puestosId) {
        return this.puestosDAO.obtenerPuestospoId(puestosId);
    }

    /**
     * @return the puestosDAO
     */
    public PuestosDAO getPuestosDAO() {
        return puestosDAO;
    }

    /**
     * @param puestosDAO the puestosDAO to set
     */
    public void setPuestosDAO(PuestosDAO puestosDAO) {
        this.puestosDAO = puestosDAO;
    }

}
