package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomfuncion;
import java.util.List;

/**
 *
 * @author mariana
 */
public class FuncionDTO extends GenericDTO {

    private List<Tsgnomfuncion> listafunciones;
    private Tsgnomfuncion funcion;

    /**
     *
     * @return
     */
    public List<Tsgnomfuncion> getListafunciones() {
        return listafunciones;
    }

    /**
     *
     * @param listafunciones
     */
    public void setListafunciones(List<Tsgnomfuncion> listafunciones) {
        this.listafunciones = listafunciones;
    }

    /**
     *
     * @return
     */
    public Tsgnomfuncion getFuncion() {
        return funcion;
    }

    /**
     *
     * @param funcion
     */
    public void setFuncion(Tsgnomfuncion funcion) {
        this.funcion = funcion;
    }

}
