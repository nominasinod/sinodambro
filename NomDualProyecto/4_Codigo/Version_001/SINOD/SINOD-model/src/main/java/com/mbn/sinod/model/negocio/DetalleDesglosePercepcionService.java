package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.DetalleDesgloPercepDTO;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;

/**
 * /**
 *
 * @author torre
 */
public interface DetalleDesglosePercepcionService extends BaseService<Tsgnomcncptoquinc, Integer> {

    /**
     *
     * @param cod_empleado
     * @param cod_cabecera
     * @return
     */
    DetalleDesgloPercepDTO detalleDesglocePercepDTO(Integer cod_empleado, Integer cod_cabecera);
}
