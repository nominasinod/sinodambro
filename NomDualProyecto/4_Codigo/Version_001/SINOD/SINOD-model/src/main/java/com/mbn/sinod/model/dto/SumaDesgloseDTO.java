package com.mbn.sinod.model.dto;

import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import com.mbn.sinod.model.entidades.Tsgnomempquincenaht;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author torre
 */
public class SumaDesgloseDTO extends GenericDTO {

    private BigDecimal impTotpercepcion;
    private BigDecimal impTotdeduccion;
    private BigDecimal impTotalemp;
    private BigDecimal cantidad;
    private Tsgnomempquincena conceptosSum;
    private List<Tsgnomempquincena> listConceptosSum;
    private Tsgnomempquincenaht conceptosSumht;
    private List<Tsgnomempquincenaht> listConceptosSumht;

    /**
     * @return the conceptosSum
     */
    public Tsgnomempquincena getConceptosSum() {
        return conceptosSum;
    }

    /**
     * @param conceptosSum the conceptosSum to set
     */
    public void setConceptosSum(Tsgnomempquincena conceptosSum) {
        this.conceptosSum = conceptosSum;
    }

    /**
     * @return the listConceptosSum
     */
    public List<Tsgnomempquincena> getListConceptosSum() {
        return listConceptosSum;
    }

    /**
     * @param listConceptosSum the listConceptosSum to set
     */
    public void setListConceptosSum(List<Tsgnomempquincena> listConceptosSum) {
        this.listConceptosSum = listConceptosSum;
    }

    /**
     * @return the conceptosSumht
     */
    public Tsgnomempquincenaht getConceptosSumht() {
        return conceptosSumht;
    }

    /**
     * @param conceptosSumht the conceptosSumht to set
     */
    public void setConceptosSumht(Tsgnomempquincenaht conceptosSumht) {
        this.conceptosSumht = conceptosSumht;
    }

    /**
     * @return the listConceptosSumht
     */
    public List<Tsgnomempquincenaht> getListConceptosSumht() {
        return listConceptosSumht;
    }

    /**
     * @param listConceptosSumht the listConceptosSumht to set
     */
    public void setListConceptosSumht(List<Tsgnomempquincenaht> listConceptosSumht) {
        this.listConceptosSumht = listConceptosSumht;
    }

    /**
     * @return the impTotpercepcion
     */
    public BigDecimal getImpTotpercepcion() {
        return impTotpercepcion;
    }

    /**
     * @param impTotpercepcion the impTotpercepcion to set
     */
    public void setImpTotpercepcion(BigDecimal impTotpercepcion) {
        this.impTotpercepcion = impTotpercepcion;
    }

    /**
     * @return the impTotdeduccion
     */
    public BigDecimal getImpTotdeduccion() {
        return impTotdeduccion;
    }

    /**
     * @param impTotdeduccion the impTotdeduccion to set
     */
    public void setImpTotdeduccion(BigDecimal impTotdeduccion) {
        this.impTotdeduccion = impTotdeduccion;
    }

    /**
     * @return the impTotalemp
     */
    public BigDecimal getImpTotalemp() {
        return impTotalemp;
    }

    /**
     * @param impTotalemp the impTotalemp to set
     */
    public void setImpTotalemp(BigDecimal impTotalemp) {
        this.impTotalemp = impTotalemp;
    }

    /**
     * @return the cantidad
     */
    public BigDecimal getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

}
