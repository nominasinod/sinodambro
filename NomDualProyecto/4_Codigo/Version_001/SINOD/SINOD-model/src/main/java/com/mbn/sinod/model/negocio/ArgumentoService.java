package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.ArgumentoDTO;
import com.mbn.sinod.model.entidades.Tsgnomargumento;

/**
 *
 * @author mariana
 */
public interface ArgumentoService extends BaseService<Tsgnomargumento, Integer> {

    /**
     *
     * @param argumentoId
     * @return
     */
    ArgumentoDTO eliminarArgumento(Integer argumentoId);

    /**
     *
     * @param argumento
     * @return
     */
    ArgumentoDTO guardar(ArgumentoDTO argumento);

    /**
     *
     * @return
     */
    ArgumentoDTO listarArgumentosConstantes();

    /**
     *
     * @return
     */
    ArgumentoDTO listarArgumentosVariables();

    /**
     *
     * @return
     */
    ArgumentoDTO listarArgumentos();

}
