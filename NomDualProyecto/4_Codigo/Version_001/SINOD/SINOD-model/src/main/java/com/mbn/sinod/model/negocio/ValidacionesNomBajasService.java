package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.InfoValidacionesNomBajasDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;

/**
 *
 * @author Ivette
 */
public interface ValidacionesNomBajasService extends BaseService<Tsgnomempleados, Integer> {

    /**
     *
     * @return
     */
    InfoValidacionesNomBajasDTO detallesValidacionesNominaBajas();

}
