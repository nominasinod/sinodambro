package com.mbn.sinod.model.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import com.mbn.sinod.model.entidades.Tsgnomempquincenaht;
import java.util.List;

/**
 *
 * @author torre
 */
public interface SumaDesgloseDAO extends GenericDAO<Tsgnomempquincena, Integer> {

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    List<Tsgnomempquincena> sumaDesglose(Integer cabecera, Integer empleado);

    /**
     *
     * @param cabecera
     * @param empleado
     * @return
     */
    List<Tsgnomempquincenaht> sumaDesgloseht(Integer cabecera, Integer empleado);
}
