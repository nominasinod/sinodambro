package com.mbn.sinod.model.negocio;

import com.mbn.sinod.model.dto.ConceptoDTO;
import com.mbn.sinod.model.entidades.Tsgnomconcepto;

/**
 *
 * @author ambrosio
 */
public interface ConceptoService extends BaseService<Tsgnomconcepto, Integer> {

    /**
     *
     * @param conceptoId
     * @return
     */
    ConceptoDTO eliminarConcepto(Integer conceptoId);

    /**
     *
     * @param concepto
     * @return
     */
    ConceptoDTO guardarConcepto(ConceptoDTO concepto);

    /**
     *
     * @param concepto
     * @return
     */
    ConceptoDTO guardarPrioridad(ConceptoDTO concepto);

    /**
     *
     * @return
     */
    ConceptoDTO listarConceptosDeduccion();

    /**
     *
     * @return
     */
    ConceptoDTO listarConceptosPercepcion();

    /**
     *
     * @return
     */
    ConceptoDTO listarConceptos();
}
