package com.mbn.sinod.web.client;

import com.mbn.sinod.model.dto.ListaAltaDePersonalDTO;
import com.mbn.sinod.web.util.ResourceBundles;
import com.mbn.sinod.web.util.StaticsConstants;
import com.mbn.sinod.web.util.TokenController;
import com.mbn.sinod.web.util.URIBuilder;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Ivette
 */
public class ListaAltaDePersonalWSClient {

    private static final RestTemplate TEMPLATE = new RestTemplate();
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(ResourceBundles.CONFIG.getBundleName());
    private static final Logger logger = Logger.getLogger(ValidarAltasWSClient.class);

    /**
     *
     * @return
     */
    public static List<ListaAltaDePersonalDTO> listarEmpleadosParaAltas() {
        JSONObject json;
        try {
            json = new JSONObject(TokenController.getAccessToken());
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put(StaticsConstants.ACCESS_TOKEN, json.get(StaticsConstants.ACCESS_VALUE));
            //System.out.println("-->Empledos > Param: " + paramMap );
            URI uri = URIBuilder.buildWSUri(BUNDLE.getString(StaticsConstants.WS_LISTAR_EMPLEADOS_PARA_ALTA_NOM), paramMap);
            System.out.println("-->validar altas > URI: " + uri);
            ListaAltaDePersonalDTO dto = TEMPLATE.getForObject(uri, ListaAltaDePersonalDTO.class);
            //System.out.println("-->Empleados > DTO obtenido ");
            return dto.getListaEmpleadosSinAltaNom();
        } catch (JSONException | RestClientException e) {
            System.err.println("-->Error: No se pueden listar los empleados para alta");
            e.printStackTrace();
            logger.error(e.getMessage());
            return null;
        }//end try catch
    }//end listar

}
