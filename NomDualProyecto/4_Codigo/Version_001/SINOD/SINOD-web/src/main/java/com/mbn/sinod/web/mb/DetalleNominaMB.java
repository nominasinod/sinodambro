package com.mbn.sinod.web.mb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbn.sinod.model.dto.ConceptosEmpleadoDTO;
import com.mbn.sinod.model.dto.DetalleDesgloseXMLDTO;
import com.mbn.sinod.model.dto.EmpQuincenaPorCabeceraDTO;
import com.mbn.sinod.model.dto.SumaDesgloseDTO;
import com.mbn.sinod.model.entidades.Tsgnomcabecera;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquincht;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import com.mbn.sinod.model.entidades.Tsgnomempquincenaht;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import com.mbn.sinod.model.entidades.Usuario;
import com.mbn.sinod.web.client.CabecerasWSClient;
import com.mbn.sinod.web.client.ConcepQuincWSClient;
import com.mbn.sinod.web.client.SumaDesgloseWSClient;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import static org.atmosphere.annotation.AnnotationUtil.logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.ToggleEvent;
import org.w3c.dom.Document;

/**
 *
 * @author ambrosio
 */
@Named(value = "detalleNominaMB")
@ViewAccessScoped
public class DetalleNominaMB implements Serializable {

    private String titulo;
    private String mensaje;
    private Integer visible;
    private BigDecimal sueldoBruto;
    private Tsgrhempleados empleado;
    private Tsgnomcabecera cabecera;
    private Tsgnomcabecera cabeceraSelecionada;
    private List<Tsgnomcabecera> listaCabecera;
    private Usuario usuario;
    private List<EmpQuincenaPorCabeceraDTO> listaEmpleadosCabecera;
    private Integer cabeceraId;
    private List<ConceptosEmpleadoDTO> listaConcepEmpleados;
    private List<Tsgnomcncptoquinc> listaConcepQuincena;
    private List<Tsgnomcncptoquinc> listaConcepQuincenaPercep;
    private List<Tsgnomcncptoquinc> listaConcepQuincenaDeduc;
    private List<Tsgnomcncptoquincht> listaConcepQuincenaHT;
    private Document doc;
    private DetalleDesgloseXMLDTO ddxmldto;
    private ArrayList<ConceptosEmpleadoDTO> listaDetallePerc;
    private ArrayList<ConceptosEmpleadoDTO> listaDetalleDeduc;
    private List<SumaDesgloseDTO> listaSuma;
    private SumaDesgloseDTO suma;
    private List<Tsgnomempquincena> totales;
    private List<Tsgnomempquincenaht> totalesht;

    /**
     *
     */
    @PostConstruct
    public void iniciarVariables() {
        this.setEmpleado((Tsgrhempleados) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("empleado"));
        this.setUsuario((Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario"));
        verifica();
        btnVisible();
        setTitulo("");
        setMensaje("");
        try {
            setListaEmpleadosCabecera(CabecerasWSClient.listarEmpleadosCabecera(
                    getCabeceraSelecionada().getCodCabeceraid()));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(DetalleNominaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        llenarConceptos();
    }

    private void llenarConceptos() {
        for (int i = 0; i < getListaEmpleadosCabecera().size(); i++) {
            setListaConcepQuincenaDeduc(new ArrayList<>());
            setListaConcepQuincenaPercep(new ArrayList<>());
            setListaConcepQuincena(ConcepQuincWSClient
                    .listarConceptosByEmpQuin(getListaEmpleadosCabecera()
                            .get(i).getIdemp_quincena())
                    .getListCncptoquinc());

            for (int j = 0; j < getListaConcepQuincena().size(); j++) {
                if (getListaConcepQuincena().get(j).getCodConceptoidFk()
                        .getCodTipoconceptoidFk().getCodTipoconceptoid().equals(1)) {
                    getListaConcepQuincenaDeduc().add(getListaConcepQuincena().get(j));
                } else {
                    getListaConcepQuincenaPercep().add(getListaConcepQuincena().get(j));
                }
            }

            getListaEmpleadosCabecera().get(i)
                    .setConceptosDeduccionEmpleado(getListaConcepQuincenaDeduc());

            getListaEmpleadosCabecera().get(i)
                    .setConceptosPercepcionesEmpleado(getListaConcepQuincenaPercep());
        }
    }

    /**
     *
     */
    public void verifica() {
        // get cookies
        HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("mbnCmbn")) {
                    setCabeceraSelecionada(CabecerasWSClient.cabeceraPorId(Integer.parseInt(cookies[i].getValue())).getCabecera());
                }
            }
        }
    }

    /**
     *
     * @param event
     */
    public void handleToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     *
     * @param ecdto
     */
    public void detalleEmpleado(EmpQuincenaPorCabeceraDTO ecdto) {
        setSuma(new SumaDesgloseDTO());
        setListaSuma(new ArrayList<>());
        setListaConcepEmpleados(ConcepQuincWSClient.listaConceptosEmpleado(ecdto.getIdemp_nom(), getCabeceraSelecionada().getCodCabeceraid()).getListaConceptosEmpleado());
        if (getCabeceraSelecionada().getCodEstatusnomidFk().getCodEstatusnomid() != 5) {
            setTotales(SumaDesgloseWSClient.sumaDesglose(ecdto.getIdemp_nom(), getCabeceraSelecionada().getCodCabeceraid()));
            getSuma().setImpTotpercepcion(getTotales().get(0).getImpTotpercepcion());
            getSuma().setImpTotdeduccion(getTotales().get(0).getImpTotdeduccion());
            getSuma().setImpTotalemp(getTotales().get(0).getImpTotalemp());
            getListaSuma().add(getSuma());
            setSueldoBruto(getTotales().get(0).getImpTotalemp());
        } else {
            setTotalesht(SumaDesgloseWSClient.sumaDesgloseht(ecdto.getIdemp_nom(), getCabeceraSelecionada().getCodCabeceraid()));
            getSuma().setImpTotpercepcion(getTotalesht().get(0).getImpTotpercepcion());
            getSuma().setImpTotdeduccion(getTotalesht().get(0).getImpTotdeduccion());
            getSuma().setImpTotalemp(getTotalesht().get(0).getImpTotalemp());
            getListaSuma().add(getSuma());
            setSueldoBruto(getTotalesht().get(0).getImpTotalemp());
        }
        setListaDetallePerc(new ArrayList<>());
        setListaDetalleDeduc(new ArrayList<>());
        for (int i = 0; i < getListaConcepEmpleados().size(); i++) {
            System.out.println("tipo " + getListaConcepEmpleados().get(i).getTipo());
            switch (getListaConcepEmpleados().get(i).getTipo()) {
                case 0:
                case 2:
                    getListaDetallePerc().add(getListaConcepEmpleados().get(i));
                    break;
                default:
                    getListaDetalleDeduc().add(getListaConcepEmpleados().get(i));
                    break;
            }
        }
//        for (int i = 0; i < getListaConcepQuincena().size(); i++) {
//            setDdxmldto(new DetalleDesgloseXMLDTO());
//            DocumentBuilder documentBuilder = null;
//            try {
//                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//            } catch (ParserConfigurationException ex) {
//                Logger.getLogger(DetalleNominaMB.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            InputSource inputSource = new InputSource();
//            inputSource.setCharacterStream(new StringReader(getListaConcepQuincena().get(i).getXmlDesgloce()));
//            try {
//                setDoc(documentBuilder.parse(inputSource));
//                getDoc().getDocumentElement().normalize();
//                getDdxmldto().setClave(getDoc().getElementsByTagName("claveConcepto").item(0).getTextContent());
//                getDdxmldto().setNombreConcepto(getDoc().getElementsByTagName("nombreConcepto").item(0).getTextContent());
////                getDdxmldto().setUnidades(getDoc().getElementsByTagName("x").item(0).getTextContent());
//                getDdxmldto().setImporte(getDoc().getElementsByTagName("importe").item(0).getTextContent());
//                if (getListaConcepQuincena().get(i).getCodConceptoidFk().getCodTipoconceptoidFk().getCodTipoconceptoid().equals(2)) {
//                    getListaDetallePerc().add(getDdxmldto());
//                } else {
//                    getListaDetalleDeduc().add(getDdxmldto());
//                }
//            } catch (SAXException | IOException ex) {
//                Logger.getLogger(DetalleNominaMB.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    private void btnVisible() {
        switch (getCabeceraSelecionada().getCodEstatusnomidFk().getCodEstatusnomid()) {
            case 1:
                setVisible(1);
                break;
            case 2:
                setVisible(2);
                break;
            case 3:
                setVisible(3);
                break;
            case 4:
                setVisible(4);
                break;
            case 5:
                setVisible(5);
                break;
            default:
                break;
        }
    }

    /**
     *
     */
    public void mostrarCalcular() {
        setTitulo("Calcular nómina");
        setMensaje("¿Desea calcular la nómina de la " + getCabeceraSelecionada().getCodNbnomina() + "?");
        if (getVisible() == 2) {
            setVisible(1);
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg').show();");
    }

    /**
     *
     */
    public void mostrarValidar() {
        setTitulo("Validar nómina");
        setMensaje("¿Desea enviar a proceso de validación " + getCabeceraSelecionada().getCodNbnomina() + "?");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg').show();");
    }

    /**
     *
     */
    public void cerrarNom() {
        setTitulo("Cerrar nómina");
        setMensaje("¿Desea cerrar la nómina " + getCabeceraSelecionada().getCodNbnomina() + "?");
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg').show();");
    }

    /**
     *
     * @throws JsonProcessingException
     */
    public void calcular() throws JsonProcessingException {
        if (CabecerasWSClient.calculoNomina(getCabeceraSelecionada().getCodCabeceraid()).getConfirmacion()) {
            setCabeceraSelecionada(CabecerasWSClient.cabeceraPorId(getCabeceraSelecionada().getCodCabeceraid()).getCabecera());
            setVisible(2);
            generarMensaje("Exito", FacesMessage.SEVERITY_INFO);
        } else {
            generarMensaje("Error", FacesMessage.SEVERITY_ERROR);
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg').hide();");
    }

    /**
     *
     */
    public void validar() {
        if (CabecerasWSClient.validaPagosNomina(getCabeceraSelecionada().getCodCabeceraid()).getConfirmacion()) {
            setVisible(3);
            generarMensaje("Exito", FacesMessage.SEVERITY_INFO);
        } else {
            generarMensaje("Error verifique la nomina", FacesMessage.SEVERITY_ERROR);
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg').hide();");

    }

    /**
     *
     */
    public void cerrar() {
        if (CabecerasWSClient.cerrarNomina(getCabeceraSelecionada().getCodCabeceraid()).getConfirmacion()) {
            setVisible(4);
            generarMensaje("Exito", FacesMessage.SEVERITY_INFO);
        } else {
            generarMensaje("Error al cerrar la nomina", FacesMessage.SEVERITY_ERROR);
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg').hide();");
    }

    /**
     *
     * @param mensaje
     * @param sever
     */
    public void generarMensaje(String mensaje, FacesMessage.Severity sever) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(sever, "Mensaje: ", mensaje));
    }

    private Map<String, Object> datosParametros(String ubicacionHeaderImage, String ubicacionFooterImage, EmpQuincenaPorCabeceraDTO empleadoDatos) {
        //inicia contenido de reporte 
        List<EmpQuincenaPorCabeceraDTO> datos = new ArrayList<>();
        datos.add(empleadoDatos);
        JRBeanCollectionDataSource empleados = new JRBeanCollectionDataSource(datos);
        detalleEmpleado(empleadoDatos);
        JRBeanCollectionDataSource persepciones = new JRBeanCollectionDataSource(getListaDetallePerc());
        JRBeanCollectionDataSource deducciones = new JRBeanCollectionDataSource(getListaDetalleDeduc());
        JRBeanCollectionDataSource total = new JRBeanCollectionDataSource(getListaSuma());
        //JRBeanCollectionDataSource ordenPorClienteServicio = new JRBeanCollectionDataSource(DetalleOrdenClienteWSClient.detalleOrdenCliente(getClienteSeleccionado().getClienteid()));
        //Termina el contenido
        //parametros de contenido 
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("urlHeaderImage", ubicacionHeaderImage);
        parametros.put("urlFooterImage", ubicacionFooterImage);
        parametros.put("detalleEmpleados", empleados);
        parametros.put("detallePersepciones", persepciones);
        parametros.put("detalleDeducciones", deducciones);
        parametros.put("detalleTotal", total);

        parametros.put("cabecera", getCabeceraSelecionada().getCodNbnomina());
        parametros.put("tipo", getCabeceraSelecionada().getCodTiponominaidFk().getCodNomina());
        parametros.put("totalEmpleados", getCabeceraSelecionada().getCnuTotalemp());
        parametros.put("fechaInicio", getCabeceraSelecionada().getCodQuincenaidFk().getFecInicio());
        parametros.put("fechaFin", getCabeceraSelecionada().getCodQuincenaidFk().getFecFin());
        parametros.put("fechaPago", getCabeceraSelecionada().getCodQuincenaidFk().getFecPago());
        parametros.put("totalPerc", getCabeceraSelecionada().getImpTotpercepcion());
        parametros.put("totalDeduc", getCabeceraSelecionada().getImpTotdeduccion());
        parametros.put("importeTotal", getCabeceraSelecionada().getImpTotalemp());
        //terminan parametros
        return parametros;
    }

    /**
     *
     */
    public void reporteNomina() {
        try {
            //inicio ruta del reporte 
            //Carpeta
            String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/") + File.separator + "reports";

            //Nombre del documento
            String ubicacionPlantilla = reportPath + File.separator + "ReporteNominaGeneral.jasper";

            //Crear objeto de java 
            File jasper = new File(ubicacionPlantilla);

            //ruta de imagenes
            String ubicacionHeaderImage = reportPath + File.separator + "images" + File.separator + "Encabezado.png";

            String ubicacionFooterImage = reportPath + File.separator + "images" + File.separator + "Pie.png";

            //Termina ruta del reporte
            Map<String, Object> parametros = datosParametros(ubicacionHeaderImage, ubicacionFooterImage, getListaEmpleadosCabecera().get(0));

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, new JREmptyDataSource());
            for (int i = 1; i < getListaEmpleadosCabecera().size(); i++) {
                parametros = datosParametros(ubicacionHeaderImage, ubicacionFooterImage, getListaEmpleadosCabecera().get(i));
                JasperPrint jasperPrint_next = JasperFillManager.fillReport(jasper.getPath(), parametros, new JREmptyDataSource());
                List pages = jasperPrint_next.getPages();
                for (int j = 0; j < pages.size(); j++) {
                    JRPrintPage object = (JRPrintPage) pages.get(j);
                    jasperPrint.addPage(object);
                }
            }

            //inicia la construccion del reporte
//            byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), parametros, new JREmptyDataSource());
            byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);

            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.setContentType("application/pdf");
            response.setContentLength(bytes.length);
            try (ServletOutputStream outStream = response.getOutputStream()) {
                outStream.write(bytes, 0, bytes.length);

                outStream.flush();
            }

            FacesContext.getCurrentInstance().responseComplete();

        } catch (IOException | JRException ioe) {
            logger.error(" Error creando reporte, causado por: " + ioe);
            java.util.logging.Logger.getLogger(DetalleNominaMB.class.getName()).log(Level.SEVERE, null, ioe);
        }
    }

    /**
     * @return the empleado
     */
    public Tsgrhempleados getEmpleado() {
        return empleado;
    }

    /**
     * @param empleado the empleado to set
     */
    public void setEmpleado(Tsgrhempleados empleado) {
        this.empleado = empleado;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the cabecera
     */
    public Tsgnomcabecera getCabecera() {
        return cabecera;
    }

    /**
     * @param cabecera the cabecera to set
     */
    public void setCabecera(Tsgnomcabecera cabecera) {
        this.cabecera = cabecera;
    }

    /**
     * @return the cabeceraSelecionada
     */
    public Tsgnomcabecera getCabeceraSelecionada() {
        return cabeceraSelecionada;
    }

    /**
     * @param cabeceraSelecionada the cabeceraSelecionada to set
     */
    public void setCabeceraSelecionada(Tsgnomcabecera cabeceraSelecionada) {
        this.cabeceraSelecionada = cabeceraSelecionada;
    }

    /**
     * @return the listaCabecera
     */
    public List<Tsgnomcabecera> getListaCabecera() {
        return listaCabecera;
    }

    /**
     * @param listaCabecera the listaCabecera to set
     */
    public void setListaCabecera(List<Tsgnomcabecera> listaCabecera) {
        this.listaCabecera = listaCabecera;
    }

    /**
     * @return the visible
     */
    public Integer getVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    /**
     *
     * @return
     */
    public List<EmpQuincenaPorCabeceraDTO> getListaEmpleadosCabecera() {
        return listaEmpleadosCabecera;
    }

    /**
     *
     * @param listaEmpleadosCabecera
     */
    public void setListaEmpleadosCabecera(List<EmpQuincenaPorCabeceraDTO> listaEmpleadosCabecera) {
        this.listaEmpleadosCabecera = listaEmpleadosCabecera;
    }

    /**
     *
     * @return
     */
    public Integer getCabeceraId() {
        return cabeceraId;
    }

    /**
     *
     * @param cabeceraId
     */
    public void setCabeceraId(Integer cabeceraId) {
        this.cabeceraId = cabeceraId;
    }

    /**
     * @return the listaConcepQuincena
     */
    public List<Tsgnomcncptoquinc> getListaConcepQuincena() {
        return listaConcepQuincena;
    }

    /**
     * @param listaConcepQuincena the listaConcepQuincena to set
     */
    public void setListaConcepQuincena(List<Tsgnomcncptoquinc> listaConcepQuincena) {
        this.listaConcepQuincena = listaConcepQuincena;
    }

    /**
     * @return the listaConcepQuincenaHT
     */
    public List<Tsgnomcncptoquincht> getListaConcepQuincenaHT() {
        return listaConcepQuincenaHT;
    }

    /**
     * @param listaConcepQuincenaHT the listaConcepQuincenaHT to set
     */
    public void setListaConcepQuincenaHT(List<Tsgnomcncptoquincht> listaConcepQuincenaHT) {
        this.listaConcepQuincenaHT = listaConcepQuincenaHT;
    }

    /**
     * @return the doc
     */
    public Document getDoc() {
        return doc;
    }

    /**
     * @param doc the doc to set
     */
    public void setDoc(Document doc) {
        this.doc = doc;
    }

    /**
     * @return the ddxmldto
     */
    public DetalleDesgloseXMLDTO getDdxmldto() {
        return ddxmldto;
    }

    /**
     * @param ddxmldto the ddxmldto to set
     */
    public void setDdxmldto(DetalleDesgloseXMLDTO ddxmldto) {
        this.ddxmldto = ddxmldto;
    }

    /**
     * @return the listaDetallePerc
     */
    public ArrayList<ConceptosEmpleadoDTO> getListaDetallePerc() {
        return listaDetallePerc;
    }

    /**
     * @param listaDetallePerc the listaDetallePerc to set
     */
    public void setListaDetallePerc(ArrayList<ConceptosEmpleadoDTO> listaDetallePerc) {
        this.listaDetallePerc = listaDetallePerc;
    }

    /**
     * @return the listaDetalleDeduc
     */
    public ArrayList<ConceptosEmpleadoDTO> getListaDetalleDeduc() {
        return listaDetalleDeduc;
    }

    /**
     * @param listaDetalleDeduc the listaDetalleDeduc to set
     */
    public void setListaDetalleDeduc(ArrayList<ConceptosEmpleadoDTO> listaDetalleDeduc) {
        this.listaDetalleDeduc = listaDetalleDeduc;
    }

    /**
     * @return the totales
     */
    public List<Tsgnomempquincena> getTotales() {
        return totales;
    }

    /**
     * @param totales the totales to set
     */
    public void setTotales(List<Tsgnomempquincena> totales) {
        this.totales = totales;
    }

    /**
     * @return the sueldoBruto
     */
    public BigDecimal getSueldoBruto() {
        return sueldoBruto;
    }

    /**
     * @param sueldoBruto the sueldoBruto to set
     */
    public void setSueldoBruto(BigDecimal sueldoBruto) {
        this.sueldoBruto = sueldoBruto;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the listaConcepQuincenaPercep
     */
    public List<Tsgnomcncptoquinc> getListaConcepQuincenaPercep() {
        return listaConcepQuincenaPercep;
    }

    /**
     * @param listaConcepQuincenaPercep the listaConcepQuincenaPercep to set
     */
    public void setListaConcepQuincenaPercep(List<Tsgnomcncptoquinc> listaConcepQuincenaPercep) {
        this.listaConcepQuincenaPercep = listaConcepQuincenaPercep;
    }

    /**
     * @return the listaConcepQuincenaDeduc
     */
    public List<Tsgnomcncptoquinc> getListaConcepQuincenaDeduc() {
        return listaConcepQuincenaDeduc;
    }

    /**
     * @param listaConcepQuincenaDeduc the listaConcepQuincenaDeduc to set
     */
    public void setListaConcepQuincenaDeduc(List<Tsgnomcncptoquinc> listaConcepQuincenaDeduc) {
        this.listaConcepQuincenaDeduc = listaConcepQuincenaDeduc;
    }

    /**
     * @return the listaConcepEmpleados
     */
    public List<ConceptosEmpleadoDTO> getListaConcepEmpleados() {
        return listaConcepEmpleados;
    }

    /**
     * @param listaConcepEmpleados the listaConcepEmpleados to set
     */
    public void setListaConcepEmpleados(List<ConceptosEmpleadoDTO> listaConcepEmpleados) {
        this.listaConcepEmpleados = listaConcepEmpleados;
    }

    /**
     * @return the totalesht
     */
    public List<Tsgnomempquincenaht> getTotalesht() {
        return totalesht;
    }

    /**
     * @param totalesht the totalesht to set
     */
    public void setTotalesht(List<Tsgnomempquincenaht> totalesht) {
        this.totalesht = totalesht;
    }

    /**
     * @return the listaSuma
     */
    public List<SumaDesgloseDTO> getListaSuma() {
        return listaSuma;
    }

    /**
     * @param listaSuma the listaSuma to set
     */
    public void setListaSuma(List<SumaDesgloseDTO> listaSuma) {
        this.listaSuma = listaSuma;
    }

    /**
     * @return the suma
     */
    public SumaDesgloseDTO getSuma() {
        return suma;
    }

    /**
     * @param suma the suma to set
     */
    public void setSuma(SumaDesgloseDTO suma) {
        this.suma = suma;
    }
}
