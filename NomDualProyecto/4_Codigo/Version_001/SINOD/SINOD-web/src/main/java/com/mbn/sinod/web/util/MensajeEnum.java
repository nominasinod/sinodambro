package com.mbn.sinod.web.util;

import javax.faces.application.FacesMessage;

/**
 * @author Annel Clase para retornar mensajes en pantalla dependiendo del tipo
 * de mensaje
 */
public class MensajeEnum {

    /**
     *
     * @param numero
     * @return
     */
    public static FacesMessage.Severity mensaje(int numero) {
        switch (numero) {
            case 1:
                return FacesMessage.SEVERITY_ERROR;
            case 2:
                return FacesMessage.SEVERITY_WARN;
            case 3:
                return FacesMessage.SEVERITY_INFO;
            default:
                return FacesMessage.SEVERITY_FATAL;
        }
    }
}
