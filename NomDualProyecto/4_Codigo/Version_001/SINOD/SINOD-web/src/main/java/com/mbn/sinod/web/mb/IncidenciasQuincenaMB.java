package com.mbn.sinod.web.mb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbn.sinod.model.dto.CorreoDTO;
import com.mbn.sinod.model.dto.IncidenciasQuincenaDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import com.mbn.sinod.model.entidades.Tsgnomquincena;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import com.mbn.sinod.model.entidades.Usuario;
import com.mbn.sinod.web.client.EmpleadosNomWSClient;
import com.mbn.sinod.web.client.EnviarCorreoWSClient;
import com.mbn.sinod.web.client.IncidenciasQuincenaWSClient;
import com.mbn.sinod.web.client.QuincenasWSClient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mariana
 */
@Named(value = "incidenciasQuincenaMB")
@ViewAccessScoped
public class IncidenciasQuincenaMB implements Serializable {

    private List<IncidenciasQuincenaDTO> listaIncidenciasQuincena;
    private IncidenciasQuincenaDTO incidenciaSeleccionada;
    private Boolean validar;
    private List<IncidenciasQuincenaDTO> listaIncidenciasValidar;
    private List<CorreoDTO> listaCorreos;
    private List<IncidenciasQuincenaDTO> lista;
    private Tsgnomquincena quincenaActual;
    private String desQuincenaActual;
    private SimpleDateFormat format;
    private String nompantalla;
    private char accion;

    private Tsgrhempleados empleadoLogeado;
    private Usuario usuario;
    private Tsgnomempleados empleadoNom;

    /**
     *
     */
    @PostConstruct
    public void iniciarVariables() {
        setListaIncidenciasValidar(new ArrayList<>());
        setListaIncidenciasQuincena(new ArrayList<>());
        setValidar(true);
        this.setEmpleadoLogeado((Tsgrhempleados) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("empleado"));
        this.usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        switch (getUsuario().getCod_rol()) {
            case 1:
                setAccion('v');
                setNompantalla("Validar incidencias");
                setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.listarIncidenciasQuincena());
                break;
            case 2:
            case 4:
                setAccion('a');
                setNompantalla("Autorizar incidencias");
                setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.incidenciasQuincenaPorArea(getEmpleadoLogeado().getCodPuesto().getCodArea().getCodArea()));
                break;
            default:
                generarMensaje("No tiene permiso para acceder a la información", FacesMessage.SEVERITY_ERROR);
                break;
        }
        setEmpleadoNom(EmpleadosNomWSClient.obtenerEmpleadonomPorIdrh(getEmpleadoLogeado().getCodEmpleado()).getEmpleado());
        setQuincenaActual(QuincenasWSClient.quincenaActual().getQuincena());
        this.format = new SimpleDateFormat("dd/MM/yyyy");
        setDesQuincenaActual("Del " + format.format(getQuincenaActual().getFecInicio()) + " al " + format.format(getQuincenaActual().getFecFin()));
    }

    /**
     *
     * @param seleccionado
     */
    public void verDetalleIncidencia(IncidenciasQuincenaDTO seleccionado) {
        setIncidenciaSeleccionada(seleccionado);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('detalleIncidencia').show();");
    }

    /**
     *
     */
    public void dialogoValidarIncidencias() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogValidarIncidencias').show();");
    }

    /**
     *
     */
    public void dialogoRechazarIncidencias() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dialogRechazarIncidencias').show();");
    }

    /**
     *
     * @throws JsonProcessingException
     */
    public void validarTodas() throws JsonProcessingException {
        for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
            getListaIncidenciasQuincena().get(i).setAutoriza(getEmpleadoNom().getCodEmpleadoid());//sgnom
            getListaIncidenciasQuincena().get(i).setModifica(getEmpleadoLogeado().getCodEmpleado());//sgrh
        }
        Boolean validarTodas = true;
        for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
            if (getListaIncidenciasQuincena().get(i).getImporte().compareTo(BigDecimal.ZERO) > 0) {
                validarTodas = true;
            } else {
                validarTodas = false;
                break;
            }
        }
        IncidenciasQuincenaDTO dtoRespuesta = new IncidenciasQuincenaDTO();
        dtoRespuesta.setListaIncidencias(getListaIncidenciasQuincena());
        if (validarTodas) {
            try {
                IncidenciasQuincenaWSClient.validarTodas(dtoRespuesta);
                generarMensaje("Incidencias validadas", FacesMessage.SEVERITY_INFO);
            } catch (JsonProcessingException ex) {
                Logger.getLogger(IncidenciasQuincenaMB.class.getName()).log(Level.SEVERE, null, ex);
                generarMensaje("Error al validar", FacesMessage.SEVERITY_ERROR);
            }
            getListaIncidenciasQuincena().clear();
            setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.listarIncidenciasQuincena());
        } else {
            generarMensaje("No puede autorizar incidencias sin monto asignado.\nPor favor actualice el importe de todas las incidencias antes de autorizar.", FacesMessage.SEVERITY_INFO);
        }
    }

    /**
     *
     */
    public void rechazarTodas() {
        for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
            getListaIncidenciasQuincena().get(i).setAutoriza(getEmpleadoNom().getCodEmpleadoid());//sgnom
            getListaIncidenciasQuincena().get(i).setModifica(getEmpleadoLogeado().getCodEmpleado());//sgrh
        }
        IncidenciasQuincenaDTO dtoRespuesta = new IncidenciasQuincenaDTO();
        dtoRespuesta.setListaIncidencias(getListaIncidenciasQuincena());
        try {
            IncidenciasQuincenaWSClient.rechazarTodas(dtoRespuesta);
            generarMensaje("Incidencias rechazadas.", FacesMessage.SEVERITY_INFO);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(IncidenciasQuincenaMB.class.getName()).log(Level.SEVERE, null, ex);
            generarMensaje("Error al rechazar.", FacesMessage.SEVERITY_ERROR);
        }
        getListaIncidenciasQuincena().clear();
        setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.listarIncidenciasQuincena());
    }

    /**
     *
     * @param bandera
     * @param index
     * @param accion
     */
    public void validarIncidencias(int bandera, int index, char accion) {
        getListaIncidenciasQuincena().get(index).setAutoriza(getEmpleadoNom().getCodEmpleadoid());//sgnom
        getListaIncidenciasQuincena().get(index).setModifica(getEmpleadoLogeado().getCodEmpleado());//sgrh
        setListaIncidenciasValidar(getListaIncidenciasQuincena());
        if (bandera == 0 && accion == 'v') {
            getListaIncidenciasValidar().get(index).setValidacion(true);
            getListaIncidenciasQuincena().get(index).setValidacion(true);
        } else if (bandera == 1 && accion == 'v') {
            getListaIncidenciasValidar().get(index).setValidacion(false);
            getListaIncidenciasQuincena().get(index).setValidacion(false);
        } else if (bandera == 0 && accion == 'a') {
            if (getListaIncidenciasValidar().get(index).getImporte().compareTo(BigDecimal.ZERO) > 0) {
                getListaIncidenciasValidar().get(index).setAceptacion(true);
                getListaIncidenciasQuincena().get(index).setAceptacion(true);
            } else {
                generarMensaje("No puede autorizar una incidencia sin importe asignado.", FacesMessage.SEVERITY_INFO);
            }
        } else if (bandera == 1 && accion == 'a') {
            getListaIncidenciasValidar().get(index).setAceptacion(false);
            getListaIncidenciasQuincena().get(index).setAceptacion(false);
        } else {
            System.out.println("ERROR VALIDAR INCIDENCIAS MB: asignación de validación");
        }
    }

    /**
     *
     */
    public void enviarCorrreos() {
        try {
            IncidenciasQuincenaDTO dtoRespuesta = new IncidenciasQuincenaDTO();
            dtoRespuesta.setListaIncidencias(getListaIncidenciasQuincena());
            EnviarCorreoWSClient.enviarCorreos(dtoRespuesta);
            generarMensaje("Los correos han sido enviados", FacesMessage.SEVERITY_INFO);

        } catch (JsonProcessingException ex) {
            generarMensaje("Error al enviar los correos", FacesMessage.SEVERITY_ERROR);
        }

    }

    /**
     *
     */
    public void guardarIncidencias() {
        IncidenciasQuincenaDTO dtoRespuesta = new IncidenciasQuincenaDTO();
        Boolean validarGuardar = true;
        Boolean autorizarGuardar = true;

        switch (getAccion()) {
            case 'v':
                for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
                    if (getListaIncidenciasQuincena().get(i).getValidacion() != null) {
                        validarGuardar = true;
                    } else {
                        validarGuardar = false;
                        break;
                    }
                }
                if (validarGuardar) {
                    dtoRespuesta.setListaIncidencias(getListaIncidenciasQuincena());
                    try {
                        IncidenciasQuincenaWSClient.validarIncidencias(dtoRespuesta);
                        generarMensaje("Se han guardado los datos.", FacesMessage.SEVERITY_INFO);
                    } catch (JsonProcessingException ex) {
                        Logger.getLogger(IncidenciasQuincenaMB.class.getName()).log(Level.SEVERE, null, ex);
                        generarMensaje("Error al guardar incidencias.", FacesMessage.SEVERITY_ERROR);
                    }
                    getListaIncidenciasQuincena().clear();
                    setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.listarIncidenciasQuincena());
                } else {
                    generarMensaje("Por favor actualice el estatus de todas las incidencias antes de guardar.", FacesMessage.SEVERITY_INFO);
                }
                break;
            case 'a':
                for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
                    if (getListaIncidenciasQuincena().get(i).getAceptacion() != null) {
                        autorizarGuardar = true;
                    } else {
                        autorizarGuardar = false;
                        break;
                    }
                }
                if (autorizarGuardar) {
                    dtoRespuesta.setListaIncidencias(getListaIncidenciasQuincena());
                    try {
                        IncidenciasQuincenaWSClient.autorizarIncidencias(dtoRespuesta);
                        generarMensaje("Se han guardado los datos.", FacesMessage.SEVERITY_INFO);
                    } catch (JsonProcessingException ex) {
                        Logger.getLogger(IncidenciasQuincenaMB.class.getName()).log(Level.SEVERE, null, ex);
                        generarMensaje("Error al guardar incidencias.", FacesMessage.SEVERITY_ERROR);
                    }
                    getListaIncidenciasQuincena().clear();
                    setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.incidenciasQuincenaPorArea(getEmpleadoLogeado().getCodPuesto().getCodArea().getCodArea()));
                } else {
                    generarMensaje("Por favor actualice el estatus de todas las incidencias antes de guardar.", FacesMessage.SEVERITY_INFO);
                }
                break;
            default:
                generarMensaje("Error al guardar.", FacesMessage.SEVERITY_FATAL);
                break;
        }
    }

    /**
     *
     * @throws JsonProcessingException
     */
    public void autorizarTodas() throws JsonProcessingException {
        for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
            getListaIncidenciasQuincena().get(i).setModifica(getEmpleadoLogeado().getCodEmpleado());//sgrh
        }
        Boolean autorizarTodas = true;
        for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
            if (getListaIncidenciasQuincena().get(i).getImporte().compareTo(BigDecimal.ZERO) > 0) {
                autorizarTodas = true;
            } else {
                autorizarTodas = false;
                break;
            }
        }
        IncidenciasQuincenaDTO dtoRespuesta = new IncidenciasQuincenaDTO();
        dtoRespuesta.setListaIncidencias(getListaIncidenciasQuincena());
        if (autorizarTodas) {
            try {
                IncidenciasQuincenaWSClient.autorizarTodas(dtoRespuesta);
                generarMensaje("Incidencias autorizadas.", FacesMessage.SEVERITY_INFO);
            } catch (JsonProcessingException ex) {
                Logger.getLogger(IncidenciasQuincenaMB.class.getName()).log(Level.SEVERE, null, ex);
                generarMensaje("Error al autorizar", FacesMessage.SEVERITY_ERROR);
            }
            getListaIncidenciasQuincena().clear();
            setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.incidenciasQuincenaPorArea(getEmpleadoLogeado().getCodPuesto().getCodArea().getCodArea()));
        } else {
            generarMensaje("No puede autorizar incidencias sin monto asignado.\nPor favor actualice el importe de todas las incidencias antes de autorizar.", FacesMessage.SEVERITY_INFO);
        }
    }

    /**
     *
     */
    public void denegarTodas() {
        for (int i = 0; i < getListaIncidenciasQuincena().size(); i++) {
            getListaIncidenciasQuincena().get(i).setModifica(getEmpleadoLogeado().getCodEmpleado());//sgrh
        }
        IncidenciasQuincenaDTO dtoRespuesta = new IncidenciasQuincenaDTO();
        dtoRespuesta.setListaIncidencias(getListaIncidenciasQuincena());
        try {
            IncidenciasQuincenaWSClient.denegarTodas(dtoRespuesta);
            generarMensaje("Incidencias denegadas.", FacesMessage.SEVERITY_INFO);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(IncidenciasQuincenaMB.class.getName()).log(Level.SEVERE, null, ex);
            generarMensaje("Error al denegar", FacesMessage.SEVERITY_ERROR);
        }
        getListaIncidenciasQuincena().clear();
        setListaIncidenciasQuincena(IncidenciasQuincenaWSClient.incidenciasQuincenaPorArea(getEmpleadoLogeado().getCodPuesto().getCodArea().getCodArea()));
    }

    /**
     *
     * @param mensaje
     * @param sever
     */
    public void generarMensaje(String mensaje, FacesMessage.Severity sever) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(sever, "Mensaje: ", mensaje));
    }

    /**
     *
     * @return
     */
    public List<IncidenciasQuincenaDTO> getListaIncidenciasQuincena() {
        return listaIncidenciasQuincena;
    }

    /**
     *
     * @param listaIncidenciasQuincena
     */
    public void setListaIncidenciasQuincena(List<IncidenciasQuincenaDTO> listaIncidenciasQuincena) {
        this.listaIncidenciasQuincena = listaIncidenciasQuincena;
    }

    /**
     *
     * @return
     */
    public IncidenciasQuincenaDTO getIncidenciaSeleccionada() {
        return incidenciaSeleccionada;
    }

    /**
     *
     * @param incidenciaSeleccionada
     */
    public void setIncidenciaSeleccionada(IncidenciasQuincenaDTO incidenciaSeleccionada) {
        this.incidenciaSeleccionada = incidenciaSeleccionada;
    }

    /**
     *
     * @return
     */
    public Boolean getValidar() {
        return validar;
    }

    /**
     *
     * @param validar
     */
    public void setValidar(Boolean validar) {
        this.validar = validar;
    }

    /**
     *
     * @return
     */
    public List<IncidenciasQuincenaDTO> getListaIncidenciasValidar() {
        return listaIncidenciasValidar;
    }

    /**
     *
     * @param listaIncidenciasValidar
     */
    public void setListaIncidenciasValidar(List<IncidenciasQuincenaDTO> listaIncidenciasValidar) {
        this.listaIncidenciasValidar = listaIncidenciasValidar;
    }

    /**
     *
     * @return
     */
    public List<CorreoDTO> getListaCorreos() {
        return listaCorreos;
    }

    /**
     *
     * @param listaCorreos
     */
    public void setListaCorreos(List<CorreoDTO> listaCorreos) {
        this.listaCorreos = listaCorreos;
    }

    /**
     *
     * @return
     */
    public List<IncidenciasQuincenaDTO> getLista() {
        return lista;
    }

    /**
     *
     * @param lista
     */
    public void setLista(List<IncidenciasQuincenaDTO> lista) {
        this.lista = lista;
    }

    /**
     *
     * @return
     */
    public Tsgrhempleados getEmpleadoLogeado() {
        return empleadoLogeado;
    }

    /**
     *
     * @param empleadoLogeado
     */
    public void setEmpleadoLogeado(Tsgrhempleados empleadoLogeado) {
        this.empleadoLogeado = empleadoLogeado;
    }

    /**
     *
     * @return
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     *
     * @param usuario
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     *
     * @return
     */
    public char getAccion() {
        return accion;
    }

    /**
     *
     * @param accion
     */
    public void setAccion(char accion) {
        this.accion = accion;
    }

    /**
     *
     * @return
     */
    public Tsgnomempleados getEmpleadoNom() {
        return empleadoNom;
    }

    /**
     *
     * @param empleadoNom
     */
    public void setEmpleadoNom(Tsgnomempleados empleadoNom) {
        this.empleadoNom = empleadoNom;
    }

    /**
     *
     * @return
     */
    public Tsgnomquincena getQuincenaActual() {
        return quincenaActual;
    }

    /**
     *
     * @param quincenaActual
     */
    public void setQuincenaActual(Tsgnomquincena quincenaActual) {
        this.quincenaActual = quincenaActual;
    }

    /**
     *
     * @return
     */
    public String getDesQuincenaActual() {
        return desQuincenaActual;
    }

    /**
     *
     * @param desQuincenaActual
     */
    public void setDesQuincenaActual(String desQuincenaActual) {
        this.desQuincenaActual = desQuincenaActual;
    }

    /**
     *
     * @return
     */
    public String getNompantalla() {
        return nompantalla;
    }

    /**
     *
     * @param nompantalla
     */
    public void setNompantalla(String nompantalla) {
        this.nompantalla = nompantalla;
    }

}
