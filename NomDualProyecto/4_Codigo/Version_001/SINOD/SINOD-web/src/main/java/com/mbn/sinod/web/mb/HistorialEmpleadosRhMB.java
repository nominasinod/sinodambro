package com.mbn.sinod.web.mb;

import com.mbn.sinod.model.dto.DetalleDesgloceAguinaldoXMLDTO;
import com.mbn.sinod.model.dto.DetalleDesgloseXMLDTO;
import com.mbn.sinod.model.dto.EmpleadosCabeceraDTO;
import com.mbn.sinod.model.dto.HistorialEmpleadosRhDTO;
import com.mbn.sinod.model.dto.InfoHistorialRhDTO;
import com.mbn.sinod.model.dto.VerInformacionDePersonalDTO;
import com.mbn.sinod.model.entidades.Tsgnomaguinaldo;
import com.mbn.sinod.model.entidades.Tsgnomcncptoquinc;
import com.mbn.sinod.model.entidades.Tsgnomempquincena;
import com.mbn.sinod.web.client.ConcepQuincWSClient;
import com.mbn.sinod.web.client.DesgloceAguinaldoWSClient;
import com.mbn.sinod.web.client.HistorialEmpleadosRhWSClient;
import com.mbn.sinod.web.client.SumaDesgloseWSClient;
import com.mbn.sinod.web.client.VerInformacionDePersonalWSClient;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Ivette
 */
@Named(value = "historialEmpleadosRhMB")
@ViewAccessScoped
public class HistorialEmpleadosRhMB implements Serializable {

    private List<HistorialEmpleadosRhDTO> historialEmpleadosRh;
    HistorialEmpleadosRhDTO empleadoSeleccionado;
    private VerInformacionDePersonalDTO detalleSeleccionado;
    InfoHistorialRhDTO infoEmpleado;
    private Integer cod_empleado;
    private List<Tsgnomcncptoquinc> listaConcepQuincena;
    private List<Tsgnomaguinaldo> listaDesgloceAguinaldo;
    private ArrayList<DetalleDesgloseXMLDTO> listaDdxmldto;
    private ArrayList<DetalleDesgloceAguinaldoXMLDTO> listaDaxmldto;
    private DetalleDesgloseXMLDTO ddxmldto;
    private DetalleDesgloceAguinaldoXMLDTO daxmldto;
    private Document doc;
    private List<EmpleadosCabeceraDTO> listaEmpleadosCabecera;
    private ArrayList<DetalleDesgloseXMLDTO> listaDetalleConceptos;
    private ArrayList<DetalleDesgloceAguinaldoXMLDTO> listaDetallesAguinaldo;
    
    private final static Logger logger = Logger.getLogger(HistorialEmpleadosRhMB.class);

    /**
     *
     */
    @PostConstruct//PERMITE QUE NUESTRA CLASE SE INICIALIZE CUANDO SEA LLAMADA
    public void iniciarVariables() {
        DetalleEmpleadosMB detalleEmpMB = new DetalleEmpleadosMB();
        setCod_empleado(detalleEmpMB.getCod_empleado());
        historialEmpleadosRh = new ArrayList<>();
        setDetalleSeleccionado(VerInformacionDePersonalWSClient.informacionPorEmpleado(cod_empleado).get(0));
        setHistorialEmpleadosRh(HistorialEmpleadosRhWSClient.informacionHistorial(cod_empleado));
        //detallesConceptos(cod_empleado);
    }

    /**
     *
     * @param ecdto
     */
    public void detallesConceptos(Integer ecdto) {
        setListaConcepQuincena(ConcepQuincWSClient.listarConceptosByEmpQuin(ecdto).getListCncptoquinc());
        setListaDetalleConceptos(new ArrayList<>());
        setListaDdxmldto(new ArrayList<>());
        for (int i = 0; i < getListaConcepQuincena().size(); i++) {
            setDdxmldto(new DetalleDesgloseXMLDTO());
            DocumentBuilder documentBuilder = null;
            try {
                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                java.util.logging.Logger.getLogger(HistorialEmpleadosRhMB.class.getName()).log(Level.SEVERE, null, ex);
            }
            InputSource inputSource = new InputSource();
            inputSource.setCharacterStream(new StringReader(getListaConcepQuincena().get(i).getXmlDesgloce()));
            try {
                setDoc(documentBuilder.parse(inputSource));
                getDoc().getDocumentElement().normalize();
                getDdxmldto().setValorConcepto(getDoc().getElementsByTagName("valor").item(0).getTextContent());
                getDdxmldto().setValorMes(getDoc().getElementsByTagName("valor").item(1).getTextContent());
                getDdxmldto().setValorLab(getDoc().getElementsByTagName("valor").item(2).getTextContent());
                getDdxmldto().setImporte(getDoc().getElementsByTagName("importe").item(0).getTextContent());
                System.out.println("ERROR HISTORIAL POR EMPLEADO ");

                getListaDdxmldto().add(i, getDdxmldto());
            } catch (SAXException | IOException ex) {
                java.util.logging.Logger.getLogger(HistorialEmpleadosRhMB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

     /**
      * 
      * @param agdto 
      */
      public void desgloceAguinaldo(Integer agdto){
        setListaDesgloceAguinaldo(DesgloceAguinaldoWSClient.listarDesgloceAguinaldo(agdto).getListAguinaldo()); 
        //setListaConcepQuincena(ConcepQuincWSClient.listarConceptosByEmpQuin(ecdto).getListCncptoquinc());
        //setListaDetalleConceptos(new ArrayList<>());
        setListaDetallesAguinaldo(new ArrayList<>());
        //setListaDdxmldto(new ArrayList<>());
        setListaDaxmldto(new ArrayList<>());
        for (int i = 0; i < getListaDesgloceAguinaldo().size(); i++) { 
            setDaxmldto(new DetalleDesgloceAguinaldoXMLDTO());
            DocumentBuilder documentBuilder = null;
            try {
                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                java.util.logging.Logger.getLogger(HistorialEmpleadosRhMB.class.getName()).log(Level.SEVERE, null, ex);
            }
            InputSource inputSource = new InputSource();
            inputSource.setCharacterStream(new StringReader(getListaDesgloceAguinaldo().get(i).getXmlDesgloce()));
            try {
                setDoc(documentBuilder.parse(inputSource));
                getDoc().getDocumentElement().normalize();
                getDaxmldto().setSalarioDiario(getDoc().getElementsByTagName("resultado").item(0).getTextContent());
                getDaxmldto().setfSalarioD(getDoc().getElementsByTagName("formula").item(0).getTextContent());
                getDaxmldto().setvSalarioD(getDoc().getElementsByTagName("valores").item(0).getTextContent());
                getDaxmldto().setDiasLaborados(getDoc().getElementsByTagName("resultado").item(1).getTextContent());
                getDaxmldto().setfDiasLab(getDoc().getElementsByTagName("formula").item(1).getTextContent());
                getDaxmldto().setvDiasLab(getDoc().getElementsByTagName("valores").item(1).getTextContent());
                getDaxmldto().setDiasPropAguinaldo(getDoc().getElementsByTagName("resultado").item(2).getTextContent());
                getDaxmldto().setPropAguinaldo(getDoc().getElementsByTagName("resultado").item(3).getTextContent());
               
                System.out.println("ERROR HISTORIAL AGUINALDO ");
                

                getListaDaxmldto().add(i, getDaxmldto());
            } catch (SAXException | IOException ex) {
                java.util.logging.Logger.getLogger(HistorialEmpleadosRhMB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    } 

    /**
     *
     * @return
     */
    public List<HistorialEmpleadosRhDTO> getHistorialEmpleadosRh() {
        return historialEmpleadosRh;
    }

    /**
     *
     * @param historialEmpleadosRh
     */
    public void setHistorialEmpleadosRh(List<HistorialEmpleadosRhDTO> historialEmpleadosRh) {
        this.historialEmpleadosRh = historialEmpleadosRh;
    }

    /**
     *
     * @return
     */
    public Integer getCod_empleado() {
        return cod_empleado;
    }

    /**
     *
     * @param cod_empleado
     */
    public void setCod_empleado(Integer cod_empleado) {
        this.cod_empleado = cod_empleado;
    }

    /**
     *
     * @return
     */
    public HistorialEmpleadosRhDTO getEmpleadoSeleccionado() {
        return empleadoSeleccionado;
    }

    /**
     *
     * @param empleadoSeleccionado
     */
    public void setEmpleadoSeleccionado(HistorialEmpleadosRhDTO empleadoSeleccionado) {
        this.empleadoSeleccionado = empleadoSeleccionado;
    }

    /**
     *
     * @return
     */
    public VerInformacionDePersonalDTO getDetalleSeleccionado() {
        return detalleSeleccionado;
    }

    /**
     *
     * @param detalleSeleccionado
     */
    public void setDetalleSeleccionado(VerInformacionDePersonalDTO detalleSeleccionado) {
        this.detalleSeleccionado = detalleSeleccionado;
    }

    /**
     *
     * @return
     */
    public InfoHistorialRhDTO getInfoEmpleado() {
        return infoEmpleado;
    }

    /**
     *
     * @param infoEmpleado
     */
    public void setInfoEmpleado(InfoHistorialRhDTO infoEmpleado) {
        this.infoEmpleado = infoEmpleado;
    }

    /**
     *
     * @return
     */
    public List<Tsgnomcncptoquinc> getListaConcepQuincena() {
        return listaConcepQuincena;
    }

    /**
     *
     * @param listaConcepQuincena
     */
    public void setListaConcepQuincena(List<Tsgnomcncptoquinc> listaConcepQuincena) {
        this.listaConcepQuincena = listaConcepQuincena;
    }

    /**
     *
     * @return
     */
    public ArrayList<DetalleDesgloseXMLDTO> getListaDdxmldto() {
        return listaDdxmldto;
    }

    /**
     *
     * @param listaDdxmldto
     */
    public void setListaDdxmldto(ArrayList<DetalleDesgloseXMLDTO> listaDdxmldto) {
        this.listaDdxmldto = listaDdxmldto;
    }

    /**
     *
     * @return
     */
    public DetalleDesgloseXMLDTO getDdxmldto() {
        return ddxmldto;
    }

    /**
     *
     * @param ddxmldto
     */
    public void setDdxmldto(DetalleDesgloseXMLDTO ddxmldto) {
        this.ddxmldto = ddxmldto;
    }

    /**
     *
     * @return
     */
    public Document getDoc() {
        return doc;
    }

    /**
     *
     * @param doc
     */
    public void setDoc(Document doc) {
        this.doc = doc;
    }

    /**
     *
     * @return
     */
    public List<EmpleadosCabeceraDTO> getListaEmpleadosCabecera() {
        return listaEmpleadosCabecera;
    }

    /**
     *
     * @param listaEmpleadosCabecera
     */
    public void setListaEmpleadosCabecera(List<EmpleadosCabeceraDTO> listaEmpleadosCabecera) {
        this.listaEmpleadosCabecera = listaEmpleadosCabecera;
    }

    /**
     *
     * @return
     */
    public ArrayList<DetalleDesgloseXMLDTO> getListaDetalleConceptos() {
        return listaDetalleConceptos;
    }

    /**
     *
     * @param listaDetalleConceptos
     */
    public void setListaDetalleConceptos(ArrayList<DetalleDesgloseXMLDTO> listaDetalleConceptos) {
        this.listaDetalleConceptos = listaDetalleConceptos;
    }

    public List<Tsgnomaguinaldo> getListaDesgloceAguinaldo() {
        return listaDesgloceAguinaldo;
    }

    public void setListaDesgloceAguinaldo(List<Tsgnomaguinaldo> listaDesgloceAguinaldo) {
        this.listaDesgloceAguinaldo = listaDesgloceAguinaldo;
    }

    public ArrayList<DetalleDesgloceAguinaldoXMLDTO> getListaDaxmldto() {
        return listaDaxmldto;
    }

    public void setListaDaxmldto(ArrayList<DetalleDesgloceAguinaldoXMLDTO> listaDaxmldto) {
        this.listaDaxmldto = listaDaxmldto;
    }

    public DetalleDesgloceAguinaldoXMLDTO getDaxmldto() {
        return daxmldto;
    }

    public void setDaxmldto(DetalleDesgloceAguinaldoXMLDTO daxmldto) {
        this.daxmldto = daxmldto;
    }

    public ArrayList<DetalleDesgloceAguinaldoXMLDTO> getListaDetallesAguinaldo() {
        return listaDetallesAguinaldo;
    }

    public void setListaDetallesAguinaldo(ArrayList<DetalleDesgloceAguinaldoXMLDTO> listaDetallesAguinaldo) {
        this.listaDetallesAguinaldo = listaDetallesAguinaldo;
    }

}
