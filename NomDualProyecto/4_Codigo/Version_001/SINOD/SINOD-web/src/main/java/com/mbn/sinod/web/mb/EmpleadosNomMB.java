package com.mbn.sinod.web.mb;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbn.sinod.model.dto.EmpleadoDTO;
import com.mbn.sinod.model.dto.EmpleadosNomDTO;
import com.mbn.sinod.model.dto.ListaAltaDePersonalDTO;
import com.mbn.sinod.model.dto.VerInformacionDePersonalDTO;
import com.mbn.sinod.model.dto.VerInformacionRhDTO;
import com.mbn.sinod.model.entidades.Tsgnomempleados;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import com.mbn.sinod.web.client.EmpleadosNomWSClient;
import com.mbn.sinod.web.client.EmpleadosWSClient;
import com.mbn.sinod.web.client.ListaAltaDePersonalWSClient;
import com.mbn.sinod.web.client.VerInformacionDePersonalWSClient;
import com.mbn.sinod.web.client.VerInformacionRhWSClient;
import com.mbn.sinod.web.util.ResourceBundles;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.primefaces.event.FlowEvent;

/**
 *
 * @author Ivette
 */
@Named(value = "empleadosNomMB")
@ViewAccessScoped
public class EmpleadosNomMB implements Serializable {

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(ResourceBundles.MESSAGES.getBundleName());
    private List<Tsgrhempleados> listarEmpleados;
    private Tsgrhempleados empleadoSeleccionado;
    private List<EmpleadoDTO> listaRH;
    private EmpleadoDTO dtoemp;
    private Tsgnomempleados empleadosNom;
    private Tsgnomempleados empleadosNomAlta;
    private Date fecha;
    private EmpleadosNomDTO listaNomina;
    private Integer cod_empleado;
    private List<Tsgnomempleados> listaempleadosNom;
    private VerInformacionDePersonalDTO detalleSeleccionado;
    private List<VerInformacionDePersonalDTO> detalleSeleccionadonom;
    private Tsgrhempleados empleadoLogeado;
    private List<ListaAltaDePersonalDTO> listaDTOEmpleados;
    private ListaAltaDePersonalDTO altaPersonalSeleccionado;
    private ListaAltaDePersonalDTO dtoListadetalle;
    private VerInformacionRhDTO dtoPersonalSeleccionado;
    private List<VerInformacionRhDTO> listapersonalSeleccionado;
    private Integer empleadoRh;

    /**
     *
     */
    @PostConstruct
    public void iniciarVariables() {
        listarEmpleados = new ArrayList<>();
        this.setEmpleadoLogeado((Tsgrhempleados) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("empleado"));
        setDetalleSeleccionado(new VerInformacionDePersonalDTO());
        setEmpleadosNom(new Tsgnomempleados());
        setEmpleadosNomAlta(new Tsgnomempleados());
        setDtoPersonalSeleccionado(new VerInformacionRhDTO());
        //setListarEmpleados(EmpleadosWSClient.listarEmpleados());
        setListaDTOEmpleados(ListaAltaDePersonalWSClient.listarEmpleadosParaAltas());
        setCod_empleado(verifica());
        if (getCod_empleado() > 0) {
            setDetalleSeleccionado(VerInformacionDePersonalWSClient.informacionPorEmpleado(getCod_empleado()).get(0));
            setEmpleadosNom(EmpleadosNomWSClient.obtenerEmpleadosnomPorIdNom(getCod_empleado()).getEmpleado());
            setListapersonalSeleccionado(VerInformacionRhWSClient.listaInformacionRh(empleadoRh));

        }
    }

    /**
     *
     * @return
     */
    public Integer verifica() {
        // get cookies
        HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("mbnEMPmbn")) {
                    return Integer.parseInt(cookies[i].getValue());
                }
            }
        }
        return 0;
    }

    /**
     *
     */
    public void busquedaInfoPersonal() {
        //setDtoemp(EmpleadosWSClient.listarEmpleadosRh(getEmpleadoSeleccionado().getCodEmpleado()));
        //setEmpleadoSeleccionado(EmpleadosWSClient.obtenerEmpleadoPorId(getDtoListadetalle().getCod_empleado()));
        //setDtoemp(EmpleadosWSClient.listarEmpleadosRh(getDtoListadetalle().getCod_empleado()));
        ///setDtoemp(EmpleadosWSClient.listarEmpleadosRh(getDtoListadetalle().getCod_empleado()));
        //setDtoPersonalSeleccionado(VerInformacionRhWSClient.listaInformacionRh(getDtoListadetalle().getCod_empleado()));
        setEmpleadoRh(getAltaPersonalSeleccionado().getCod_empleado());
//        getDtoPersonalSeleccionado().setListaInformacionRh(VerInformacionRhWSClient.listaInformacionRh(getEmpleadoRh()));
//        System.out.println("dtoListaRH "+getDtoListadetalle().getCod_empleado());
//        setListapersonalSeleccionado(getDtoPersonalSeleccionado().getListaInformacionRh());
        setEmpleadoSeleccionado(EmpleadosWSClient.obtenerEmpleadoPorId(getAltaPersonalSeleccionado().getCod_empleado()));
    }

    /**
     *
     * @throws JsonProcessingException
     */
    public void guardar() throws JsonProcessingException {

        EmpleadosNomDTO dtoRespuesta = new EmpleadosNomDTO();

        getEmpleadosNomAlta().setAudCodcreadopor(getEmpleadoLogeado().getCodEmpleado());
        getEmpleadosNomAlta().setAudFeccreacion(new Date());
        getEmpleadosNomAlta().setCodEmpleadoFk(getEmpleadoSeleccionado().getCodEmpleado());
        getEmpleadosNomAlta().setBolEstatus(true);
        getEmpleadosNomAlta().setCodValidaciones('n');

        dtoRespuesta.setEmpleado(getEmpleadosNomAlta());
        EmpleadosNomWSClient.guardarEmpleado(dtoRespuesta);

        mostrarMensaje("Los datos han sido guardados exitosamente", "succes");

    }

    /**
     *
     * @throws JsonProcessingException
     */
    public void editar() throws JsonProcessingException {

        EmpleadosNomDTO dtoRespuesta = new EmpleadosNomDTO();

        getEmpleadosNom().setAudCodmodificadopor(getEmpleadoLogeado().getCodEmpleado());
        getEmpleadosNom().setAudFecmodificacion(new Date());

        dtoRespuesta.setEmpleado(getEmpleadosNom());
        EmpleadosNomWSClient.guardarEmpleado(dtoRespuesta);

        mostrarMensaje("Los datos han sido actualizados exitosamente", "succes");

    }

    //MENSAJES
    private String mensaje;

    /**
     *
     * @return
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     *
     * @param mensaje
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     *
     * @param mensaje
     * @param severidad
     */
    public void mostrarMensaje(String mensaje, String severidad) {
        FacesContext context = FacesContext.getCurrentInstance();
        switch (severidad) {
            case "info":
                context.addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "", mensaje));//FacesMessage.SEVERITY_INFO, summary, detail
                break;
            case "error":
                context.addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje));
                break;
            case "succes":
                context.addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Operación exitosa", mensaje));
                break;
            default:
                context.addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "", ""));
                break;
        }
    }

    /**
     *
     */
    public void generarMensaje() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje: ", getMensaje()));
    }

    /**
     *
     * @param event
     * @return
     */
    public String onFlowProcess(FlowEvent event) {

        return event.getNewStep();

    }

    /**
     *
     * @return
     */
    public Tsgrhempleados getEmpleadoSeleccionado() {
        return empleadoSeleccionado;
    }

    /**
     *
     * @param empleadoSeleccionado
     */
    public void setEmpleadoSeleccionado(Tsgrhempleados empleadoSeleccionado) {
        this.empleadoSeleccionado = empleadoSeleccionado;
    }

    /**
     *
     * @return
     */
    public List<Tsgrhempleados> getListarEmpleados() {
        return listarEmpleados;
    }

    /**
     *
     * @param listarEmpleados
     */
    public void setListarEmpleados(List<Tsgrhempleados> listarEmpleados) {
        this.listarEmpleados = listarEmpleados;
    }

    /**
     *
     * @return
     */
    public List<EmpleadoDTO> getListaRH() {
        return listaRH;
    }

    /**
     *
     * @param listaRH
     */
    public void setListaRH(List<EmpleadoDTO> listaRH) {
        this.listaRH = listaRH;
    }

    /**
     *
     * @return
     */
    public EmpleadoDTO getDtoemp() {
        return dtoemp;
    }

    /**
     *
     * @param dtoemp
     */
    public void setDtoemp(EmpleadoDTO dtoemp) {
        this.dtoemp = dtoemp;
    }

    /**
     *
     * @return
     */
    public Tsgnomempleados getEmpleadosNom() {
        return empleadosNom;
    }

    /**
     *
     * @param empleadosNom
     */
    public void setEmpleadosNom(Tsgnomempleados empleadosNom) {
        this.empleadosNom = empleadosNom;
    }

    /**
     *
     * @return
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     *
     * @param fecha
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     *
     * @return
     */
    public Integer getCod_empleado() {
        return cod_empleado;
    }

    /**
     *
     * @param cod_empleado
     */
    public void setCod_empleado(Integer cod_empleado) {
        this.cod_empleado = cod_empleado;
    }

    /**
     *
     * @return
     */
    public EmpleadosNomDTO getListaNomina() {
        return listaNomina;
    }

    /**
     *
     * @param listaNomina
     */
    public void setListaNomina(EmpleadosNomDTO listaNomina) {
        this.listaNomina = listaNomina;
    }

    /**
     *
     * @return
     */
    public List<Tsgnomempleados> getListaempleadosNom() {
        return listaempleadosNom;
    }

    /**
     *
     * @param listaempleadosNom
     */
    public void setListaempleadosNom(List<Tsgnomempleados> listaempleadosNom) {
        this.listaempleadosNom = listaempleadosNom;
    }

    /**
     * @return the detalleSeleccionado
     */
    public VerInformacionDePersonalDTO getDetalleSeleccionado() {
        return detalleSeleccionado;
    }

    /**
     * @param detalleSeleccionado the detalleSeleccionado to set
     */
    public void setDetalleSeleccionado(VerInformacionDePersonalDTO detalleSeleccionado) {
        this.detalleSeleccionado = detalleSeleccionado;
    }

    /**
     *
     * @return
     */
    public Tsgnomempleados getEmpleadosNomAlta() {
        return empleadosNomAlta;
    }

    /**
     *
     * @param empleadosNomAlta
     */
    public void setEmpleadosNomAlta(Tsgnomempleados empleadosNomAlta) {
        this.empleadosNomAlta = empleadosNomAlta;
    }

    /**
     *
     * @return
     */
    public Tsgrhempleados getEmpleadoLogeado() {
        return empleadoLogeado;
    }

    /**
     *
     * @param empleadoLogeado
     */
    public void setEmpleadoLogeado(Tsgrhempleados empleadoLogeado) {
        this.empleadoLogeado = empleadoLogeado;
    }

    /**
     *
     * @return
     */
    public List<ListaAltaDePersonalDTO> getListaDTOEmpleados() {
        return listaDTOEmpleados;
    }

    /**
     *
     * @param listaDTOEmpleados
     */
    public void setListaDTOEmpleados(List<ListaAltaDePersonalDTO> listaDTOEmpleados) {
        this.listaDTOEmpleados = listaDTOEmpleados;
    }

    /**
     *
     * @return
     */
    public ListaAltaDePersonalDTO getDtoListadetalle() {
        return dtoListadetalle;
    }

    /**
     *
     * @param dtoListadetalle
     */
    public void setDtoListadetalle(ListaAltaDePersonalDTO dtoListadetalle) {
        this.dtoListadetalle = dtoListadetalle;
    }

    /**
     *
     * @return
     */
    public List<VerInformacionDePersonalDTO> getDetalleSeleccionadonom() {
        return detalleSeleccionadonom;
    }

    /**
     *
     * @param detalleSeleccionadonom
     */
    public void setDetalleSeleccionadonom(List<VerInformacionDePersonalDTO> detalleSeleccionadonom) {
        this.detalleSeleccionadonom = detalleSeleccionadonom;
    }

    /**
     *
     * @return
     */
    public VerInformacionRhDTO getDtoPersonalSeleccionado() {
        return dtoPersonalSeleccionado;
    }

    /**
     *
     * @param dtoPersonalSeleccionado
     */
    public void setDtoPersonalSeleccionado(VerInformacionRhDTO dtoPersonalSeleccionado) {
        this.dtoPersonalSeleccionado = dtoPersonalSeleccionado;
    }

    /**
     *
     * @return
     */
    public List<VerInformacionRhDTO> getListapersonalSeleccionado() {
        return listapersonalSeleccionado;
    }

    /**
     *
     * @param listapersonalSeleccionado
     */
    public void setListapersonalSeleccionado(List<VerInformacionRhDTO> listapersonalSeleccionado) {
        this.listapersonalSeleccionado = listapersonalSeleccionado;
    }

    /**
     *
     * @return
     */
    public Integer getEmpleadoRh() {
        return empleadoRh;
    }

    /**
     *
     * @param empleadoRh
     */
    public void setEmpleadoRh(Integer empleadoRh) {
        this.empleadoRh = empleadoRh;
    }

    /**
     * @return the altaPersonalSeleccionado
     */
    public ListaAltaDePersonalDTO getAltaPersonalSeleccionado() {
        return altaPersonalSeleccionado;
    }

    /**
     * @param altaPersonalSeleccionado the altaPersonalSeleccionado to set
     */
    public void setAltaPersonalSeleccionado(ListaAltaDePersonalDTO altaPersonalSeleccionado) {
        this.altaPersonalSeleccionado = altaPersonalSeleccionado;
    }

}
