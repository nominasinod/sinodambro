package com.mbn.sinod.web.mb;

import com.mbn.sinod.model.dto.VerInformacionDePersonalDTO;
import com.mbn.sinod.model.entidades.Tsgnomaguinaldo;
import com.mbn.sinod.model.entidades.Tsgnomquincena;
import com.mbn.sinod.model.entidades.Tsgrhareas;
import com.mbn.sinod.model.entidades.Tsgrhempleados;
import com.mbn.sinod.model.entidades.Tsgrhpuestos;
import com.mbn.sinod.web.client.VerInformacionDePersonalWSClient;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.apache.deltaspike.core.api.scope.ViewAccessScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Ivette
 */
@Named(value = "verInformacionDePersonalMB")
@ViewAccessScoped
public class VerInformacionDePersonalMB implements Serializable {

    private List<Tsgrhempleados> listarPersonal;
    private Tsgrhempleados personalSeleccionado;
    private Integer idPersonal;
    private List<VerInformacionDePersonalDTO> listaVerInformacionDePersonal;
    private VerInformacionDePersonalDTO detalleSeleccionado;
    private Tsgrhareas areaSeleccionada;
    private Tsgrhpuestos puestoSeleccionado;
    private Tsgnomquincena quincenaSeleccionada;
    private Tsgnomaguinaldo aguinaldoSeleccionado;

    private Integer cod_empleado;

    private final static Logger logger = Logger.getLogger(VerInformacionDePersonalMB.class);

    /**
     *
     */
    @PostConstruct
    public void iniciarVariables() {
        DetalleEmpleadosMB detalleEmpMB = new DetalleEmpleadosMB();
        setCod_empleado(detalleEmpMB.getCod_empleado());
        setDetalleSeleccionado(VerInformacionDePersonalWSClient.informacionPorEmpleado(cod_empleado).get(0));
        System.out.println("Detalles empleado: " + detalleEmpMB.getCod_empleado());
    }

    /**
     *
     * @param verInformacionDePersonalDTO
     */
    public void busquedaPersonalPorId(VerInformacionDePersonalDTO verInformacionDePersonalDTO) {
        setDetalleSeleccionado(verInformacionDePersonalDTO);
        System.out.println("Ver Empleado: " + cod_empleado);
    }

    /**
     *
     * @return
     */
    public List<Tsgrhempleados> getListarPersonal() {
        return listarPersonal;
    }

    /**
     *
     * @param listarPersonal
     */
    public void setListarPersonal(List<Tsgrhempleados> listarPersonal) {
        this.listarPersonal = listarPersonal;
    }

    /**
     *
     * @return
     */
    public Tsgrhempleados getPersonalSeleccionado() {
        return personalSeleccionado;
    }

    /**
     *
     * @param personalSeleccionado
     */
    public void setPersonalSeleccionado(Tsgrhempleados personalSeleccionado) {
        this.personalSeleccionado = personalSeleccionado;
    }

    /**
     *
     * @return
     */
    public Integer getIdPersonal() {
        return idPersonal;
    }

    /**
     *
     * @param idPersonal
     */
    public void setIdPersonal(Integer idPersonal) {
        this.idPersonal = idPersonal;
    }

    /**
     *
     * @return
     */
    public List<VerInformacionDePersonalDTO> getListaVerInformacionDePersonal() {
        return listaVerInformacionDePersonal;
    }

    /**
     *
     * @param listaVerInformacionDePersonal
     */
    public void setListaVerInformacionDePersonal(List<VerInformacionDePersonalDTO> listaVerInformacionDePersonal) {
        this.listaVerInformacionDePersonal = listaVerInformacionDePersonal;
    }

    /**
     *
     * @return
     */
    public Tsgrhareas getAreaSeleccionada() {
        return areaSeleccionada;
    }

    /**
     *
     * @param areaSeleccionada
     */
    public void setAreaSeleccionada(Tsgrhareas areaSeleccionada) {
        this.areaSeleccionada = areaSeleccionada;
    }

    /**
     *
     * @return
     */
    public Tsgrhpuestos getPuestoSeleccionado() {
        return puestoSeleccionado;
    }

    /**
     *
     * @param puestoSeleccionado
     */
    public void setPuestoSeleccionado(Tsgrhpuestos puestoSeleccionado) {
        this.puestoSeleccionado = puestoSeleccionado;
    }

    /**
     *
     * @return
     */
    public Tsgnomquincena getQuincenaSeleccionada() {
        return quincenaSeleccionada;
    }

    /**
     *
     * @param quincenaSeleccionada
     */
    public void setQuincenaSeleccionada(Tsgnomquincena quincenaSeleccionada) {
        this.quincenaSeleccionada = quincenaSeleccionada;
    }

    /**
     *
     * @return
     */
    public Tsgnomaguinaldo getAguinaldoSeleccionado() {
        return aguinaldoSeleccionado;
    }

    /**
     *
     * @param aguinaldoSeleccionado
     */
    public void setAguinaldoSeleccionado(Tsgnomaguinaldo aguinaldoSeleccionado) {
        this.aguinaldoSeleccionado = aguinaldoSeleccionado;
    }

    /**
     *
     * @return
     */
    public VerInformacionDePersonalDTO getDetalleSeleccionado() {
        return detalleSeleccionado;
    }

    /**
     *
     * @param detalleSeleccionado
     */
    public void setDetalleSeleccionado(VerInformacionDePersonalDTO detalleSeleccionado) {
        this.detalleSeleccionado = detalleSeleccionado;
    }

    /**
     *
     * @return
     */
    public Integer getCod_empleado() {
        return cod_empleado;
    }

    /**
     *
     * @param cod_empleado
     */
    public void setCod_empleado(Integer cod_empleado) {
        this.cod_empleado = cod_empleado;
    }
}
