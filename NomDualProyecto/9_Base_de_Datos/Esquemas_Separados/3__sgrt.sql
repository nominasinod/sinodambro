
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;



CREATE TYPE sgrt.destinatario AS ENUM (
    'USR',
    'EMPLEADO',
    'GRUPO',
    'DEPTO'
);


--
-- TOC entry 808 (class 1247 OID 33472)
-- Name: edoticket; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.edoticket AS ENUM (
    'Abierto',
    'Cerrado'
);


--
-- TOC entry 811 (class 1247 OID 33478)
-- Name: encriptacion; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.encriptacion AS ENUM (
    'NONE',
    'SSL'
);


--
-- TOC entry 814 (class 1247 OID 33484)
-- Name: estatus; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.estatus AS ENUM (
    'Enviado',
    'Pendiente'
);


--
-- TOC entry 817 (class 1247 OID 33490)
-- Name: estatus_compromiso; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.estatus_compromiso AS ENUM (
    'Pendiente',
    'Terminado'
);


--
-- TOC entry 820 (class 1247 OID 33496)
-- Name: modulo; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.modulo AS ENUM (
    'SGRT',
    'SSV'
);


--
-- TOC entry 823 (class 1247 OID 33502)
-- Name: origencontac; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.origencontac AS ENUM (
    'Web',
    'Email',
    'Reunion',
    'Telefono',
    'Otro'
);


--
-- TOC entry 826 (class 1247 OID 33514)
-- Name: prioridad; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja'
);


--
-- TOC entry 829 (class 1247 OID 33522)
-- Name: protocolo; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.protocolo AS ENUM (
    'POP',
    'IMAP'
);


--
-- TOC entry 832 (class 1247 OID 33528)
-- Name: tipo; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.tipo AS ENUM (
    'ReunionAgendada',
    'ReunionCancelada',
    'NuevaSolicitud',
    'SolicitudPagada',
    'ViaticoCancelado'
);


--
-- TOC entry 835 (class 1247 OID 33540)
-- Name: tipo_compromiso; Type: TYPE; Schema: sgrt; Owner: -
--

CREATE TYPE sgrt.tipo_compromiso AS ENUM (
    'Acuerdo',
    'Pendiente'
);


--
-- TOC entry 409 (class 1255 OID 33547)
-- Name: buscar_asistentes_minuta(integer); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) RETURNS TABLE(nombre_asistente text, area_asistente character varying)
    LANGUAGE sql
    AS '







SELECT







des_nombre as nombre_asistente,







CONCAT((SELECT area.des_nbarea FROM sgrh.tsgrhempleados emp LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area WHERE emp.cod_empleado=invitado.cod_empleado),







des_empresa) as area_asistente







FROM sgrt.tsgrtinvitados invitado WHERE  invitado.cnu_asiste=''1'' and invitado.cod_reunion=reunionid;







';


--
-- TOC entry 410 (class 1255 OID 33548)
-- Name: buscar_compromisos_roles_list(integer); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) RETURNS TABLE(cod_compromiso integer, des_descripcion character varying, cod_estatus sgrt.estatus_compromiso, fec_solicitud text, fec_compromiso text, validador text, verificador text, ejecutor text)
    LANGUAGE plpgsql
    AS '















BEGIN







RETURN QUERY







select







CAST(ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as INTEGER) AS cod_compromiso,







com.des_descripcion,







com.cod_estatus,







CAST(to_char(com.fec_solicitud, ''DD/MM/YYYY'') as text) as fec_solicitud,







CAST(to_char(com.fec_compromiso, ''DD/MM/YYYY'') as text) as fec_compromiso,







(select CONCAT(des_nombre, '' '', des_nombres, '' '', des_apepaterno, '' '', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_validador) AS validador,







(select CONCAT(des_nombre, '' '', des_nombres, '' '', des_apepaterno, '' '', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_verificador) AS verificador,







(select CONCAT(des_nombre, '' '', des_nombres, '' '', des_apepaterno, '' '', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_ejecutor) AS ejecutor







from sgrt.tsgrtcompromisos com where com.cod_reunion=reunionid;















END;







';


--
-- TOC entry 411 (class 1255 OID 33549)
-- Name: buscar_minutas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, cantidad_minutas integer)
    LANGUAGE sql
    AS '







SELECT







cod_area,







cod_acronimo as des_nbarea,







cast((SELECT







COUNT(*)







FROM sgrt.tsgrtreuniones reu







INNER JOIN sgrh.tsgrhempleados emp







ON reu.cod_responsable=emp.cod_empleado







INNER JOIN sgrh.tsgrhareas area







ON emp.cod_area=area.cod_area







WHERE area.cod_area=a.cod_area and







reu.fec_fecha >= cast(fecha_inicio as date)







AND reu.fec_fecha <=  cast(fecha_fin as date)







) as INTEGER) AS cantidad_minutas







FROM sgrh.tsgrhareas a







';


--
-- TOC entry 412 (class 1255 OID 33550)
-- Name: buscar_proxima_reunion(integer); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) RETURNS SETOF record
    LANGUAGE sql
    AS '







  select







  reunion.cod_reunion,







  reunion.des_nombre,







  CAST(to_char(reunion.fec_fecha, ''DD/MM/YYYY'')as text) as fec_fecha,







  reunion.cod_lugar,







  CAST(reunion.tim_hora as text),







  lugar.des_nombre,







  lugar.cod_ciudad,







ciudad.des_nbciudad,







ciudad.cod_estadorep,







estado.des_nbestado,







estado.cod_estadorep







from sgrt.tsgrtreuniones reunion inner join sgrt.tsgrtlugares lugar on reunion.cod_lugar=lugar.cod_lugar







inner join sgrt.tsgrtciudades ciudad on lugar.cod_ciudad=ciudad.cod_ciudad







inner join sgrt.tsgrtestados estado on ciudad.cod_estadorep=estado.cod_estadorep







where cod_reunion=(select cod_proximareunion from sgrt.tsgrtreuniones where cod_reunion=reunionId);















';


--
-- TOC entry 413 (class 1255 OID 33551)
-- Name: compromisos_areas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, tipo text, num integer)
    LANGUAGE sql
    AS '







select







cod_area,







cod_acronimo as des_nbarea,







CAST(''Terminado'' as text) as tipo,







cast((SELECT







COUNT(*)







FROM sgrt.tsgrtcompromisos reu







INNER JOIN sgrh.tsgrhempleados emp







ON reu.cod_ejecutor=emp.cod_empleado







INNER JOIN sgrh.tsgrhareas area







ON emp.cod_area=area.cod_area







WHERE reu.cod_estatus=''Terminado'' AND







area.cod_area=a.cod_area AND







reu.fec_compromiso >= cast(fecha_inicio as date)







AND reu.fec_compromiso <= cast(fecha_fin as date)







)as INTEGER) as num







FROM







sgrh.tsgrhareas a







UNION







select







cod_area,







cod_acronimo as des_nbarea,







CAST(''Pendiente'' as text) as tipo,







cast((SELECT







COUNT(*)







FROM sgrt.tsgrtcompromisos reu







INNER JOIN sgrh.tsgrhempleados emp







ON reu.cod_ejecutor=emp.cod_empleado







INNER JOIN sgrh.tsgrhareas area







ON emp.cod_area=area.cod_area







WHERE reu.cod_estatus=''Pendiente'' and







area.cod_area=a.cod_area AND







reu.fec_compromiso >= cast(fecha_inicio as date)







AND reu.fec_compromiso <= cast(fecha_fin as date))as INTEGER) as num







FROM







sgrh.tsgrhareas a







';


--
-- TOC entry 414 (class 1255 OID 33552)
-- Name: compromisos_dia(text); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.compromisos_dia(fechacompromiso text) RETURNS TABLE(cod_empleado integer, nombre text, area text, fec_compromiso text, des_descripcion text, cod_estatus text, tiempo_demora integer)
    LANGUAGE sql
    AS '







SELECT







emp.cod_empleado,







CONCAT(emp.des_nombre, '' '', emp.des_nombres, '' '', emp.des_apepaterno, '' '', emp.des_apematerno) as nombre,







(SELECT cod_acronimo FROM sgrh.tsgrhareas WHERE cod_area=emp.cod_area) as area,







cast(to_char(com.fec_compromiso,''DD/MM/YYYY'') as text) fec_compromiso,







com.des_descripcion,







cast(com.cod_estatus as text),







cast((select count(the_day) from







    (select generate_series(com.fec_compromiso+1, com.fec_entrega, ''1 day'') as the_day) days







where extract(''dow'' from the_day) not in (0,6))as INTEGER) as tiempo_demora







FROM sgrt.tsgrtcompromisos com







LEFT JOIN sgrh.tsgrhempleados emp ON com.cod_ejecutor=emp.cod_empleado or com.cod_validador=emp.cod_empleado or com.cod_verificador=emp.cod_empleado







WHERE com.fec_compromiso=cast(fechaCompromiso as date);







';


--
-- TOC entry 415 (class 1255 OID 33553)
-- Name: compromisos_generales(); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.compromisos_generales() RETURNS TABLE(cod_empleado integer, cod_reunion integer, nombre text, rol text, area text, descripcion text, minuta text, estatus text, fec_registro text, fec_compromiso text, dias_habiles integer, tiempo_demora integer)
    LANGUAGE sql
    AS '







SELECT







emp.cod_empleado,







reunion.cod_reunion,







CONCAT(emp.des_nombre, '' '', emp.des_nombres, '' '', emp.des_apepaterno, '' '', emp.des_apematerno) as nombre,







CAST(''Validador'' as text) as rol,







cast(area.cod_acronimo as text) as area,







cast(com.des_descripcion as text) as descripcion,







cast(reunion.des_nombre as text) as minuta,







cast(com.cod_estatus as text)as estatus,







cast(to_char(com.fec_solicitud,''DD/MM/YYYY'') as text) as fec_registro,







cast(to_char(com.fec_compromiso,''DD/MM/YYYY'') as text),







cast((select count(the_day) from







    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, ''1 day'') as the_day) days







where extract(''dow'' from the_day) not in (0,6))as INTEGER) as dias_habiles,







cast((select count(the_day) from







    (select generate_series(com.fec_compromiso+1, com.fec_entrega, ''1 day'') as the_day) days







where extract(''dow'' from the_day) not in (0,6))as INTEGER) as tiempo_demora







FROM sgrt.tsgrtcompromisos com







LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion







LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_validador







LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area







WHERE emp.cod_empleado=com.cod_validador







UNION







SELECT







emp.cod_empleado,







reunion.cod_reunion,







CONCAT(emp.des_nombre, '' '', emp.des_nombres, '' '', emp.des_apepaterno, '' '', emp.des_apematerno) as nombre,







CAST(''Verificador'' as text) as rol,







cast(area.cod_acronimo as text) as area,







cast(com.des_descripcion as text) as descripcion,







cast(reunion.des_nombre as text) as minuta,







cast(com.cod_estatus as text)as estatus,







cast(to_char(com.fec_solicitud,''DD/MM/YYYY'') as text) as fec_registro,







cast(to_char(com.fec_compromiso,''DD/MM/YYYY'') as text),







cast((select count(the_day) from







    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, ''1 day'') as the_day) days







where extract(''dow'' from the_day) not in (0,6))as INTEGER) as dias_habiles,







cast((select count(the_day) from







    (select generate_series(com.fec_compromiso+1, com.fec_entrega, ''1 day'') as the_day) days







where extract(''dow'' from the_day) not in (0,6))as INTEGER) as tiempo_demora







FROM sgrt.tsgrtcompromisos com







LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion







LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_verificador







LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area







WHERE emp.cod_empleado=com.cod_verificador







UNION







SELECT







emp.cod_empleado,







reunion.cod_reunion,







CONCAT(emp.des_nombre, '' '', emp.des_nombres, '' '', emp.des_apepaterno, '' '', emp.des_apematerno) as nombre,







CAST(''Ejecutor'' as text) as rol,







cast(area.cod_acronimo as text) as area,







cast(com.des_descripcion as text) as descripcion,







cast(reunion.des_nombre as text) as minuta,







cast(com.cod_estatus as text)as estatus,







cast(to_char(com.fec_solicitud,''DD/MM/YYYY'') as text) as fec_registro,







cast(to_char(com.fec_compromiso,''DD/MM/YYYY'') as text),







cast((select count(the_day) from







    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, ''1 day'') as the_day) days







where extract(''dow'' from the_day) not in (0,6))as INTEGER) as dias_habiles,







cast((select count(the_day) from







    (select generate_series(com.fec_compromiso+1, com.fec_entrega, ''1 day'') as the_day) days







where extract(''dow'' from the_day) not in (0,6))as INTEGER) as tiempo_demora







FROM sgrt.tsgrtcompromisos com







LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion







LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_ejecutor







LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area







WHERE emp.cod_empleado=com.cod_ejecutor







';


--
-- TOC entry 416 (class 1255 OID 33554)
-- Name: reporte_por_tema(integer); Type: FUNCTION; Schema: sgrt; Owner: -
--

CREATE FUNCTION sgrt.reporte_por_tema(reunionid integer) RETURNS TABLE(nombre_minuta character varying, responsable text, fecha text, acuerdos integer, pendientes integer, total integer, tiempo_invertido text)
    LANGUAGE plpgsql
    AS '







DECLARE







    var_r record;







BEGIN







   FOR var_r IN(SELECT * FROM sgrt.tsgrtreuniones WHERE cod_reunion=reunionId OR cod_proximareunion=reunionId)







     LOOP







              RETURN QUERY







		select c.nombre_minuta, c.responsable, c.fecha,a.Acuerdos, b.Pendientes, cast(COALESCE(a.Acuerdos,0)+COALESCE(b.Pendientes,0)as int) AS Total, c.tiempo_invertido from







		(select cast(count(cod_tipocompromiso) as int) as Acuerdos from sgrt.tsgrtcompromisos where cod_tipocompromiso=''Acuerdo'' and cod_reunion=var_r.cod_reunion) a,







		(select cast(count(cod_tipocompromiso)as int) as Pendientes from sgrt.tsgrtcompromisos where cod_tipocompromiso=''Pendiente'' and cod_reunion=var_r.cod_reunion) b,







		(select reunion.des_nombre AS nombre_minuta, CONCAT(empleado.des_nombre, '' '', empleado.des_nombres, '' '', empleado.des_apepaterno, '' '', empleado.des_apematerno) AS responsable,







		CAST(to_char(reunion.fec_fecha, ''DD/MM/YYYY'') as text) as fecha, CAST(to_char(reunion.tim_duracion,''HH24:MI'') as text) as tiempo_invertido from







		sgrt.tsgrtreuniones reunion,







		sgrh.tsgrhempleados empleado







		WHERE reunion.cod_responsable=empleado.cod_empleado and







		cod_reunion=var_r.cod_reunion) c;







            END LOOP;







END; ';


--
-- TOC entry 287 (class 1259 OID 33856)
-- Name: seq_agenda; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_agenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 288 (class 1259 OID 33858)
-- Name: seq_archivo; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_archivo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 289 (class 1259 OID 33860)
-- Name: seq_asistente; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_asistente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 290 (class 1259 OID 33862)
-- Name: seq_attach; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_attach
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 291 (class 1259 OID 33864)
-- Name: seq_categoriafaq; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_categoriafaq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 292 (class 1259 OID 33866)
-- Name: seq_chat; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_chat
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999999999
    CACHE 1;


--
-- TOC entry 293 (class 1259 OID 33868)
-- Name: seq_ciudad; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_ciudad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 294 (class 1259 OID 33870)
-- Name: seq_comentsagenda; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_comentsagenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 295 (class 1259 OID 33872)
-- Name: seq_comentsreunion; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_comentsreunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 296 (class 1259 OID 33874)
-- Name: seq_compromiso; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_compromiso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 297 (class 1259 OID 33876)
-- Name: seq_contacto; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_contacto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 298 (class 1259 OID 33878)
-- Name: seq_correo; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_correo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 299 (class 1259 OID 33880)
-- Name: seq_depto; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_depto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 300 (class 1259 OID 33882)
-- Name: seq_edoacuerdo; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_edoacuerdo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 301 (class 1259 OID 33884)
-- Name: seq_elemento; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_elemento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 302 (class 1259 OID 33886)
-- Name: seq_estadorep; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_estadorep
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 303 (class 1259 OID 33888)
-- Name: seq_faq; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_faq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 304 (class 1259 OID 33890)
-- Name: seq_grupo; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 305 (class 1259 OID 33892)
-- Name: seq_invitado; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_invitado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 306 (class 1259 OID 33894)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 307 (class 1259 OID 33896)
-- Name: seq_mensaje; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_mensaje
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 308 (class 1259 OID 33898)
-- Name: seq_nota; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_nota
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 309 (class 1259 OID 33900)
-- Name: seq_plantillacorreo; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_plantillacorreo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 310 (class 1259 OID 33902)
-- Name: seq_prioridad; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_prioridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 311 (class 1259 OID 33904)
-- Name: seq_resp; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_resp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 312 (class 1259 OID 33906)
-- Name: seq_respuesta; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_respuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 313 (class 1259 OID 33908)
-- Name: seq_respuestas_participantes; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_respuestas_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 314 (class 1259 OID 33910)
-- Name: seq_reunion; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_reunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 315 (class 1259 OID 33912)
-- Name: seq_servicio; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_servicio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 316 (class 1259 OID 33914)
-- Name: seq_solicitud; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_solicitud
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 317 (class 1259 OID 33916)
-- Name: seq_ticket; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_ticket
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 318 (class 1259 OID 33918)
-- Name: seq_topico; Type: SEQUENCE; Schema: sgrt; Owner: -
--

CREATE SEQUENCE sgrt.seq_topico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_with_oids = false;

--
-- TOC entry 319 (class 1259 OID 33920)
-- Name: tsgrtagenda; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtagenda (
    cod_agenda integer DEFAULT nextval('sgrt.seq_agenda'::regclass) NOT NULL,
    des_texto character varying(200) NOT NULL,
    cnu_tratado smallint DEFAULT '0'::smallint,
    cod_reunion integer NOT NULL
);


--
-- TOC entry 320 (class 1259 OID 33925)
-- Name: tsgrtarchivos; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtarchivos (
    cod_archivo integer DEFAULT nextval('sgrt.seq_archivo'::regclass) NOT NULL,
    bin_archivo bytea,
    cod_reunion integer NOT NULL
);


--
-- TOC entry 321 (class 1259 OID 33932)
-- Name: tsgrtasistentes; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtasistentes (
    cod_asistente integer DEFAULT nextval('sgrt.seq_asistente'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    cod_empleado integer,
    cnu_asiste smallint NOT NULL,
    cod_invitado integer
);


--
-- TOC entry 322 (class 1259 OID 33936)
-- Name: tsgrtattchticket; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtattchticket (
    cod_attach integer DEFAULT nextval('sgrt.seq_attach'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_tamano character varying(20) NOT NULL,
    des_nombre character varying(128) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    bin_attach bytea
);


--
-- TOC entry 323 (class 1259 OID 33943)
-- Name: tsgrtayudatopico; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtayudatopico (
    cod_topico integer DEFAULT nextval('sgrt.seq_topico'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer NOT NULL,
    des_topico character varying(36) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 324 (class 1259 OID 33947)
-- Name: tsgrtcategoriafaq; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtcategoriafaq (
    cod_categoriafaq integer DEFAULT nextval('sgrt.seq_categoriafaq'::regclass) NOT NULL,
    cnu_tipo smallint NOT NULL,
    des_categoria character varying(255) NOT NULL,
    des_descripcion character varying(255) NOT NULL,
    des_notas character varying(255) NOT NULL,
    tim_ultactualiza timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    fec_ultactualizadopor integer NOT NULL,
    cod_creadopor integer NOT NULL
);


--
-- TOC entry 325 (class 1259 OID 33954)
-- Name: tsgrtchat; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtchat (
    cod_chat integer DEFAULT nextval('sgrt.seq_chat'::regclass) NOT NULL,
    chat character varying NOT NULL
);


--
-- TOC entry 326 (class 1259 OID 33961)
-- Name: tsgrtciudades; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtciudades (
    cod_ciudad integer DEFAULT nextval('sgrt.seq_ciudad'::regclass) NOT NULL,
    des_nbciudad character varying(100) NOT NULL,
    cod_estadorep integer NOT NULL
);


--
-- TOC entry 327 (class 1259 OID 33965)
-- Name: tsgrtcomentariosagenda; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtcomentariosagenda (
    cod_comentsagenda integer DEFAULT nextval('sgrt.seq_comentsagenda'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_agenda integer NOT NULL,
    cod_invitado integer NOT NULL
);


--
-- TOC entry 328 (class 1259 OID 33972)
-- Name: tsgrtcomentariosreunion; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtcomentariosreunion (
    cod_commentsreunion integer DEFAULT nextval('sgrt.seq_comentsreunion'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_invitado integer NOT NULL,
    cod_reunion integer NOT NULL
);


--
-- TOC entry 329 (class 1259 OID 33979)
-- Name: tsgrtcompromisos; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtcompromisos (
    cod_compromiso integer DEFAULT nextval('sgrt.seq_compromiso'::regclass) NOT NULL,
    des_descripcion character varying(200) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_compromiso date NOT NULL,
    cod_reunion integer NOT NULL,
    cod_validador integer NOT NULL,
    cod_verificador integer NOT NULL,
    cod_estado integer,
    des_valor character varying(45),
    cod_ejecutor integer NOT NULL,
    cod_tipoejecutor character varying(10),
    cnu_revisado smallint,
    cod_estatus sgrt.estatus_compromiso DEFAULT 'Pendiente'::sgrt.estatus_compromiso NOT NULL,
    cod_tipocompromiso sgrt.tipo_compromiso DEFAULT 'Pendiente'::sgrt.tipo_compromiso NOT NULL,
    cod_chat integer,
    fec_entrega date
);


--
-- TOC entry 330 (class 1259 OID 33985)
-- Name: tsgrtcorreo; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtcorreo (
    cod_correo integer DEFAULT nextval('sgrt.seq_correo'::regclass) NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer,
    des_nbusuario character varying(32) NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(70) NOT NULL,
    des_contrasena character varying(30) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_dirhost character varying(125) NOT NULL,
    cod_protocolo sgrt.protocolo DEFAULT 'POP'::sgrt.protocolo NOT NULL,
    cod_encriptacion sgrt.encriptacion DEFAULT 'NONE'::sgrt.encriptacion NOT NULL,
    cod_puerto integer,
    cnu_frecsinc smallint NOT NULL,
    cnu_nummaxcorreo smallint NOT NULL,
    cnu_eliminar smallint NOT NULL,
    cnu_errores smallint NOT NULL,
    fec_ulterror timestamp without time zone,
    fec_ultsincr timestamp without time zone,
    cnu_smtpactivo smallint,
    des_smtphost character varying(125) NOT NULL,
    cod_smtpport integer,
    cnu_smtpsecure smallint NOT NULL,
    cnu_smtpauth smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_usuario integer NOT NULL
);


--
-- TOC entry 331 (class 1259 OID 33991)
-- Name: tsgrtdatossolicitud; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtdatossolicitud (
    cod_datosolicitud integer NOT NULL,
    cod_elemento integer NOT NULL,
    des_descripcion character varying(45) NOT NULL,
    cod_solicitud integer NOT NULL,
    cod_edosolicitud integer NOT NULL
);


--
-- TOC entry 332 (class 1259 OID 33994)
-- Name: tsgrtdepartamento; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtdepartamento (
    cod_depto integer DEFAULT nextval('sgrt.seq_depto'::regclass) NOT NULL,
    cod_plantillacorreo integer NOT NULL,
    cod_correo integer NOT NULL,
    cod_manager integer DEFAULT 0 NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_ncorto character varying(10) NOT NULL,
    des_firma text NOT NULL,
    cnu_publico smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 333 (class 1259 OID 34003)
-- Name: tsgrtedosolicitudes; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtedosolicitudes (
    cod_edosolicitud integer NOT NULL,
    cod_nbedosolicitud integer NOT NULL
);


--
-- TOC entry 334 (class 1259 OID 34006)
-- Name: tsgrtelementos; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtelementos (
    cod_elemento integer DEFAULT nextval('sgrt.seq_elemento'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_activo smallint NOT NULL
);


--
-- TOC entry 335 (class 1259 OID 34010)
-- Name: tsgrtestados; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtestados (
    cod_estadorep integer DEFAULT nextval('sgrt.seq_estadorep'::regclass) NOT NULL,
    des_nbestado character varying(60) NOT NULL
);


--
-- TOC entry 336 (class 1259 OID 34014)
-- Name: tsgrtfaq; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtfaq (
    cod_faq integer DEFAULT nextval('sgrt.seq_faq'::regclass) NOT NULL,
    cod_categoriafaq integer NOT NULL,
    des_pregunta character varying(255) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_respuesta character varying(255) NOT NULL,
    des_notasint character varying(255) NOT NULL,
    fec_ultactualizacion timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_ultactualizacionpor integer NOT NULL
);


--
-- TOC entry 337 (class 1259 OID 34021)
-- Name: tsgrtgrupo; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtgrupo (
    cod_grupo integer DEFAULT nextval('sgrt.seq_grupo'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_crear smallint NOT NULL,
    cnu_editar smallint NOT NULL,
    cnu_borrar smallint NOT NULL,
    cnu_cerrar smallint NOT NULL,
    cnu_transferir smallint NOT NULL,
    cnu_prohibir smallint NOT NULL,
    cnu_administrar smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 338 (class 1259 OID 34026)
-- Name: tsgrtinvitados; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtinvitados (
    cod_invitado integer DEFAULT nextval('sgrt.seq_invitado'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    des_nombre character varying,
    des_correo character varying,
    cnu_invitacionenv smallint NOT NULL,
    cnu_asiste smallint NOT NULL,
    cod_empleado integer,
    des_empresa character varying
);


--
-- TOC entry 339 (class 1259 OID 34033)
-- Name: tsgrtlugares; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtlugares (
    cod_lugar integer DEFAULT nextval('sgrt.seq_lugar'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    cod_ciudad integer NOT NULL
);


--
-- TOC entry 340 (class 1259 OID 34037)
-- Name: tsgrtmsjticket; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtmsjticket (
    cod_mensaje integer DEFAULT nextval('sgrt.seq_mensaje'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_usuario integer DEFAULT 0 NOT NULL,
    des_mensaje text NOT NULL,
    cod_fuente character varying(16) DEFAULT NULL::character varying,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 341 (class 1259 OID 34047)
-- Name: tsgrtnota; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtnota (
    cod_nota integer DEFAULT nextval('sgrt.seq_nota'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_fuente character varying(32) NOT NULL,
    des_titulo character varying(255) DEFAULT 'Nota INTEGERerna Generica'::character varying NOT NULL,
    des_nota text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 342 (class 1259 OID 34055)
-- Name: tsgrtplantillacorreos; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtplantillacorreos (
    cod_plantillacorreo integer DEFAULT nextval('sgrt.seq_plantillacorreo'::regclass) NOT NULL,
    des_nombre character varying(32) NOT NULL,
    des_notas text,
    cod_tipodestinario sgrt.destinatario DEFAULT 'USR'::sgrt.destinatario,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    des_asunto character varying(45),
    des_cuerpo character varying(255)
);


--
-- TOC entry 343 (class 1259 OID 34063)
-- Name: tsgrtprioridad; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtprioridad (
    cod_prioridad integer DEFAULT nextval('sgrt.seq_prioridad'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    des_descripcion character varying(30) NOT NULL,
    cod_color character varying(7) NOT NULL,
    cnu_valprioridad smallint NOT NULL,
    cnu_publica smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 344 (class 1259 OID 34067)
-- Name: tsgrtresppredefinida; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtresppredefinida (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_resp'::regclass) NOT NULL,
    cod_depto integer NOT NULL,
    cnu_activo smallint NOT NULL,
    des_titulo character varying(125) NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 345 (class 1259 OID 34074)
-- Name: tsgrtrespuesta; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtrespuesta (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_respuesta'::regclass) NOT NULL,
    cod_mensaje integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


--
-- TOC entry 346 (class 1259 OID 34081)
-- Name: tsgrtreuniones; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtreuniones (
    cod_reunion integer DEFAULT nextval('sgrt.seq_reunion'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    fec_fecha date NOT NULL,
    des_objetivo character varying(700) NOT NULL,
    cod_lugar integer NOT NULL,
    cod_responsable integer NOT NULL,
    cod_proximareunion integer,
    cod_creadorreunion integer NOT NULL,
    tim_duracion time without time zone,
    tim_hora time without time zone
);


--
-- TOC entry 347 (class 1259 OID 34088)
-- Name: tsgrtservicios; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtservicios (
    cod_servicio integer DEFAULT nextval('sgrt.seq_servicio'::regclass) NOT NULL,
    des_nombre_servicio character varying(45) NOT NULL,
    des_descripcion character varying(100) NOT NULL,
    fec_contratacion date NOT NULL
);


--
-- TOC entry 348 (class 1259 OID 34092)
-- Name: tsgrtsolicitudservicios; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtsolicitudservicios (
    cod_solicitud integer DEFAULT nextval('sgrt.seq_solicitud'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_servicio integer NOT NULL
);


--
-- TOC entry 349 (class 1259 OID 34096)
-- Name: tsgrtticket; Type: TABLE; Schema: sgrt; Owner: -
--

CREATE TABLE sgrt.tsgrtticket (
    cod_ticket integer DEFAULT nextval('sgrt.seq_ticket'::regclass) NOT NULL,
    des_folio character varying(45) NOT NULL,
    cod_reunion integer,
    cod_acuerdo integer,
    cod_responsable integer,
    cod_validador integer,
    cod_depto integer NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_topico integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_tema character varying(64) DEFAULT '[Sin Asunto]'::character varying NOT NULL,
    des_temaayuda character varying(255) DEFAULT NULL::character varying,
    cod_telefono character varying(16) DEFAULT NULL::character varying,
    cod_extension character varying(8) DEFAULT NULL::character varying,
    cod_estadot sgrt.edoticket DEFAULT 'Abierto'::sgrt.edoticket NOT NULL,
    cod_origent sgrt.origencontac DEFAULT 'Otro'::sgrt.origencontac NOT NULL,
    cnu_expirado smallint NOT NULL,
    cnu_atendido smallint NOT NULL,
    fec_exp timestamp without time zone,
    fec_reap timestamp without time zone,
    fec_cierre timestamp without time zone,
    fec_ultimomsg timestamp without time zone,
    fec_ultimaresp timestamp without time zone,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_ejecutor integer
);


--
-- TOC entry 3696 (class 0 OID 33920)
-- Dependencies: 319
-- Data for Name: tsgrtagenda; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3697 (class 0 OID 33925)
-- Dependencies: 320
-- Data for Name: tsgrtarchivos; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3698 (class 0 OID 33932)
-- Dependencies: 321
-- Data for Name: tsgrtasistentes; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3699 (class 0 OID 33936)
-- Dependencies: 322
-- Data for Name: tsgrtattchticket; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3700 (class 0 OID 33943)
-- Dependencies: 323
-- Data for Name: tsgrtayudatopico; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3701 (class 0 OID 33947)
-- Dependencies: 324
-- Data for Name: tsgrtcategoriafaq; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3702 (class 0 OID 33954)
-- Dependencies: 325
-- Data for Name: tsgrtchat; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3703 (class 0 OID 33961)
-- Dependencies: 326
-- Data for Name: tsgrtciudades; Type: TABLE DATA; Schema: sgrt; Owner: -
--

INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (6, 'ZACATLAN DE LAS MANZANAS', 1);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (7, 'CIUDAD DE MEXICO', 4);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (8, 'XALAPA', 3);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (9, 'POZA RICA', 3);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (10, 'TLAXCALA', 2);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (11, 'APIZACO', 2);


--
-- TOC entry 3704 (class 0 OID 33965)
-- Dependencies: 327
-- Data for Name: tsgrtcomentariosagenda; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3705 (class 0 OID 33972)
-- Dependencies: 328
-- Data for Name: tsgrtcomentariosreunion; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3706 (class 0 OID 33979)
-- Dependencies: 329
-- Data for Name: tsgrtcompromisos; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3707 (class 0 OID 33985)
-- Dependencies: 330
-- Data for Name: tsgrtcorreo; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3708 (class 0 OID 33991)
-- Dependencies: 331
-- Data for Name: tsgrtdatossolicitud; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3709 (class 0 OID 33994)
-- Dependencies: 332
-- Data for Name: tsgrtdepartamento; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3710 (class 0 OID 34003)
-- Dependencies: 333
-- Data for Name: tsgrtedosolicitudes; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3711 (class 0 OID 34006)
-- Dependencies: 334
-- Data for Name: tsgrtelementos; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3712 (class 0 OID 34010)
-- Dependencies: 335
-- Data for Name: tsgrtestados; Type: TABLE DATA; Schema: sgrt; Owner: -
--

INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (1, 'PUEBLA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (2, 'TLAXCALA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (3, 'VERACRUZ');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (4, 'DISTRITO FEDERAL');


--
-- TOC entry 3713 (class 0 OID 34014)
-- Dependencies: 336
-- Data for Name: tsgrtfaq; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3714 (class 0 OID 34021)
-- Dependencies: 337
-- Data for Name: tsgrtgrupo; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3715 (class 0 OID 34026)
-- Dependencies: 338
-- Data for Name: tsgrtinvitados; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3716 (class 0 OID 34033)
-- Dependencies: 339
-- Data for Name: tsgrtlugares; Type: TABLE DATA; Schema: sgrt; Owner: -
--

INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (2, 'TLAXCALA DE XICONTECATL', 10);
INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (3, 'SANTA ANA', 10);
INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (4, 'CHIUATEMPAN', 10);


--
-- TOC entry 3717 (class 0 OID 34037)
-- Dependencies: 340
-- Data for Name: tsgrtmsjticket; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3718 (class 0 OID 34047)
-- Dependencies: 341
-- Data for Name: tsgrtnota; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3719 (class 0 OID 34055)
-- Dependencies: 342
-- Data for Name: tsgrtplantillacorreos; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3720 (class 0 OID 34063)
-- Dependencies: 343
-- Data for Name: tsgrtprioridad; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3721 (class 0 OID 34067)
-- Dependencies: 344
-- Data for Name: tsgrtresppredefinida; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3722 (class 0 OID 34074)
-- Dependencies: 345
-- Data for Name: tsgrtrespuesta; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3723 (class 0 OID 34081)
-- Dependencies: 346
-- Data for Name: tsgrtreuniones; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3724 (class 0 OID 34088)
-- Dependencies: 347
-- Data for Name: tsgrtservicios; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3725 (class 0 OID 34092)
-- Dependencies: 348
-- Data for Name: tsgrtsolicitudservicios; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3726 (class 0 OID 34096)
-- Dependencies: 349
-- Data for Name: tsgrtticket; Type: TABLE DATA; Schema: sgrt; Owner: -
--



--
-- TOC entry 3733 (class 0 OID 0)
-- Dependencies: 287
-- Name: seq_agenda; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_agenda', 1, false);


--
-- TOC entry 3734 (class 0 OID 0)
-- Dependencies: 288
-- Name: seq_archivo; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_archivo', 1, false);


--
-- TOC entry 3735 (class 0 OID 0)
-- Dependencies: 289
-- Name: seq_asistente; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_asistente', 1, false);


--
-- TOC entry 3736 (class 0 OID 0)
-- Dependencies: 290
-- Name: seq_attach; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_attach', 1, false);


--
-- TOC entry 3737 (class 0 OID 0)
-- Dependencies: 291
-- Name: seq_categoriafaq; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_categoriafaq', 1, false);


--
-- TOC entry 3738 (class 0 OID 0)
-- Dependencies: 292
-- Name: seq_chat; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_chat', 1, false);


--
-- TOC entry 3739 (class 0 OID 0)
-- Dependencies: 293
-- Name: seq_ciudad; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_ciudad', 11, true);


--
-- TOC entry 3740 (class 0 OID 0)
-- Dependencies: 294
-- Name: seq_comentsagenda; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_comentsagenda', 1, false);


--
-- TOC entry 3741 (class 0 OID 0)
-- Dependencies: 295
-- Name: seq_comentsreunion; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_comentsreunion', 1, false);


--
-- TOC entry 3742 (class 0 OID 0)
-- Dependencies: 296
-- Name: seq_compromiso; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_compromiso', 1, false);


--
-- TOC entry 3743 (class 0 OID 0)
-- Dependencies: 297
-- Name: seq_contacto; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_contacto', 1, false);


--
-- TOC entry 3744 (class 0 OID 0)
-- Dependencies: 298
-- Name: seq_correo; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_correo', 1, false);


--
-- TOC entry 3745 (class 0 OID 0)
-- Dependencies: 299
-- Name: seq_depto; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_depto', 1, false);


--
-- TOC entry 3746 (class 0 OID 0)
-- Dependencies: 300
-- Name: seq_edoacuerdo; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_edoacuerdo', 1, false);


--
-- TOC entry 3747 (class 0 OID 0)
-- Dependencies: 301
-- Name: seq_elemento; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_elemento', 1, false);


--
-- TOC entry 3748 (class 0 OID 0)
-- Dependencies: 302
-- Name: seq_estadorep; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_estadorep', 4, true);


--
-- TOC entry 3749 (class 0 OID 0)
-- Dependencies: 303
-- Name: seq_faq; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_faq', 1, false);


--
-- TOC entry 3750 (class 0 OID 0)
-- Dependencies: 304
-- Name: seq_grupo; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_grupo', 1, false);


--
-- TOC entry 3751 (class 0 OID 0)
-- Dependencies: 305
-- Name: seq_invitado; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_invitado', 1, false);


--
-- TOC entry 3752 (class 0 OID 0)
-- Dependencies: 306
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_lugar', 4, true);


--
-- TOC entry 3753 (class 0 OID 0)
-- Dependencies: 307
-- Name: seq_mensaje; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_mensaje', 1, false);


--
-- TOC entry 3754 (class 0 OID 0)
-- Dependencies: 308
-- Name: seq_nota; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_nota', 1, false);


--
-- TOC entry 3755 (class 0 OID 0)
-- Dependencies: 309
-- Name: seq_plantillacorreo; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_plantillacorreo', 1, false);


--
-- TOC entry 3756 (class 0 OID 0)
-- Dependencies: 310
-- Name: seq_prioridad; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_prioridad', 1, false);


--
-- TOC entry 3757 (class 0 OID 0)
-- Dependencies: 311
-- Name: seq_resp; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_resp', 1, false);


--
-- TOC entry 3758 (class 0 OID 0)
-- Dependencies: 312
-- Name: seq_respuesta; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_respuesta', 1, false);


--
-- TOC entry 3759 (class 0 OID 0)
-- Dependencies: 313
-- Name: seq_respuestas_participantes; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_respuestas_participantes', 1, false);


--
-- TOC entry 3760 (class 0 OID 0)
-- Dependencies: 314
-- Name: seq_reunion; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_reunion', 1, false);


--
-- TOC entry 3761 (class 0 OID 0)
-- Dependencies: 315
-- Name: seq_servicio; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_servicio', 1, false);


--
-- TOC entry 3762 (class 0 OID 0)
-- Dependencies: 316
-- Name: seq_solicitud; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_solicitud', 1, false);


--
-- TOC entry 3763 (class 0 OID 0)
-- Dependencies: 317
-- Name: seq_ticket; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_ticket', 1, false);


--
-- TOC entry 3764 (class 0 OID 0)
-- Dependencies: 318
-- Name: seq_topico; Type: SEQUENCE SET; Schema: sgrt; Owner: -
--

SELECT pg_catalog.setval('sgrt.seq_topico', 1, false);


--
-- TOC entry 3316 (class 2606 OID 34362)
-- Name: tsgrtagenda cod_agenda; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT cod_agenda UNIQUE (cod_agenda);


--
-- TOC entry 3320 (class 2606 OID 34364)
-- Name: tsgrtarchivos cod_archivo; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_archivo UNIQUE (cod_archivo);


--
-- TOC entry 3326 (class 2606 OID 34366)
-- Name: tsgrtasistentes cod_asistente; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT cod_asistente UNIQUE (cod_asistente);


--
-- TOC entry 3330 (class 2606 OID 34368)
-- Name: tsgrtattchticket cod_attach; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_attach UNIQUE (cod_attach);


--
-- TOC entry 3391 (class 2606 OID 34370)
-- Name: tsgrtfaq cod_categofaq; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_categofaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3342 (class 2606 OID 34372)
-- Name: tsgrtcategoriafaq cod_categoriafaq; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT cod_categoriafaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3350 (class 2606 OID 34374)
-- Name: tsgrtcomentariosagenda cod_comentsagenda; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT cod_comentsagenda UNIQUE (cod_comentsagenda);


--
-- TOC entry 3363 (class 2606 OID 34376)
-- Name: tsgrtcorreo cod_correo; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT cod_correo UNIQUE (cod_correo);


--
-- TOC entry 3369 (class 2606 OID 34378)
-- Name: tsgrtdatossolicitud cod_datosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT cod_datosolicitud UNIQUE (cod_datosolicitud);


--
-- TOC entry 3373 (class 2606 OID 34380)
-- Name: tsgrtdepartamento cod_depto; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT cod_depto UNIQUE (cod_depto);


--
-- TOC entry 3450 (class 2606 OID 34382)
-- Name: tsgrtticket cod_deptoticket; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_deptoticket UNIQUE (cod_depto);


--
-- TOC entry 3379 (class 2606 OID 34384)
-- Name: tsgrtedosolicitudes cod_edosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT cod_edosolicitud UNIQUE (cod_edosolicitud);


--
-- TOC entry 3383 (class 2606 OID 34386)
-- Name: tsgrtelementos cod_elemento; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT cod_elemento UNIQUE (cod_elemento);


--
-- TOC entry 3452 (class 2606 OID 34388)
-- Name: tsgrtticket cod_empleado; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_empleado UNIQUE (cod_empleado);


--
-- TOC entry 3387 (class 2606 OID 34390)
-- Name: tsgrtestados cod_estadorep; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT cod_estadorep UNIQUE (cod_estadorep);


--
-- TOC entry 3393 (class 2606 OID 34392)
-- Name: tsgrtfaq cod_faq; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_faq UNIQUE (cod_faq);


--
-- TOC entry 3397 (class 2606 OID 34394)
-- Name: tsgrtgrupo cod_grupo; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT cod_grupo UNIQUE (cod_grupo);


--
-- TOC entry 3401 (class 2606 OID 34396)
-- Name: tsgrtinvitados cod_invitado; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT cod_invitado UNIQUE (cod_invitado);


--
-- TOC entry 3406 (class 2606 OID 34398)
-- Name: tsgrtlugares cod_lugar; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT cod_lugar UNIQUE (cod_lugar);


--
-- TOC entry 3410 (class 2606 OID 34400)
-- Name: tsgrtmsjticket cod_mensaje; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT cod_mensaje UNIQUE (cod_mensaje);


--
-- TOC entry 3414 (class 2606 OID 34402)
-- Name: tsgrtnota cod_nota; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT cod_nota UNIQUE (cod_nota);


--
-- TOC entry 3418 (class 2606 OID 34404)
-- Name: tsgrtplantillacorreos cod_plantillacorreo; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT cod_plantillacorreo UNIQUE (cod_plantillacorreo);


--
-- TOC entry 3422 (class 2606 OID 34406)
-- Name: tsgrtprioridad cod_prioridad; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT cod_prioridad UNIQUE (cod_prioridad);


--
-- TOC entry 3434 (class 2606 OID 34408)
-- Name: tsgrtrespuesta cod_respuesta; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT cod_respuesta UNIQUE (cod_respuesta);


--
-- TOC entry 3428 (class 2606 OID 34410)
-- Name: tsgrtresppredefinida cod_respuestapredf; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT cod_respuestapredf UNIQUE (cod_respuesta);


--
-- TOC entry 3322 (class 2606 OID 34412)
-- Name: tsgrtarchivos cod_reunionarchivos; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_reunionarchivos UNIQUE (cod_reunion);


--
-- TOC entry 3438 (class 2606 OID 34414)
-- Name: tsgrtreuniones cod_reuniones; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT cod_reuniones UNIQUE (cod_reunion);


--
-- TOC entry 3442 (class 2606 OID 34416)
-- Name: tsgrtservicios cod_servicio; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT cod_servicio UNIQUE (cod_servicio);


--
-- TOC entry 3446 (class 2606 OID 34418)
-- Name: tsgrtsolicitudservicios cod_solicitud; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT cod_solicitud UNIQUE (cod_solicitud);


--
-- TOC entry 3454 (class 2606 OID 34420)
-- Name: tsgrtticket cod_ticket; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_ticket UNIQUE (cod_ticket);


--
-- TOC entry 3332 (class 2606 OID 34422)
-- Name: tsgrtattchticket cod_ticketattach; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_ticketattach UNIQUE (cod_ticket);


--
-- TOC entry 3336 (class 2606 OID 34424)
-- Name: tsgrtayudatopico cod_topico; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT cod_topico UNIQUE (cod_topico);


--
-- TOC entry 3318 (class 2606 OID 34426)
-- Name: tsgrtagenda tsgrtagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT tsgrtagenda_pkey PRIMARY KEY (cod_agenda);


--
-- TOC entry 3324 (class 2606 OID 34428)
-- Name: tsgrtarchivos tsgrtarchivos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT tsgrtarchivos_pkey PRIMARY KEY (cod_archivo);


--
-- TOC entry 3328 (class 2606 OID 34430)
-- Name: tsgrtasistentes tsgrtasistentes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT tsgrtasistentes_pkey PRIMARY KEY (cod_asistente);


--
-- TOC entry 3334 (class 2606 OID 34432)
-- Name: tsgrtattchticket tsgrtattchticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT tsgrtattchticket_pkey PRIMARY KEY (cod_attach);


--
-- TOC entry 3338 (class 2606 OID 34434)
-- Name: tsgrtayudatopico tsgrtayudatopico_des_topico_key; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_des_topico_key UNIQUE (des_topico);


--
-- TOC entry 3340 (class 2606 OID 34436)
-- Name: tsgrtayudatopico tsgrtayudatopico_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_pkey PRIMARY KEY (cod_topico);


--
-- TOC entry 3344 (class 2606 OID 34438)
-- Name: tsgrtcategoriafaq tsgrtcategoriafaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT tsgrtcategoriafaq_pkey PRIMARY KEY (cod_categoriafaq);


--
-- TOC entry 3346 (class 2606 OID 34440)
-- Name: tsgrtchat tsgrtchat_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtchat
    ADD CONSTRAINT tsgrtchat_pkey PRIMARY KEY (cod_chat);


--
-- TOC entry 3348 (class 2606 OID 34442)
-- Name: tsgrtciudades tsgrtciudades_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT tsgrtciudades_pkey PRIMARY KEY (cod_ciudad);


--
-- TOC entry 3353 (class 2606 OID 34444)
-- Name: tsgrtcomentariosagenda tsgrtcomentariosagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT tsgrtcomentariosagenda_pkey PRIMARY KEY (cod_comentsagenda);


--
-- TOC entry 3356 (class 2606 OID 34446)
-- Name: tsgrtcomentariosreunion tsgrtcomentariosreunion_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT tsgrtcomentariosreunion_pkey PRIMARY KEY (cod_commentsreunion);


--
-- TOC entry 3359 (class 2606 OID 34448)
-- Name: tsgrtcompromisos tsgrtcompromisos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT tsgrtcompromisos_pkey PRIMARY KEY (cod_compromiso);


--
-- TOC entry 3365 (class 2606 OID 34450)
-- Name: tsgrtcorreo tsgrtcorreo_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3367 (class 2606 OID 34452)
-- Name: tsgrtcorreo tsgrtcorreo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_pkey PRIMARY KEY (cod_correo);


--
-- TOC entry 3371 (class 2606 OID 34454)
-- Name: tsgrtdatossolicitud tsgrtdatossolicitud_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT tsgrtdatossolicitud_pkey PRIMARY KEY (cod_datosolicitud);


--
-- TOC entry 3375 (class 2606 OID 34456)
-- Name: tsgrtdepartamento tsgrtdepartamento_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3377 (class 2606 OID 34458)
-- Name: tsgrtdepartamento tsgrtdepartamento_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_pkey PRIMARY KEY (cod_depto);


--
-- TOC entry 3381 (class 2606 OID 34460)
-- Name: tsgrtedosolicitudes tsgrtedosolicitudes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT tsgrtedosolicitudes_pkey PRIMARY KEY (cod_edosolicitud);


--
-- TOC entry 3385 (class 2606 OID 34462)
-- Name: tsgrtelementos tsgrtelementos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT tsgrtelementos_pkey PRIMARY KEY (cod_elemento);


--
-- TOC entry 3389 (class 2606 OID 34464)
-- Name: tsgrtestados tsgrtestados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT tsgrtestados_pkey PRIMARY KEY (cod_estadorep);


--
-- TOC entry 3395 (class 2606 OID 34466)
-- Name: tsgrtfaq tsgrtfaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT tsgrtfaq_pkey PRIMARY KEY (cod_faq);


--
-- TOC entry 3399 (class 2606 OID 34468)
-- Name: tsgrtgrupo tsgrtgrupo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT tsgrtgrupo_pkey PRIMARY KEY (cod_grupo);


--
-- TOC entry 3404 (class 2606 OID 34470)
-- Name: tsgrtinvitados tsgrtinvitados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT tsgrtinvitados_pkey PRIMARY KEY (cod_invitado);


--
-- TOC entry 3408 (class 2606 OID 34472)
-- Name: tsgrtlugares tsgrtlugares_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT tsgrtlugares_pkey PRIMARY KEY (cod_lugar);


--
-- TOC entry 3412 (class 2606 OID 34474)
-- Name: tsgrtmsjticket tsgrtmsjticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT tsgrtmsjticket_pkey PRIMARY KEY (cod_mensaje);


--
-- TOC entry 3416 (class 2606 OID 34476)
-- Name: tsgrtnota tsgrtnota_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT tsgrtnota_pkey PRIMARY KEY (cod_nota);


--
-- TOC entry 3420 (class 2606 OID 34478)
-- Name: tsgrtplantillacorreos tsgrtplantillacorreos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT tsgrtplantillacorreos_pkey PRIMARY KEY (cod_plantillacorreo);


--
-- TOC entry 3424 (class 2606 OID 34480)
-- Name: tsgrtprioridad tsgrtprioridad_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3426 (class 2606 OID 34482)
-- Name: tsgrtprioridad tsgrtprioridad_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_pkey PRIMARY KEY (cod_prioridad);


--
-- TOC entry 3430 (class 2606 OID 34484)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_des_titulo_key; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_des_titulo_key UNIQUE (des_titulo);


--
-- TOC entry 3432 (class 2606 OID 34486)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3436 (class 2606 OID 34488)
-- Name: tsgrtrespuesta tsgrtrespuesta_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT tsgrtrespuesta_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3440 (class 2606 OID 34490)
-- Name: tsgrtreuniones tsgrtreuniones_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT tsgrtreuniones_pkey PRIMARY KEY (cod_reunion);


--
-- TOC entry 3444 (class 2606 OID 34492)
-- Name: tsgrtservicios tsgrtservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT tsgrtservicios_pkey PRIMARY KEY (cod_servicio);


--
-- TOC entry 3448 (class 2606 OID 34494)
-- Name: tsgrtsolicitudservicios tsgrtsolicitudservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT tsgrtsolicitudservicios_pkey PRIMARY KEY (cod_solicitud);


--
-- TOC entry 3456 (class 2606 OID 34496)
-- Name: tsgrtticket tsgrtticket_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3458 (class 2606 OID 34498)
-- Name: tsgrtticket tsgrtticket_des_folio_key; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_folio_key UNIQUE (des_folio);


--
-- TOC entry 3460 (class 2606 OID 34500)
-- Name: tsgrtticket tsgrtticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_pkey PRIMARY KEY (cod_ticket);


--
-- TOC entry 3361 (class 2606 OID 34502)
-- Name: tsgrtcompromisos unique_cod_chat; Type: CONSTRAINT; Schema: sgrt; Owner: -
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT unique_cod_chat UNIQUE (cod_chat);


--
-- TOC entry 3357 (class 1259 OID 34537)
-- Name: fki_fk_cod_chat; Type: INDEX; Schema: sgrt; Owner: -
--

CREATE INDEX fki_fk_cod_chat ON sgrt.tsgrtcompromisos USING btree (cod_chat);


--
-- TOC entry 3402 (class 1259 OID 34538)
-- Name: fki_fk_cod_empleado; Type: INDEX; Schema: sgrt; Owner: -
--

CREATE INDEX fki_fk_cod_empleado ON sgrt.tsgrtinvitados USING btree (cod_empleado);


--
-- TOC entry 3354 (class 1259 OID 34539)
-- Name: fki_fk_cod_invitado; Type: INDEX; Schema: sgrt; Owner: -
--

CREATE INDEX fki_fk_cod_invitado ON sgrt.tsgrtcomentariosreunion USING btree (cod_invitado);


--
-- TOC entry 3351 (class 1259 OID 34540)
-- Name: fki_fk_cod_invitados; Type: INDEX; Schema: sgrt; Owner: -
--

CREATE INDEX fki_fk_cod_invitados ON sgrt.tsgrtcomentariosagenda USING btree (cod_invitado);

