
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;



COMMENT ON SCHEMA sgco IS 'Sistema de Gestion de Conocimiento de la Organizacion.';


--
-- TOC entry 204 (class 1259 OID 33555)
-- Name: seq_sistema; Type: SEQUENCE; Schema: sgco; Owner: -
--

CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 205 (class 1259 OID 33557)
-- Name: seq_tipousuario; Type: SEQUENCE; Schema: sgco; Owner: -
--

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 206 (class 1259 OID 33559)
-- Name: seq_usuarios; Type: SEQUENCE; Schema: sgco; Owner: -
--

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_with_oids = false;

--
-- TOC entry 207 (class 1259 OID 33561)
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: -
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


--
-- TOC entry 208 (class 1259 OID 33564)
-- Name: tsgcotipousuario; Type: TABLE; Schema: sgco; Owner: -
--

CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol character varying(35) NOT NULL
);


--
-- TOC entry 209 (class 1259 OID 33567)
-- Name: tsgcousuarios; Type: TABLE; Schema: sgco; Owner: -
--

CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(60) NOT NULL,
    des_contrasenacorreo character varying(50) NOT NULL,
    cod_usuariosistema character varying(30) NOT NULL,
    des_contrasenasistema character varying(30) NOT NULL
);


--
-- TOC entry 3364 (class 0 OID 33561)
-- Dependencies: 207
-- Data for Name: tsgcosistemas; Type: TABLE DATA; Schema: sgco; Owner: -
--

INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (1, 'Sistema de Recursos Humanos y Ambiente de Trabajo', 'SGRHAT');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (2, 'Sistema de Integración', 'SISAT');


--
-- TOC entry 3365 (class 0 OID 33564)
-- Dependencies: 208
-- Data for Name: tsgcotipousuario; Type: TABLE DATA; Schema: sgco; Owner: -
--



--
-- TOC entry 3366 (class 0 OID 33567)
-- Dependencies: 209
-- Data for Name: tsgcousuarios; Type: TABLE DATA; Schema: sgco; Owner: -
--

INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (2, 11, 'adrian.suarez@gmail.com', '123456789', 'adrian_suarez', '123456789');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (3, 12, 'carlos.antonio@gmail.com', 'abcdefg', 'carlos_antonio', 'abcdefg');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (4, 13, 'angel.roano@gmail.com', '123456', 'angel_roano', '123456');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (1, 10, 'mateorj96@gmail.com', '143100365m@teorj96', 'mateorj96', 'root');


--
-- TOC entry 3373 (class 0 OID 0)
-- Dependencies: 204
-- Name: seq_sistema; Type: SEQUENCE SET; Schema: sgco; Owner: -
--

SELECT pg_catalog.setval('sgco.seq_sistema', 1, false);


--
-- TOC entry 3374 (class 0 OID 0)
-- Dependencies: 205
-- Name: seq_tipousuario; Type: SEQUENCE SET; Schema: sgco; Owner: -
--

SELECT pg_catalog.setval('sgco.seq_tipousuario', 1, false);


--
-- TOC entry 3375 (class 0 OID 0)
-- Dependencies: 206
-- Name: seq_usuarios; Type: SEQUENCE SET; Schema: sgco; Owner: -
--

SELECT pg_catalog.setval('sgco.seq_usuarios', 1, false);


--
-- TOC entry 3228 (class 2606 OID 34272)
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: -
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- TOC entry 3232 (class 2606 OID 34274)
-- Name: tsgcotipousuario cod_archivo; Type: CONSTRAINT; Schema: sgco; Owner: -
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);


--
-- TOC entry 3236 (class 2606 OID 34276)
-- Name: tsgcousuarios cod_asistente; Type: CONSTRAINT; Schema: sgco; Owner: -
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);


--
-- TOC entry 3230 (class 2606 OID 34278)
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: -
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);


--
-- TOC entry 3234 (class 2606 OID 34280)
-- Name: tsgcotipousuario tsgcotipousuario_pkey; Type: CONSTRAINT; Schema: sgco; Owner: -
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT tsgcotipousuario_pkey PRIMARY KEY (cod_tipousuario);


--
-- TOC entry 3238 (class 2606 OID 34282)
-- Name: tsgcousuarios tsgcousuarios_pkey; Type: CONSTRAINT; Schema: sgco; Owner: -
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT tsgcousuarios_pkey PRIMARY KEY (cod_usuario);
