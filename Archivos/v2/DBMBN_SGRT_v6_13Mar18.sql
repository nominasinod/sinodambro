
--===*** SELECCIONAR Y EJECUTAR PRIMERO en esquema public de la bd POSTGRES ***===

CREATE USER suite WITH
  LOGIN
  SUPERUSER
  INHERIT
  CREATEDB
  CREATEROLE
  REPLICATION;

COMMENT ON ROLE suite IS 'Usuario principal de la base de datos de la suite de desarrollo.';


--===*** SELECCIONAR Y EJECUTAR SEGUNDO en esquema public de la bd POSTGRES ***===

CREATE TABLESPACE TBS_suite
  OWNER suite
  LOCATION 'C:\TBS_SUITE';


--===*** SELECCIONAR Y EJECUTAR TERCERO en esquema public de la bd POSTGRES ***===

CREATE DATABASE suite
    WITH
    OWNER = suite
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Mexico.1252'
    LC_CTYPE = 'Spanish_Mexico.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


--===*** SELECCIONAR Y EJECUTAR CUARTO en esquema public de la bd POSTGRES ***===
------------------------------------------
ALTER TABLESPACE tbs_suite
  OWNER TO suite;

COMMENT ON TABLESPACE tbs_suite
  IS 'TABLESPACE dedicado a la base de datos de la suite de desarrollo.';
------------------------------------------

ALTER DEFAULT PRIVILEGES
GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT ALL ON TABLES TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT USAGE, SELECT ON SEQUENCES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT USAGE, SELECT ON SEQUENCES TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT EXECUTE ON FUNCTIONS TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT EXECUTE ON FUNCTIONS TO PUBLIC;

ALTER DEFAULT PRIVILEGES
GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES
GRANT USAGE ON TYPES TO postgres;

ALTER DEFAULT PRIVILEGES
GRANT USAGE ON TYPES TO PUBLIC;




--	EJECUTAR DENTRO DE LA BASE DE DATOS SUITE
--===*** SELECCIONAR Y EJECUTAR QUINTO***===
-- Base de datos: suite
-- SCHEMA: sgrt
-- DROP SCHEMA sgrt ;

CREATE SCHEMA sgrt
    AUTHORIZATION suite;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT SELECT, USAGE ON SEQUENCES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgrt
GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;

GRANT ALL ON SCHEMA sgrt TO suite;

SET ROLE TO suite;




------------------------------
----...:::  DROPS  :::...-----
------------------------------
DROP TYPE destinatario;
DROP TYPE edoticket;
DROP TYPE encriptacion;
DROP TYPE estatus;
DROP TYPE estatus_compromiso;
DROP TYPE modulo;
DROP TYPE origencontac;
DROP TYPE prioridad;
DROP TYPE protocolo;
DROP TYPE tipo;
DROP TYPE tipo_compromiso;


DROP FUNCTION sgrt.buscar_asistentes_minuta(reunionid INTEGER);
DROP FUNCTION sgrt.buscar_compromisos_roles_list(reunionid INTEGER)
DROP FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text);
DROP FUNCTION sgrt.buscar_proxima_reunion(reunionid INTEGER, OUT cod_reunion INTEGER, OUT des_nombre_reunion VARCHAR, OUT fec_fecha text, OUT cod_lugar INTEGER, OUT tim_hora VARCHAR, OUT des_nombre_lugar VARCHAR, OUT cod_ciudad INTEGER, OUT des_nbciudad VARCHAR, OUT cod_estadorep_ciudad INTEGER, OUT des_nbestado VARCHAR, OUT cod_estadorep_estado INTEGER);
DROP FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text);
DROP FUNCTION sgrt.compromisos_dia(fechacompromiso text);
DROP FUNCTION sgrt.compromisos_generales();
DROP FUNCTION sgrt.reporte_por_tema(reunionid INTEGER);


DROP SEQUENCE sgrt.seq_agenda;
DROP SEQUENCE sgrt.seq_archivo;
DROP SEQUENCE sgrt.seq_asistente;
DROP SEQUENCE sgrt.seq_attach;
DROP SEQUENCE sgrt.seq_categoriafaq;
DROP SEQUENCE sgrt.seq_chat;
DROP SEQUENCE sgrt.seq_ciudad;
DROP SEQUENCE sgrt.seq_comentsagenda;
DROP SEQUENCE sgrt.seq_comentsreunion;
DROP SEQUENCE sgrt.seq_compromiso;
DROP SEQUENCE sgrt.seq_contacto;
DROP SEQUENCE sgrt.seq_correo;
DROP SEQUENCE sgrt.seq_depto;
DROP SEQUENCE sgrt.seq_edoacuerdo;
DROP SEQUENCE sgrt.seq_elemento;
DROP SEQUENCE sgrt.seq_estadorep;
DROP SEQUENCE sgrt.seq_faq;
DROP SEQUENCE sgrt.seq_grupo;
DROP SEQUENCE sgrt.seq_invitado;
DROP SEQUENCE sgrt.seq_lugar;
DROP SEQUENCE sgrt.seq_mensaje;
DROP SEQUENCE sgrt.seq_nota;
DROP SEQUENCE sgrt.seq_plantillacorreo;
DROP SEQUENCE sgrt.seq_prioridad;
DROP SEQUENCE sgrt.seq_edosolicitudes;
DROP SEQUENCE sgrt.seq_resp;
DROP SEQUENCE sgrt.seq_respuesta;
DROP SEQUENCE sgrt.seq_reunion;
DROP SEQUENCE sgrt.seq_servicio;
DROP SEQUENCE sgrt.seq_solicitud;
DROP SEQUENCE sgrt.seq_ticket;
DROP SEQUENCE sgrt.seq_topico;


DROP TABLE sgrt.tsgrtagenda;
DROP TABLE sgrt.tsgrtarchivos;
DROP TABLE sgrt.tsgrtasistentes;
DROP TABLE sgrt.tsgrtattchticket;
DROP TABLE sgrt.tsgrtayudatopico;
DROP TABLE sgrt.tsgrtcategoriafaq;
DROP TABLE sgrt.tsgrtchat;
DROP TABLE sgrt.tsgrtciudades;
DROP TABLE sgrt.tsgrtcomentariosagenda;
DROP TABLE sgrt.tsgrtcomentariosreunion;
DROP TABLE sgrt.tsgrtcompromisos;
DROP TABLE sgrt.tsgrtcorreo;
DROP TABLE sgrt.tsgrtdatossolicitud;
DROP TABLE sgrt.tsgrtdepartamento;
DROP TABLE sgrt.tsgrtedosolicitudes;
DROP TABLE sgrt.tsgrtelementos;
DROP TABLE sgrt.tsgrtestados;
DROP TABLE sgrt.tsgrtfaq;
DROP TABLE sgrt.tsgrtgrupo;
DROP TABLE sgrt.tsgrtinvitados;
DROP TABLE sgrt.tsgrtlugares;
DROP TABLE sgrt.tsgrtmsjticket;
DROP TABLE sgrt.tsgrtnota;
DROP TABLE sgrt.tsgrtplantillacorreos;
DROP TABLE sgrt.tsgrtprioridad;
DROP TABLE sgrt.tsgrtresppredefinida;
DROP TABLE sgrt.tsgrtrespuesta;
DROP TABLE sgrt.tsgrtreuniones;
DROP TABLE sgrt.tsgrtservicios;
DROP TABLE sgrt.tsgrtsolicitudservicios;
DROP TABLE sgrt.tsgrtticket;


ALTER TABLE ONLY sgrt.tsgrtagenda DROP CONSTRAINT tsgrtagenda_pkey;
ALTER TABLE ONLY sgrt.tsgrtarchivos DROP CONSTRAINT tsgrtarchivos_pkey;
ALTER TABLE ONLY sgrt.tsgrtasistentes DROP CONSTRAINT tsgrtasistentes_pkey;
ALTER TABLE ONLY sgrt.tsgrtattchticket DROP CONSTRAINT tsgrtattchticket_pkey;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT tsgrtayudatopico_pkey;
ALTER TABLE ONLY sgrt.tsgrtcategoriafaq DROP CONSTRAINT tsgrtcategoriafaq_pkey;
ALTER TABLE ONLY sgrt.tsgrtchat DROP CONSTRAINT tsgrtchat_pkey;
ALTER TABLE ONLY sgrt.tsgrtciudades DROP CONSTRAINT tsgrtciudades_pkey;
ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda DROP CONSTRAINT tsgrtcomentariosagenda_pkey;
ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion DROP CONSTRAINT tsgrtcomentariosreunion_pkey;
ALTER TABLE ONLY sgrt.tsgrtcompromisos DROP CONSTRAINT tsgrtcompromisos_pkey;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT tsgrtcorreo_pkey;
ALTER TABLE ONLY sgrt.tsgrtdatossolicitud DROP CONSTRAINT tsgrtdatossolicitud_pkey;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT tsgrtdepartamento_pkey;
ALTER TABLE ONLY sgrt.tsgrtelementos DROP CONSTRAINT tsgrtelementos_pkey;
ALTER TABLE ONLY sgrt.tsgrtestados DROP CONSTRAINT tsgrtestados_pkey;
ALTER TABLE ONLY sgrt.tsgrtfaq DROP CONSTRAINT tsgrtfaq_pkey;
ALTER TABLE ONLY sgrt.tsgrtgrupo DROP CONSTRAINT tsgrtgrupo_pkey;
ALTER TABLE ONLY sgrt.tsgrtinvitados DROP CONSTRAINT tsgrtinvitados_pkey;
ALTER TABLE ONLY sgrt.tsgrtlugares DROP CONSTRAINT tsgrtlugares_pkey;
ALTER TABLE ONLY sgrt.tsgrtmsjticket DROP CONSTRAINT tsgrtmsjticket_pkey;
ALTER TABLE ONLY sgrt.tsgrtnota DROP CONSTRAINT tsgrtnota_pkey;
ALTER TABLE ONLY sgrt.tsgrtplantillacorreos DROP CONSTRAINT tsgrtplantillacorreos_pkey;
ALTER TABLE ONLY sgrt.tsgrtprioridad DROP CONSTRAINT tsgrtprioridad_pkey;
ALTER TABLE ONLY sgrt.tsgrtresppredefinida DROP CONSTRAINT tsgrtresppredefinida_pkey;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT tsgrtrespuesta_pkey;
ALTER TABLE ONLY sgrt.tsgrtreuniones DROP CONSTRAINT tsgrtreuniones_pkey;
ALTER TABLE ONLY sgrt.tsgrtservicios DROP CONSTRAINT tsgrtservicios_pkey;
ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios DROP CONSTRAINT tsgrtsolicitudservicios_pkey;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT tsgrtticket_pkey;
ALTER TABLE ONLY sgrt.tsgrtedosolicitudes DROP CONSTRAINT tsgrtedosolicitudes_pkey;


DROP INDEX sgrt.fki_fk_cod_chat;
DROP INDEX sgrt.fki_fk_cod_empleado;
DROP INDEX sgrt.fki_fk_cod_invitado;
DROP INDEX sgrt.fki_fk_cod_invitados;


ALTER TABLE ONLY sgrt.tsgrtagenda DROP CONSTRAINT cod_agenda;
ALTER TABLE ONLY sgrt.tsgrtarchivos DROP CONSTRAINT cod_archivo;
ALTER TABLE ONLY sgrt.tsgrtasistentes DROP CONSTRAINT cod_asistente;
ALTER TABLE ONLY sgrt.tsgrtattchticket DROP CONSTRAINT cod_attach;
ALTER TABLE ONLY sgrt.tsgrtfaq DROP CONSTRAINT cod_categofaq;
ALTER TABLE ONLY sgrt.tsgrtcategoriafaq DROP CONSTRAINT cod_categoriafaq;
ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda DROP CONSTRAINT cod_comentsagenda;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT cod_correo;
ALTER TABLE ONLY sgrt.tsgrtdatossolicitud DROP CONSTRAINT cod_datosolicitud;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT cod_depto;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT cod_deptoticket;
ALTER TABLE ONLY sgrt.tsgrtedosolicitudes DROP CONSTRAINT cod_edosolicitud;
ALTER TABLE ONLY sgrt.tsgrtelementos DROP CONSTRAINT cod_elemento;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT cod_empleado;
ALTER TABLE ONLY sgrt.tsgrtestados DROP CONSTRAINT cod_estadorep;
ALTER TABLE ONLY sgrt.tsgrtfaq DROP CONSTRAINT cod_faq;
ALTER TABLE ONLY sgrt.tsgrtgrupo DROP CONSTRAINT cod_grupo;
ALTER TABLE ONLY sgrt.tsgrtinvitados DROP CONSTRAINT cod_invitado;
ALTER TABLE ONLY sgrt.tsgrtlugares DROP CONSTRAINT cod_lugar;
ALTER TABLE ONLY sgrt.tsgrtmsjticket DROP CONSTRAINT cod_mensaje;
ALTER TABLE ONLY sgrt.tsgrtnota DROP CONSTRAINT cod_nota;
ALTER TABLE ONLY sgrt.tsgrtplantillacorreos DROP CONSTRAINT cod_plantillacorreo;
ALTER TABLE ONLY sgrt.tsgrtprioridad DROP CONSTRAINT cod_prioridad;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT cod_respuesta;
ALTER TABLE ONLY sgrt.tsgrtresppredefinida DROP CONSTRAINT cod_respuestapredf;
ALTER TABLE ONLY sgrt.tsgrtarchivos DROP CONSTRAINT cod_reunionarchivos;
ALTER TABLE ONLY sgrt.tsgrtreuniones DROP CONSTRAINT cod_reuniones;
ALTER TABLE ONLY sgrt.tsgrtservicios DROP CONSTRAINT cod_servicio;
ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios DROP CONSTRAINT cod_solicitud;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT cod_ticket;
ALTER TABLE ONLY sgrt.tsgrtattchticket DROP CONSTRAINT cod_ticketattach;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT cod_topico;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT tsgrtayudatopico_des_topico_key;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT tsgrtcorreo_des_correo_key;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT tsgrtdepartamento_des_nombre_key;
ALTER TABLE ONLY sgrt.tsgrtprioridad DROP CONSTRAINT tsgrtprioridad_des_nombre_key;
ALTER TABLE ONLY sgrt.tsgrtresppredefinida DROP CONSTRAINT tsgrtresppredefinida_des_titulo_key;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT tsgrtticket_des_correo_key;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT tsgrtticket_des_folio_key;
ALTER TABLE ONLY sgrt.tsgrtcompromisos DROP CONSTRAINT unique_cod_chat;



ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda DROP CONSTRAINT fk_cod_agenda;
ALTER TABLE ONLY sgrt.tsgrtfaq DROP CONSTRAINT fk_cod_categoria;
ALTER TABLE ONLY sgrt.tsgrtcompromisos DROP CONSTRAINT fk_cod_chat;
ALTER TABLE ONLY sgrt.tsgrtlugares DROP CONSTRAINT fk_cod_ciudad;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT fk_cod_correo;
ALTER TABLE ONLY sgrt.tsgrtcategoriafaq DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtfaq DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtgrupo DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtnota DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtplantillacorreos DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtprioridad DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtresppredefinida DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtattchticket DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtmsjticket DROP CONSTRAINT fk_cod_creadopor;
ALTER TABLE ONLY sgrt.tsgrtreuniones DROP CONSTRAINT fk_cod_creadorreunion;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT fk_cod_depto;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT fk_cod_depto;
ALTER TABLE ONLY sgrt.tsgrtresppredefinida DROP CONSTRAINT fk_cod_depto;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT fk_cod_depto;
ALTER TABLE ONLY sgrt.tsgrtdatossolicitud DROP CONSTRAINT fk_cod_edosolicitud;
ALTER TABLE ONLY sgrt.tsgrtdatossolicitud DROP CONSTRAINT fk_cod_elemento;
ALTER TABLE ONLY sgrt.tsgrtasistentes DROP CONSTRAINT fk_cod_empleado;
ALTER TABLE ONLY sgrt.tsgrtnota DROP CONSTRAINT fk_cod_empleado;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT fk_cod_empleado;
ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios DROP CONSTRAINT fk_cod_empleado;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT fk_cod_empleado;
ALTER TABLE ONLY sgrt.tsgrtinvitados DROP CONSTRAINT fk_cod_empleado;
ALTER TABLE ONLY sgrt.tsgrtasistentes DROP CONSTRAINT fk_cod_invitado;
ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion DROP CONSTRAINT fk_cod_invitado;
ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda DROP CONSTRAINT fk_cod_invitado;
ALTER TABLE ONLY sgrt.tsgrtreuniones DROP CONSTRAINT fk_cod_lugar;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT fk_cod_mensaje;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT fk_cod_plantillacorreo;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT fk_cod_prioridad;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT fk_cod_prioridad;
ALTER TABLE ONLY sgrt.tsgrtreuniones DROP CONSTRAINT fk_cod_responsable;
ALTER TABLE ONLY sgrt.tsgrtcompromisos DROP CONSTRAINT fk_cod_reunion;
ALTER TABLE ONLY sgrt.tsgrtarchivos DROP CONSTRAINT fk_cod_reunion;
ALTER TABLE ONLY sgrt.tsgrtinvitados DROP CONSTRAINT fk_cod_reunion;
ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion DROP CONSTRAINT fk_cod_reunion;
ALTER TABLE ONLY sgrt.tsgrtagenda DROP CONSTRAINT fk_cod_reunion;
ALTER TABLE ONLY sgrt.tsgrtreuniones DROP CONSTRAINT fk_cod_reunionanterior;
ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios DROP CONSTRAINT fk_cod_servicio;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtgrupo DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtnota DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtplantillacorreos DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtprioridad DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtresppredefinida DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtattchticket DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtmsjticket DROP CONSTRAINT fk_cod_sistemasuite;
ALTER TABLE ONLY sgrt.tsgrtdatossolicitud DROP CONSTRAINT fk_cod_solicitud;
ALTER TABLE ONLY sgrt.tsgrtnota DROP CONSTRAINT fk_cod_ticket;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT fk_cod_ticket;
ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios DROP CONSTRAINT fk_cod_ticket;
ALTER TABLE ONLY sgrt.tsgrtattchticket DROP CONSTRAINT fk_cod_ticket;
ALTER TABLE ONLY sgrt.tsgrtmsjticket DROP CONSTRAINT fk_cod_ticket;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT fk_cod_topico;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtdepartamento DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtfaq DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtgrupo DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtayudatopico DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtnota DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtplantillacorreos DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtprioridad DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtrespuesta DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtresppredefinida DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtticket DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtattchticket DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtmsjticket DROP CONSTRAINT fk_cod_ultactualizacionpor;
ALTER TABLE ONLY sgrt.tsgrtcorreo DROP CONSTRAINT fk_cod_usuario;
ALTER TABLE ONLY sgrt.tsgrtciudades DROP CONSTRAINT fk_estadorep;


DROP FUNCTION sgrt.buscar_asistentes_minuta;
DROP FUNCTION sgrt.buscar_compromisos_roles_list;
DROP FUNCTION sgrt.buscar_minutas_fechas;
DROP FUNCTION sgrt.buscar_proxima_reunion;
DROP FUNCTION sgrt.compromisos_areas_fechas;
DROP FUNCTION sgrt.compromisos_dia;
DROP FUNCTION sgrt.compromisos_generales;
DROP FUNCTION sgrt.reporte_por_tema;


-----------------------------------------------------------------------
-----------------------------------------------------------------------

CREATE TYPE sgrt.destinatario AS ENUM (
    'USR',
    'EMPLEADO',
    'GRUPO',
    'DEPTO'
);

CREATE TYPE sgrt.edoticket AS ENUM (
    'Abierto',
    'Cerrado'
);

CREATE TYPE sgrt.encriptacion AS ENUM (
    'NONE',
    'SSL'
);

CREATE TYPE sgrt.estatus AS ENUM (
    'Enviado',
    'Pendiente'
);

CREATE TYPE sgrt.estatus_compromiso AS ENUM (
    'Pendiente',
    'Terminado'
);

CREATE TYPE sgrt.modulo AS ENUM (
    'SGRT',
    'SSV'
);

CREATE TYPE sgrt.origencontac AS ENUM (
    'Web',
    'Email',
    'Reunion',
    'Telefono',
    'Otro'
);

CREATE TYPE sgrt.prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja'
);

CREATE TYPE sgrt.protocolo AS ENUM (
    'POP',
    'IMAP'
);

CREATE TYPE sgrt.tipo AS ENUM (
    'ReunionAgendada',
    'ReunionCancelada',
    'NuevaSolicitud',
    'SolicitudPagada',
    'ViaticoCancelado'
);

CREATE TYPE sgrt.tipo_compromiso AS ENUM (
    'Acuerdo',
    'Pendiente'
);



-------------------------------------
-------------------------------------



CREATE SEQUENCE sgrt.seq_agenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_archivo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_asistente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_attach
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_categoriafaq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_chat
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999999999
    CACHE 1;

CREATE SEQUENCE sgrt.seq_ciudad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_comentsagenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_comentsreunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_edosolicitudes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
	
CREATE SEQUENCE sgrt.seq_compromiso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgrt.seq_contacto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_correo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_depto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_edoacuerdo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_elemento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_estadorep
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_faq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_invitado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_mensaje
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_nota
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_plantillacorreo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_prioridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_resp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_respuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_reunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_servicio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_solicitud
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_ticket
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 CREATE SEQUENCE sgrt.seq_topico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

 ------------------------------------
 ------------------------------------


 CREATE TABLE sgrt.tsgrtagenda (
    cod_agenda INTEGER DEFAULT nextval('sgrt.seq_agenda'),
    des_texto VARCHAR(200) NOT NULL,
    cnu_tratado SMALLINT DEFAULT '0',
    cod_reunion INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtarchivos (
    cod_archivo INTEGER DEFAULT nextval('sgrt.seq_archivo') NOT NULL,
    bin_archivo bytea,
    cod_reunion INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtasistentes (
    cod_asistente INTEGER DEFAULT nextval('sgrt.seq_asistente') NOT NULL,
    cod_reunion INTEGER NOT NULL,
    cod_empleado INTEGER,
    cnu_asiste SMALLINT  NOT NULL,
    cod_invitado INTEGER
);

 CREATE TABLE sgrt.tsgrtattchticket (
    cod_attach INTEGER DEFAULT nextval('sgrt.seq_attach') NOT NULL,
    cod_ticket INTEGER NOT NULL,
    cod_tamano VARCHAR(20) NOT NULL,
    des_nombre VARCHAR(128) NOT NULL,
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone,
    bin_attach bytea
);

 CREATE TABLE sgrt.tsgrtayudatopico (
    cod_topico INTEGER DEFAULT nextval('sgrt.seq_topico') NOT NULL,
    cnu_activo SMALLINT NOT NULL,
    cnu_autorespuesta SMALLINT NOT NULL,
    cod_prioridad SMALLINT NOT NULL,
    cod_depto INTEGER NOT NULL,
    des_topico VARCHAR(36) NOT NULL,
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtcategoriafaq (
    cod_categoriafaq INTEGER DEFAULT nextval('sgrt.seq_categoriafaq') NOT NULL,
    cnu_tipo SMALLINT  NOT NULL,
    des_categoria VARCHAR(255) NOT NULL,
    des_descripcion VARCHAR(255) NOT NULL,
    des_notas VARCHAR(255) NOT NULL,
    tim_ultactualiza TIMESTAMP without time zone NOT NULL,
    fec_creacion TIMESTAMP without time zone NOT NULL,
    fec_ultactualizadopor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtchat (
    cod_chat INTEGER DEFAULT nextval('sgrt.seq_chat') NOT NULL,
    chat VARCHAR NOT NULL
);

 CREATE TABLE sgrt.TSGRTCIUDADES (
    COD_CIUDAD INTEGER DEFAULT nextval('sgrt.seq_ciudad') NOT NULL,
    des_nbciudad VARCHAR(100) NOT NULL,
    cod_estadorep INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtcomentariosagenda (
    cod_comentsagenda INTEGER DEFAULT nextval('sgrt.seq_comentsagenda') NOT NULL,
    des_comentario VARCHAR(500) NOT NULL,
    cod_agenda INTEGER NOT NULL,
    cod_invitado INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtcomentariosreunion (
    cod_commentsreunion INTEGER DEFAULT nextval('sgrt.seq_comentsreunion') NOT NULL,
    des_comentario VARCHAR(500) NOT NULL,
    cod_invitado INTEGER NOT NULL,
    cod_reunion INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtcompromisos (
    cod_compromiso INTEGER DEFAULT nextval('sgrt.seq_compromiso') NOT NULL,
    des_descripcion VARCHAR(200) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_compromiso date NOT NULL,
    cod_reunion INTEGER NOT NULL,
    cod_validador INTEGER NOT NULL,
    cod_verificador INTEGER NOT NULL,
    cod_estado INTEGER,
    des_valor VARCHAR(45),
    cod_ejecutor INTEGER NOT NULL,
    cod_tipoejecutor VARCHAR(10),
    cnu_revisado SMALLINT ,
    cod_estatus sgrt.estatus_compromiso DEFAULT 'Pendiente' NOT NULL,
    cod_tipocompromiso sgrt.tipo_compromiso DEFAULT 'Pendiente' NOT NULL,
    cod_chat INTEGER,
    fec_entrega date
);

 CREATE TABLE sgrt.tsgrtcorreo (
    cod_correo INTEGER DEFAULT nextval('sgrt.seq_correo') NOT NULL,
    cnu_autorespuesta SMALLINT  NOT NULL,
    cod_prioridad SMALLINT  NOT NULL,
    cod_depto INTEGER,
    des_nbusuario VARCHAR(32) NOT NULL,
    des_correo VARCHAR(50) NOT NULL,
    des_nombre VARCHAR(70) NOT NULL,
    des_contrasena VARCHAR(30) NOT NULL,
    cnu_activo SMALLINT  NOT NULL,
    des_dirhost VARCHAR(125) NOT NULL,
    cod_protocolo sgrt.protocolo DEFAULT 'POP' NOT NULL,
    cod_encriptacion sgrt.encriptacion DEFAULT 'NONE' NOT NULL,
    cod_puerto INTEGER,
    cnu_frecsinc SMALLINT  NOT NULL,
    cnu_nummaxcorreo SMALLINT  NOT NULL,
    cnu_eliminar SMALLINT  NOT NULL,
    cnu_errores SMALLINT  NOT NULL,
    fec_ulterror TIMESTAMP without time zone,
    fec_ultsincr TIMESTAMP without time zone,
    cnu_smtpactivo SMALLINT ,
    des_smtphost VARCHAR(125) NOT NULL,
    cod_smtpport INTEGER,
    cnu_smtpsecure SMALLINT  NOT NULL,
    cnu_smtpauth SMALLINT  NOT NULL,
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone,
    cod_usuario INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtdatossolicitud (
    cod_datosolicitud INTEGER NOT NULL,
    cod_elemento INTEGER NOT NULL,
    des_descripcion VARCHAR(45) NOT NULL,
    cod_solicitud INTEGER NOT NULL,
    cod_edosolicitud INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtdepartamento (
    cod_depto INTEGER DEFAULT nextval('sgrt.seq_depto') NOT NULL,
    cod_plantillacorreo INTEGER NOT NULL,
    cod_correo INTEGER NOT NULL,
    cod_manager INTEGER DEFAULT 0 NOT NULL,
    des_nombre VARCHAR(50) NOT NULL,
    cod_ncorto VARCHAR(10) NOT NULL,
    des_firma text NOT NULL,
    cnu_publico SMALLINT  NOT NULL,
    cod_sistemasuite INTEGER DEFAULT 1 NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtedosolicitudes (
    cod_edosolicitud INTEGER DEFAULT nextval('sgrt.seq_edosolicitudes') NOT NULL,
    cod_nbedosolicitud INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtelementos (
    cod_elemento INTEGER DEFAULT nextval('sgrt.seq_elemento') NOT NULL,
    des_nombre VARCHAR(50) NOT NULL,
    cnu_activo SMALLINT NOT NULL
);

 CREATE TABLE sgrt.TSGRTESTADOS (
    COD_ESTADOREP INTEGER DEFAULT nextval('sgrt.seq_estadorep') NOT NULL,
    des_nbestado VARCHAR(60) NOT NULL
);

 CREATE TABLE sgrt.tsgrtfaq (
    cod_faq INTEGER DEFAULT nextval('sgrt.seq_faq') NOT NULL,
    cod_categoriafaq INTEGER NOT NULL,
    des_pregunta VARCHAR(255) NOT NULL,
    cnu_activo SMALLINT  NOT NULL,
    des_respuesta VARCHAR(255) NOT NULL,
    des_notasint VARCHAR(255) NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone NOT NULL,
    fec_creacion TIMESTAMP without time zone NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    cod_ultactualizacionpor INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtgrupo (
    cod_grupo INTEGER DEFAULT nextval('sgrt.seq_grupo') NOT NULL,
    cnu_activo SMALLINT  NOT NULL,
    des_nombre VARCHAR(50) NOT NULL,
    cnu_crear SMALLINT  NOT NULL,
    cnu_editar SMALLINT  NOT NULL,
    cnu_borrar SMALLINT  NOT NULL,
    cnu_cerrar SMALLINT  NOT NULL,
    cnu_transferir SMALLINT  NOT NULL,
    cnu_prohibir SMALLINT  NOT NULL,
    cnu_administrar SMALLINT  NOT NULL,
    cod_sistemasuite INTEGER DEFAULT 1 NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtinvitados (
    cod_invitado INTEGER DEFAULT nextval('sgrt.seq_invitado') NOT NULL,
    cod_reunion INTEGER NOT NULL,
    des_nombre VARCHAR,
    des_correo VARCHAR,
    cnu_invitacionenv SMALLINT  NOT NULL,
    cnu_asiste SMALLINT  NOT NULL,
    cod_empleado INTEGER,
    des_empresa VARCHAR
);

 CREATE TABLE sgrt.tsgrtlugares (
    cod_lugar INTEGER DEFAULT nextval('sgrt.seq_lugar') NOT NULL,
    des_nombre VARCHAR(60) NOT NULL,
    cod_ciudad INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtmsjticket (
    cod_mensaje INTEGER DEFAULT nextval('sgrt.seq_mensaje') NOT NULL,
    cod_ticket INTEGER NOT NULL,
    cod_usuario INTEGER DEFAULT 0 NOT NULL,
    des_mensaje text NOT NULL,
    cod_fuente VARCHAR(16) DEFAULT NULL,
    cod_sistemasuite INTEGER DEFAULT 1 NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtnota (
    cod_nota INTEGER DEFAULT nextval('sgrt.seq_nota') NOT NULL,
    cod_ticket INTEGER NOT NULL,
    cod_empleado INTEGER NOT NULL,
    des_fuente VARCHAR(32) NOT NULL,
    des_titulo VARCHAR(255) DEFAULT 'Nota INTEGERerna Generica' NOT NULL,
    des_nota text NOT NULL,
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtplantillacorreos (
    cod_plantillacorreo INTEGER DEFAULT nextval('sgrt.seq_plantillacorreo') NOT NULL,
    des_nombre VARCHAR(32) NOT NULL,
    des_notas text,
    cod_tipodestinario sgrt.destinatario DEFAULT 'USR',
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone,
    des_asunto VARCHAR(45),
    des_cuerpo VARCHAR(255)
);

CREATE TABLE sgrt.tsgrtprioridad (
    cod_prioridad INTEGER DEFAULT nextval('sgrt.seq_prioridad') NOT NULL,
    des_nombre VARCHAR(60) NOT NULL,
    des_descripcion VARCHAR(30) NOT NULL,
    cod_color VARCHAR(7) NOT NULL,
    cnu_valprioridad SMALLINT  NOT NULL,
    cnu_publica SMALLINT  NOT NULL,
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtresppredefinida (
    cod_respuesta INTEGER DEFAULT nextval('sgrt.seq_resp') NOT NULL,
    cod_depto INTEGER NOT NULL,
    cnu_activo SMALLINT  NOT NULL,
    des_titulo VARCHAR(125) NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtrespuesta (
    cod_respuesta INTEGER DEFAULT nextval('sgrt.seq_respuesta') NOT NULL,
    cod_mensaje INTEGER NOT NULL,
    cod_ticket INTEGER NOT NULL,
    cod_empleado INTEGER NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite INTEGER NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone
);

 CREATE TABLE sgrt.tsgrtreuniones (
    cod_reunion INTEGER DEFAULT nextval('sgrt.seq_reunion') NOT NULL,
    des_nombre VARCHAR(45) NOT NULL,
    fec_fecha date NOT NULL,
    des_objetivo VARCHAR(700) NOT NULL,
    cod_lugar INTEGER NOT NULL,
    cod_responsable INTEGER NOT NULL,
    cod_proximareunion INTEGER,
    cod_creadorreunion INTEGER NOT NULL,
    tim_duracion time without time zone,
    tim_hora time without time zone
);

 CREATE TABLE sgrt.tsgrtservicios (
    cod_servicio INTEGER DEFAULT nextval('sgrt.seq_servicio') NOT NULL,
    des_nombre_servicio VARCHAR(45) NOT NULL,
    des_descripcion VARCHAR(100) NOT NULL,
    fec_contratacion date NOT NULL
);

 CREATE TABLE sgrt.tsgrtsolicitudservicios (
    cod_solicitud INTEGER DEFAULT nextval('sgrt.seq_solicitud') NOT NULL,
    cod_empleado INTEGER NOT NULL,
    cod_ticket INTEGER NOT NULL,
    cod_servicio INTEGER NOT NULL
);

 CREATE TABLE sgrt.tsgrtticket (
    cod_ticket INTEGER DEFAULT nextval('sgrt.seq_ticket') NOT NULL,
    des_folio VARCHAR(45) NOT NULL,
    cod_reunion INTEGER,
    cod_acuerdo INTEGER,
    cod_responsable INTEGER,
    cod_validador INTEGER,
    cod_depto INTEGER NOT NULL,
    cod_prioridad SMALLINT NOT NULL,
    cod_topico INTEGER NOT NULL,
    cod_empleado INTEGER NOT NULL,
    des_correo VARCHAR(50) NOT NULL,
    des_nombre VARCHAR(50) NOT NULL,
    des_tema VARCHAR(64) DEFAULT '[Sin Asunto]' NOT NULL,
    des_temaayuda VARCHAR(255) DEFAULT NULL,
    cod_telefono VARCHAR(16) DEFAULT NULL,
    cod_extension VARCHAR(8) DEFAULT NULL,
    cod_estadot sgrt.edoticket DEFAULT 'Abierto' NOT NULL,
    cod_origent sgrt.origencontac DEFAULT 'Otro' NOT NULL,
    cnu_expirado SMALLINT  NOT NULL,
    cnu_atendido SMALLINT  NOT NULL,
    fec_exp TIMESTAMP without time zone,
    fec_reap TIMESTAMP without time zone,
    fec_cierre TIMESTAMP without time zone,
    fec_ultimomsg TIMESTAMP without time zone,
    fec_ultimaresp TIMESTAMP without time zone,
    cod_sistemasuite INTEGER DEFAULT 1 NOT NULL,
    fec_ultactualizacion TIMESTAMP without time zone,
    cod_ultactualizacionpor INTEGER NOT NULL,
    cod_creadopor INTEGER NOT NULL,
    fec_creacion TIMESTAMP without time zone,
    cod_ejecutor INTEGER
);


 -----------------------------------
 -----------------------------------

 ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT tsgrtedosolicitudes_pkey PRIMARY KEY (cod_edosolicitud);
	
 ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT tsgrtagenda_pkey PRIMARY KEY (cod_agenda);

 ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT tsgrtarchivos_pkey PRIMARY KEY (cod_archivo);

 ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT tsgrtasistentes_pkey PRIMARY KEY (cod_asistente);

 ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT tsgrtattchticket_pkey PRIMARY KEY (cod_attach);

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_pkey PRIMARY KEY (cod_topico);

 ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT tsgrtcategoriafaq_pkey PRIMARY KEY (cod_categoriafaq);

 ALTER TABLE ONLY sgrt.tsgrtchat
    ADD CONSTRAINT tsgrtchat_pkey PRIMARY KEY (cod_chat);

 ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT tsgrtciudades_pkey PRIMARY KEY (cod_ciudad);

 ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT tsgrtcomentariosagenda_pkey PRIMARY KEY (cod_comentsagenda);

 ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT tsgrtcomentariosreunion_pkey PRIMARY KEY (cod_commentsreunion);

 ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT tsgrtcompromisos_pkey PRIMARY KEY (cod_compromiso);

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_pkey PRIMARY KEY (cod_correo);

 ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT tsgrtdatossolicitud_pkey PRIMARY KEY (cod_datosolicitud);

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_pkey PRIMARY KEY (cod_depto);

 ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT tsgrtelementos_pkey PRIMARY KEY (cod_elemento);

 ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT tsgrtestados_pkey PRIMARY KEY (cod_estadorep);

 ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT tsgrtfaq_pkey PRIMARY KEY (cod_faq);

 ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT tsgrtgrupo_pkey PRIMARY KEY (cod_grupo);

 ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT tsgrtinvitados_pkey PRIMARY KEY (cod_invitado);

 ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT tsgrtlugares_pkey PRIMARY KEY (cod_lugar);

 ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT tsgrtmsjticket_pkey PRIMARY KEY (cod_mensaje);

 ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT tsgrtnota_pkey PRIMARY KEY (cod_nota);

 ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT tsgrtplantillacorreos_pkey PRIMARY KEY (cod_plantillacorreo);

 ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_pkey PRIMARY KEY (cod_prioridad);

 ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_pkey PRIMARY KEY (cod_respuesta);

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT tsgrtrespuesta_pkey PRIMARY KEY (cod_respuesta);

 ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT tsgrtreuniones_pkey PRIMARY KEY (cod_reunion);

 ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT tsgrtservicios_pkey PRIMARY KEY (cod_servicio);

 ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT tsgrtsolicitudservicios_pkey PRIMARY KEY (cod_solicitud);

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_pkey PRIMARY KEY (cod_ticket);



 -----------------------------------
 -----------------------------------



CREATE INDEX fki_fk_cod_chat ON sgrt.tsgrtcompromisos USING btree (cod_chat);
CREATE INDEX fki_fk_cod_empleado ON sgrt.tsgrtinvitados USING btree (cod_empleado);
CREATE INDEX fki_fk_cod_invitado ON sgrt.tsgrtcomentariosreunion USING btree (cod_invitado);
CREATE INDEX fki_fk_cod_invitados ON sgrt.tsgrtcomentariosagenda USING btree (cod_invitado);


 -----------------------------------
 -----------------------------------


 ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT cod_agenda UNIQUE (cod_agenda);

 ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_archivo UNIQUE (cod_archivo);

 ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT cod_asistente UNIQUE (cod_asistente);

 ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_attach UNIQUE (cod_attach);

 ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_categofaq UNIQUE (cod_categoriafaq);

 ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT cod_categoriafaq UNIQUE (cod_categoriafaq);

 ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT cod_comentsagenda UNIQUE (cod_comentsagenda);

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT cod_correo UNIQUE (cod_correo);

 ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT cod_datosolicitud UNIQUE (cod_datosolicitud);

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT cod_depto UNIQUE (cod_depto);

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_deptoticket UNIQUE (cod_depto);

 ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT cod_edosolicitud UNIQUE (cod_edosolicitud);

 ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT cod_elemento UNIQUE (cod_elemento);

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_empleado UNIQUE (cod_empleado);

 ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT cod_estadorep UNIQUE (cod_estadorep);

 ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_faq UNIQUE (cod_faq);

 ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT cod_grupo UNIQUE (cod_grupo);

 ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT cod_invitado UNIQUE (cod_invitado);

 ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT cod_lugar UNIQUE (cod_lugar);

 ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT cod_mensaje UNIQUE (cod_mensaje);

 ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT cod_nota UNIQUE (cod_nota);

 ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT cod_plantillacorreo UNIQUE (cod_plantillacorreo);

 ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT cod_prioridad UNIQUE (cod_prioridad);

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT cod_respuesta UNIQUE (cod_respuesta);

 ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT cod_respuestapredf UNIQUE (cod_respuesta);

 ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_reunionarchivos UNIQUE (cod_reunion);

 ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT cod_reuniones UNIQUE (cod_reunion);

 ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT cod_servicio UNIQUE (cod_servicio);

 ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT cod_solicitud UNIQUE (cod_solicitud);

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_ticket UNIQUE (cod_ticket);

 ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_ticketattach UNIQUE (cod_ticket);

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT cod_topico UNIQUE (cod_topico);

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_des_topico_key UNIQUE (des_topico);

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_des_correo_key UNIQUE (des_correo);

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_des_nombre_key UNIQUE (des_nombre);

 ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_des_nombre_key UNIQUE (des_nombre);

 ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_des_titulo_key UNIQUE (des_titulo);

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_correo_key UNIQUE (des_correo);

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_folio_key UNIQUE (des_folio);

 ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT unique_cod_chat UNIQUE (cod_chat);



 -----------------------------------
 -----------------------------------



 ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_agenda FOREIGN KEY (cod_agenda) REFERENCES sgrt.tsgrtagenda(cod_agenda) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_categoria FOREIGN KEY (cod_categoriafaq) REFERENCES sgrt.tsgrtcategoriafaq(cod_categoriafaq) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_chat FOREIGN KEY (cod_chat) REFERENCES sgrt.tsgrtchat(cod_chat);

 ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_correo FOREIGN KEY (cod_correo) REFERENCES sgrt.tsgrtcorreo(cod_correo) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_creadorreunion FOREIGN KEY (cod_creadorreunion) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_edosolicitud FOREIGN KEY (cod_edosolicitud) REFERENCES sgrt.tsgrtedosolicitudes(cod_edosolicitud) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_elemento FOREIGN KEY (cod_elemento) REFERENCES sgrt.tsgrtelementos(cod_elemento) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_mensaje FOREIGN KEY (cod_mensaje) REFERENCES sgrt.tsgrtmsjticket(cod_mensaje) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_plantillacorreo FOREIGN KEY (cod_plantillacorreo) REFERENCES sgrt.tsgrtplantillacorreos(cod_plantillacorreo) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_responsable FOREIGN KEY (cod_responsable) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_reunionanterior FOREIGN KEY (cod_proximareunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_servicio FOREIGN KEY (cod_servicio) REFERENCES sgrt.tsgrtservicios(cod_servicio) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_solicitud FOREIGN KEY (cod_solicitud) REFERENCES sgrt.tsgrtsolicitudservicios(cod_solicitud) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_topico FOREIGN KEY (cod_topico) REFERENCES sgrt.tsgrtayudatopico(cod_topico) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_usuario FOREIGN KEY (cod_usuario) REFERENCES sgco.tsgcousuarios(cod_usuario) ON UPDATE CASCADE ON DELETE CASCADE;

 ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT fk_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;





-------------------------------------
-------------------------------------

CREATE FUNCTION sgrt.buscar_asistentes_minuta(reunionid INTEGER) RETURNS TABLE(nombre_asistente text, area_asistente VARCHAR)
    LANGUAGE sql
    AS $$
SELECT
des_nombre as nombre_asistente,
CONCAT((SELECT area.des_nbarea FROM sgrh.tsgrhempleados emp LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area WHERE emp.cod_empleado=invitado.cod_empleado),
des_empresa) as area_asistente
FROM sgrt.tsgrtinvitados invitado WHERE  invitado.cnu_asiste='1' and invitado.cod_reunion=reunionid;
$$;

-------------------------------------
-------------------------------------

CREATE FUNCTION sgrt.buscar_compromisos_roles_list(reunionid INTEGER) RETURNS TABLE(cod_compromiso INTEGER, des_descripcion VARCHAR, cod_estatus sgrt.estatus_compromiso, fec_solicitud text, fec_compromiso text, validador text, verificador text, ejecutor text)
    LANGUAGE plpgsql
    AS $$

BEGIN
RETURN QUERY
select
CAST(ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as INTEGER) AS cod_compromiso,
com.des_descripcion,
com.cod_estatus,
CAST(to_char(com.fec_solicitud, 'DD/MM/YYYY') as text) as fec_solicitud,
CAST(to_char(com.fec_compromiso, 'DD/MM/YYYY') as text) as fec_compromiso,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_validador) AS validador,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_verificador) AS verificador,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_ejecutor) AS ejecutor
from sgrt.tsgrtcompromisos com where com.cod_reunion=reunionid;

END;
$$;

-------------------------------------
-------------------------------------

CREATE FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area INTEGER, des_nbarea VARCHAR, cantidad_minutas INTEGER)
    LANGUAGE sql
    AS $$
SELECT
cod_area,
cod_acronimo as des_nbarea,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtreuniones reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_responsable=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE area.cod_area=a.cod_area and
reu.fec_fecha >= cast(fecha_inicio as date)
AND reu.fec_fecha <=  cast(fecha_fin as date)
) as INTEGER) AS cantidad_minutas
FROM sgrh.tsgrhareas a
$$;

-------------------------------------
-------------------------------------

CREATE FUNCTION sgrt.buscar_proxima_reunion(reunionid INTEGER, OUT cod_reunion INTEGER, OUT des_nombre_reunion VARCHAR, OUT fec_fecha text, OUT cod_lugar INTEGER, OUT tim_hora VARCHAR, OUT des_nombre_lugar VARCHAR, OUT cod_ciudad INTEGER, OUT des_nbciudad VARCHAR, OUT cod_estadorep_ciudad INTEGER, OUT des_nbestado VARCHAR, OUT cod_estadorep_estado INTEGER) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  select
  reunion.cod_reunion,
  reunion.des_nombre,
  CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY')as text) as fec_fecha,
  reunion.cod_lugar,
  CAST(reunion.tim_hora as text),
  lugar.des_nombre,
  lugar.cod_ciudad,
ciudad.des_nbciudad,
ciudad.cod_estadorep,
estado.des_nbestado,
estado.cod_estadorep
from sgrt.tsgrtreuniones reunion inner join sgrt.tsgrtlugares lugar on reunion.cod_lugar=lugar.cod_lugar
inner join sgrt.tsgrtciudades ciudad on lugar.cod_ciudad=ciudad.cod_ciudad
inner join sgrt.tsgrtestados estado on ciudad.cod_estadorep=estado.cod_estadorep
where cod_reunion=(select cod_proximareunion from sgrt.tsgrtreuniones where cod_reunion=reunionId);

$$;

-------------------------------------
-------------------------------------


CREATE FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area INTEGER, des_nbarea VARCHAR, tipo text, num INTEGER)
    LANGUAGE sql
    AS $$
select
cod_area,
cod_acronimo as des_nbarea,
CAST('Terminado' as text) as tipo,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtcompromisos reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_ejecutor=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE reu.cod_estatus='Terminado' AND
area.cod_area=a.cod_area AND
reu.fec_compromiso >= cast(fecha_inicio as date)
AND reu.fec_compromiso <= cast(fecha_fin as date)
)as INTEGER) as num
FROM
sgrh.tsgrhareas a
UNION
select
cod_area,
cod_acronimo as des_nbarea,
CAST('Pendiente' as text) as tipo,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtcompromisos reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_ejecutor=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE reu.cod_estatus='Pendiente' and
area.cod_area=a.cod_area AND
reu.fec_compromiso >= cast(fecha_inicio as date)
AND reu.fec_compromiso <= cast(fecha_fin as date))as INTEGER) as num
FROM
sgrh.tsgrhareas a
$$;

-------------------------------------
-------------------------------------

CREATE FUNCTION sgrt.compromisos_dia(fechacompromiso text) RETURNS TABLE(cod_empleado INTEGER, nombre text, area text, fec_compromiso text, des_descripcion text, cod_estatus text, tiempo_demora INTEGER)
    LANGUAGE sql
    AS $$
SELECT
emp.cod_empleado,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
(SELECT cod_acronimo FROM sgrh.tsgrhareas WHERE cod_area=emp.cod_area) as area,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text) fec_compromiso,
com.des_descripcion,
cast(com.cod_estatus as text),
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrh.tsgrhempleados emp ON com.cod_ejecutor=emp.cod_empleado or com.cod_validador=emp.cod_empleado or com.cod_verificador=emp.cod_empleado
WHERE com.fec_compromiso=cast(fechaCompromiso as date);
$$;

-------------------------------------
-------------------------------------

CREATE FUNCTION sgrt.compromisos_generales() RETURNS TABLE(cod_empleado INTEGER, cod_reunion INTEGER, nombre text, rol text, area text, descripcion text, minuta text, estatus text, fec_registro text, fec_compromiso text, dias_habiles INTEGER, tiempo_demora INTEGER)
    LANGUAGE sql
    AS $$
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Validador' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_validador
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_validador
UNION
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Verificador' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_verificador
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_verificador
UNION
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Ejecutor' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_ejecutor
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_ejecutor
$$;

-------------------------------------
-------------------------------------

CREATE FUNCTION sgrt.reporte_por_tema(reunionid INTEGER) RETURNS TABLE(nombre_minuta VARCHAR, responsable text, fecha text, acuerdos INTEGER, pendientes INTEGER, total INTEGER, tiempo_invertido text)
    LANGUAGE plpgsql
    AS $$
DECLARE
    var_r record;
BEGIN
   FOR var_r IN(SELECT * FROM sgrt.tsgrtreuniones WHERE cod_reunion=reunionId OR cod_proximareunion=reunionId)
     LOOP
              RETURN QUERY
		select c.nombre_minuta, c.responsable, c.fecha,a.Acuerdos, b.Pendientes, cast(COALESCE(a.Acuerdos,0)+COALESCE(b.Pendientes,0)as int) AS Total, c.tiempo_invertido from
		(select cast(count(cod_tipocompromiso) as int) as Acuerdos from sgrt.tsgrtcompromisos where cod_tipocompromiso='Acuerdo' and cod_reunion=var_r.cod_reunion) a,
		(select cast(count(cod_tipocompromiso)as int) as Pendientes from sgrt.tsgrtcompromisos where cod_tipocompromiso='Pendiente' and cod_reunion=var_r.cod_reunion) b,
		(select reunion.des_nombre AS nombre_minuta, CONCAT(empleado.des_nombre, ' ', empleado.des_nombres, ' ', empleado.des_apepaterno, ' ', empleado.des_apematerno) AS responsable,
		CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY') as text) as fecha, CAST(to_char(reunion.tim_duracion,'HH24:MI') as text) as tiempo_invertido from
		sgrt.tsgrtreuniones reunion,
		sgrh.tsgrhempleados empleado
		WHERE reunion.cod_responsable=empleado.cod_empleado and
		cod_reunion=var_r.cod_reunion) c;
            END LOOP;
END; $$;
