

CREATE SCHEMA sgco
    AUTHORIZATION suite;

COMMENT ON SCHEMA sgco
    IS 'Sistema de Gestion de Conocimiento de la Organizacion.';

GRANT ALL ON SCHEMA sgco TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT ALL ON TABLES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT SELECT, USAGE ON SEQUENCES TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT EXECUTE ON FUNCTIONS TO suite WITH GRANT OPTION;

ALTER DEFAULT PRIVILEGES IN SCHEMA sgco
GRANT USAGE ON TYPES TO suite WITH GRANT OPTION;



-----------------------------------------------------------------------
-----------------------------------------------------------------------

DROP TABLE sgco.tsgcosistemas CASCADE;
DROP TABLE sgco.tsgcotipousuario CASCADE;
DROP TABLE sgco.tsgcousuarios CASCADE;


DROP SEQUENCE sgco.seq_sistema;
DROP SEQUENCE sgco.seq_tipousuario;
DROP SEQUENCE sgco.seq_usuarios;

-----------------------------------------------------------------------
-----------------------------------------------------------------------

CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



-----------------------------------------------------------------------
-----------------------------------------------------------------------


 CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300),
    PRIMARY KEY (cod_sistema)
);

 CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol character varying(35) NOT NULL,
    PRIMARY KEY (cod_tipousuario)
);

 CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo varchar(60) NOT NULL,
    des_contrasenacorreo character varying(50) NOT NULL,
    cod_usuariosistema character varying(30) NOT NULL,
    des_contrasenasistema character varying(30) NOT NULL,
    PRIMARY KEY(cod_usuario)
);



-----------------------------------------------------------------------
-----------------------------------------------------------------------


ALTER TABLE ONLY sgco.tsgcosistemas
  ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);

ALTER TABLE ONLY sgco.tsgcotipousuario
  ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);

ALTER TABLE ONLY sgco.tsgcousuarios
  ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);
