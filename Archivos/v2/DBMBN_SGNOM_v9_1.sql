create table "sgnom"."tsgnomalguinaldo" (
"cod_aguinaldoid" int4 not null,
"imp_aguinaldo" numeric(10,2) not null,
"cod_tipoaguinaldo" char(1) collate "default" not null,
"cod_empquincenaid_fk" int4 not null,
"bol_estatus" bool not null,
"xml_desgloce" xml,
constraint "nom_aguinaldo_id" primary key ("cod_aguinaldoid") 
)
without oids;
comment on table "sgnom"."tsgnomalguinaldo" is 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';
alter table "sgnom"."tsgnomalguinaldo" owner to "suite";

create table "sgnom"."tsgnomargumento" (
"cod_argumentoid" int4 not null,
"cod_nbargumento" varchar(30) collate "default" not null,
"cod_clavearg" varchar(5) collate "default" not null,
"imp_valorconst" numeric(10,2),
"des_funcionbd" varchar(60) collate "default",
"bol_estatus" bool not null,
"txt_descripcion" text collate "default",
"aud_ucrea" int4 not null,
"aud_uactualizacion" int4,
"aud_feccrea" date not null,
"aud_fecactualizacion" date,
constraint "nom_cat_argumento_id" primary key ("cod_argumentoid") 
)
without oids;
alter table "sgnom"."tsgnomargumento" owner to "suite";

create table "sgnom"."tsgnombitacora" (
"cod_bitacoraid" int4 not null,
"xml_bitacora" xml not null,
"cod_tablaid_fk" int4 not null,
constraint "nom_bitacora_id" primary key ("cod_bitacoraid") 
)
without oids;
alter table "sgnom"."tsgnombitacora" owner to "suite";

create table "sgnom"."tsgnomcabecera" (
"cod_cabeceraid" int4 not null,
"cod_nbnomina" varchar(40) collate "default" not null,
"fec_creacion" date not null,
"fec_ejecucion" date not null,
"fec_cierre" date not null,
"imp_totpercepcion" numeric(10,2) not null,
"imp_totdeduccion" numeric(10,2) not null,
"imp_totalemp" numeric(10,2) not null,
"cod_quincenaid_fk" int4 not null,
"cod_tiponominaid_fk" int4 not null,
"cod_estatusnomid_fk" int4 not null,
"aud_ucrea" int4 not null,
"aud_uactualiza" int4,
"aud_feccrea" date not null,
"aud_fecactualiza" date,
constraint "nom_cabecera_id" primary key ("cod_cabeceraid") 
)
without oids;
alter table "sgnom"."tsgnomcabecera" owner to "suite";

create table "sgnom"."tsgnomcabeceraht" (
"cod_cabeceraid" int4 not null,
"cod_nbnomina" varchar(40) collate "default" not null,
"fec_creacion" date not null,
"fec_ejecucion" date not null,
"fec_cierre" date not null,
"imp_totpercepcion" numeric(10,2) not null,
"imp_totdeduccion" numeric(10,2) not null,
"imp_totalemp" numeric(10,2) not null,
"cod_quincenaid_fk" int4 not null,
"cod_tiponominaid_fk" int4 not null,
"cod_estatusnomid_fk" int4 not null,
"aud_ucrea" int4 not null,
"aud_uactualiza" int4,
"aud_feccrea" date not null,
"aud_fecactualiza" date,
constraint "nom_cabecera_copia_id" primary key ("cod_cabeceraid") 
)
without oids;
alter table "sgnom"."tsgnomcabeceraht" owner to "suite";

create table "sgnom"."tsgnomcalculo" (
"cod_calculoid" int4 not null,
"cod_tpcalculo" varchar(25) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_tipo_calculo_id" primary key ("cod_calculoid") 
)
without oids;
comment on table "sgnom"."tsgnomcalculo" is 'bol_estatus

(

activo, inactivo

)';
alter table "sgnom"."tsgnomcalculo" owner to "suite";

create table "sgnom"."tsgnomcatincidencia" (
"cod_catincidenciaid" int4 not null,
"cod_claveincidencia" varchar(5) collate "default",
"cod_nbincidencia" varchar(20) collate "default",
"cod_perfilincidencia" varchar(25) collate "default",
"bol_estatus" bool,
"cod_tipoincidencia" char(1) collate "default",
constraint "cat_incidencia_id" primary key ("cod_catincidenciaid") 
)
without oids;
comment on table "sgnom"."tsgnomcatincidencia" is 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';
alter table "sgnom"."tsgnomcatincidencia" owner to "suite";

create table "sgnom"."tsgnomclasificador" (
"cod_clasificadorid" int4 not null,
"cod_tpclasificador" varchar(20) collate "default",
"bol_estatus" bool,
constraint "nom_cat_tipo_clasificador_id" primary key ("cod_clasificadorid") 
)
without oids;
alter table "sgnom"."tsgnomclasificador" owner to "suite";

create table "sgnom"."tsgnomcncptoquinc" (
"cod_cncptoquincid" int4 not null,
"cod_empquincenaid_fk" int4 not null,
"cod_conceptoid_fk" int4 not null,
"imp_concepto" numeric(10,2) not null,
"imp_gravado" numeric(10,2),
"imp_exento" numeric(10,2),
"xml_desgloce" xml,
constraint "nom_conceptos_quincena_id" primary key ("cod_cncptoquincid") 
)
without oids;
alter table "sgnom"."tsgnomcncptoquinc" owner to "suite";

create table "sgnom"."tsgnomcncptoquincht" (
"cod_cncptoquinchtid" int4 not null,
"cod_empquincenaid_fk" int4 not null,
"cod_conceptoid_fk" int4 not null,
"imp_concepto" numeric(10,2) not null,
"imp_gravado" numeric(10,2),
"imp_exento" numeric(10,2),
"xml_desgloce" xml,
constraint "nom_conceptos_quincena_copia_id" primary key ("cod_cncptoquinchtid") 
)
without oids;
alter table "sgnom"."tsgnomcncptoquincht" owner to "suite";

create table "sgnom"."tsgnomconcepto" (
"cod_conceptoid" int4 not null,
"cod_nbconcepto" varchar(20) collate "default" not null,
"cod_claveconcepto" varchar(4) collate "default" not null,
"cnu_prioricalculo" int4 not null,
"cnu_articulo" int4 not null,
"bol_estatus" bool not null,
"cod_formulaid_fk" int4 not null,
"cod_tipoconceptoid_fk" int4 not null,
"cod_calculoid_fk" int4 not null,
"cod_conceptosatid_fk" int4 not null,
"cod_frecuenciapago" varchar(20) collate "default" not null,
"cod_partidaprep" int4 not null,
"cnu_cuentacontable" int4 not null,
"cod_gravado" char(1) collate "default" not null,
"cod_excento" char(1) collate "default" not null,
"bol_retroactividad" bool not null,
"cnu_topeex" int4 not null,
"cod_clasificadorid_fk" int4 not null,
"aud_ualta" int4 not null,
"aud_uactualizacion" int4,
"aud_feccrea" date not null,
"aud_fecactualizacion" date,
constraint "nom_cat_concepto_id" primary key ("cod_conceptoid") 
)
without oids;
comment on table "sgnom"."tsgnomconcepto" is 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';
alter table "sgnom"."tsgnomconcepto" owner to "suite";

create table "sgnom"."tsgnomconceptosat" (
"cod_conceptosatid" int4 not null,
"des_conceptosat" varchar(51) collate "default" not null,
"des_descconcepto" varchar(51) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_concepto_sat_id" primary key ("cod_conceptosatid") 
)
without oids;
comment on table "sgnom"."tsgnomconceptosat" is 'bol_estatus

(

activo, inactivo

)';
alter table "sgnom"."tsgnomconceptosat" owner to "suite";

create table "sgnom"."tsgnomconfpago" (
"cod_confpagoid" int4 not null,
"bol_pagoempleado" bool,
"bol_pagorh" bool,
"bol_pagofinanzas" bool,
"cod_empquincenaid_fk" int4,
constraint "nom_conf_pago_pkey" primary key ("cod_confpagoid") 
)
without oids;
comment on table "sgnom"."tsgnomconfpago" is 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';
alter table "sgnom"."tsgnomconfpago" owner to "suite";

create table "sgnom"."tsgnomejercicio" (
"cod_ejercicioid" int4 not null,
"cnu_valorejercicio" int4 not null,
"bol_estatus" bool not null,
constraint "nom_cat_ejercicio_id" primary key ("cod_ejercicioid") 
)
without oids;
alter table "sgnom"."tsgnomejercicio" owner to "suite";

create table "sgnom"."tsgnomempleados" (
"cod_empleadoid" int4 not null,
"fec_ingreso" date not null,
"fec_salida" date,
"bol_estatus" bool not null,
"cod_empleado_fk" int4 not null,
"imp_sueldoimss" numeric(10,2),
"imp_honorarios" numeric(10,2),
"cod_tipoimss" char(1) collate "default",
"cod_tipohonorarios" char(1) collate "default",
"cod_banco" varchar(50) collate "default",
"cod_sucursal" int4,
"cod_cuenta" int4,
"cod_clabe" int4,
"txt_descripcionbaja" text collate "default",
"aud_codcreadopor" int4 not null,
"aud_feccreacion" date not null,
"aud_codmodificadopor" int4,
"aud_fecmodificacion" date,
constraint "nom_empleado_id" primary key ("cod_empleadoid") 
)
without oids;
comment on table "sgnom"."tsgnomempleados" is 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';
alter table "sgnom"."tsgnomempleados" owner to "suite";

create table "sgnom"."tsgnomempquincena" (
"cod_empquincenaid" int4 not null,
"cod_empleadoid_fk" int4 not null,
"cod_cabeceraid_fk" int4 not null,
"imp_totpercepcion" numeric(10,2) not null,
"imp_totdeduccion" numeric(10,2) not null,
"imp_totalemp" numeric(10,2) not null,
"bol_estatusemp" bool not null,
constraint "nom_empleado_quincena_id" primary key ("cod_empquincenaid") 
)
without oids;
comment on table "sgnom"."tsgnomempquincena" is 'bol_estatusemp

(

activo, inactivo

)';
alter table "sgnom"."tsgnomempquincena" owner to "suite";

create table "sgnom"."tsgnomempquincenaht" (
"cod_empquincenahtid" int4 not null,
"cod_empleadoid_fk" int4 not null,
"cod_cabeceraid_fk" int4 not null,
"imp_totpercepcion" numeric(10,2) not null,
"imp_totdeduccion" numeric(10,2) not null,
"imp_totalemp" numeric(10,2) not null,
"bol_estatusemp" bool not null,
constraint "nom_empleado_quincena_copia_id" primary key ("cod_empquincenahtid") 
)
without oids;
alter table "sgnom"."tsgnomempquincenaht" owner to "suite";

create table "sgnom"."tsgnomestatusnom" (
"cod_estatusnomid" int4 not null,
"cod_estatusnomina" varchar(15) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_estatus_nomina_id" primary key ("cod_estatusnomid") 
)
without oids;
comment on table "sgnom"."tsgnomestatusnom" is 'cod_estatusnomid 

estatus

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';
alter table "sgnom"."tsgnomestatusnom" owner to "suite";

create table "sgnom"."tsgnomformula" (
"cod_formulaid" int4 not null,
"des_nbformula" varchar(60) collate "default" not null,
"des_formula" varchar(250) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_formula_id" primary key ("cod_formulaid") 
)
without oids;
comment on table "sgnom"."tsgnomformula" is 'bol_estatus

(

activo, inactivo

)';
alter table "sgnom"."tsgnomformula" owner to "suite";

create table "sgnom"."tsgnomfuncion" (
"cod_funcionid" int4 not null,
"cod_nbfuncion" varchar(15) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_funcion_id" primary key ("cod_funcionid") 
)
without oids;
alter table "sgnom"."tsgnomfuncion" owner to "suite";

create table "sgnom"."tsgnomhisttabla" (
"cod_tablaid" int4 not null,
"cod_nbtabla" varchar(18) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_tabla_id" primary key ("cod_tablaid") 
)
without oids;
alter table "sgnom"."tsgnomhisttabla" owner to "suite";

create table "sgnom"."tsgnomincidencia" (
"cod_incidenciaid" int4 not null,
"cod_catincidenciaid_fk" int4 not null,
"cnu_cantidad" int2,
"des_actividad" varchar(100) collate "default",
"txt_comentarios" text collate "default",
"cod_empreporta_fk" int4,
"cod_empautoriza_fk" int4,
"imp_monto" numeric(10,2),
"xml_detcantidad" xml,
"bol_estatus" bool,
"cod_quincenaid_fk" int4 not null,
"bol_validacion" bool,
"aud_feccrea" date not null,
"aud_fecvalidacion" date,
constraint "nom_incidencia_id" primary key ("cod_incidenciaid") 
)
without oids;
comment on table "sgnom"."tsgnomincidencia" is 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';
alter table "sgnom"."tsgnomincidencia" owner to "suite";

create table "sgnom"."tsgnommanterceros" (
"cod_mantercerosid" int4 not null,
"cod_conceptoid_fk" int4,
"imp_monto" numeric(10,2),
"cod_quincenainicio_fk" int4,
"cod_quincenafin_fk" int4,
"cod_empleadoid_fk" int4,
"cod_frecuenciapago" varchar(20) collate "default",
"bol_estatus" bool,
"aud_ucrea" int4 not null,
"aud_uactualizacion" int4,
"aud_feccrea" date not null,
"aud_fecactualizacion" varchar(255),
constraint "nom_manuales_terceros_pkey" primary key ("cod_mantercerosid") 
)
without oids;
alter table "sgnom"."tsgnommanterceros" owner to "suite";

create table "sgnom"."tsgnomquincena" (
"cod_quincenaid" int4 not null,
"des_quincena" varchar(70) collate "default" not null,
"fec_inicio" date not null,
"fec_fin" date not null,
"fec_pago" date not null,
"fec_dispersion" date not null,
"cnu_numquincena" int4 not null,
"cod_ejercicioid_fk" int4 not null,
"bol_estatus" bool not null,
constraint "nom_cat_quincena_id" primary key ("cod_quincenaid") 
)
without oids;
alter table "sgnom"."tsgnomquincena" owner to "suite";

create table "sgnom"."tsgnomtipoconcepto" (
"cod_tipoconceptoid" int4 not null,
"cod_tipoconcepto" varchar(25) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_tipo_conepto_id" primary key ("cod_tipoconceptoid") 
)
without oids;
comment on table "sgnom"."tsgnomtipoconcepto" is 'bol_estatus

(

activo, inactivo

)';
alter table "sgnom"."tsgnomtipoconcepto" owner to "suite";

create table "sgnom"."tsgnomtiponomina" (
"cod_tiponominaid" int4 not null,
"cod_nomina" varchar(30) collate "default" not null,
"bol_estatus" bool not null,
constraint "nom_cat_tipo_nomina_id" primary key ("cod_tiponominaid") 
)
without oids;
comment on table "sgnom"."tsgnomtiponomina" is 'bol_estatus (activa, inactiva)';
alter table "sgnom"."tsgnomtiponomina" owner to "suite";


alter table "sgnom"."tsgnomalguinaldo" add constraint "nom_empleados_quincena_fk_aguinaldos" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
alter table "sgnom"."tsgnombitacora" add constraint "nom_cat_tabla_id_fk_nom_cat_tablas" foreign key ("cod_tablaid_fk") references "sgnom"."tsgnomhisttabla" ("cod_tablaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcabecera" add constraint "nom_cat_estatus_nomina_id_fk_cabeceras" foreign key ("cod_estatusnomid_fk") references "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcabecera" add constraint "nom_cat_quincena_id_fk_cabeceras" foreign key ("cod_quincenaid_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcabecera" add constraint "nom_cat_tipo_nomina_id_fk_cabeceras" foreign key ("cod_tiponominaid_fk") references "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcabeceraht" add constraint "nom_cat_estatus_nomina_id_copia_fk_cabeceras" foreign key ("cod_estatusnomid_fk") references "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcabeceraht" add constraint "nom_cat_quincena_id_copia_fk_cabeceras" foreign key ("cod_quincenaid_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcabeceraht" add constraint "nom_cat_tipo_nomina_id_copia_fk_cabeceras" foreign key ("cod_tiponominaid_fk") references "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcncptoquinc" add constraint "concepto_quincena_id_fk_conceptos_quincena" foreign key ("cod_conceptoid_fk") references "sgnom"."tsgnomconcepto" ("cod_conceptoid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcncptoquinc" add constraint "empleado_concepto_id_fk_conceptos_quincena" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcncptoquincht" add constraint "concepto_quincena_id_copia_fk_conceptos_quincena" foreign key ("cod_conceptoid_fk") references "sgnom"."tsgnomconcepto" ("cod_conceptoid") on delete no action on update cascade;
alter table "sgnom"."tsgnomcncptoquincht" add constraint "empleado_concepto_id_copia_fk_conceptos_quincena" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomconcepto" add constraint "nom_cat_clasificador_id_fk_cat_conceptos" foreign key ("cod_clasificadorid_fk") references "sgnom"."tsgnomclasificador" ("cod_clasificadorid") on delete no action on update cascade;
alter table "sgnom"."tsgnomconcepto" add constraint "nom_concepto_sat_id_fk_cat_conceptos" foreign key ("cod_conceptosatid_fk") references "sgnom"."tsgnomconceptosat" ("cod_conceptosatid") on delete no action on update cascade;
alter table "sgnom"."tsgnomconcepto" add constraint "nom_formula_id_fk_cat_conceptos" foreign key ("cod_formulaid_fk") references "sgnom"."tsgnomformula" ("cod_formulaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomconcepto" add constraint "nom_tipo_calculo_id_fk_cat_conceptos" foreign key ("cod_calculoid_fk") references "sgnom"."tsgnomcalculo" ("cod_calculoid") on delete no action on update cascade;
alter table "sgnom"."tsgnomconcepto" add constraint "nom_tipo_concepto_id_fk_cat_conceptos" foreign key ("cod_tipoconceptoid_fk") references "sgnom"."tsgnomtipoconcepto" ("cod_tipoconceptoid") on delete no action on update cascade;
alter table "sgnom"."tsgnomconfpago" add constraint "nom_empleados_quincena_fk_conf_pago" foreign key ("cod_empquincenaid_fk") references "sgnom"."tsgnomempquincena" ("cod_empquincenaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomempquincena" add constraint "nom_cabecera_id_fk_empleados_quincena" foreign key ("cod_cabeceraid_fk") references "sgnom"."tsgnomcabecera" ("cod_cabeceraid") on delete no action on update cascade;
alter table "sgnom"."tsgnomempquincena" add constraint "nom_empleado_quincena_id_fk_empleados_quincena" foreign key ("cod_empleadoid_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
alter table "sgnom"."tsgnomempquincenaht" add constraint "nom_cabecera_id_fk_empleados_quincena_copia" foreign key ("cod_cabeceraid_fk") references "sgnom"."tsgnomcabecera" ("cod_cabeceraid") on delete no action on update cascade;
alter table "sgnom"."tsgnomempquincenaht" add constraint "nom_empleado_quincena_id_fk_empleados_quincena_copia" foreign key ("cod_empleadoid_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
alter table "sgnom"."tsgnomincidencia" add constraint "nom_cat_incidencia_id_fk_incidencias" foreign key ("cod_catincidenciaid_fk") references "sgnom"."tsgnomcatincidencia" ("cod_catincidenciaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomincidencia" add constraint "nom_cat_quincena_id_fk_incidencias" foreign key ("cod_quincenaid_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update cascade;
alter table "sgnom"."tsgnomincidencia" add constraint "nom_emp_autoriza_fk_incidencias" foreign key ("cod_empautoriza_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
alter table "sgnom"."tsgnomincidencia" add constraint "nom_emp_reporta_fk_incidencias" foreign key ("cod_empreporta_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update cascade;
alter table "sgnom"."tsgnommanterceros" add constraint "nom_cat_conceptos_fk_manuales_terceros" foreign key ("cod_conceptoid_fk") references "sgnom"."tsgnomconcepto" ("cod_conceptoid") on delete no action on update no action;
alter table "sgnom"."tsgnommanterceros" add constraint "nom_cat_quincenas_fk_manuales_terceros_fin" foreign key ("cod_quincenafin_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update no action;
alter table "sgnom"."tsgnommanterceros" add constraint "nom_cat_quincenas_fk_manuales_terceros_inicio" foreign key ("cod_quincenainicio_fk") references "sgnom"."tsgnomquincena" ("cod_quincenaid") on delete no action on update no action;
alter table "sgnom"."tsgnommanterceros" add constraint "nom_empleados_fk_manuales_terceros" foreign key ("cod_empleadoid_fk") references "sgnom"."tsgnomempleados" ("cod_empleadoid") on delete no action on update no action;
alter table "sgnom"."tsgnomquincena" add constraint "nom_cat_ejercicio_id_fk_cat_quincenas" foreign key ("cod_ejercicioid_fk") references "sgnom"."tsgnomejercicio" ("cod_ejercicioid") on delete no action on update cascade;

