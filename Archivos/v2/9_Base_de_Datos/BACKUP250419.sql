--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7
-- Dumped by pg_dump version 10.7

-- Started on 2019-04-25 23:20:56 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10 (class 2615 OID 16395)
-- Name: sgco; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgco;


ALTER SCHEMA sgco OWNER TO postgres;

--
-- TOC entry 4351 (class 0 OID 0)
-- Dependencies: 10
-- Name: SCHEMA sgco; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgco IS 'Sistema de Gestion de Conocimiento de la Organizacion.';


--
-- TOC entry 12 (class 2615 OID 16396)
-- Name: sgrh; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgrh;


ALTER SCHEMA sgrh OWNER TO postgres;

--
-- TOC entry 4352 (class 0 OID 0)
-- Dependencies: 12
-- Name: SCHEMA sgrh; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sgrh IS 'ESQUEMA QUE CONTIENE LAS TABLAS DE SISTEMA DE GESTION DE RECURSOS HUMANOS';


--
-- TOC entry 11 (class 2615 OID 16397)
-- Name: sgrt; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sgrt;


ALTER SCHEMA sgrt OWNER TO postgres;

--
-- TOC entry 9 (class 2615 OID 16398)
-- Name: sisat; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sisat;


ALTER SCHEMA sisat OWNER TO postgres;

--
-- TOC entry 4353 (class 0 OID 0)
-- Dependencies: 9
-- Name: SCHEMA sisat; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA sisat IS 'Esquema que contiene las tablas del Sistema de Seleccion y Adquisicion de Talentos';


--
-- TOC entry 1 (class 3079 OID 13003)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 4354 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 2 (class 3079 OID 16399)
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- TOC entry 4355 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'Functions that manipulate whole tables, including crosstab';


--
-- TOC entry 798 (class 1247 OID 16421)
-- Name: edo_encuesta; Type: TYPE; Schema: sgrh; Owner: postgres
--

CREATE TYPE sgrh.edo_encuesta AS ENUM (
    '--',
    'En proceso',
    'Corregido',
    'Aceptado'
);


ALTER TYPE sgrh.edo_encuesta OWNER TO postgres;

--
-- TOC entry 801 (class 1247 OID 16430)
-- Name: destinatario; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.destinatario AS ENUM (
    'USR',
    'EMPLEADO',
    'GRUPO',
    'DEPTO'
);


ALTER TYPE sgrt.destinatario OWNER TO postgres;

--
-- TOC entry 804 (class 1247 OID 16440)
-- Name: edoticket; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.edoticket AS ENUM (
    'Abierto',
    'Cerrado'
);


ALTER TYPE sgrt.edoticket OWNER TO postgres;

--
-- TOC entry 807 (class 1247 OID 16446)
-- Name: encriptacion; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.encriptacion AS ENUM (
    'NONE',
    'SSL'
);


ALTER TYPE sgrt.encriptacion OWNER TO postgres;

--
-- TOC entry 810 (class 1247 OID 16452)
-- Name: estatus; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.estatus AS ENUM (
    'Enviado',
    'Pendiente'
);


ALTER TYPE sgrt.estatus OWNER TO postgres;

--
-- TOC entry 813 (class 1247 OID 16458)
-- Name: estatus_compromiso; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.estatus_compromiso AS ENUM (
    'Pendiente',
    'Terminado'
);


ALTER TYPE sgrt.estatus_compromiso OWNER TO postgres;

--
-- TOC entry 816 (class 1247 OID 16464)
-- Name: modulo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.modulo AS ENUM (
    'SGRT',
    'SSV'
);


ALTER TYPE sgrt.modulo OWNER TO postgres;

--
-- TOC entry 819 (class 1247 OID 16470)
-- Name: origencontac; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.origencontac AS ENUM (
    'Web',
    'Email',
    'Reunion',
    'Telefono',
    'Otro'
);


ALTER TYPE sgrt.origencontac OWNER TO postgres;

--
-- TOC entry 822 (class 1247 OID 16482)
-- Name: prioridad; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja'
);


ALTER TYPE sgrt.prioridad OWNER TO postgres;

--
-- TOC entry 825 (class 1247 OID 16490)
-- Name: protocolo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.protocolo AS ENUM (
    'POP',
    'IMAP'
);


ALTER TYPE sgrt.protocolo OWNER TO postgres;

--
-- TOC entry 828 (class 1247 OID 16496)
-- Name: tipo; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.tipo AS ENUM (
    'ReunionAgendada',
    'ReunionCancelada',
    'NuevaSolicitud',
    'SolicitudPagada',
    'ViaticoCancelado'
);


ALTER TYPE sgrt.tipo OWNER TO postgres;

--
-- TOC entry 831 (class 1247 OID 16508)
-- Name: tipo_compromiso; Type: TYPE; Schema: sgrt; Owner: postgres
--

CREATE TYPE sgrt.tipo_compromiso AS ENUM (
    'Acuerdo',
    'Pendiente'
);


ALTER TYPE sgrt.tipo_compromiso OWNER TO postgres;

--
-- TOC entry 404 (class 1255 OID 16513)
-- Name: crosstab_report_encuesta(integer); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.crosstab_report_encuesta(integer) RETURNS TABLE(pregunta character varying, resp1 character varying, resp2 character varying, resp3 character varying, resp4 character varying, resp5 character varying)
    LANGUAGE sql
    AS $_$        
            SELECT * FROM crosstab(
                'SELECT p.des_pregunta AS rowid, 
                        cr.cod_ponderacion as attribute, 
                        cr.des_respuesta as value
                FROM sgrh.tsgrhpreguntasenc p
                INNER JOIN sgrh.tsgrhencuesta e ON p.cod_encuesta = e.cod_encuesta
                LEFT JOIN sgrh.tsgrhrespuestasenc r ON p.cod_pregunta = r.cod_pregunta
                LEFT JOIN sgrh.tsgrhcatrespuestas cr ON r.cod_catrespuesta = cr.cod_catrespuesta
                WHERE e.cod_encuesta = ' || $1
			) 
            AS (
                pregunta VARCHAR(200), 
                resp1 VARCHAR(200), 
                resp2 VARCHAR(200), 
                resp3 VARCHAR(200), 
                resp4 VARCHAR(200), 
                resp5 VARCHAR(200)
            );
    $_$;


ALTER FUNCTION sgrh.crosstab_report_encuesta(integer) OWNER TO postgres;

--
-- TOC entry 405 (class 1255 OID 16514)
-- Name: factualizarfecha(); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.factualizarfecha() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare begin
	new.fec_modificacion:=current_date;
	return new;

end;
$$;


ALTER FUNCTION sgrh.factualizarfecha() OWNER TO postgres;

--
-- TOC entry 406 (class 1255 OID 16515)
-- Name: buscar_asistentes_minuta(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) RETURNS TABLE(nombre_asistente text, area_asistente character varying)
    LANGUAGE sql
    AS $$
SELECT
des_nombre as nombre_asistente,
CONCAT((SELECT area.des_nbarea FROM sgrh.tsgrhempleados emp LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area WHERE emp.cod_empleado=invitado.cod_empleado),
des_empresa) as area_asistente
FROM sgrt.tsgrtinvitados invitado WHERE  invitado.cnu_asiste='1' and invitado.cod_reunion=reunionid;
$$;


ALTER FUNCTION sgrt.buscar_asistentes_minuta(reunionid integer) OWNER TO postgres;

--
-- TOC entry 407 (class 1255 OID 16516)
-- Name: buscar_compromisos_roles_list(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) RETURNS TABLE(cod_compromiso integer, des_descripcion character varying, cod_estatus sgrt.estatus_compromiso, fec_solicitud text, fec_compromiso text, validador text, verificador text, ejecutor text)
    LANGUAGE plpgsql
    AS $$

BEGIN
RETURN QUERY
select
CAST(ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as INTEGER) AS cod_compromiso,
com.des_descripcion,
com.cod_estatus,
CAST(to_char(com.fec_solicitud, 'DD/MM/YYYY') as text) as fec_solicitud,
CAST(to_char(com.fec_compromiso, 'DD/MM/YYYY') as text) as fec_compromiso,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_validador) AS validador,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_verificador) AS verificador,
(select CONCAT(des_nombre, ' ', des_nombres, ' ', des_apepaterno, ' ', des_apematerno) from sgrh.tsgrhempleados where cod_empleado=com.cod_ejecutor) AS ejecutor
from sgrt.tsgrtcompromisos com where com.cod_reunion=reunionid;

END;
$$;


ALTER FUNCTION sgrt.buscar_compromisos_roles_list(reunionid integer) OWNER TO postgres;

--
-- TOC entry 408 (class 1255 OID 16517)
-- Name: buscar_minutas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, cantidad_minutas integer)
    LANGUAGE sql
    AS $$
SELECT
cod_area,
cod_acronimo as des_nbarea,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtreuniones reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_responsable=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE area.cod_area=a.cod_area and
reu.fec_fecha >= cast(fecha_inicio as date)
AND reu.fec_fecha <=  cast(fecha_fin as date)
) as INTEGER) AS cantidad_minutas
FROM sgrh.tsgrhareas a
$$;


ALTER FUNCTION sgrt.buscar_minutas_fechas(fecha_inicio text, fecha_fin text) OWNER TO postgres;

--
-- TOC entry 409 (class 1255 OID 16518)
-- Name: buscar_proxima_reunion(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) RETURNS SETOF record
    LANGUAGE sql
    AS $$
  select
  reunion.cod_reunion,
  reunion.des_nombre,
  CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY')as text) as fec_fecha,
  reunion.cod_lugar,
  CAST(reunion.tim_hora as text),
  lugar.des_nombre,
  lugar.cod_ciudad,
ciudad.des_nbciudad,
ciudad.cod_estadorep,
estado.des_nbestado,
estado.cod_estadorep
from sgrt.tsgrtreuniones reunion inner join sgrt.tsgrtlugares lugar on reunion.cod_lugar=lugar.cod_lugar
inner join sgrt.tsgrtciudades ciudad on lugar.cod_ciudad=ciudad.cod_ciudad
inner join sgrt.tsgrtestados estado on ciudad.cod_estadorep=estado.cod_estadorep
where cod_reunion=(select cod_proximareunion from sgrt.tsgrtreuniones where cod_reunion=reunionId);

$$;


ALTER FUNCTION sgrt.buscar_proxima_reunion(reunionid integer, OUT cod_reunion integer, OUT des_nombre_reunion character varying, OUT fec_fecha text, OUT cod_lugar integer, OUT tim_hora character varying, OUT des_nombre_lugar character varying, OUT cod_ciudad integer, OUT des_nbciudad character varying, OUT cod_estadorep_ciudad integer, OUT des_nbestado character varying, OUT cod_estadorep_estado integer) OWNER TO postgres;

--
-- TOC entry 410 (class 1255 OID 16519)
-- Name: compromisos_areas_fechas(text, text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) RETURNS TABLE(cod_area integer, des_nbarea character varying, tipo text, num integer)
    LANGUAGE sql
    AS $$
select
cod_area,
cod_acronimo as des_nbarea,
CAST('Terminado' as text) as tipo,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtcompromisos reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_ejecutor=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE reu.cod_estatus='Terminado' AND
area.cod_area=a.cod_area AND
reu.fec_compromiso >= cast(fecha_inicio as date)
AND reu.fec_compromiso <= cast(fecha_fin as date)
)as INTEGER) as num
FROM
sgrh.tsgrhareas a
UNION
select
cod_area,
cod_acronimo as des_nbarea,
CAST('Pendiente' as text) as tipo,
cast((SELECT
COUNT(*)
FROM sgrt.tsgrtcompromisos reu
INNER JOIN sgrh.tsgrhempleados emp
ON reu.cod_ejecutor=emp.cod_empleado
INNER JOIN sgrh.tsgrhareas area
ON emp.cod_area=area.cod_area
WHERE reu.cod_estatus='Pendiente' and
area.cod_area=a.cod_area AND
reu.fec_compromiso >= cast(fecha_inicio as date)
AND reu.fec_compromiso <= cast(fecha_fin as date))as INTEGER) as num
FROM
sgrh.tsgrhareas a
$$;


ALTER FUNCTION sgrt.compromisos_areas_fechas(fecha_inicio text, fecha_fin text) OWNER TO postgres;

--
-- TOC entry 411 (class 1255 OID 16520)
-- Name: compromisos_dia(text); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_dia(fechacompromiso text) RETURNS TABLE(cod_empleado integer, nombre text, area text, fec_compromiso text, des_descripcion text, cod_estatus text, tiempo_demora integer)
    LANGUAGE sql
    AS $$
SELECT
emp.cod_empleado,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
(SELECT cod_acronimo FROM sgrh.tsgrhareas WHERE cod_area=emp.cod_area) as area,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text) fec_compromiso,
com.des_descripcion,
cast(com.cod_estatus as text),
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrh.tsgrhempleados emp ON com.cod_ejecutor=emp.cod_empleado or com.cod_validador=emp.cod_empleado or com.cod_verificador=emp.cod_empleado
WHERE com.fec_compromiso=cast(fechaCompromiso as date);
$$;


ALTER FUNCTION sgrt.compromisos_dia(fechacompromiso text) OWNER TO postgres;

--
-- TOC entry 412 (class 1255 OID 16521)
-- Name: compromisos_generales(); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.compromisos_generales() RETURNS TABLE(cod_empleado integer, cod_reunion integer, nombre text, rol text, area text, descripcion text, minuta text, estatus text, fec_registro text, fec_compromiso text, dias_habiles integer, tiempo_demora integer)
    LANGUAGE sql
    AS $$
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Validador' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_validador
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_validador
UNION
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Verificador' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_verificador
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_verificador
UNION
SELECT
emp.cod_empleado,
reunion.cod_reunion,
CONCAT(emp.des_nombre, ' ', emp.des_nombres, ' ', emp.des_apepaterno, ' ', emp.des_apematerno) as nombre,
CAST('Ejecutor' as text) as rol,
cast(area.cod_acronimo as text) as area,
cast(com.des_descripcion as text) as descripcion,
cast(reunion.des_nombre as text) as minuta,
cast(com.cod_estatus as text)as estatus,
cast(to_char(com.fec_solicitud,'DD/MM/YYYY') as text) as fec_registro,
cast(to_char(com.fec_compromiso,'DD/MM/YYYY') as text),
cast((select count(the_day) from
    (select generate_series(com.fec_solicitud+1, com.fec_compromiso, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as dias_habiles,
cast((select count(the_day) from
    (select generate_series(com.fec_compromiso+1, com.fec_entrega, '1 day') as the_day) days
where extract('dow' from the_day) not in (0,6))as INTEGER) as tiempo_demora
FROM sgrt.tsgrtcompromisos com
LEFT JOIN sgrt.tsgrtreuniones reunion ON reunion.cod_reunion=com.cod_reunion
LEFT JOIN sgrh.tsgrhempleados emp ON emp.cod_empleado=com.cod_ejecutor
LEFT JOIN sgrh.tsgrhareas area ON area.cod_area=emp.cod_area
WHERE emp.cod_empleado=com.cod_ejecutor
$$;


ALTER FUNCTION sgrt.compromisos_generales() OWNER TO postgres;

--
-- TOC entry 413 (class 1255 OID 16522)
-- Name: reporte_por_tema(integer); Type: FUNCTION; Schema: sgrt; Owner: postgres
--

CREATE FUNCTION sgrt.reporte_por_tema(reunionid integer) RETURNS TABLE(nombre_minuta character varying, responsable text, fecha text, acuerdos integer, pendientes integer, total integer, tiempo_invertido text)
    LANGUAGE plpgsql
    AS $$
DECLARE
    var_r record;
BEGIN
   FOR var_r IN(SELECT * FROM sgrt.tsgrtreuniones WHERE cod_reunion=reunionId OR cod_proximareunion=reunionId)
     LOOP
              RETURN QUERY
		select c.nombre_minuta, c.responsable, c.fecha,a.Acuerdos, b.Pendientes, cast(COALESCE(a.Acuerdos,0)+COALESCE(b.Pendientes,0)as int) AS Total, c.tiempo_invertido from
		(select cast(count(cod_tipocompromiso) as int) as Acuerdos from sgrt.tsgrtcompromisos where cod_tipocompromiso='Acuerdo' and cod_reunion=var_r.cod_reunion) a,
		(select cast(count(cod_tipocompromiso)as int) as Pendientes from sgrt.tsgrtcompromisos where cod_tipocompromiso='Pendiente' and cod_reunion=var_r.cod_reunion) b,
		(select reunion.des_nombre AS nombre_minuta, CONCAT(empleado.des_nombre, ' ', empleado.des_nombres, ' ', empleado.des_apepaterno, ' ', empleado.des_apematerno) AS responsable,
		CAST(to_char(reunion.fec_fecha, 'DD/MM/YYYY') as text) as fecha, CAST(to_char(reunion.tim_duracion,'HH24:MI') as text) as tiempo_invertido from
		sgrt.tsgrtreuniones reunion,
		sgrh.tsgrhempleados empleado
		WHERE reunion.cod_responsable=empleado.cod_empleado and
		cod_reunion=var_r.cod_reunion) c;
            END LOOP;
END; $$;


ALTER FUNCTION sgrt.reporte_por_tema(reunionid integer) OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16523)
-- Name: seq_sistema; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_sistema OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16525)
-- Name: seq_tipousuario; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_tipousuario OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16527)
-- Name: seq_usuarios; Type: SEQUENCE; Schema: sgco; Owner: postgres
--

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_usuarios OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 207 (class 1259 OID 16529)
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


ALTER TABLE sgco.tsgcosistemas OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16532)
-- Name: tsgcotipousuario; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol character varying(35) NOT NULL
);


ALTER TABLE sgco.tsgcotipousuario OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16535)
-- Name: tsgcousuarios; Type: TABLE; Schema: sgco; Owner: postgres
--

CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(60) NOT NULL,
    des_contrasenacorreo character varying(50) NOT NULL,
    cod_usuariosistema character varying(30) NOT NULL,
    des_contrasenasistema character varying(30) NOT NULL
);


ALTER TABLE sgco.tsgcousuarios OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16538)
-- Name: seq_area; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_area
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_area OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16540)
-- Name: seq_capacitaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_capacitaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_capacitaciones OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 16542)
-- Name: seq_cartaasignacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cartaasignacion OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16544)
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cat_encuesta_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cat_encuesta_participantes OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16546)
-- Name: seq_catrespuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_catrespuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_catrespuestas OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16548)
-- Name: seq_clientes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_clientes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_clientes OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16550)
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contrataciones OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16552)
-- Name: seq_contratos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_contratos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contratos OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16554)
-- Name: seq_empleado; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_empleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_empleado OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16556)
-- Name: seq_encuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_encuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_encuestas OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16558)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_escolaridad OWNER TO postgres;

--
-- TOC entry 362 (class 1259 OID 18522)
-- Name: seq_estatus; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_estatus
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_estatus OWNER TO postgres;

--
-- TOC entry 379 (class 1259 OID 18898)
-- Name: seq_evacapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evacapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacapacitacion OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16560)
-- Name: seq_evacontestadas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evacontestadas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacontestadas OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16562)
-- Name: seq_evaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_evaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evaluaciones OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16564)
-- Name: seq_experiencialab; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_experiencialab
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_experiencialab OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 16566)
-- Name: seq_factoreseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_factoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_factoreseva OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16568)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_idiomas OWNER TO postgres;

--
-- TOC entry 376 (class 1259 OID 18847)
-- Name: seq_logistica; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_logistica
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_logistica OWNER TO postgres;

--
-- TOC entry 364 (class 1259 OID 18528)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_lugar OWNER TO postgres;

--
-- TOC entry 365 (class 1259 OID 18530)
-- Name: seq_modo; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_modo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_modo OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16570)
-- Name: seq_perfiles; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_perfiles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_perfiles OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16572)
-- Name: seq_plancapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_plancapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_plancapacitacion OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 16574)
-- Name: seq_planesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_planesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_planesoperativos OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16578)
-- Name: seq_preguntasenc; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_preguntasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntasenc OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 16580)
-- Name: seq_preguntaseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_preguntaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntaseva OWNER TO postgres;

--
-- TOC entry 361 (class 1259 OID 18516)
-- Name: seq_proceso; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_proceso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proceso OWNER TO postgres;

--
-- TOC entry 363 (class 1259 OID 18524)
-- Name: seq_proveedor; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_proveedor
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_proveedor OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16584)
-- Name: seq_puestos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_puestos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_puestos OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16588)
-- Name: seq_respuestasenc; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_respuestasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestasenc OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16590)
-- Name: seq_respuestaseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_respuestaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestaseva OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 16592)
-- Name: seq_revplanesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_revplanesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_revplanesoperativos OWNER TO postgres;

--
-- TOC entry 368 (class 1259 OID 18592)
-- Name: seq_rolempleado; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_rolempleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_rolempleado OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16594)
-- Name: seq_subfactoreseva; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_subfactoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_subfactoreseva OWNER TO postgres;

--
-- TOC entry 366 (class 1259 OID 18532)
-- Name: seq_tipocapacitacion; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_tipocapacitacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_tipocapacitacion OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 16598)
-- Name: seq_validaevaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_validaevaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_validaevaluaciones OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 16600)
-- Name: tsgrhareas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhareas (
    cod_area integer DEFAULT nextval('sgrh.seq_area'::regclass) NOT NULL,
    des_nbarea character varying(50) NOT NULL,
    cod_acronimo character varying(5) NOT NULL,
    cnu_activo boolean NOT NULL,
    cod_sistemasuite integer
);


ALTER TABLE sgrh.tsgrhareas OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 16604)
-- Name: tsgrhasignacionesemp; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhasignacionesemp (
    cod_asignacion integer NOT NULL,
    cod_empleado integer NOT NULL,
    cod_puesto integer NOT NULL,
    cod_asignadopor integer NOT NULL,
    cod_modificadopor integer,
    status character varying(10) NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhasignacionesemp OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 16607)
-- Name: tsgrhcapacitaciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcapacitaciones (
    cod_capacitacion integer DEFAULT nextval('sgrh.seq_capacitaciones'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_tipocurso character varying(40) NOT NULL,
    des_nbcurso character varying(50) NOT NULL,
    des_organismo character varying(50) NOT NULL,
    fec_termino date NOT NULL,
    des_duracion character varying(40) NOT NULL,
    bin_documento bytea
);


ALTER TABLE sgrh.tsgrhcapacitaciones OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 16614)
-- Name: tsgrhcartaasignacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_cartaasignacion'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcartaasignacion OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 16623)
-- Name: tsgrhcatrespuestas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcatrespuestas (
    cod_catrespuesta integer DEFAULT nextval('sgrh.seq_catrespuestas'::regclass) NOT NULL,
    des_respuesta character varying(100) NOT NULL,
    cod_ponderacion integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatrespuestas OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 16627)
-- Name: tsgrhclientes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhclientes (
    cod_cliente integer DEFAULT nextval('sgrh.seq_clientes'::regclass) NOT NULL,
    des_nbcliente character varying(90),
    des_direccioncte character varying(150) NOT NULL,
    des_nbcontactocte character varying(70) NOT NULL,
    des_correocte character varying(50) NOT NULL,
    cod_telefonocte character varying(16) DEFAULT NULL::character varying
);


ALTER TABLE sgrh.tsgrhclientes OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 16632)
-- Name: tsgrhcontrataciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcontrataciones (
    cod_contratacion integer DEFAULT nextval('sgrh.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date,
    des_esquema character varying(30) NOT NULL,
    cod_salarioestmin numeric(6,2) NOT NULL,
    cod_salarioestmax numeric(6,2),
    tim_jornada time without time zone,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontrataciones OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 16638)
-- Name: tsgrhcontratos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhcontratos (
    cod_contrato integer DEFAULT nextval('sgrh.seq_contratos'::regclass) NOT NULL,
    des_nbconsultor character varying(45) NOT NULL,
    des_appaterno character varying(45) NOT NULL,
    des_apmaterno character varying(45) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontratos OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 16644)
-- Name: tsgrhempleados; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhempleados (
    cod_empleado integer DEFAULT nextval('sgrh.seq_empleado'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_apepaterno character varying(40) NOT NULL,
    des_apematerno character varying(40),
    des_direccion character varying(150) NOT NULL,
    fec_nacimiento date NOT NULL,
    des_lugarnacimiento character varying(50) NOT NULL,
    cod_edad integer NOT NULL,
    des_correo character varying(50),
    cod_tiposangre character varying(5) NOT NULL,
    cod_telefonocasa character varying(16),
    cod_telefonocelular character varying(16),
    cod_telemergencia character varying(16),
    bin_identificacion bytea,
    bin_pasaporte bytea,
    bin_visa bytea,
    cod_licenciamanejo character varying(20),
    fec_ingreso date NOT NULL,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    cod_empleadoactivo boolean,
    cod_estatusempleado integer NOT NULL,
    cod_estadocivil integer NOT NULL,
    cod_rol integer,
    cod_puesto integer,
    cod_diasvacaciones integer,
    cod_sistemasuite integer,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_creadopor integer,
    cod_modificadopor integer,
    cod_area integer NOT NULL
);


ALTER TABLE sgrh.tsgrhempleados OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 16653)
-- Name: tsgrhencuesta; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhencuesta (
    cod_encuesta integer DEFAULT nextval('sgrh.seq_encuestas'::regclass) NOT NULL,
    des_nbencuesta character varying(50) NOT NULL,
    cod_edoencuesta character varying(20) NOT NULL,
    fec_fechaencuesta date NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    des_elementosvalidar character varying(200),
    des_defectos character varying(200),
    des_introduccion character varying(200),
    cod_aceptado boolean,
    cod_edoeliminar boolean,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_area integer NOT NULL,
    CONSTRAINT tsgrhencuesta_cod_edoencuesta_check CHECK (((cod_edoencuesta)::text = ANY (ARRAY[('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhencuesta OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 16663)
-- Name: tsgrhencuesta_participantes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhencuesta_participantes (
    cod_participantenc integer DEFAULT nextval('sgrh.seq_cat_encuesta_participantes'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_encuesta integer NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_respuesta integer,
    respuesta_abierta text
);


ALTER TABLE sgrh.tsgrhencuesta_participantes OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 16670)
-- Name: tsgrhescolaridad; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhescolaridad (
    cod_escolaridad integer DEFAULT nextval('sgrh.seq_escolaridad'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbinstitucion character varying(70) NOT NULL,
    des_nivelestudios character varying(30) NOT NULL,
    cod_titulo boolean NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    bin_titulo bytea
);


ALTER TABLE sgrh.tsgrhescolaridad OWNER TO postgres;

--
-- TOC entry 373 (class 1259 OID 18662)
-- Name: tsgrhestatuscapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhestatuscapacitacion (
    cod_estatus integer DEFAULT nextval('sgrh.seq_estatus'::regclass) NOT NULL,
    des_estatus character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhestatuscapacitacion OWNER TO postgres;

--
-- TOC entry 380 (class 1259 OID 18900)
-- Name: tsgrhevacapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacapacitacion (
    cod_evacapacitacion integer DEFAULT nextval('sgrh.seq_evacapacitacion'::regclass) NOT NULL,
    cod_plancapacitacion integer NOT NULL,
    cod_empleado integer,
    des_estado character varying(50) NOT NULL,
    des_evaluacion character varying(50),
    auf_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhevacapacitacion OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 16680)
-- Name: tsgrhevacontestadas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacontestadas (
    cod_evacontestada integer DEFAULT nextval('sgrh.seq_evacontestadas'::regclass) NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_evaluador integer NOT NULL,
    cod_evaluado integer NOT NULL,
    cod_total integer NOT NULL,
    bin_reporte bytea
);


ALTER TABLE sgrh.tsgrhevacontestadas OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 16687)
-- Name: tsgrhevaluaciones; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevaluaciones (
    cod_evaluacion integer DEFAULT nextval('sgrh.seq_evaluaciones'::regclass) NOT NULL,
    des_nbevaluacion character varying(60) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    des_edoevaluacion character varying(30) DEFAULT '--'::character varying,
    cod_edoeliminar boolean,
    CONSTRAINT tsgrhevaluaciones_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhevaluaciones OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 16695)
-- Name: tsgrhexperienciaslaborales; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sgrh.seq_experiencialab'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbempresa character varying(50) NOT NULL,
    des_nbpuesto character varying(50) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    txt_actividades text NOT NULL,
    des_ubicacion character varying(70),
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sgrh.tsgrhexperienciaslaborales OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 16702)
-- Name: tsgrhfactoreseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhfactoreseva (
    cod_factor integer DEFAULT nextval('sgrh.seq_factoreseva'::regclass) NOT NULL,
    des_nbfactor character varying(60) NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhfactoreseva OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 16706)
-- Name: tsgrhidiomas; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhidiomas (
    cod_idioma integer DEFAULT nextval('sgrh.seq_idiomas'::regclass) NOT NULL,
    des_nbidioma character varying(45) NOT NULL,
    por_dominiooral integer,
    por_dominioescrito integer,
    cod_empleado integer
);


ALTER TABLE sgrh.tsgrhidiomas OWNER TO postgres;

--
-- TOC entry 377 (class 1259 OID 18849)
-- Name: tsgrhlogistica; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhlogistica (
    cod_logistica integer DEFAULT nextval('sgrh.seq_logistica'::regclass) NOT NULL,
    tim_horario integer NOT NULL,
    des_requerimientos character varying(200),
    fec_fecinicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_capacitacion integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhlogistica OWNER TO postgres;

--
-- TOC entry 372 (class 1259 OID 18646)
-- Name: tsgrhlugares; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhlugares (
    cod_lugar integer DEFAULT nextval('sgrh.seq_lugar'::regclass) NOT NULL,
    des_lugar character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhlugares OWNER TO postgres;

--
-- TOC entry 367 (class 1259 OID 18551)
-- Name: tsgrhmodo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhmodo (
    cod_modo integer DEFAULT nextval('sgrh.seq_modo'::regclass) NOT NULL,
    des_nbmodo character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhmodo OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 16710)
-- Name: tsgrhperfiles; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhperfiles (
    cod_perfil integer DEFAULT nextval('sgrh.seq_perfiles'::regclass) NOT NULL,
    des_perfil character varying(100) NOT NULL
);


ALTER TABLE sgrh.tsgrhperfiles OWNER TO postgres;

--
-- TOC entry 375 (class 1259 OID 18795)
-- Name: tsgrhplancapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhplancapacitacion (
    cod_plancapacitacion integer DEFAULT nextval('sgrh.seq_plancapacitacion'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_modo integer NOT NULL,
    cod_tipocapacitacion integer NOT NULL,
    des_criterios character varying(200) NOT NULL,
    cod_proceso integer NOT NULL,
    des_instructor character varying(50) NOT NULL,
    cod_proveedor integer NOT NULL,
    cod_estatus integer NOT NULL,
    des_comentarios character varying(200),
    des_evaluacion character varying(50),
    cod_lugar integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date NOT NULL,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhplancapacitacion OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 16726)
-- Name: tsgrhplanoperativo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhplanoperativo (
    cod_planoperativo integer DEFAULT nextval('sgrh.seq_planesoperativos'::regclass) NOT NULL,
    des_nbplan character varying(100) NOT NULL,
    cod_version character varying(5) NOT NULL,
    cod_anio integer NOT NULL,
    cod_estatus character varying(20) NOT NULL,
    bin_planoperativo bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhplanoperativo OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 16735)
-- Name: tsgrhpreguntasenc; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpreguntasenc (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntasenc'::regclass) NOT NULL,
    des_pregunta character varying(200) NOT NULL,
    cod_tipopregunta boolean,
    cod_edoeliminar boolean,
    cod_encuesta integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntasenc OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 16739)
-- Name: tsgrhpreguntaseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpreguntaseva (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntaseva'::regclass) NOT NULL,
    des_pregunta character varying(100) NOT NULL,
    cod_edoeliminar boolean NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_subfactor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntaseva OWNER TO postgres;

--
-- TOC entry 370 (class 1259 OID 18614)
-- Name: tsgrhprocesos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhprocesos (
    cod_proceso integer DEFAULT nextval('sgrh.seq_proceso'::regclass) NOT NULL,
    des_nbproceso character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhprocesos OWNER TO postgres;

--
-- TOC entry 371 (class 1259 OID 18630)
-- Name: tsgrhproveedores; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhproveedores (
    cod_proveedor integer DEFAULT nextval('sgrh.seq_proveedor'::regclass) NOT NULL,
    des_nbproveedor character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhproveedores OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 16747)
-- Name: tsgrhpuestos; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhpuestos (
    cod_puesto integer DEFAULT nextval('sgrh.seq_puestos'::regclass) NOT NULL,
    des_puesto character varying(100) NOT NULL,
    cod_area integer NOT NULL,
    cod_acronimo character varying(5)
);


ALTER TABLE sgrh.tsgrhpuestos OWNER TO postgres;

--
-- TOC entry 378 (class 1259 OID 18872)
-- Name: tsgrhrelacionroles; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrelacionroles (
    cod_plancapacitacion integer NOT NULL,
    cod_rolempleado integer NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrelacionroles OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 16755)
-- Name: tsgrhrespuestasenc; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrespuestasenc (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestasenc'::regclass) NOT NULL,
    cod_catrespuesta integer,
    cod_pregunta integer NOT NULL,
    cod_edoeliminar boolean
);


ALTER TABLE sgrh.tsgrhrespuestasenc OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 16759)
-- Name: tsgrhrespuestaseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrespuestaseva (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestaseva'::regclass) NOT NULL,
    des_respuesta character varying(200) NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_evacontestada integer NOT NULL
);


ALTER TABLE sgrh.tsgrhrespuestaseva OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 16763)
-- Name: tsgrhrevplanoperativo; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrevplanoperativo (
    cod_revplanoperativo integer DEFAULT nextval('sgrh.seq_revplanesoperativos'::regclass) NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    cod_participante1 integer,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_participante5 integer,
    cod_planoperativo integer,
    des_puntosatratar character varying(250),
    des_acuerdosobtenidos character varying(500),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhrevplanoperativo OWNER TO postgres;

--
-- TOC entry 369 (class 1259 OID 18598)
-- Name: tsgrhrolempleado; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhrolempleado (
    cod_rolempleado integer DEFAULT nextval('sgrh.seq_rolempleado'::regclass) NOT NULL,
    des_nbrol character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhrolempleado OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 16772)
-- Name: tsgrhsubfactoreseva; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhsubfactoreseva (
    cod_subfactor integer DEFAULT nextval('sgrh.seq_subfactoreseva'::regclass) NOT NULL,
    des_nbsubfactor character varying(60) NOT NULL,
    cod_factor integer NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhsubfactoreseva OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 18711)
-- Name: tsgrhtipocapacitacion; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhtipocapacitacion (
    cod_tipocapacitacion integer DEFAULT nextval('sgrh.seq_tipocapacitacion'::regclass) NOT NULL,
    des_nbtipocapacitacion character varying(50) NOT NULL,
    aud_feccreacion date NOT NULL,
    aud_fecmodificacion date,
    aud_creadopor integer NOT NULL,
    aud_modificadopor integer
);


ALTER TABLE sgrh.tsgrhtipocapacitacion OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 16780)
-- Name: tsgrhvalidaevaluaciondes; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhvalidaevaluaciondes (
    cod_validacion integer DEFAULT nextval('sgrh.seq_validaevaluaciones'::regclass) NOT NULL,
    des_edoevaluacion character varying(30) NOT NULL,
    fec_validacion date,
    cod_lugar integer,
    tim_duracion time without time zone,
    des_defectosevalucacion character varying(1000),
    cod_edoeliminar boolean,
    cod_evaluacion integer,
    cod_participante1 integer NOT NULL,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_evaluador integer,
    cod_evaluado integer,
    CONSTRAINT tsgrhvalidaevaluaciondes_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhvalidaevaluaciondes OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 16788)
-- Name: seq_agenda; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_agenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_agenda OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 16790)
-- Name: seq_archivo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_archivo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_archivo OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 16792)
-- Name: seq_asistente; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_asistente
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_asistente OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 16794)
-- Name: seq_attach; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_attach
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_attach OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 16796)
-- Name: seq_categoriafaq; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_categoriafaq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_categoriafaq OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 16798)
-- Name: seq_chat; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_chat
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999999999
    CACHE 1;


ALTER TABLE sgrt.seq_chat OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 16800)
-- Name: seq_ciudad; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_ciudad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ciudad OWNER TO postgres;

--
-- TOC entry 271 (class 1259 OID 16802)
-- Name: seq_comentsagenda; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_comentsagenda
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsagenda OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 16804)
-- Name: seq_comentsreunion; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_comentsreunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_comentsreunion OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 16806)
-- Name: seq_compromiso; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_compromiso
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_compromiso OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 16808)
-- Name: seq_contacto; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_contacto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_contacto OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 16810)
-- Name: seq_correo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_correo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_correo OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 16812)
-- Name: seq_depto; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_depto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_depto OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 16814)
-- Name: seq_edoacuerdo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_edoacuerdo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_edoacuerdo OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 16816)
-- Name: seq_elemento; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_elemento
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_elemento OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 16818)
-- Name: seq_estadorep; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_estadorep
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_estadorep OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 16820)
-- Name: seq_faq; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_faq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_faq OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 16822)
-- Name: seq_grupo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_grupo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_grupo OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 16824)
-- Name: seq_invitado; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_invitado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_invitado OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 16826)
-- Name: seq_lugar; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_lugar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_lugar OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 16828)
-- Name: seq_mensaje; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_mensaje
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_mensaje OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 16830)
-- Name: seq_nota; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_nota
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_nota OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 16832)
-- Name: seq_plantillacorreo; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_plantillacorreo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_plantillacorreo OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 16834)
-- Name: seq_prioridad; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_prioridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_prioridad OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 16836)
-- Name: seq_resp; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_resp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_resp OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 16838)
-- Name: seq_respuesta; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_respuesta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuesta OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 16840)
-- Name: seq_respuestas_participantes; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_respuestas_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_respuestas_participantes OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 16842)
-- Name: seq_reunion; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_reunion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_reunion OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 16844)
-- Name: seq_servicio; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_servicio
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_servicio OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 16846)
-- Name: seq_solicitud; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_solicitud
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_solicitud OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 16848)
-- Name: seq_ticket; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_ticket
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_ticket OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 16850)
-- Name: seq_topico; Type: SEQUENCE; Schema: sgrt; Owner: postgres
--

CREATE SEQUENCE sgrt.seq_topico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrt.seq_topico OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 16852)
-- Name: tsgrtagenda; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtagenda (
    cod_agenda integer DEFAULT nextval('sgrt.seq_agenda'::regclass) NOT NULL,
    des_texto character varying(200) NOT NULL,
    cnu_tratado smallint DEFAULT '0'::smallint,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtagenda OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 16857)
-- Name: tsgrtarchivos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtarchivos (
    cod_archivo integer DEFAULT nextval('sgrt.seq_archivo'::regclass) NOT NULL,
    bin_archivo bytea,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtarchivos OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 16864)
-- Name: tsgrtasistentes; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtasistentes (
    cod_asistente integer DEFAULT nextval('sgrt.seq_asistente'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    cod_empleado integer,
    cnu_asiste smallint NOT NULL,
    cod_invitado integer
);


ALTER TABLE sgrt.tsgrtasistentes OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 16868)
-- Name: tsgrtattchticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtattchticket (
    cod_attach integer DEFAULT nextval('sgrt.seq_attach'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_tamano character varying(20) NOT NULL,
    des_nombre character varying(128) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    bin_attach bytea
);


ALTER TABLE sgrt.tsgrtattchticket OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 16875)
-- Name: tsgrtayudatopico; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtayudatopico (
    cod_topico integer DEFAULT nextval('sgrt.seq_topico'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer NOT NULL,
    des_topico character varying(36) NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtayudatopico OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 16879)
-- Name: tsgrtcategoriafaq; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcategoriafaq (
    cod_categoriafaq integer DEFAULT nextval('sgrt.seq_categoriafaq'::regclass) NOT NULL,
    cnu_tipo smallint NOT NULL,
    des_categoria character varying(255) NOT NULL,
    des_descripcion character varying(255) NOT NULL,
    des_notas character varying(255) NOT NULL,
    tim_ultactualiza timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    fec_ultactualizadopor integer NOT NULL,
    cod_creadopor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcategoriafaq OWNER TO postgres;

--
-- TOC entry 302 (class 1259 OID 16886)
-- Name: tsgrtchat; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtchat (
    cod_chat integer DEFAULT nextval('sgrt.seq_chat'::regclass) NOT NULL,
    chat character varying NOT NULL
);


ALTER TABLE sgrt.tsgrtchat OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 16893)
-- Name: tsgrtciudades; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtciudades (
    cod_ciudad integer DEFAULT nextval('sgrt.seq_ciudad'::regclass) NOT NULL,
    des_nbciudad character varying(100) NOT NULL,
    cod_estadorep integer NOT NULL
);


ALTER TABLE sgrt.tsgrtciudades OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 16897)
-- Name: tsgrtcomentariosagenda; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcomentariosagenda (
    cod_comentsagenda integer DEFAULT nextval('sgrt.seq_comentsagenda'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_agenda integer NOT NULL,
    cod_invitado integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosagenda OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 16904)
-- Name: tsgrtcomentariosreunion; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcomentariosreunion (
    cod_commentsreunion integer DEFAULT nextval('sgrt.seq_comentsreunion'::regclass) NOT NULL,
    des_comentario character varying(500) NOT NULL,
    cod_invitado integer NOT NULL,
    cod_reunion integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcomentariosreunion OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 16911)
-- Name: tsgrtcompromisos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcompromisos (
    cod_compromiso integer DEFAULT nextval('sgrt.seq_compromiso'::regclass) NOT NULL,
    des_descripcion character varying(200) NOT NULL,
    fec_solicitud date NOT NULL,
    fec_compromiso date NOT NULL,
    cod_reunion integer NOT NULL,
    cod_validador integer NOT NULL,
    cod_verificador integer NOT NULL,
    cod_estado integer,
    des_valor character varying(45),
    cod_ejecutor integer NOT NULL,
    cod_tipoejecutor character varying(10),
    cnu_revisado smallint,
    cod_estatus sgrt.estatus_compromiso DEFAULT 'Pendiente'::sgrt.estatus_compromiso NOT NULL,
    cod_tipocompromiso sgrt.tipo_compromiso DEFAULT 'Pendiente'::sgrt.tipo_compromiso NOT NULL,
    cod_chat integer,
    fec_entrega date
);


ALTER TABLE sgrt.tsgrtcompromisos OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 16917)
-- Name: tsgrtcorreo; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtcorreo (
    cod_correo integer DEFAULT nextval('sgrt.seq_correo'::regclass) NOT NULL,
    cnu_autorespuesta smallint NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_depto integer,
    des_nbusuario character varying(32) NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(70) NOT NULL,
    des_contrasena character varying(30) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_dirhost character varying(125) NOT NULL,
    cod_protocolo sgrt.protocolo DEFAULT 'POP'::sgrt.protocolo NOT NULL,
    cod_encriptacion sgrt.encriptacion DEFAULT 'NONE'::sgrt.encriptacion NOT NULL,
    cod_puerto integer,
    cnu_frecsinc smallint NOT NULL,
    cnu_nummaxcorreo smallint NOT NULL,
    cnu_eliminar smallint NOT NULL,
    cnu_errores smallint NOT NULL,
    fec_ulterror timestamp without time zone,
    fec_ultsincr timestamp without time zone,
    cnu_smtpactivo smallint,
    des_smtphost character varying(125) NOT NULL,
    cod_smtpport integer,
    cnu_smtpsecure smallint NOT NULL,
    cnu_smtpauth smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_usuario integer NOT NULL
);


ALTER TABLE sgrt.tsgrtcorreo OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 16923)
-- Name: tsgrtdatossolicitud; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtdatossolicitud (
    cod_datosolicitud integer NOT NULL,
    cod_elemento integer NOT NULL,
    des_descripcion character varying(45) NOT NULL,
    cod_solicitud integer NOT NULL,
    cod_edosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtdatossolicitud OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 16926)
-- Name: tsgrtdepartamento; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtdepartamento (
    cod_depto integer DEFAULT nextval('sgrt.seq_depto'::regclass) NOT NULL,
    cod_plantillacorreo integer NOT NULL,
    cod_correo integer NOT NULL,
    cod_manager integer DEFAULT 0 NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_ncorto character varying(10) NOT NULL,
    des_firma text NOT NULL,
    cnu_publico smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtdepartamento OWNER TO postgres;

--
-- TOC entry 310 (class 1259 OID 16935)
-- Name: tsgrtedosolicitudes; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtedosolicitudes (
    cod_edosolicitud integer NOT NULL,
    cod_nbedosolicitud integer NOT NULL
);


ALTER TABLE sgrt.tsgrtedosolicitudes OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 16938)
-- Name: tsgrtelementos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtelementos (
    cod_elemento integer DEFAULT nextval('sgrt.seq_elemento'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_activo smallint NOT NULL
);


ALTER TABLE sgrt.tsgrtelementos OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 16942)
-- Name: tsgrtestados; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtestados (
    cod_estadorep integer DEFAULT nextval('sgrt.seq_estadorep'::regclass) NOT NULL,
    des_nbestado character varying(60) NOT NULL
);


ALTER TABLE sgrt.tsgrtestados OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 16946)
-- Name: tsgrtfaq; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtfaq (
    cod_faq integer DEFAULT nextval('sgrt.seq_faq'::regclass) NOT NULL,
    cod_categoriafaq integer NOT NULL,
    des_pregunta character varying(255) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_respuesta character varying(255) NOT NULL,
    des_notasint character varying(255) NOT NULL,
    fec_ultactualizacion timestamp without time zone NOT NULL,
    fec_creacion timestamp without time zone NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_ultactualizacionpor integer NOT NULL
);


ALTER TABLE sgrt.tsgrtfaq OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 16953)
-- Name: tsgrtgrupo; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtgrupo (
    cod_grupo integer DEFAULT nextval('sgrt.seq_grupo'::regclass) NOT NULL,
    cnu_activo smallint NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cnu_crear smallint NOT NULL,
    cnu_editar smallint NOT NULL,
    cnu_borrar smallint NOT NULL,
    cnu_cerrar smallint NOT NULL,
    cnu_transferir smallint NOT NULL,
    cnu_prohibir smallint NOT NULL,
    cnu_administrar smallint NOT NULL,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtgrupo OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 16958)
-- Name: tsgrtinvitados; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtinvitados (
    cod_invitado integer DEFAULT nextval('sgrt.seq_invitado'::regclass) NOT NULL,
    cod_reunion integer NOT NULL,
    des_nombre character varying,
    des_correo character varying,
    cnu_invitacionenv smallint NOT NULL,
    cnu_asiste smallint NOT NULL,
    cod_empleado integer,
    des_empresa character varying
);


ALTER TABLE sgrt.tsgrtinvitados OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 16965)
-- Name: tsgrtlugares; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtlugares (
    cod_lugar integer DEFAULT nextval('sgrt.seq_lugar'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    cod_ciudad integer NOT NULL
);


ALTER TABLE sgrt.tsgrtlugares OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 16969)
-- Name: tsgrtmsjticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtmsjticket (
    cod_mensaje integer DEFAULT nextval('sgrt.seq_mensaje'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_usuario integer DEFAULT 0 NOT NULL,
    des_mensaje text NOT NULL,
    cod_fuente character varying(16) DEFAULT NULL::character varying,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtmsjticket OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 16979)
-- Name: tsgrtnota; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtnota (
    cod_nota integer DEFAULT nextval('sgrt.seq_nota'::regclass) NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_fuente character varying(32) NOT NULL,
    des_titulo character varying(255) DEFAULT 'Nota INTEGERerna Generica'::character varying NOT NULL,
    des_nota text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtnota OWNER TO postgres;

--
-- TOC entry 319 (class 1259 OID 16987)
-- Name: tsgrtplantillacorreos; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtplantillacorreos (
    cod_plantillacorreo integer DEFAULT nextval('sgrt.seq_plantillacorreo'::regclass) NOT NULL,
    des_nombre character varying(32) NOT NULL,
    des_notas text,
    cod_tipodestinario sgrt.destinatario DEFAULT 'USR'::sgrt.destinatario,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    des_asunto character varying(45),
    des_cuerpo character varying(255)
);


ALTER TABLE sgrt.tsgrtplantillacorreos OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 16995)
-- Name: tsgrtprioridad; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtprioridad (
    cod_prioridad integer DEFAULT nextval('sgrt.seq_prioridad'::regclass) NOT NULL,
    des_nombre character varying(60) NOT NULL,
    des_descripcion character varying(30) NOT NULL,
    cod_color character varying(7) NOT NULL,
    cnu_valprioridad smallint NOT NULL,
    cnu_publica smallint NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtprioridad OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 16999)
-- Name: tsgrtresppredefinida; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtresppredefinida (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_resp'::regclass) NOT NULL,
    cod_depto integer NOT NULL,
    cnu_activo smallint NOT NULL,
    des_titulo character varying(125) NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtresppredefinida OWNER TO postgres;

--
-- TOC entry 322 (class 1259 OID 17006)
-- Name: tsgrtrespuesta; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtrespuesta (
    cod_respuesta integer DEFAULT nextval('sgrt.seq_respuesta'::regclass) NOT NULL,
    cod_mensaje integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_respuesta text NOT NULL,
    cod_sistemasuite integer NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone
);


ALTER TABLE sgrt.tsgrtrespuesta OWNER TO postgres;

--
-- TOC entry 323 (class 1259 OID 17013)
-- Name: tsgrtreuniones; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtreuniones (
    cod_reunion integer DEFAULT nextval('sgrt.seq_reunion'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    fec_fecha date NOT NULL,
    des_objetivo character varying(700) NOT NULL,
    cod_lugar integer NOT NULL,
    cod_responsable integer NOT NULL,
    cod_proximareunion integer,
    cod_creadorreunion integer NOT NULL,
    tim_duracion time without time zone,
    tim_hora time without time zone
);


ALTER TABLE sgrt.tsgrtreuniones OWNER TO postgres;

--
-- TOC entry 324 (class 1259 OID 17020)
-- Name: tsgrtservicios; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtservicios (
    cod_servicio integer DEFAULT nextval('sgrt.seq_servicio'::regclass) NOT NULL,
    des_nombre_servicio character varying(45) NOT NULL,
    des_descripcion character varying(100) NOT NULL,
    fec_contratacion date NOT NULL
);


ALTER TABLE sgrt.tsgrtservicios OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 17024)
-- Name: tsgrtsolicitudservicios; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtsolicitudservicios (
    cod_solicitud integer DEFAULT nextval('sgrt.seq_solicitud'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_ticket integer NOT NULL,
    cod_servicio integer NOT NULL
);


ALTER TABLE sgrt.tsgrtsolicitudservicios OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 17028)
-- Name: tsgrtticket; Type: TABLE; Schema: sgrt; Owner: postgres
--

CREATE TABLE sgrt.tsgrtticket (
    cod_ticket integer DEFAULT nextval('sgrt.seq_ticket'::regclass) NOT NULL,
    des_folio character varying(45) NOT NULL,
    cod_reunion integer,
    cod_acuerdo integer,
    cod_responsable integer,
    cod_validador integer,
    cod_depto integer NOT NULL,
    cod_prioridad smallint NOT NULL,
    cod_topico integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(50) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    des_tema character varying(64) DEFAULT '[Sin Asunto]'::character varying NOT NULL,
    des_temaayuda character varying(255) DEFAULT NULL::character varying,
    cod_telefono character varying(16) DEFAULT NULL::character varying,
    cod_extension character varying(8) DEFAULT NULL::character varying,
    cod_estadot sgrt.edoticket DEFAULT 'Abierto'::sgrt.edoticket NOT NULL,
    cod_origent sgrt.origencontac DEFAULT 'Otro'::sgrt.origencontac NOT NULL,
    cnu_expirado smallint NOT NULL,
    cnu_atendido smallint NOT NULL,
    fec_exp timestamp without time zone,
    fec_reap timestamp without time zone,
    fec_cierre timestamp without time zone,
    fec_ultimomsg timestamp without time zone,
    fec_ultimaresp timestamp without time zone,
    cod_sistemasuite integer DEFAULT 1 NOT NULL,
    fec_ultactualizacion timestamp without time zone,
    cod_ultactualizacionpor integer NOT NULL,
    cod_creadopor integer NOT NULL,
    fec_creacion timestamp without time zone,
    cod_ejecutor integer
);


ALTER TABLE sgrt.tsgrtticket OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 17042)
-- Name: seq_aceptaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_aceptaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_aceptaciones OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 17044)
-- Name: seq_asignaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_asignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_asignaciones OWNER TO postgres;

--
-- TOC entry 329 (class 1259 OID 17046)
-- Name: seq_candidatos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_candidatos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_candidatos OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 17048)
-- Name: seq_cartaasignaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_cartaasignaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cartaasignaciones OWNER TO postgres;

--
-- TOC entry 331 (class 1259 OID 17050)
-- Name: seq_cotizaciones; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_cotizaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cotizaciones OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 17052)
-- Name: seq_cursos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_cursos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_cursos OWNER TO postgres;

--
-- TOC entry 333 (class 1259 OID 17054)
-- Name: seq_entrevistas; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_entrevistas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_entrevistas OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 17056)
-- Name: seq_envios; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_envios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_envios OWNER TO postgres;

--
-- TOC entry 335 (class 1259 OID 17058)
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_escolaridad OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 17060)
-- Name: seq_experiencias; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_experiencias
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_experiencias OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 17062)
-- Name: seq_firmas; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_firmas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_firmas OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 17064)
-- Name: seq_habilidades; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_habilidades
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_habilidades OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 17066)
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_idiomas OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 17068)
-- Name: seq_ordenservicios; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_ordenservicios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_ordenservicios OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 17070)
-- Name: seq_prospectos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_prospectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_prospectos OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 17072)
-- Name: seq_proyectos; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_proyectos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_proyectos OWNER TO postgres;

--
-- TOC entry 343 (class 1259 OID 17074)
-- Name: seq_vacantes; Type: SEQUENCE; Schema: sisat; Owner: postgres
--

CREATE SEQUENCE sisat.seq_vacantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sisat.seq_vacantes OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 17076)
-- Name: tsisatasignaciones; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatasignaciones (
    cod_asignacion integer DEFAULT nextval('sisat.seq_asignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    cod_cliente integer NOT NULL,
    des_correocte character varying(40) NOT NULL,
    cod_telefonocte character varying(16) NOT NULL,
    des_direccioncte character varying(200),
    cod_empleado integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatasignaciones OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 17080)
-- Name: tsisatcandidatos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcandidatos (
    cod_candidato integer DEFAULT nextval('sisat.seq_candidatos'::regclass) NOT NULL,
    des_nombre character varying(50) NOT NULL,
    cod_perfil integer NOT NULL,
    imp_sueldo numeric(6,2),
    imp_sueldodia numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_cargasocial numeric(6,2),
    imp_prestaciones numeric(6,2),
    imp_viaticos numeric(6,2),
    imp_subtotalcandidato numeric(6,2),
    imp_costoadmin numeric(6,2),
    cnu_financiamiento smallint,
    imp_isr numeric(6,2),
    imp_financiamiento numeric(6,2),
    imp_adicionales numeric(6,2),
    imp_subtotaladmin1 numeric(6,2),
    imp_comisiones numeric(6,2),
    imp_otrosgastos numeric(6,2),
    imp_subtotaladmin2 numeric(6,2),
    imp_total numeric(6,2),
    imp_iva numeric(6,2),
    por_utilidad numeric(4,2),
    imp_utilidad numeric(6,2),
    imp_tarifa numeric(6,2)
);


ALTER TABLE sisat.tsisatcandidatos OWNER TO postgres;

--
-- TOC entry 346 (class 1259 OID 17084)
-- Name: tsisatcartaaceptacion; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcartaaceptacion (
    cod_aceptacion integer DEFAULT nextval('sisat.seq_aceptaciones'::regclass) NOT NULL,
    des_objetivo character varying(200) NOT NULL,
    txt_oferta text,
    des_esquema character varying(30) NOT NULL,
    tim_jornada time without time zone,
    txt_especificaciones text,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatcartaaceptacion OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 17091)
-- Name: tsisatcartaasignacion; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatcartaasignacion OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 17098)
-- Name: tsisatcotizaciones; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcotizaciones (
    cod_cotizacion integer DEFAULT nextval('sisat.seq_cotizaciones'::regclass) NOT NULL,
    des_nbciudad character varying(20),
    des_nbestado character varying(20),
    fec_fecha date,
    des_nbcontacto character varying(50),
    cod_puesto integer,
    des_compania character varying(50),
    des_nbservicio character varying(50),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_nbatentamente character varying(60),
    des_correoatentamente character varying(50),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatcotizaciones OWNER TO postgres;

--
-- TOC entry 349 (class 1259 OID 17105)
-- Name: tsisatcursosycerticados; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatcursosycerticados (
    cod_curso integer DEFAULT nextval('sisat.seq_cursos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_curso character varying(100) NOT NULL,
    des_institucion character varying(70) NOT NULL,
    fec_termino date NOT NULL
);


ALTER TABLE sisat.tsisatcursosycerticados OWNER TO postgres;

--
-- TOC entry 350 (class 1259 OID 17109)
-- Name: tsisatentrevistas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatentrevistas (
    cod_entrevista integer DEFAULT nextval('sisat.seq_entrevistas'::regclass) NOT NULL,
    des_nbentrevistador character varying(90) NOT NULL,
    des_puesto character varying(50) NOT NULL,
    des_correoent character varying(40) NOT NULL,
    cod_telefonoent character varying(16) NOT NULL,
    des_direccionent character varying(200),
    tim_horarioent time without time zone,
    fec_fechaent date NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatentrevistas OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 17113)
-- Name: tsisatenviocorreos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatenviocorreos (
    cod_envio integer DEFAULT nextval('sisat.seq_envios'::regclass) NOT NULL,
    des_destinatario character varying(90) NOT NULL,
    des_asunto character varying(50) NOT NULL,
    des_mensaje character varying(200) NOT NULL,
    bin_adjunto bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatenviocorreos OWNER TO postgres;

--
-- TOC entry 352 (class 1259 OID 17120)
-- Name: tsisatescolaridad; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatescolaridad (
    cod_escolaridad integer DEFAULT nextval('sisat.seq_escolaridad'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_escolaridad character varying(45) NOT NULL,
    des_escuela character varying(70) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    cod_estatus character varying(20)
);


ALTER TABLE sisat.tsisatescolaridad OWNER TO postgres;

--
-- TOC entry 353 (class 1259 OID 17124)
-- Name: tsisatexperienciaslaborales; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sisat.seq_experiencias'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_empresa character varying(50) NOT NULL,
    des_puesto character varying(40) NOT NULL,
    fec_inicio date,
    fec_termino date,
    des_ubicacion character varying(70),
    txt_funciones text,
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sisat.tsisatexperienciaslaborales OWNER TO postgres;

--
-- TOC entry 354 (class 1259 OID 17131)
-- Name: tsisatfirmas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatfirmas (
    cod_firma integer DEFAULT nextval('sisat.seq_firmas'::regclass) NOT NULL,
    cod_solicita integer,
    cod_puestosolicita integer,
    cod_autoriza integer,
    cod_puestoautoriza integer,
    cod_contratacion integer NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatfirmas OWNER TO postgres;

--
-- TOC entry 355 (class 1259 OID 17135)
-- Name: tsisathabilidades; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisathabilidades (
    cod_habilidad integer DEFAULT nextval('sisat.seq_habilidades'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    des_habilidad1 character varying(50),
    des_habilidad2 character varying(50),
    des_habilidad3 character varying(50),
    des_habilidad4 character varying(50),
    des_habilidad5 character varying(50),
    des_habilidad6 character varying(50),
    des_habilidad7 character varying(50),
    des_habilidad8 character varying(50),
    des_habilidad9 character varying(50),
    des_habilidad10 character varying(50),
    des_habilidad11 character varying(50),
    des_habilidad12 character varying(50),
    des_habilidad13 character varying(50),
    des_habilidad14 character varying(50),
    des_habilidad15 character varying(50),
    des_habilidad16 character varying(50),
    des_habilidad17 character varying(50),
    des_habilidad18 character varying(50),
    des_habilidad19 character varying(50),
    des_habilidad20 character varying(50),
    des_habilidad21 character varying(50),
    des_habilidad22 character varying(50),
    des_habilidad23 character varying(50),
    des_habilidad24 character varying(50),
    des_habilidad25 character varying(50),
    des_habilidad26 character varying(50),
    des_habilidad27 character varying(50),
    des_habilidad28 character varying(50),
    des_habilidad29 character varying(50),
    des_habilidad30 character varying(50),
    des_dominio1 integer DEFAULT 0,
    des_dominio2 integer DEFAULT 0,
    des_dominio3 integer DEFAULT 0,
    des_dominio4 integer DEFAULT 0,
    des_dominio5 integer DEFAULT 0,
    des_dominio6 integer DEFAULT 0,
    des_dominio7 integer DEFAULT 0,
    des_dominio8 integer DEFAULT 0,
    des_dominio9 integer DEFAULT 0,
    des_dominio10 integer DEFAULT 0,
    des_dominio11 integer DEFAULT 0,
    des_dominio12 integer DEFAULT 0,
    des_dominio13 integer DEFAULT 0,
    des_dominio14 integer DEFAULT 0,
    des_dominio15 integer DEFAULT 0,
    des_dominio16 integer DEFAULT 0,
    des_dominio17 integer DEFAULT 0,
    des_dominio18 integer DEFAULT 0,
    des_dominio19 integer DEFAULT 0,
    des_dominio20 integer DEFAULT 0,
    des_dominio21 integer DEFAULT 0,
    des_dominio22 integer DEFAULT 0,
    des_dominio23 integer DEFAULT 0,
    des_dominio24 integer DEFAULT 0,
    des_dominio25 integer DEFAULT 0,
    des_dominio26 integer DEFAULT 0,
    des_dominio27 integer DEFAULT 0,
    des_dominio28 integer DEFAULT 0,
    des_dominio29 integer DEFAULT 0,
    des_dominio30 integer DEFAULT 0
);


ALTER TABLE sisat.tsisathabilidades OWNER TO postgres;

--
-- TOC entry 356 (class 1259 OID 17172)
-- Name: tsisatidiomas; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatidiomas (
    cod_idioma integer DEFAULT nextval('sisat.seq_idiomas'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_nbidioma character varying(20) NOT NULL,
    cod_nivel character varying(20) NOT NULL,
    des_certificado character varying(40)
);


ALTER TABLE sisat.tsisatidiomas OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 17176)
-- Name: tsisatordenservicio; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatordenservicio (
    cod_ordenservicio integer DEFAULT nextval('sisat.seq_ordenservicios'::regclass) NOT NULL,
    cod_estadorep integer NOT NULL,
    cod_ciudad integer NOT NULL,
    fec_fecha date NOT NULL,
    des_nbcontacto character varying(50),
    cod_puesto integer,
    des_nbcompania character varying(50),
    des_nbservicio character varying(60),
    cnu_cantidad smallint,
    txt_concepto text,
    imp_inversionhr numeric(6,2),
    txt_condicionescomer text,
    des_ubcnconsultor character varying(100),
    fec_finservicio date,
    cod_gpy integer NOT NULL,
    des_correogpy character varying(50) NOT NULL,
    cod_cliente integer NOT NULL,
    des_correoclte character varying(50) NOT NULL,
    des_empresaclte character varying(50),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatordenservicio OWNER TO postgres;

--
-- TOC entry 358 (class 1259 OID 17183)
-- Name: tsisatprospectos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatprospectos (
    cod_prospecto integer DEFAULT nextval('sisat.seq_prospectos'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_appaterno character varying(40) NOT NULL,
    des_apmaterno character varying(40),
    des_lugarnacimiento character varying(50) NOT NULL,
    fec_nacimiento date NOT NULL,
    cod_edad integer NOT NULL,
    cod_edocivil character varying(15) NOT NULL,
    des_nbpadre character varying(70),
    des_nbmadre character varying(70),
    cod_numhermanos integer,
    des_nbcalle character varying(60),
    cod_numcasa integer,
    des_colonia character varying(60),
    des_localidad character varying(60),
    des_municipio character varying(60),
    des_estado character varying(60),
    cod_cpostal integer,
    cod_tiposangre character varying(5),
    des_emailmbn character varying(40),
    des_emailpersonal character varying(40),
    des_pasatiempo character varying(200),
    cod_telefonocasa character varying(16) DEFAULT NULL::character varying,
    cod_telefonomovil character varying(16) DEFAULT NULL::character varying,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    des_nacionalidad character varying(30),
    cod_administrador integer,
    fec_fechacoment date,
    txt_comentarios text,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatprospectos OWNER TO postgres;

--
-- TOC entry 359 (class 1259 OID 17192)
-- Name: tsisatproyectos; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatproyectos (
    cod_proyecto integer DEFAULT nextval('sisat.seq_proyectos'::regclass) NOT NULL,
    cod_prospecto integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_nbcliente character varying(50) NOT NULL,
    des_nbresponsable character varying(50) NOT NULL,
    des_correo character varying(50) NOT NULL,
    cod_telefono character varying(16) NOT NULL,
    des_direccion character varying(200) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatproyectos OWNER TO postgres;

--
-- TOC entry 360 (class 1259 OID 17196)
-- Name: tsisatvacantes; Type: TABLE; Schema: sisat; Owner: postgres
--

CREATE TABLE sisat.tsisatvacantes (
    cod_vacante integer DEFAULT nextval('sisat.seq_vacantes'::regclass) NOT NULL,
    des_rqvacante character varying(200) NOT NULL,
    cnu_anexperiencia smallint,
    txt_experiencia text,
    des_escolaridad character varying(50),
    txt_herramientas text,
    txt_habilidades text,
    des_lugartrabajo character varying(100),
    imp_sueldo numeric(6,2),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date NOT NULL
);


ALTER TABLE sisat.tsisatvacantes OWNER TO postgres;

--
-- TOC entry 4170 (class 0 OID 16529)
-- Dependencies: 207
-- Data for Name: tsgcosistemas; Type: TABLE DATA; Schema: sgco; Owner: postgres
--

INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (1, 'Sistema de Recursos Humanos y Ambiente de Trabajo', 'SGRHAT');
INSERT INTO sgco.tsgcosistemas (cod_sistema, des_nbsistema, des_descripcion) VALUES (2, 'Sistema de Integración', 'SISAT');


--
-- TOC entry 4171 (class 0 OID 16532)
-- Dependencies: 208
-- Data for Name: tsgcotipousuario; Type: TABLE DATA; Schema: sgco; Owner: postgres
--



--
-- TOC entry 4172 (class 0 OID 16535)
-- Dependencies: 209
-- Data for Name: tsgcousuarios; Type: TABLE DATA; Schema: sgco; Owner: postgres
--

INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (2, 11, 'adrian.suarez@gmail.com', '123456789', 'adrian_suarez', '123456789');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (3, 12, 'carlos.antonio@gmail.com', 'abcdefg', 'carlos_antonio', 'abcdefg');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (4, 13, 'angel.roano@gmail.com', '123456', 'angel_roano', '123456');
INSERT INTO sgco.tsgcousuarios (cod_usuario, cod_empleado, des_correo, des_contrasenacorreo, cod_usuariosistema, des_contrasenasistema) VALUES (1, 10, 'mateorj96@gmail.com', '143100365m@teorj96', 'mateorj96', 'root');


--
-- TOC entry 4200 (class 0 OID 16600)
-- Dependencies: 237
-- Data for Name: tsgrhareas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite) VALUES (2, 'FABRICA DE SOFTWARE', 'FS', true, 1);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite) VALUES (3, 'DISEÑO', 'DS', true, 1);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite) VALUES (4, 'SOPORTE TECNICO', 'ST', true, 1);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite) VALUES (1, 'BASE DE DATOS', 'DB', true, 2);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite) VALUES (5, 'RECURSOS HUMANOS', 'RH', true, 1);
INSERT INTO sgrh.tsgrhareas (cod_area, des_nbarea, cod_acronimo, cnu_activo, cod_sistemasuite) VALUES (6, 'Marketing', 'mrg', true, 2);


--
-- TOC entry 4201 (class 0 OID 16604)
-- Dependencies: 238
-- Data for Name: tsgrhasignacionesemp; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4202 (class 0 OID 16607)
-- Dependencies: 239
-- Data for Name: tsgrhcapacitaciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4203 (class 0 OID 16614)
-- Dependencies: 240
-- Data for Name: tsgrhcartaasignacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4204 (class 0 OID 16623)
-- Dependencies: 241
-- Data for Name: tsgrhcatrespuestas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (1, 'Nunca', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (2, 'Algunas veces', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (3, 'Regular', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (4, 'Con frecuencia', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (5, 'Siempre', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (6, 'Muy malo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (7, 'Malo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (8, 'Bueno', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (9, 'Muy bueno', 5);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (10, 'Muy bajo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (11, 'Bajo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (12, 'Alto', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (13, 'Muy alto', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (14, 'Muy incómodo', 1);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (15, 'Incómodo', 2);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (16, 'Soportable', 3);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (17, 'Confortable', 4);
INSERT INTO sgrh.tsgrhcatrespuestas (cod_catrespuesta, des_respuesta, cod_ponderacion) VALUES (18, 'Muy confortable', 5);


--
-- TOC entry 4205 (class 0 OID 16627)
-- Dependencies: 242
-- Data for Name: tsgrhclientes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (1, 'Pemex', 'Tamaulipas', 'Samuel Velzaco', 'samuel-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (2, 'Cable vision', 'Mexico', 'Alfredo Gomez', 'alfgom-@hotmail.com', '5578457687');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (3, 'Telecom', 'Mexico', 'Miguel Romero', 'romero@gmail.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (4, 'Bancomer', 'Mexico', 'Jose Mauro', 'jose@bbva.com', '55344344334');
INSERT INTO sgrh.tsgrhclientes (cod_cliente, des_nbcliente, des_direccioncte, des_nbcontactocte, des_correocte, cod_telefonocte) VALUES (5, 'Nuevo', 'conocido', 'josue', 'dfd2@nuevo.com', '556322');


--
-- TOC entry 4206 (class 0 OID 16632)
-- Dependencies: 243
-- Data for Name: tsgrhcontrataciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4207 (class 0 OID 16638)
-- Dependencies: 244
-- Data for Name: tsgrhcontratos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4208 (class 0 OID 16644)
-- Dependencies: 245
-- Data for Name: tsgrhempleados; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (10, 'MATEO', NULL, 'RODRIGUEZ', 'JUAREZ', 'DOMICILIO CONOCIDO', '1996-04-09', 'MECATLAN, VER.', 23, 'mateorj96@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ROJM960409HPZDAT22', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', NULL, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (11, 'ADRIAN', NULL, 'SUAREZ', 'DE LA CRUZ', 'DOMICILIO CONOCIDO', '1996-06-14', 'AHUACATLAN, PUE.', 23, 'adrian.suarezc@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'SDCA960614HPZDBT23', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (12, 'CARLOS', NULL, 'ANTONIO', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1996-03-12', 'AHUACATLAN, PUE.', 23, 'trinidad.carlos@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ANTC960312HPZDCT24', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (13, 'ANGEL', 'ANTONIO', 'ROANO', 'ALVARADO', 'DOMICILIO CONOCIDO', '1995-02-19', 'TETELA, PUE.', 23, 'angel.antonio.roa@gmail.com', 'O+', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-14', NULL, NULL, 'ROAA950219HPZDDT25', NULL, NULL, NULL, true, 1, 0, NULL, 1, NULL, NULL, '2018-12-01', '2018-12-01', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (14, 'JUAN', '', 'MARQUEZ', 'SAVEDO', 'DOMICILIO CONOCIDO', '1995-02-12', 'APIZACO, TLAX', 23, 'maito1.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'MASJ950212HPZDET26', NULL, '', '', true, 1, 0, NULL, 1, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 1);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (15, 'ANTONIO', '', 'HERRERA', 'CHAVEZ', 'DOMICILIO CONOCIDO', '1995-02-20', 'TLAXCALA, TLAX', 23, 'maito2.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'HECA950220HPZDFT27', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (16, 'JAVIER', '', 'CHICHARITO', 'HERNANDEZ', 'DOMICILIO CONOCIDO', '1995-02-21', 'AHUCATLAN, PUE.', 23, 'maito3.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'CHHJ950221HPZDGT28', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (17, 'MANUEL', '', 'GONZALEZ', 'PEREZ', 'DOMICILIO CONOCIDO', '1995-02-22', 'TEPANGO PUE.', 23, 'maito4.example@gmail.com', 'O-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GOPM950222HPZDHT29', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (18, 'SERGIO', '', 'RAMOS', 'FLORES', 'DOMICILIO CONOCIDO', '1995-03-01', 'ZAPOTITLAN, PUE.', 23, 'maito5.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'RAFS950301HPZDIT30', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (19, 'CARINE', '', 'BENZEMA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-02', 'AQUIXTLA, PUE.', 23, 'maito6.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'BEAC950302HPZDJT31', NULL, '', '', true, 1, 0, NULL, 2, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 2);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (20, 'CAROLINA', '', 'JIMENEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '1995-03-03', 'ZACATLAN, PUE.', 23, 'maito7.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'JIVC950303HPZDKT32', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (21, 'VERONICA', '', 'SANCHEZ', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-04', 'CHIGNAHUAPAN, PUE.', 23, 'maito8.example@gmail.com', 'B+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'SATV950304HPZDLT33', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (22, 'HEIDY', '', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-05', 'TLAXCO, TLAX.', 23, 'maito9.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TRMH950305HPZDMT34', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (23, 'JESUS', 'MIGUEL', 'VELAZCO', 'MARQUEZ', 'DOMICILIO CONOCIDO', '1995-03-06', 'POZA RICA, VER.', 23, 'maito10.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'MVMJ950306HPZDNT35', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (24, 'RAUL', '', 'ESPINOZA', 'MARTINEZ', 'DOMICILIO CONOCIDO', '1995-03-07', 'XALAPA, VER.', 23, 'maito11.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESMR950307HPZDOT36', NULL, '', '', true, 1, 0, NULL, 3, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 3);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (25, 'JOSE', 'EDUARDO', 'TRINIDAD', 'MORELOS', 'DOMICILIO CONOCIDO', '1995-03-08', 'COAHUILA, COAH.', 23, 'maito12.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ETMJ950308HPZDPT37', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (26, 'SARAHI', '', 'GONZALEZ', 'SUAREZ', 'DOMICILIO CONOCIDO', '1995-03-09', 'HUACHINANGO, PUE.', 23, 'maito13.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GOSS950309HPZDQT38', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (27, 'FEDERICO', '', 'GUZMAN', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-10', 'TULANCINGO, HID.', 23, 'maito14.example@gmail.com', 'A+', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'GUTF950310HPZDRT39', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (28, 'JOSE', 'IVAN', 'VACILIO', 'SANCHEZ', 'DOMICILIO CONOCIDO', '1995-03-11', 'XOXONANCATLA, PUE.', 23, 'maito15.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'IVSJ950311HPZDTT40', NULL, '', '', true, 1, 0, NULL, 4, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 4);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (29, 'ERICA', '', 'ESPINOZA', 'CANDELARIA', 'DOMICILIO CONOCIDO', '1995-03-12', 'XICOTEPEC, PUE.', 23, 'maito16.example@gmail.com', 'B-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESCE950312HPZDUT41', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (30, 'ROBERTO', '', 'ORTEGA', 'ALVAREZ', 'DOMICILIO CONOCIDO', '1995-03-13', 'JICOLAPA, PUE.', 23, 'maito17.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ORAR950313HPZDVT42', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (31, 'CESARIO', '', 'TELLEZ', 'REYES', 'DOMICILIO CONOCIDO', '1995-03-14', 'SANTA INES, PUE.', 23, 'maito18.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'TERC950314HPZDWT43', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (32, 'CARLOS', 'EFREN', 'SANCHEZ', 'JUAN', 'DOMICILIO CONOCIDO', '1995-03-15', 'CHOLULA, PUE.', 23, 'maito19.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'ESJC950315HPZDXT44', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (33, 'JOSE', '', 'DE LOS SANTOS', 'TRINIDAD', 'DOMICILIO CONOCIDO', '1995-03-16', 'DF, DF.', 23, 'maito20.example@gmail.com', 'A-', '', '', '', NULL, NULL, NULL, '', '2019-02-14', '', '', 'DETJ950316HPZDYT45', NULL, '', '', true, 1, 0, NULL, 5, NULL, NULL, '2019-02-14', '2019-02-14', 10, NULL, 5);
INSERT INTO sgrh.tsgrhempleados (cod_empleado, des_nombre, des_nombres, des_apepaterno, des_apematerno, des_direccion, fec_nacimiento, des_lugarnacimiento, cod_edad, des_correo, cod_tiposangre, cod_telefonocasa, cod_telefonocelular, cod_telemergencia, bin_identificacion, bin_pasaporte, bin_visa, cod_licenciamanejo, fec_ingreso, cod_rfc, cod_nss, cod_curp, bin_foto, cod_tipofoto, cod_extensionfoto, cod_empleadoactivo, cod_estatusempleado, cod_estadocivil, cod_rol, cod_puesto, cod_diasvacaciones, cod_sistemasuite, fec_creacion, fec_modificacion, cod_creadopor, cod_modificadopor, cod_area) VALUES (34, 'FIDEL', '', 'SANCHEZ', 'VAZQUEZ', 'DOMICILIO CONOCIDO', '0022-01-17', 'ZACATELCO, TLAX.', 0, 'maito21.example@gmail.com', 'A-', '', '757581', '', NULL, NULL, NULL, '', '0012-08-19', 'dfgt45', '', 'SAVF950317HPZDZT46', NULL, '', '', true, 1, 1, 2, 5, NULL, NULL, '0012-08-19', '2019-04-11', 10, 11, 5);


--
-- TOC entry 4209 (class 0 OID 16653)
-- Dependencies: 246
-- Data for Name: tsgrhencuesta; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (5, 'ENCUESTA 00000001', 'Aceptado', '2018-12-10', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2018-12-10', 5);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (6, 'ENCUESTA 00000002', 'Aceptado', '2018-12-10', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2018-12-10', 5);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (8, 'ENCUESTA 00000004', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (9, 'ENCUESTA 00000005', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (10, 'ENCUESTA 00000006', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (11, 'ENCUESTA 00000007', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (12, 'ENCUESTA 00000008', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 1);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (13, 'ENCUESTA 00000009', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (14, 'ENCUESTA 00000010', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (15, 'ENCUESTA 00000011', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (16, 'ENCUESTA 00000012', 'Aceptado', '2019-02-10', 2, '00:10:00', NULL, NULL, NULL, true, false, 10, 10, '2019-02-10', '2019-02-10', 2);
INSERT INTO sgrh.tsgrhencuesta (cod_encuesta, des_nbencuesta, cod_edoencuesta, fec_fechaencuesta, cod_lugar, tim_duracion, des_elementosvalidar, des_defectos, des_introduccion, cod_aceptado, cod_edoeliminar, cod_creadopor, cod_modificadopor, fec_creacion, fec_modificacion, cod_area) VALUES (7, 'ENCUESTA 00000003', 'Aceptado', '2018-12-08', 2, '00:15:00', NULL, NULL, NULL, true, false, 10, 10, '2018-12-10', '2019-02-15', 1);


--
-- TOC entry 4210 (class 0 OID 16663)
-- Dependencies: 247
-- Data for Name: tsgrhencuesta_participantes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (1, 29, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (2, 29, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (3, 29, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (4, 29, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (5, 29, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (6, 29, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (7, 29, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (8, 29, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (9, 29, 5, 90, 516, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (10, 29, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (11, 29, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (12, 29, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (13, 29, 5, 94, 528, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (14, 29, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (15, 29, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (16, 29, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (17, 29, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (18, 29, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (19, 29, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (20, 29, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (21, 29, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (22, 29, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (23, 29, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (24, 30, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (25, 30, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (26, 30, 5, 84, 489, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (27, 30, 5, 85, 494, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (28, 30, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (29, 30, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (30, 30, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (31, 30, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (32, 30, 5, 90, 515, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (33, 30, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (34, 30, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (35, 30, 5, 93, 522, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (36, 30, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (37, 30, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (38, 30, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (39, 30, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (40, 30, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (41, 30, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (42, 30, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (43, 30, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (44, 30, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (45, 30, 5, 105, 568, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (46, 30, 5, 106, 573, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (47, 31, 5, 82, 479, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (48, 31, 5, 83, 484, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (49, 31, 5, 84, 487, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (50, 31, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (51, 31, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (52, 31, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (53, 31, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (54, 31, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (55, 31, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (56, 31, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (57, 31, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (58, 31, 5, 93, 524, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (59, 31, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (60, 31, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (61, 31, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (62, 31, 5, 98, 537, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (63, 31, 5, 99, 544, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (64, 31, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (65, 31, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (66, 31, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (67, 31, 5, 104, 564, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (68, 31, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (69, 31, 5, 106, 574, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (70, 32, 5, 82, 478, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (71, 32, 5, 83, 483, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (72, 32, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (73, 32, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (74, 32, 5, 86, 498, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (75, 32, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (76, 32, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (77, 32, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (78, 32, 5, 90, 517, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (79, 32, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (80, 32, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (81, 32, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (82, 32, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (83, 32, 5, 95, 533, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (84, 32, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (85, 32, 5, 98, 539, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (86, 32, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (87, 32, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (88, 32, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (89, 32, 5, 102, 557, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (90, 32, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (91, 32, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (92, 32, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (93, 33, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (94, 33, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (95, 33, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (96, 33, 5, 85, 493, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (97, 33, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (98, 33, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (99, 33, 5, 88, 507, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (100, 33, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (101, 33, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (102, 33, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (103, 33, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (104, 33, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (105, 33, 5, 94, 529, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (106, 33, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (107, 33, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (108, 33, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (109, 33, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (110, 33, 5, 100, 548, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (111, 33, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (112, 33, 5, 102, 558, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (113, 33, 5, 104, 565, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (114, 33, 5, 105, 570, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (115, 33, 5, 106, 575, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (116, 34, 5, 82, 477, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (117, 34, 5, 83, 482, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (118, 34, 5, 84, 488, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (119, 34, 5, 85, 492, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (120, 34, 5, 86, 499, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (121, 34, 5, 87, 502, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (122, 34, 5, 88, 508, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (123, 34, 5, 89, 512, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (124, 34, 5, 90, 518, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (125, 34, 5, 91, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (126, 34, 5, 92, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (127, 34, 5, 93, 523, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (128, 34, 5, 94, 527, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (129, 34, 5, 95, 532, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (130, 34, 5, 97, NULL, '');
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (131, 34, 5, 98, 538, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (132, 34, 5, 99, 543, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (133, 34, 5, 100, 549, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (134, 34, 5, 101, 553, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (135, 34, 5, 102, 559, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (136, 34, 5, 104, 563, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (137, 34, 5, 105, 569, NULL);
INSERT INTO sgrh.tsgrhencuesta_participantes (cod_participantenc, cod_empleado, cod_encuesta, cod_pregunta, cod_respuesta, respuesta_abierta) VALUES (138, 34, 5, 106, 575, NULL);


--
-- TOC entry 4211 (class 0 OID 16670)
-- Dependencies: 248
-- Data for Name: tsgrhescolaridad; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4336 (class 0 OID 18662)
-- Dependencies: 373
-- Data for Name: tsgrhestatuscapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'No aprobado', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'En revisión', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Pendiente', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Detenido', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhestatuscapacitacion (cod_estatus, des_estatus, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (5, 'Aprobado', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4343 (class 0 OID 18900)
-- Dependencies: 380
-- Data for Name: tsgrhevacapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4212 (class 0 OID 16680)
-- Dependencies: 249
-- Data for Name: tsgrhevacontestadas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4213 (class 0 OID 16687)
-- Dependencies: 250
-- Data for Name: tsgrhevaluaciones; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4214 (class 0 OID 16695)
-- Dependencies: 251
-- Data for Name: tsgrhexperienciaslaborales; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4215 (class 0 OID 16702)
-- Dependencies: 252
-- Data for Name: tsgrhfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4216 (class 0 OID 16706)
-- Dependencies: 253
-- Data for Name: tsgrhidiomas; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (1, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (2, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (3, 'Ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (4, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (5, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (6, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (7, 'ingles', 30, 30, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (8, 'ingles', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (9, 'Ingles', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (10, 'Frances', 20, 20, 34);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (11, 'Fran', 20, 20, NULL);
INSERT INTO sgrh.tsgrhidiomas (cod_idioma, des_nbidioma, por_dominiooral, por_dominioescrito, cod_empleado) VALUES (12, 'Fran', 20, 20, NULL);


--
-- TOC entry 4340 (class 0 OID 18849)
-- Dependencies: 377
-- Data for Name: tsgrhlogistica; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4335 (class 0 OID 18646)
-- Dependencies: 372
-- Data for Name: tsgrhlugares; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4330 (class 0 OID 18551)
-- Dependencies: 367
-- Data for Name: tsgrhmodo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Interno', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhmodo (cod_modo, des_nbmodo, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Externo', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4217 (class 0 OID 16710)
-- Dependencies: 254
-- Data for Name: tsgrhperfiles; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (1, 'Administrador');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (2, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (3, 'Prueba');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (4, '.NET, java scrip');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (5, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (6, 'Nuevo');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (7, 'luness');
INSERT INTO sgrh.tsgrhperfiles (cod_perfil, des_perfil) VALUES (8, 'nuevo');


--
-- TOC entry 4338 (class 0 OID 18795)
-- Dependencies: 375
-- Data for Name: tsgrhplancapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4218 (class 0 OID 16726)
-- Dependencies: 255
-- Data for Name: tsgrhplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4219 (class 0 OID 16735)
-- Dependencies: 256
-- Data for Name: tsgrhpreguntasenc; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (107, 'En su centro de trabajo las oportunidades de desarrollo laboral solo las reciben unas cuantas personas privilegiadas', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (109, 'En  su centro de trabajo se cuenta con programas de capacitación en materia de igualdad laboral y no discriminación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (111, 'En su centro de trabajo para lograr la contratación, una promoción o un ascenso cuentan más las recomendaciones que los conocimientos y capacidades de la persona.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (113, 'En su centro de trabajo la competencia por mejores puestos, condiciones laborales o salariales es justa y equitativa.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (115, 'En su centro de trabajo se cuenta con un sistema de evaluación de desempeño del personal.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (117, 'Usted siente que se le trata con respeto en su trabajo actual.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (119, 'En su centro de trabajo todas las personas que laboran obtienen un trato digno y decente.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (126, 'En su centro de trabajo existen campañas de difusión internas de promoción de la igualdad laboral y no discriminación.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (137, 'La organización cuenta con planes y acciones específicos destinados a mejorar mi trabajo.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (141, 'El nivel de compromiso por apoyar el trabajo de los demás en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (143, 'Mi jefe me respalda frente a sus superiores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (145, 'Participo de las actividades culturales y recreacionales que la organización realiza.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (147, 'Mi jefe me brinda la retroalimentación necesaria para reforzar mis puntos débiles según la evaluación de desempeño.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (149, 'Los jefes reconocen y valoran mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (151, 'La distribución de la carga de trabajo que tiene mi área es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (153, '¿Cómo calificaría su nivel de satisfacción con el trabajo que realiza en la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (155, 'Te agradeceremos nos hagas llegar algunos comentarios acerca de aspectos que ayudarían a mejorar nuestro ambiente de trabajo.', true, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (157, 'Usted tiene el suficiente tiempo para realizar su trabajo habitual:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (159, '¿Considera que recibe una justa retribución económica por las labores desempeñadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (161, '¿Cómo calificaría su nivel de satisfacción por trabajar en la organización?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (121, 'En su centro de trabajo, en general hay personas que discriminan, tratan mal o le faltan el respeto a sus compañeras/os, colegas o subordinadas/os.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (124, 'En su centro de trabajo  las y los superiores reciben un trato mucho más respetuoso que subordinados(as) y personal administrativo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (128, 'En su centro de trabajo las cargas de trabajo se distribuyen de acuerdo a la responsabilidad del cargo.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (130, 'En mi oficina se fomenta y desarrolla el trabajo en equipo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (132, 'Existe comunicación dentro de mi grupo de trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (134, 'Siento que no me alcanza el tiempo para completar mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (136, 'La relación entre compañeros de trabajo en la organización es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (138, 'La organización otorga buenos y equitativos beneficios a los trabajadores', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (140, 'Las remuneraciones están al nivel de los sueldos de mis colegas en el mercado', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (95, 'Soy responsable del trabajo que realizo', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (94, 'Mi superior me motiva a cumplir con mi trabajo de la manera que yo considere mejor.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (93, 'Considero que necesito capacitación en alguna área de mi interés y que forma parte importante de mi desarrollo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (142, 'Siento apoyo en mi jefe cuando me encuentro en dificultades', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (91, '¿Cree que su trabajo es compatible con los objetivos de la empresa?', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (90, 'Cuento con los materiales y equipos necesarios para realizar mi trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (89, 'Está conforme con la limpieza, higiene y salubridad en su lugar de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (88, 'Si  hay  un  nuevo Plan  Estratégico, estoy dispuesto a servir de voluntario para iniciar los cambios.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (87, 'En esta Institución, la gente planifica cuidadosamente antes de tomar acción.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (144, 'Mi jefe me da autonomía para tomar las decisiones necesarias para el cumplimiento de mis responsabilidades.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (86, 'Yo aporto al proceso de planificación en mi área de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (83, 'En mi organización está claramente definida su Misión y Visión.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (146, 'Mi jefe me proporciona información suficiente, adecuada para realizar bien mi trabajo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (148, 'El nivel de recursos (materiales, equipos e infraestructura) con los que cuento para realizar bien mi trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (150, 'Mi remuneración, comparada con lo que otros ganan y hacen en la organización, está acorde con las responsabilidades de mi cargo', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (152, '¿Cómo calificaría su nivel de satisfacción por pertenecer a la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (154, '¿Cómo calificaría su nivel de identificación con la organización?', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (102, 'Siento que formo parte de un equipo que trabaja hacia una meta común', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (101, 'En mi grupo de trabajo, solucionar el problema es más importante que encontrar algún culpable.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (100, 'Mis compañeros y yo trabajamos juntos de manera efectiva', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (99, 'El horario de trabajo me permite atender mis necesidades personales', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (98, 'Me siento comprometido para alcanzar las metas establecidas.', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (156, 'En relación a las condiciones físicas de su puesto de trabajo (iluminación, temperatura, etc.)  usted considera que éste es:', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (158, '¿Está usted de acuerdo en cómo está gestionado el departamento en el que trabaja respecto a las metas que éste tiene encomendadas?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (82, 'Me siento muy satisfecho con mi ambiente de trabajo.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (160, 'Considera que su remuneración está por encima de la media en su entorno social, fuera de la empresa?', false, false, 9);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (92, 'Considero que me pagan lo justo por mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (97, 'Conozco las exigencias de mi trabajo.', true, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (108, 'En su centro de trabajo mujeres y hombres tienen por igual oportunidades de ascenso y capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (110, 'En los últimos 12 meses usted ha participado  en programas de capacitación.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (112, 'En su centro de trabajo se ha despedido a alguna mujer por embarazo u orillado a renunciar al regresar de su licencia de maternidad.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (114, 'En su centro de trabajo mujeres y hombres tienen las mismas oportunidades para ocupar puestos de decisión.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (116, 'En los últimos 12 meses le han realizado una evaluación de desempeño.', false, false, 6);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (118, 'En su centro de trabajo quienes realizan tareas personales para las y los jefes logran privilegios.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (120, 'En su centro de trabajo las valoraciones que se realizan a sus actividades dependen más de la calidad y responsabilidad que de cualquier otra cuestión personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (122, 'En su centro de trabajo debido a sus características personales hay personas que sufren un trato inferior o de burla.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (125, 'En su centro de trabajo las y los superiores están abiertos a la comunicación con el personal.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (127, 'En su centro de trabajo las funciones y tareas se transmiten de manera clara y precisa.', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (85, 'Existe un plan para lograr los  objetivos de la organización.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (84, 'La  dirección manifiesta sus objetivos de tal forma que se crea un sentido común de misión e identidad entre sus miembros.', false, false, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (129, 'Si manifiesto mi preocupación sobre algún asunto relacionado con la igualdad de género o prácticas discriminatorias, se le da seguimiento', false, false, 7);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (131, 'Para el desempeño de mis labores mi ambiente de trabajo es', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (133, 'Existe comunicación fluida entre mi Región y la sede central.', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (135, 'Los jefes en la organización se preocupan por mantener elevado el nivel de motivación del personal', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (139, 'En la organización las funciones están claramente definidas', false, false, 8);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (205, 'preguntaejemplo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (106, 'Hay evidencia de que mi jefe me apoya utilizando mis ideas o propuestas para mejorar el trabajo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (105, 'Tengo mucho trabajo y poco tiempo para realizarlo', false, true, 5);
INSERT INTO sgrh.tsgrhpreguntasenc (cod_pregunta, des_pregunta, cod_tipopregunta, cod_edoeliminar, cod_encuesta) VALUES (104, 'Mi superior inmediato toma acciones que refuerzan el objetivo común de la Institución.', false, true, 5);


--
-- TOC entry 4220 (class 0 OID 16739)
-- Dependencies: 257
-- Data for Name: tsgrhpreguntaseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4333 (class 0 OID 18614)
-- Dependencies: 370
-- Data for Name: tsgrhprocesos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Plan Capacitacion GN', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Plan Capacitacion GPR', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Plan Capacitacion GR', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhprocesos (cod_proceso, des_nbproceso, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Plan Capacitacion APE', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4334 (class 0 OID 18630)
-- Dependencies: 371
-- Data for Name: tsgrhproveedores; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4221 (class 0 OID 16747)
-- Dependencies: 258
-- Data for Name: tsgrhpuestos; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (1, 'DISEÑO Y MANTENIMIENTO DE BASE DE DATOS', 1, 'DBA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (2, 'DESARROLLO BACKEND DE SOFTWARE COMERCIALIZABLE', 2, 'BACK');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (3, 'DISEÑO FRONTEND DE APLICATIVOS COMERCIALIZABLES', 3, 'FRONT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (4, 'SOLUCION DE PROBLEMAS CON EL APLICATIVO DESARROLLADO Y MANTENIMIENTO DE CODIGO FUENTE', 4, 'QA');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (5, 'RESPONSABLE DE CAPACITACIÓN', 5, 'RC');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (6, 'RESPONSABLE DE RECURSOS HUMANOS Y AMBIENTE DE TRABAJO', 5, 'RRHAT');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (7, 'RESPOSABLE DE GESTIÓN DE RECURSOS ', 4, 'rdg');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (9, 'Soporte', 1, 'bds');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (8, 'Soporte', 6, 'bds');
INSERT INTO sgrh.tsgrhpuestos (cod_puesto, des_puesto, cod_area, cod_acronimo) VALUES (11, 'Prueba', 4, 'stp');


--
-- TOC entry 4341 (class 0 OID 18872)
-- Dependencies: 378
-- Data for Name: tsgrhrelacionroles; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4222 (class 0 OID 16755)
-- Dependencies: 259
-- Data for Name: tsgrhrespuestasenc; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (475, 1, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (476, 2, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (477, 3, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (478, 4, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (479, 5, 82, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (480, 1, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (481, 2, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (482, 3, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (483, 4, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (484, 5, 83, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (485, 1, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (486, 2, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (487, 3, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (488, 4, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (489, 5, 84, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (490, 1, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (491, 2, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (492, 3, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (493, 4, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (494, 5, 85, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (495, 1, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (496, 2, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (497, 3, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (498, 4, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (499, 5, 86, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (500, 1, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (501, 2, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (502, 3, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (503, 4, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (504, 5, 87, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (505, 1, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (506, 2, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (507, 3, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (508, 4, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (509, 5, 88, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (510, 1, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (511, 2, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (512, 3, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (513, 4, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (514, 5, 89, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (515, 1, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (516, 2, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (517, 3, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (518, 4, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (519, 5, 90, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (520, 1, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (521, 2, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (522, 3, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (523, 4, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (524, 5, 93, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (525, 1, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (526, 2, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (527, 3, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (528, 4, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (529, 5, 94, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (530, 1, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (531, 2, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (532, 3, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (533, 4, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (534, 5, 95, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (535, 1, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (536, 2, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (537, 3, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (538, 4, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (539, 5, 98, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (540, 1, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (541, 2, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (542, 3, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (543, 4, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (544, 5, 99, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (545, 1, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (546, 2, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (547, 3, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (548, 4, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (549, 5, 100, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (550, 1, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (551, 2, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (552, 3, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (553, 4, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (554, 5, 101, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (555, 1, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (556, 2, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (557, 3, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (558, 4, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (559, 5, 102, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (561, 1, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (562, 2, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (563, 3, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (564, 4, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (565, 5, 104, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (566, 1, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (567, 2, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (568, 3, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (569, 4, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (570, 5, 105, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (571, 1, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (572, 2, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (573, 3, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (574, 4, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (575, 5, 106, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (576, 1, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (577, 2, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (578, 3, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (579, 4, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (580, 5, 156, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (581, 1, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (582, 2, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (583, 3, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (584, 4, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (585, 5, 157, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (586, 1, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (587, 2, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (588, 3, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (589, 4, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (590, 5, 158, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (591, 1, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (592, 2, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (593, 3, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (594, 4, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (595, 5, 159, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (596, 1, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (597, 2, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (598, 3, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (599, 4, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (600, 5, 160, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (601, 1, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (602, 2, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (603, 3, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (604, 4, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (605, 5, 161, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (606, 1, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (607, 2, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (608, 3, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (609, 4, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (610, 5, 117, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (611, 1, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (612, 2, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (613, 3, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (614, 4, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (615, 5, 118, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (616, 1, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (617, 2, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (618, 3, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (619, 4, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (620, 5, 119, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (621, 1, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (622, 2, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (623, 3, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (624, 4, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (625, 5, 120, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (626, 1, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (627, 2, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (628, 3, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (629, 4, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (630, 5, 121, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (631, 1, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (632, 2, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (633, 3, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (634, 4, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (635, 5, 122, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (637, 1, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (638, 2, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (639, 3, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (640, 4, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (641, 5, 124, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (642, 1, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (643, 2, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (644, 3, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (645, 4, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (646, 5, 125, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (647, 1, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (648, 2, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (649, 3, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (650, 4, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (651, 5, 126, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (652, 1, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (653, 2, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (654, 3, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (655, 4, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (656, 5, 127, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (657, 1, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (658, 2, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (659, 3, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (660, 4, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (661, 5, 128, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (662, 1, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (663, 2, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (664, 3, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (665, 4, 129, false);
INSERT INTO sgrh.tsgrhrespuestasenc (cod_respuesta, cod_catrespuesta, cod_pregunta, cod_edoeliminar) VALUES (666, 5, 129, false);


--
-- TOC entry 4223 (class 0 OID 16759)
-- Dependencies: 260
-- Data for Name: tsgrhrespuestaseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4224 (class 0 OID 16763)
-- Dependencies: 261
-- Data for Name: tsgrhrevplanoperativo; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4332 (class 0 OID 18598)
-- Dependencies: 369
-- Data for Name: tsgrhrolempleado; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4225 (class 0 OID 16772)
-- Dependencies: 262
-- Data for Name: tsgrhsubfactoreseva; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4337 (class 0 OID 18711)
-- Dependencies: 374
-- Data for Name: tsgrhtipocapacitacion; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--

INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (1, 'Curso', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (2, 'Taller', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (3, 'Seminario', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (4, 'Autoestudio', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (5, 'Mentoreo', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (6, 'Diplomado', '2019-04-23', '2019-04-23', 10, 10);
INSERT INTO sgrh.tsgrhtipocapacitacion (cod_tipocapacitacion, des_nbtipocapacitacion, aud_feccreacion, aud_fecmodificacion, aud_creadopor, aud_modificadopor) VALUES (7, 'Congreso', '2019-04-23', '2019-04-23', 10, 10);


--
-- TOC entry 4226 (class 0 OID 16780)
-- Dependencies: 263
-- Data for Name: tsgrhvalidaevaluaciondes; Type: TABLE DATA; Schema: sgrh; Owner: postgres
--



--
-- TOC entry 4259 (class 0 OID 16852)
-- Dependencies: 296
-- Data for Name: tsgrtagenda; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4260 (class 0 OID 16857)
-- Dependencies: 297
-- Data for Name: tsgrtarchivos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4261 (class 0 OID 16864)
-- Dependencies: 298
-- Data for Name: tsgrtasistentes; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4262 (class 0 OID 16868)
-- Dependencies: 299
-- Data for Name: tsgrtattchticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4263 (class 0 OID 16875)
-- Dependencies: 300
-- Data for Name: tsgrtayudatopico; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4264 (class 0 OID 16879)
-- Dependencies: 301
-- Data for Name: tsgrtcategoriafaq; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4265 (class 0 OID 16886)
-- Dependencies: 302
-- Data for Name: tsgrtchat; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4266 (class 0 OID 16893)
-- Dependencies: 303
-- Data for Name: tsgrtciudades; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--

INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (6, 'ZACATLAN DE LAS MANZANAS', 1);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (7, 'CIUDAD DE MEXICO', 4);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (8, 'XALAPA', 3);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (9, 'POZA RICA', 3);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (10, 'TLAXCALA', 2);
INSERT INTO sgrt.tsgrtciudades (cod_ciudad, des_nbciudad, cod_estadorep) VALUES (11, 'APIZACO', 2);


--
-- TOC entry 4267 (class 0 OID 16897)
-- Dependencies: 304
-- Data for Name: tsgrtcomentariosagenda; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4268 (class 0 OID 16904)
-- Dependencies: 305
-- Data for Name: tsgrtcomentariosreunion; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4269 (class 0 OID 16911)
-- Dependencies: 306
-- Data for Name: tsgrtcompromisos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4270 (class 0 OID 16917)
-- Dependencies: 307
-- Data for Name: tsgrtcorreo; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4271 (class 0 OID 16923)
-- Dependencies: 308
-- Data for Name: tsgrtdatossolicitud; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4272 (class 0 OID 16926)
-- Dependencies: 309
-- Data for Name: tsgrtdepartamento; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4273 (class 0 OID 16935)
-- Dependencies: 310
-- Data for Name: tsgrtedosolicitudes; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4274 (class 0 OID 16938)
-- Dependencies: 311
-- Data for Name: tsgrtelementos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4275 (class 0 OID 16942)
-- Dependencies: 312
-- Data for Name: tsgrtestados; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--

INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (1, 'PUEBLA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (2, 'TLAXCALA');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (3, 'VERACRUZ');
INSERT INTO sgrt.tsgrtestados (cod_estadorep, des_nbestado) VALUES (4, 'DISTRITO FEDERAL');


--
-- TOC entry 4276 (class 0 OID 16946)
-- Dependencies: 313
-- Data for Name: tsgrtfaq; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4277 (class 0 OID 16953)
-- Dependencies: 314
-- Data for Name: tsgrtgrupo; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4278 (class 0 OID 16958)
-- Dependencies: 315
-- Data for Name: tsgrtinvitados; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4279 (class 0 OID 16965)
-- Dependencies: 316
-- Data for Name: tsgrtlugares; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--

INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (2, 'TLAXCALA DE XICONTECATL', 10);
INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (3, 'SANTA ANA', 10);
INSERT INTO sgrt.tsgrtlugares (cod_lugar, des_nombre, cod_ciudad) VALUES (4, 'CHIUATEMPAN', 10);


--
-- TOC entry 4280 (class 0 OID 16969)
-- Dependencies: 317
-- Data for Name: tsgrtmsjticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4281 (class 0 OID 16979)
-- Dependencies: 318
-- Data for Name: tsgrtnota; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4282 (class 0 OID 16987)
-- Dependencies: 319
-- Data for Name: tsgrtplantillacorreos; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4283 (class 0 OID 16995)
-- Dependencies: 320
-- Data for Name: tsgrtprioridad; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4284 (class 0 OID 16999)
-- Dependencies: 321
-- Data for Name: tsgrtresppredefinida; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4285 (class 0 OID 17006)
-- Dependencies: 322
-- Data for Name: tsgrtrespuesta; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4286 (class 0 OID 17013)
-- Dependencies: 323
-- Data for Name: tsgrtreuniones; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4287 (class 0 OID 17020)
-- Dependencies: 324
-- Data for Name: tsgrtservicios; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4288 (class 0 OID 17024)
-- Dependencies: 325
-- Data for Name: tsgrtsolicitudservicios; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4289 (class 0 OID 17028)
-- Dependencies: 326
-- Data for Name: tsgrtticket; Type: TABLE DATA; Schema: sgrt; Owner: postgres
--



--
-- TOC entry 4307 (class 0 OID 17076)
-- Dependencies: 344
-- Data for Name: tsisatasignaciones; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4308 (class 0 OID 17080)
-- Dependencies: 345
-- Data for Name: tsisatcandidatos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4309 (class 0 OID 17084)
-- Dependencies: 346
-- Data for Name: tsisatcartaaceptacion; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4310 (class 0 OID 17091)
-- Dependencies: 347
-- Data for Name: tsisatcartaasignacion; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4311 (class 0 OID 17098)
-- Dependencies: 348
-- Data for Name: tsisatcotizaciones; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4312 (class 0 OID 17105)
-- Dependencies: 349
-- Data for Name: tsisatcursosycerticados; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4313 (class 0 OID 17109)
-- Dependencies: 350
-- Data for Name: tsisatentrevistas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4314 (class 0 OID 17113)
-- Dependencies: 351
-- Data for Name: tsisatenviocorreos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4315 (class 0 OID 17120)
-- Dependencies: 352
-- Data for Name: tsisatescolaridad; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4316 (class 0 OID 17124)
-- Dependencies: 353
-- Data for Name: tsisatexperienciaslaborales; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4317 (class 0 OID 17131)
-- Dependencies: 354
-- Data for Name: tsisatfirmas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4318 (class 0 OID 17135)
-- Dependencies: 355
-- Data for Name: tsisathabilidades; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4319 (class 0 OID 17172)
-- Dependencies: 356
-- Data for Name: tsisatidiomas; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4320 (class 0 OID 17176)
-- Dependencies: 357
-- Data for Name: tsisatordenservicio; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4321 (class 0 OID 17183)
-- Dependencies: 358
-- Data for Name: tsisatprospectos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4322 (class 0 OID 17192)
-- Dependencies: 359
-- Data for Name: tsisatproyectos; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4323 (class 0 OID 17196)
-- Dependencies: 360
-- Data for Name: tsisatvacantes; Type: TABLE DATA; Schema: sisat; Owner: postgres
--



--
-- TOC entry 4357 (class 0 OID 0)
-- Dependencies: 204
-- Name: seq_sistema; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_sistema', 1, false);


--
-- TOC entry 4358 (class 0 OID 0)
-- Dependencies: 205
-- Name: seq_tipousuario; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_tipousuario', 1, false);


--
-- TOC entry 4359 (class 0 OID 0)
-- Dependencies: 206
-- Name: seq_usuarios; Type: SEQUENCE SET; Schema: sgco; Owner: postgres
--

SELECT pg_catalog.setval('sgco.seq_usuarios', 1, false);


--
-- TOC entry 4360 (class 0 OID 0)
-- Dependencies: 210
-- Name: seq_area; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_area', 6, true);


--
-- TOC entry 4361 (class 0 OID 0)
-- Dependencies: 211
-- Name: seq_capacitaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_capacitaciones', 1, false);


--
-- TOC entry 4362 (class 0 OID 0)
-- Dependencies: 212
-- Name: seq_cartaasignacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cartaasignacion', 1, false);


--
-- TOC entry 4363 (class 0 OID 0)
-- Dependencies: 213
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cat_encuesta_participantes', 138, true);


--
-- TOC entry 4364 (class 0 OID 0)
-- Dependencies: 214
-- Name: seq_catrespuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_catrespuestas', 200, false);


--
-- TOC entry 4365 (class 0 OID 0)
-- Dependencies: 215
-- Name: seq_clientes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_clientes', 5, true);


--
-- TOC entry 4366 (class 0 OID 0)
-- Dependencies: 216
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_contrataciones', 1, false);


--
-- TOC entry 4367 (class 0 OID 0)
-- Dependencies: 217
-- Name: seq_contratos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_contratos', 1, false);


--
-- TOC entry 4368 (class 0 OID 0)
-- Dependencies: 218
-- Name: seq_empleado; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_empleado', 13, true);


--
-- TOC entry 4369 (class 0 OID 0)
-- Dependencies: 219
-- Name: seq_encuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_encuestas', 17, true);


--
-- TOC entry 4370 (class 0 OID 0)
-- Dependencies: 220
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_escolaridad', 1, false);


--
-- TOC entry 4371 (class 0 OID 0)
-- Dependencies: 362
-- Name: seq_estatus; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_estatus', 5, true);


--
-- TOC entry 4372 (class 0 OID 0)
-- Dependencies: 379
-- Name: seq_evacapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evacapacitacion', 1, false);


--
-- TOC entry 4373 (class 0 OID 0)
-- Dependencies: 221
-- Name: seq_evacontestadas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evacontestadas', 1, false);


--
-- TOC entry 4374 (class 0 OID 0)
-- Dependencies: 222
-- Name: seq_evaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_evaluaciones', 1, false);


--
-- TOC entry 4375 (class 0 OID 0)
-- Dependencies: 223
-- Name: seq_experiencialab; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_experiencialab', 1, false);


--
-- TOC entry 4376 (class 0 OID 0)
-- Dependencies: 224
-- Name: seq_factoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_factoreseva', 1, false);


--
-- TOC entry 4377 (class 0 OID 0)
-- Dependencies: 225
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_idiomas', 12, true);


--
-- TOC entry 4378 (class 0 OID 0)
-- Dependencies: 376
-- Name: seq_logistica; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_logistica', 1, false);


--
-- TOC entry 4379 (class 0 OID 0)
-- Dependencies: 364
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_lugar', 1, false);


--
-- TOC entry 4380 (class 0 OID 0)
-- Dependencies: 365
-- Name: seq_modo; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_modo', 2, true);


--
-- TOC entry 4381 (class 0 OID 0)
-- Dependencies: 226
-- Name: seq_perfiles; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_perfiles', 8, true);


--
-- TOC entry 4382 (class 0 OID 0)
-- Dependencies: 227
-- Name: seq_plancapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_plancapacitacion', 1, false);


--
-- TOC entry 4383 (class 0 OID 0)
-- Dependencies: 228
-- Name: seq_planesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_planesoperativos', 1, false);


--
-- TOC entry 4384 (class 0 OID 0)
-- Dependencies: 229
-- Name: seq_preguntasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_preguntasenc', 205, true);


--
-- TOC entry 4385 (class 0 OID 0)
-- Dependencies: 230
-- Name: seq_preguntaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_preguntaseva', 1, false);


--
-- TOC entry 4386 (class 0 OID 0)
-- Dependencies: 361
-- Name: seq_proceso; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_proceso', 4, true);


--
-- TOC entry 4387 (class 0 OID 0)
-- Dependencies: 363
-- Name: seq_proveedor; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_proveedor', 1, false);


--
-- TOC entry 4388 (class 0 OID 0)
-- Dependencies: 231
-- Name: seq_puestos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_puestos', 11, true);


--
-- TOC entry 4389 (class 0 OID 0)
-- Dependencies: 232
-- Name: seq_respuestasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_respuestasenc', 500, false);


--
-- TOC entry 4390 (class 0 OID 0)
-- Dependencies: 233
-- Name: seq_respuestaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_respuestaseva', 1, false);


--
-- TOC entry 4391 (class 0 OID 0)
-- Dependencies: 234
-- Name: seq_revplanesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_revplanesoperativos', 1, false);


--
-- TOC entry 4392 (class 0 OID 0)
-- Dependencies: 368
-- Name: seq_rolempleado; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_rolempleado', 1, false);


--
-- TOC entry 4393 (class 0 OID 0)
-- Dependencies: 235
-- Name: seq_subfactoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_subfactoreseva', 1, false);


--
-- TOC entry 4394 (class 0 OID 0)
-- Dependencies: 366
-- Name: seq_tipocapacitacion; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_tipocapacitacion', 7, true);


--
-- TOC entry 4395 (class 0 OID 0)
-- Dependencies: 236
-- Name: seq_validaevaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_validaevaluaciones', 1, false);


--
-- TOC entry 4396 (class 0 OID 0)
-- Dependencies: 264
-- Name: seq_agenda; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_agenda', 1, false);


--
-- TOC entry 4397 (class 0 OID 0)
-- Dependencies: 265
-- Name: seq_archivo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_archivo', 1, false);


--
-- TOC entry 4398 (class 0 OID 0)
-- Dependencies: 266
-- Name: seq_asistente; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_asistente', 1, false);


--
-- TOC entry 4399 (class 0 OID 0)
-- Dependencies: 267
-- Name: seq_attach; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_attach', 1, false);


--
-- TOC entry 4400 (class 0 OID 0)
-- Dependencies: 268
-- Name: seq_categoriafaq; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_categoriafaq', 1, false);


--
-- TOC entry 4401 (class 0 OID 0)
-- Dependencies: 269
-- Name: seq_chat; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_chat', 1, false);


--
-- TOC entry 4402 (class 0 OID 0)
-- Dependencies: 270
-- Name: seq_ciudad; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_ciudad', 11, true);


--
-- TOC entry 4403 (class 0 OID 0)
-- Dependencies: 271
-- Name: seq_comentsagenda; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_comentsagenda', 1, false);


--
-- TOC entry 4404 (class 0 OID 0)
-- Dependencies: 272
-- Name: seq_comentsreunion; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_comentsreunion', 1, false);


--
-- TOC entry 4405 (class 0 OID 0)
-- Dependencies: 273
-- Name: seq_compromiso; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_compromiso', 1, false);


--
-- TOC entry 4406 (class 0 OID 0)
-- Dependencies: 274
-- Name: seq_contacto; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_contacto', 1, false);


--
-- TOC entry 4407 (class 0 OID 0)
-- Dependencies: 275
-- Name: seq_correo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_correo', 1, false);


--
-- TOC entry 4408 (class 0 OID 0)
-- Dependencies: 276
-- Name: seq_depto; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_depto', 1, false);


--
-- TOC entry 4409 (class 0 OID 0)
-- Dependencies: 277
-- Name: seq_edoacuerdo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_edoacuerdo', 1, false);


--
-- TOC entry 4410 (class 0 OID 0)
-- Dependencies: 278
-- Name: seq_elemento; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_elemento', 1, false);


--
-- TOC entry 4411 (class 0 OID 0)
-- Dependencies: 279
-- Name: seq_estadorep; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_estadorep', 4, true);


--
-- TOC entry 4412 (class 0 OID 0)
-- Dependencies: 280
-- Name: seq_faq; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_faq', 1, false);


--
-- TOC entry 4413 (class 0 OID 0)
-- Dependencies: 281
-- Name: seq_grupo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_grupo', 1, false);


--
-- TOC entry 4414 (class 0 OID 0)
-- Dependencies: 282
-- Name: seq_invitado; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_invitado', 1, false);


--
-- TOC entry 4415 (class 0 OID 0)
-- Dependencies: 283
-- Name: seq_lugar; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_lugar', 4, true);


--
-- TOC entry 4416 (class 0 OID 0)
-- Dependencies: 284
-- Name: seq_mensaje; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_mensaje', 1, false);


--
-- TOC entry 4417 (class 0 OID 0)
-- Dependencies: 285
-- Name: seq_nota; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_nota', 1, false);


--
-- TOC entry 4418 (class 0 OID 0)
-- Dependencies: 286
-- Name: seq_plantillacorreo; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_plantillacorreo', 1, false);


--
-- TOC entry 4419 (class 0 OID 0)
-- Dependencies: 287
-- Name: seq_prioridad; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_prioridad', 1, false);


--
-- TOC entry 4420 (class 0 OID 0)
-- Dependencies: 288
-- Name: seq_resp; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_resp', 1, false);


--
-- TOC entry 4421 (class 0 OID 0)
-- Dependencies: 289
-- Name: seq_respuesta; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_respuesta', 1, false);


--
-- TOC entry 4422 (class 0 OID 0)
-- Dependencies: 290
-- Name: seq_respuestas_participantes; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_respuestas_participantes', 1, false);


--
-- TOC entry 4423 (class 0 OID 0)
-- Dependencies: 291
-- Name: seq_reunion; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_reunion', 1, false);


--
-- TOC entry 4424 (class 0 OID 0)
-- Dependencies: 292
-- Name: seq_servicio; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_servicio', 1, false);


--
-- TOC entry 4425 (class 0 OID 0)
-- Dependencies: 293
-- Name: seq_solicitud; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_solicitud', 1, false);


--
-- TOC entry 4426 (class 0 OID 0)
-- Dependencies: 294
-- Name: seq_ticket; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_ticket', 1, false);


--
-- TOC entry 4427 (class 0 OID 0)
-- Dependencies: 295
-- Name: seq_topico; Type: SEQUENCE SET; Schema: sgrt; Owner: postgres
--

SELECT pg_catalog.setval('sgrt.seq_topico', 1, false);


--
-- TOC entry 4428 (class 0 OID 0)
-- Dependencies: 327
-- Name: seq_aceptaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_aceptaciones', 1, false);


--
-- TOC entry 4429 (class 0 OID 0)
-- Dependencies: 328
-- Name: seq_asignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_asignaciones', 1, false);


--
-- TOC entry 4430 (class 0 OID 0)
-- Dependencies: 329
-- Name: seq_candidatos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_candidatos', 1, false);


--
-- TOC entry 4431 (class 0 OID 0)
-- Dependencies: 330
-- Name: seq_cartaasignaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_cartaasignaciones', 1, false);


--
-- TOC entry 4432 (class 0 OID 0)
-- Dependencies: 331
-- Name: seq_cotizaciones; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_cotizaciones', 1, false);


--
-- TOC entry 4433 (class 0 OID 0)
-- Dependencies: 332
-- Name: seq_cursos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_cursos', 1, false);


--
-- TOC entry 4434 (class 0 OID 0)
-- Dependencies: 333
-- Name: seq_entrevistas; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_entrevistas', 1, false);


--
-- TOC entry 4435 (class 0 OID 0)
-- Dependencies: 334
-- Name: seq_envios; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_envios', 1, false);


--
-- TOC entry 4436 (class 0 OID 0)
-- Dependencies: 335
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_escolaridad', 1, false);


--
-- TOC entry 4437 (class 0 OID 0)
-- Dependencies: 336
-- Name: seq_experiencias; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_experiencias', 1, false);


--
-- TOC entry 4438 (class 0 OID 0)
-- Dependencies: 337
-- Name: seq_firmas; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_firmas', 1, false);


--
-- TOC entry 4439 (class 0 OID 0)
-- Dependencies: 338
-- Name: seq_habilidades; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_habilidades', 1, false);


--
-- TOC entry 4440 (class 0 OID 0)
-- Dependencies: 339
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_idiomas', 1, false);


--
-- TOC entry 4441 (class 0 OID 0)
-- Dependencies: 340
-- Name: seq_ordenservicios; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_ordenservicios', 1, false);


--
-- TOC entry 4442 (class 0 OID 0)
-- Dependencies: 341
-- Name: seq_prospectos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_prospectos', 1, false);


--
-- TOC entry 4443 (class 0 OID 0)
-- Dependencies: 342
-- Name: seq_proyectos; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_proyectos', 1, false);


--
-- TOC entry 4444 (class 0 OID 0)
-- Dependencies: 343
-- Name: seq_vacantes; Type: SEQUENCE SET; Schema: sisat; Owner: postgres
--

SELECT pg_catalog.setval('sisat.seq_vacantes', 1, false);


--
-- TOC entry 3542 (class 2606 OID 17204)
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- TOC entry 3546 (class 2606 OID 17206)
-- Name: tsgcotipousuario cod_archivo; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);


--
-- TOC entry 3550 (class 2606 OID 17208)
-- Name: tsgcousuarios cod_asistente; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);


--
-- TOC entry 3544 (class 2606 OID 17210)
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);


--
-- TOC entry 3548 (class 2606 OID 17212)
-- Name: tsgcotipousuario tsgcotipousuario_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT tsgcotipousuario_pkey PRIMARY KEY (cod_tipousuario);


--
-- TOC entry 3552 (class 2606 OID 17214)
-- Name: tsgcousuarios tsgcousuarios_pkey; Type: CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT tsgcousuarios_pkey PRIMARY KEY (cod_usuario);


--
-- TOC entry 3562 (class 2606 OID 17216)
-- Name: tsgrhcatrespuestas catrespuestas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcatrespuestas
    ADD CONSTRAINT catrespuestas_pkey PRIMARY KEY (cod_catrespuesta);


--
-- TOC entry 3790 (class 2606 OID 18556)
-- Name: tsgrhmodo cod_capacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_capacitacion_pk PRIMARY KEY (cod_modo);


--
-- TOC entry 3800 (class 2606 OID 18667)
-- Name: tsgrhestatuscapacitacion cod_estatus_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_estatus_pk PRIMARY KEY (cod_estatus);


--
-- TOC entry 3798 (class 2606 OID 18651)
-- Name: tsgrhlugares cod_lugar_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_lugar_pk PRIMARY KEY (cod_lugar);


--
-- TOC entry 3794 (class 2606 OID 18619)
-- Name: tsgrhprocesos cod_procesos_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_procesos_pk PRIMARY KEY (cod_proceso);


--
-- TOC entry 3796 (class 2606 OID 18635)
-- Name: tsgrhproveedores cod_proveedor_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_proveedor_pk PRIMARY KEY (cod_proveedor);


--
-- TOC entry 3810 (class 2606 OID 18876)
-- Name: tsgrhrelacionroles cod_relacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_relacion_pk PRIMARY KEY (cod_plancapacitacion, cod_rolempleado);


--
-- TOC entry 3792 (class 2606 OID 18603)
-- Name: tsgrhrolempleado cod_rolempleado_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_rolempleado_pk PRIMARY KEY (cod_rolempleado);


--
-- TOC entry 3802 (class 2606 OID 18716)
-- Name: tsgrhtipocapacitacion cod_tipocapacitacion_pk; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_tipocapacitacion_pk PRIMARY KEY (cod_tipocapacitacion);


--
-- TOC entry 3556 (class 2606 OID 17218)
-- Name: tsgrhasignacionesemp pk_cod_asignacion_asignacionesempleados; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT pk_cod_asignacion_asignacionesempleados PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3812 (class 2606 OID 18905)
-- Name: tsgrhevacapacitacion pk_cod_evaluacioncap_evacapacitacionesemp; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT pk_cod_evaluacioncap_evacapacitacionesemp PRIMARY KEY (cod_evacapacitacion);


--
-- TOC entry 3554 (class 2606 OID 17222)
-- Name: tsgrhareas tsgrhareas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT tsgrhareas_pkey PRIMARY KEY (cod_area);


--
-- TOC entry 3558 (class 2606 OID 17224)
-- Name: tsgrhcapacitaciones tsgrhcapacitaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT tsgrhcapacitaciones_pkey PRIMARY KEY (cod_capacitacion);


--
-- TOC entry 3560 (class 2606 OID 17226)
-- Name: tsgrhcartaasignacion tsgrhcartaasignacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT tsgrhcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3564 (class 2606 OID 17228)
-- Name: tsgrhclientes tsgrhclientes_des_correocte_key; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_des_correocte_key UNIQUE (des_correocte);


--
-- TOC entry 3566 (class 2606 OID 17230)
-- Name: tsgrhclientes tsgrhclientes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_pkey PRIMARY KEY (cod_cliente);


--
-- TOC entry 3568 (class 2606 OID 17232)
-- Name: tsgrhcontrataciones tsgrhcontrataciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT tsgrhcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- TOC entry 3570 (class 2606 OID 17234)
-- Name: tsgrhcontratos tsgrhcontratos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT tsgrhcontratos_pkey PRIMARY KEY (cod_contrato);


--
-- TOC entry 3572 (class 2606 OID 17236)
-- Name: tsgrhempleados tsgrhempleados_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT tsgrhempleados_pkey PRIMARY KEY (cod_empleado);


--
-- TOC entry 3576 (class 2606 OID 17238)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_pkey PRIMARY KEY (cod_participantenc);


--
-- TOC entry 3574 (class 2606 OID 17240)
-- Name: tsgrhencuesta tsgrhencuesta_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT tsgrhencuesta_pkey PRIMARY KEY (cod_encuesta);


--
-- TOC entry 3578 (class 2606 OID 17242)
-- Name: tsgrhescolaridad tsgrhescolaridad_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT tsgrhescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 3580 (class 2606 OID 17244)
-- Name: tsgrhevacontestadas tsgrhevacontestadas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT tsgrhevacontestadas_pkey PRIMARY KEY (cod_evacontestada);


--
-- TOC entry 3582 (class 2606 OID 17246)
-- Name: tsgrhevaluaciones tsgrhevaluaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT tsgrhevaluaciones_pkey PRIMARY KEY (cod_evaluacion);


--
-- TOC entry 3584 (class 2606 OID 17248)
-- Name: tsgrhexperienciaslaborales tsgrhexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT tsgrhexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 3586 (class 2606 OID 17250)
-- Name: tsgrhfactoreseva tsgrhfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhfactoreseva
    ADD CONSTRAINT tsgrhfactoreseva_pkey PRIMARY KEY (cod_factor);


--
-- TOC entry 3588 (class 2606 OID 17252)
-- Name: tsgrhidiomas tsgrhidiomas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhidiomas
    ADD CONSTRAINT tsgrhidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 3590 (class 2606 OID 17254)
-- Name: tsgrhperfiles tsgrhperfiles_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhperfiles
    ADD CONSTRAINT tsgrhperfiles_pkey PRIMARY KEY (cod_perfil);


--
-- TOC entry 3804 (class 2606 OID 18803)
-- Name: tsgrhplancapacitacion tsgrhplancapacitacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT tsgrhplancapacitacion_pkey PRIMARY KEY (cod_plancapacitacion);


--
-- TOC entry 3592 (class 2606 OID 17260)
-- Name: tsgrhplanoperativo tsgrhplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT tsgrhplanoperativo_pkey PRIMARY KEY (cod_planoperativo);


--
-- TOC entry 3594 (class 2606 OID 17262)
-- Name: tsgrhpreguntasenc tsgrhpreguntasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT tsgrhpreguntasenc_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 3596 (class 2606 OID 17264)
-- Name: tsgrhpreguntaseva tsgrhpreguntaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT tsgrhpreguntaseva_pkey PRIMARY KEY (cod_pregunta);


--
-- TOC entry 3598 (class 2606 OID 17268)
-- Name: tsgrhpuestos tsgrhpuestos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT tsgrhpuestos_pkey PRIMARY KEY (cod_puesto);


--
-- TOC entry 3806 (class 2606 OID 18856)
-- Name: tsgrhlogistica tsgrhreglogistica_cod_capacitacion_key; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_cod_capacitacion_key UNIQUE (cod_capacitacion);


--
-- TOC entry 3808 (class 2606 OID 18854)
-- Name: tsgrhlogistica tsgrhreglogistica_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT tsgrhreglogistica_pkey PRIMARY KEY (cod_logistica);


--
-- TOC entry 3600 (class 2606 OID 17274)
-- Name: tsgrhrespuestasenc tsgrhrespuestasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT tsgrhrespuestasenc_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3602 (class 2606 OID 17276)
-- Name: tsgrhrespuestaseva tsgrhrespuestaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT tsgrhrespuestaseva_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3604 (class 2606 OID 17278)
-- Name: tsgrhrevplanoperativo tsgrhrevplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT tsgrhrevplanoperativo_pkey PRIMARY KEY (cod_revplanoperativo);


--
-- TOC entry 3606 (class 2606 OID 17280)
-- Name: tsgrhsubfactoreseva tsgrhsubfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT tsgrhsubfactoreseva_pkey PRIMARY KEY (cod_subfactor);


--
-- TOC entry 3608 (class 2606 OID 17284)
-- Name: tsgrhvalidaevaluaciondes tsgrhvalidaevaluaciondes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT tsgrhvalidaevaluaciondes_pkey PRIMARY KEY (cod_validacion);


--
-- TOC entry 3610 (class 2606 OID 17286)
-- Name: tsgrtagenda cod_agenda; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT cod_agenda UNIQUE (cod_agenda);


--
-- TOC entry 3614 (class 2606 OID 17288)
-- Name: tsgrtarchivos cod_archivo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_archivo UNIQUE (cod_archivo);


--
-- TOC entry 3620 (class 2606 OID 17290)
-- Name: tsgrtasistentes cod_asistente; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT cod_asistente UNIQUE (cod_asistente);


--
-- TOC entry 3624 (class 2606 OID 17292)
-- Name: tsgrtattchticket cod_attach; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_attach UNIQUE (cod_attach);


--
-- TOC entry 3685 (class 2606 OID 17294)
-- Name: tsgrtfaq cod_categofaq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_categofaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3636 (class 2606 OID 17296)
-- Name: tsgrtcategoriafaq cod_categoriafaq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT cod_categoriafaq UNIQUE (cod_categoriafaq);


--
-- TOC entry 3644 (class 2606 OID 17298)
-- Name: tsgrtcomentariosagenda cod_comentsagenda; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT cod_comentsagenda UNIQUE (cod_comentsagenda);


--
-- TOC entry 3657 (class 2606 OID 17300)
-- Name: tsgrtcorreo cod_correo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT cod_correo UNIQUE (cod_correo);


--
-- TOC entry 3663 (class 2606 OID 17302)
-- Name: tsgrtdatossolicitud cod_datosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT cod_datosolicitud UNIQUE (cod_datosolicitud);


--
-- TOC entry 3667 (class 2606 OID 17304)
-- Name: tsgrtdepartamento cod_depto; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT cod_depto UNIQUE (cod_depto);


--
-- TOC entry 3744 (class 2606 OID 17306)
-- Name: tsgrtticket cod_deptoticket; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_deptoticket UNIQUE (cod_depto);


--
-- TOC entry 3673 (class 2606 OID 17308)
-- Name: tsgrtedosolicitudes cod_edosolicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT cod_edosolicitud UNIQUE (cod_edosolicitud);


--
-- TOC entry 3677 (class 2606 OID 17310)
-- Name: tsgrtelementos cod_elemento; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT cod_elemento UNIQUE (cod_elemento);


--
-- TOC entry 3746 (class 2606 OID 17312)
-- Name: tsgrtticket cod_empleado; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_empleado UNIQUE (cod_empleado);


--
-- TOC entry 3681 (class 2606 OID 17314)
-- Name: tsgrtestados cod_estadorep; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT cod_estadorep UNIQUE (cod_estadorep);


--
-- TOC entry 3687 (class 2606 OID 17316)
-- Name: tsgrtfaq cod_faq; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT cod_faq UNIQUE (cod_faq);


--
-- TOC entry 3691 (class 2606 OID 17318)
-- Name: tsgrtgrupo cod_grupo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT cod_grupo UNIQUE (cod_grupo);


--
-- TOC entry 3695 (class 2606 OID 17320)
-- Name: tsgrtinvitados cod_invitado; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT cod_invitado UNIQUE (cod_invitado);


--
-- TOC entry 3700 (class 2606 OID 17322)
-- Name: tsgrtlugares cod_lugar; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT cod_lugar UNIQUE (cod_lugar);


--
-- TOC entry 3704 (class 2606 OID 17324)
-- Name: tsgrtmsjticket cod_mensaje; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT cod_mensaje UNIQUE (cod_mensaje);


--
-- TOC entry 3708 (class 2606 OID 17326)
-- Name: tsgrtnota cod_nota; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT cod_nota UNIQUE (cod_nota);


--
-- TOC entry 3712 (class 2606 OID 17328)
-- Name: tsgrtplantillacorreos cod_plantillacorreo; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT cod_plantillacorreo UNIQUE (cod_plantillacorreo);


--
-- TOC entry 3716 (class 2606 OID 17330)
-- Name: tsgrtprioridad cod_prioridad; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT cod_prioridad UNIQUE (cod_prioridad);


--
-- TOC entry 3728 (class 2606 OID 17332)
-- Name: tsgrtrespuesta cod_respuesta; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT cod_respuesta UNIQUE (cod_respuesta);


--
-- TOC entry 3722 (class 2606 OID 17334)
-- Name: tsgrtresppredefinida cod_respuestapredf; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT cod_respuestapredf UNIQUE (cod_respuesta);


--
-- TOC entry 3616 (class 2606 OID 17336)
-- Name: tsgrtarchivos cod_reunionarchivos; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT cod_reunionarchivos UNIQUE (cod_reunion);


--
-- TOC entry 3732 (class 2606 OID 17338)
-- Name: tsgrtreuniones cod_reuniones; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT cod_reuniones UNIQUE (cod_reunion);


--
-- TOC entry 3736 (class 2606 OID 17340)
-- Name: tsgrtservicios cod_servicio; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT cod_servicio UNIQUE (cod_servicio);


--
-- TOC entry 3740 (class 2606 OID 17342)
-- Name: tsgrtsolicitudservicios cod_solicitud; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT cod_solicitud UNIQUE (cod_solicitud);


--
-- TOC entry 3748 (class 2606 OID 17344)
-- Name: tsgrtticket cod_ticket; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT cod_ticket UNIQUE (cod_ticket);


--
-- TOC entry 3626 (class 2606 OID 17346)
-- Name: tsgrtattchticket cod_ticketattach; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT cod_ticketattach UNIQUE (cod_ticket);


--
-- TOC entry 3630 (class 2606 OID 17348)
-- Name: tsgrtayudatopico cod_topico; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT cod_topico UNIQUE (cod_topico);


--
-- TOC entry 3612 (class 2606 OID 17350)
-- Name: tsgrtagenda tsgrtagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT tsgrtagenda_pkey PRIMARY KEY (cod_agenda);


--
-- TOC entry 3618 (class 2606 OID 17352)
-- Name: tsgrtarchivos tsgrtarchivos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT tsgrtarchivos_pkey PRIMARY KEY (cod_archivo);


--
-- TOC entry 3622 (class 2606 OID 17354)
-- Name: tsgrtasistentes tsgrtasistentes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT tsgrtasistentes_pkey PRIMARY KEY (cod_asistente);


--
-- TOC entry 3628 (class 2606 OID 17356)
-- Name: tsgrtattchticket tsgrtattchticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT tsgrtattchticket_pkey PRIMARY KEY (cod_attach);


--
-- TOC entry 3632 (class 2606 OID 17358)
-- Name: tsgrtayudatopico tsgrtayudatopico_des_topico_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_des_topico_key UNIQUE (des_topico);


--
-- TOC entry 3634 (class 2606 OID 17360)
-- Name: tsgrtayudatopico tsgrtayudatopico_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT tsgrtayudatopico_pkey PRIMARY KEY (cod_topico);


--
-- TOC entry 3638 (class 2606 OID 17362)
-- Name: tsgrtcategoriafaq tsgrtcategoriafaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT tsgrtcategoriafaq_pkey PRIMARY KEY (cod_categoriafaq);


--
-- TOC entry 3640 (class 2606 OID 17364)
-- Name: tsgrtchat tsgrtchat_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtchat
    ADD CONSTRAINT tsgrtchat_pkey PRIMARY KEY (cod_chat);


--
-- TOC entry 3642 (class 2606 OID 17366)
-- Name: tsgrtciudades tsgrtciudades_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT tsgrtciudades_pkey PRIMARY KEY (cod_ciudad);


--
-- TOC entry 3647 (class 2606 OID 17368)
-- Name: tsgrtcomentariosagenda tsgrtcomentariosagenda_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT tsgrtcomentariosagenda_pkey PRIMARY KEY (cod_comentsagenda);


--
-- TOC entry 3650 (class 2606 OID 17370)
-- Name: tsgrtcomentariosreunion tsgrtcomentariosreunion_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT tsgrtcomentariosreunion_pkey PRIMARY KEY (cod_commentsreunion);


--
-- TOC entry 3653 (class 2606 OID 17372)
-- Name: tsgrtcompromisos tsgrtcompromisos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT tsgrtcompromisos_pkey PRIMARY KEY (cod_compromiso);


--
-- TOC entry 3659 (class 2606 OID 17374)
-- Name: tsgrtcorreo tsgrtcorreo_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3661 (class 2606 OID 17376)
-- Name: tsgrtcorreo tsgrtcorreo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT tsgrtcorreo_pkey PRIMARY KEY (cod_correo);


--
-- TOC entry 3665 (class 2606 OID 17378)
-- Name: tsgrtdatossolicitud tsgrtdatossolicitud_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT tsgrtdatossolicitud_pkey PRIMARY KEY (cod_datosolicitud);


--
-- TOC entry 3669 (class 2606 OID 17380)
-- Name: tsgrtdepartamento tsgrtdepartamento_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3671 (class 2606 OID 17382)
-- Name: tsgrtdepartamento tsgrtdepartamento_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT tsgrtdepartamento_pkey PRIMARY KEY (cod_depto);


--
-- TOC entry 3675 (class 2606 OID 17384)
-- Name: tsgrtedosolicitudes tsgrtedosolicitudes_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtedosolicitudes
    ADD CONSTRAINT tsgrtedosolicitudes_pkey PRIMARY KEY (cod_edosolicitud);


--
-- TOC entry 3679 (class 2606 OID 17386)
-- Name: tsgrtelementos tsgrtelementos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtelementos
    ADD CONSTRAINT tsgrtelementos_pkey PRIMARY KEY (cod_elemento);


--
-- TOC entry 3683 (class 2606 OID 17388)
-- Name: tsgrtestados tsgrtestados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtestados
    ADD CONSTRAINT tsgrtestados_pkey PRIMARY KEY (cod_estadorep);


--
-- TOC entry 3689 (class 2606 OID 17390)
-- Name: tsgrtfaq tsgrtfaq_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT tsgrtfaq_pkey PRIMARY KEY (cod_faq);


--
-- TOC entry 3693 (class 2606 OID 17392)
-- Name: tsgrtgrupo tsgrtgrupo_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT tsgrtgrupo_pkey PRIMARY KEY (cod_grupo);


--
-- TOC entry 3698 (class 2606 OID 17394)
-- Name: tsgrtinvitados tsgrtinvitados_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT tsgrtinvitados_pkey PRIMARY KEY (cod_invitado);


--
-- TOC entry 3702 (class 2606 OID 17396)
-- Name: tsgrtlugares tsgrtlugares_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT tsgrtlugares_pkey PRIMARY KEY (cod_lugar);


--
-- TOC entry 3706 (class 2606 OID 17398)
-- Name: tsgrtmsjticket tsgrtmsjticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT tsgrtmsjticket_pkey PRIMARY KEY (cod_mensaje);


--
-- TOC entry 3710 (class 2606 OID 17400)
-- Name: tsgrtnota tsgrtnota_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT tsgrtnota_pkey PRIMARY KEY (cod_nota);


--
-- TOC entry 3714 (class 2606 OID 17402)
-- Name: tsgrtplantillacorreos tsgrtplantillacorreos_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT tsgrtplantillacorreos_pkey PRIMARY KEY (cod_plantillacorreo);


--
-- TOC entry 3718 (class 2606 OID 17404)
-- Name: tsgrtprioridad tsgrtprioridad_des_nombre_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_des_nombre_key UNIQUE (des_nombre);


--
-- TOC entry 3720 (class 2606 OID 17406)
-- Name: tsgrtprioridad tsgrtprioridad_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT tsgrtprioridad_pkey PRIMARY KEY (cod_prioridad);


--
-- TOC entry 3724 (class 2606 OID 17408)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_des_titulo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_des_titulo_key UNIQUE (des_titulo);


--
-- TOC entry 3726 (class 2606 OID 17410)
-- Name: tsgrtresppredefinida tsgrtresppredefinida_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT tsgrtresppredefinida_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3730 (class 2606 OID 17412)
-- Name: tsgrtrespuesta tsgrtrespuesta_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT tsgrtrespuesta_pkey PRIMARY KEY (cod_respuesta);


--
-- TOC entry 3734 (class 2606 OID 17414)
-- Name: tsgrtreuniones tsgrtreuniones_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT tsgrtreuniones_pkey PRIMARY KEY (cod_reunion);


--
-- TOC entry 3738 (class 2606 OID 17416)
-- Name: tsgrtservicios tsgrtservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtservicios
    ADD CONSTRAINT tsgrtservicios_pkey PRIMARY KEY (cod_servicio);


--
-- TOC entry 3742 (class 2606 OID 17418)
-- Name: tsgrtsolicitudservicios tsgrtsolicitudservicios_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT tsgrtsolicitudservicios_pkey PRIMARY KEY (cod_solicitud);


--
-- TOC entry 3750 (class 2606 OID 17420)
-- Name: tsgrtticket tsgrtticket_des_correo_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_correo_key UNIQUE (des_correo);


--
-- TOC entry 3752 (class 2606 OID 17422)
-- Name: tsgrtticket tsgrtticket_des_folio_key; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_des_folio_key UNIQUE (des_folio);


--
-- TOC entry 3754 (class 2606 OID 17424)
-- Name: tsgrtticket tsgrtticket_pkey; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT tsgrtticket_pkey PRIMARY KEY (cod_ticket);


--
-- TOC entry 3655 (class 2606 OID 17426)
-- Name: tsgrtcompromisos unique_cod_chat; Type: CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT unique_cod_chat UNIQUE (cod_chat);


--
-- TOC entry 3756 (class 2606 OID 17428)
-- Name: tsisatasignaciones tsisatasignaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT tsisatasignaciones_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3758 (class 2606 OID 17430)
-- Name: tsisatcandidatos tsisatcandidatos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT tsisatcandidatos_pkey PRIMARY KEY (cod_candidato);


--
-- TOC entry 3760 (class 2606 OID 17432)
-- Name: tsisatcartaaceptacion tsisatcartaaceptacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT tsisatcartaaceptacion_pkey PRIMARY KEY (cod_aceptacion);


--
-- TOC entry 3762 (class 2606 OID 17434)
-- Name: tsisatcartaasignacion tsisatcartaasignacion_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT tsisatcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- TOC entry 3764 (class 2606 OID 17436)
-- Name: tsisatcotizaciones tsisatcotizaciones_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT tsisatcotizaciones_pkey PRIMARY KEY (cod_cotizacion);


--
-- TOC entry 3766 (class 2606 OID 17438)
-- Name: tsisatcursosycerticados tsisatcursosycerticados_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcursosycerticados
    ADD CONSTRAINT tsisatcursosycerticados_pkey PRIMARY KEY (cod_curso);


--
-- TOC entry 3768 (class 2606 OID 17440)
-- Name: tsisatentrevistas tsisatentrevistas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT tsisatentrevistas_pkey PRIMARY KEY (cod_entrevista);


--
-- TOC entry 3770 (class 2606 OID 17442)
-- Name: tsisatenviocorreos tsisatenviocorreos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT tsisatenviocorreos_pkey PRIMARY KEY (cod_envio);


--
-- TOC entry 3772 (class 2606 OID 17444)
-- Name: tsisatescolaridad tsisatescolaridad_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT tsisatescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- TOC entry 3774 (class 2606 OID 17446)
-- Name: tsisatexperienciaslaborales tsisatexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT tsisatexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- TOC entry 3776 (class 2606 OID 17448)
-- Name: tsisatfirmas tsisatfirmas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT tsisatfirmas_pkey PRIMARY KEY (cod_firma);


--
-- TOC entry 3778 (class 2606 OID 17450)
-- Name: tsisathabilidades tsisathabilidades_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisathabilidades
    ADD CONSTRAINT tsisathabilidades_pkey PRIMARY KEY (cod_habilidad);


--
-- TOC entry 3780 (class 2606 OID 17452)
-- Name: tsisatidiomas tsisatidiomas_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT tsisatidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- TOC entry 3782 (class 2606 OID 17454)
-- Name: tsisatordenservicio tsisatordenservicio_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT tsisatordenservicio_pkey PRIMARY KEY (cod_ordenservicio);


--
-- TOC entry 3784 (class 2606 OID 17456)
-- Name: tsisatprospectos tsisatprospectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT tsisatprospectos_pkey PRIMARY KEY (cod_prospecto);


--
-- TOC entry 3786 (class 2606 OID 17458)
-- Name: tsisatproyectos tsisatproyectos_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT tsisatproyectos_pkey PRIMARY KEY (cod_proyecto);


--
-- TOC entry 3788 (class 2606 OID 17460)
-- Name: tsisatvacantes tsisatvacantes_pkey; Type: CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT tsisatvacantes_pkey PRIMARY KEY (cod_vacante);


--
-- TOC entry 3651 (class 1259 OID 17461)
-- Name: fki_fk_cod_chat; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_chat ON sgrt.tsgrtcompromisos USING btree (cod_chat);


--
-- TOC entry 3696 (class 1259 OID 17462)
-- Name: fki_fk_cod_empleado; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_empleado ON sgrt.tsgrtinvitados USING btree (cod_empleado);


--
-- TOC entry 3648 (class 1259 OID 17463)
-- Name: fki_fk_cod_invitado; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_invitado ON sgrt.tsgrtcomentariosreunion USING btree (cod_invitado);


--
-- TOC entry 3645 (class 1259 OID 17464)
-- Name: fki_fk_cod_invitados; Type: INDEX; Schema: sgrt; Owner: postgres
--

CREATE INDEX fki_fk_cod_invitados ON sgrt.tsgrtcomentariosagenda USING btree (cod_invitado);


--
-- TOC entry 4045 (class 2620 OID 17465)
-- Name: tsgrhencuesta tg_actualizarfecha; Type: TRIGGER; Schema: sgrh; Owner: postgres
--

CREATE TRIGGER tg_actualizarfecha BEFORE UPDATE ON sgrh.tsgrhencuesta FOR EACH ROW EXECUTE PROCEDURE sgrh.factualizarfecha();


--
-- TOC entry 3813 (class 2606 OID 17466)
-- Name: tsgcousuarios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgco; Owner: postgres
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4034 (class 2606 OID 18857)
-- Name: tsgrhlogistica cod_capacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_capacitacion_fk FOREIGN KEY (cod_capacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4012 (class 2606 OID 18557)
-- Name: tsgrhmodo cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4014 (class 2606 OID 18604)
-- Name: tsgrhrolempleado cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4016 (class 2606 OID 18620)
-- Name: tsgrhprocesos cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4018 (class 2606 OID 18636)
-- Name: tsgrhproveedores cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4020 (class 2606 OID 18652)
-- Name: tsgrhlugares cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4022 (class 2606 OID 18668)
-- Name: tsgrhestatuscapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4024 (class 2606 OID 18717)
-- Name: tsgrhtipocapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4026 (class 2606 OID 18804)
-- Name: tsgrhplancapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4035 (class 2606 OID 18862)
-- Name: tsgrhlogistica cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4037 (class 2606 OID 18877)
-- Name: tsgrhrelacionroles cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4043 (class 2606 OID 18916)
-- Name: tsgrhevacapacitacion cod_creadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_creadopor_fk FOREIGN KEY (aud_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4013 (class 2606 OID 18562)
-- Name: tsgrhmodo cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhmodo
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4015 (class 2606 OID 18609)
-- Name: tsgrhrolempleado cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrolempleado
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4017 (class 2606 OID 18625)
-- Name: tsgrhprocesos cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhprocesos
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4019 (class 2606 OID 18641)
-- Name: tsgrhproveedores cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhproveedores
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4021 (class 2606 OID 18657)
-- Name: tsgrhlugares cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlugares
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4023 (class 2606 OID 18673)
-- Name: tsgrhestatuscapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhestatuscapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4025 (class 2606 OID 18722)
-- Name: tsgrhtipocapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhtipocapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4027 (class 2606 OID 18809)
-- Name: tsgrhplancapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4036 (class 2606 OID 18867)
-- Name: tsgrhlogistica cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhlogistica
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4038 (class 2606 OID 18882)
-- Name: tsgrhrelacionroles cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4044 (class 2606 OID 18921)
-- Name: tsgrhevacapacitacion cod_modificadopor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT cod_modificadopor_fk FOREIGN KEY (aud_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4039 (class 2606 OID 18887)
-- Name: tsgrhrelacionroles cod_plancapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_plancapacitacion_fk FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4040 (class 2606 OID 18892)
-- Name: tsgrhrelacionroles cod_rolempleado_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrelacionroles
    ADD CONSTRAINT cod_rolempleado_fk FOREIGN KEY (cod_rolempleado) REFERENCES sgrh.tsgrhrolempleado(cod_rolempleado);


--
-- TOC entry 3819 (class 2606 OID 17471)
-- Name: tsgrhcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3832 (class 2606 OID 17476)
-- Name: tsgrhempleados fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3856 (class 2606 OID 17481)
-- Name: tsgrhpuestos fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3857 (class 2606 OID 17491)
-- Name: tsgrhrespuestasenc fk_cod_catrespuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_catrespuesta FOREIGN KEY (cod_catrespuesta) REFERENCES sgrh.tsgrhcatrespuestas(cod_catrespuesta);


--
-- TOC entry 3820 (class 2606 OID 17496)
-- Name: tsgrhcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3833 (class 2606 OID 17501)
-- Name: tsgrhempleados fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3830 (class 2606 OID 17506)
-- Name: tsgrhcontratos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3828 (class 2606 OID 17511)
-- Name: tsgrhcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3837 (class 2606 OID 17516)
-- Name: tsgrhencuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3848 (class 2606 OID 17521)
-- Name: tsgrhevaluaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3851 (class 2606 OID 17526)
-- Name: tsgrhplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3861 (class 2606 OID 17531)
-- Name: tsgrhrevplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3821 (class 2606 OID 17536)
-- Name: tsgrhcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3850 (class 2606 OID 17546)
-- Name: tsgrhexperienciaslaborales fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3844 (class 2606 OID 17551)
-- Name: tsgrhescolaridad fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3818 (class 2606 OID 17556)
-- Name: tsgrhcapacitaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3822 (class 2606 OID 17561)
-- Name: tsgrhcartaasignacion fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3853 (class 2606 OID 17566)
-- Name: tsgrhpreguntasenc fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3859 (class 2606 OID 17571)
-- Name: tsgrhrespuestaseva fk_cod_evacontestada; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_evacontestada FOREIGN KEY (cod_evacontestada) REFERENCES sgrh.tsgrhevacontestadas(cod_evacontestada) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3854 (class 2606 OID 17576)
-- Name: tsgrhpreguntaseva fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3845 (class 2606 OID 17581)
-- Name: tsgrhevacontestadas fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3871 (class 2606 OID 17586)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3846 (class 2606 OID 17591)
-- Name: tsgrhevacontestadas fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3872 (class 2606 OID 17596)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3847 (class 2606 OID 17601)
-- Name: tsgrhevacontestadas fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3873 (class 2606 OID 17606)
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3870 (class 2606 OID 17611)
-- Name: tsgrhsubfactoreseva fk_cod_factor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT fk_cod_factor FOREIGN KEY (cod_factor) REFERENCES sgrh.tsgrhfactoreseva(cod_factor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3823 (class 2606 OID 17616)
-- Name: tsgrhcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3838 (class 2606 OID 17621)
-- Name: tsgrhencuesta fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3874 (class 2606 OID 17626)
-- Name: tsgrhvalidaevaluaciondes fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3862 (class 2606 OID 17631)
-- Name: tsgrhrevplanoperativo fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3834 (class 2606 OID 17636)
-- Name: tsgrhempleados fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3831 (class 2606 OID 17641)
-- Name: tsgrhcontratos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3829 (class 2606 OID 17646)
-- Name: tsgrhcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3839 (class 2606 OID 17651)
-- Name: tsgrhencuesta fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3849 (class 2606 OID 17656)
-- Name: tsgrhevaluaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3852 (class 2606 OID 17661)
-- Name: tsgrhplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3863 (class 2606 OID 17666)
-- Name: tsgrhrevplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3824 (class 2606 OID 17671)
-- Name: tsgrhcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3875 (class 2606 OID 17681)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3864 (class 2606 OID 17686)
-- Name: tsgrhrevplanoperativo fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3876 (class 2606 OID 17691)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3865 (class 2606 OID 17696)
-- Name: tsgrhrevplanoperativo fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3877 (class 2606 OID 17701)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3866 (class 2606 OID 17706)
-- Name: tsgrhrevplanoperativo fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3878 (class 2606 OID 17711)
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3867 (class 2606 OID 17716)
-- Name: tsgrhrevplanoperativo fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3868 (class 2606 OID 17721)
-- Name: tsgrhrevplanoperativo fk_cod_participante5; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante5 FOREIGN KEY (cod_participante5) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3825 (class 2606 OID 17726)
-- Name: tsgrhcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3869 (class 2606 OID 17731)
-- Name: tsgrhrevplanoperativo fk_cod_planoperativo; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_planoperativo FOREIGN KEY (cod_planoperativo) REFERENCES sgrh.tsgrhplanoperativo(cod_planoperativo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3858 (class 2606 OID 17736)
-- Name: tsgrhrespuestasenc fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3860 (class 2606 OID 17741)
-- Name: tsgrhrespuestaseva fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntaseva(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3835 (class 2606 OID 17746)
-- Name: tsgrhempleados fk_cod_puesto; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3826 (class 2606 OID 17751)
-- Name: tsgrhcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3827 (class 2606 OID 17756)
-- Name: tsgrhcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3814 (class 2606 OID 17761)
-- Name: tsgrhareas fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3836 (class 2606 OID 17766)
-- Name: tsgrhempleados fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3855 (class 2606 OID 17771)
-- Name: tsgrhpreguntaseva fk_cod_subfactor; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_subfactor FOREIGN KEY (cod_subfactor) REFERENCES sgrh.tsgrhsubfactoreseva(cod_subfactor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3815 (class 2606 OID 17776)
-- Name: tsgrhasignacionesemp fk_codasignadopor_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codasignadopor_asignacionesempleados FOREIGN KEY (cod_asignadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 4041 (class 2606 OID 18906)
-- Name: tsgrhevacapacitacion fk_codempleado_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_codempleado_evacapacitacionesemp FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3816 (class 2606 OID 17786)
-- Name: tsgrhasignacionesemp fk_codprospecto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codprospecto_asignacionesempleados FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3817 (class 2606 OID 17791)
-- Name: tsgrhasignacionesemp fk_codpuesto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codpuesto_asignacionesempleados FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 4042 (class 2606 OID 18911)
-- Name: tsgrhevacapacitacion fk_plancapacitacion_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacion
    ADD CONSTRAINT fk_plancapacitacion_evacapacitacionesemp FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);


--
-- TOC entry 4029 (class 2606 OID 18819)
-- Name: tsgrhplancapacitacion plancap_tipocapacitacion_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancap_tipocapacitacion_fk FOREIGN KEY (cod_tipocapacitacion) REFERENCES sgrh.tsgrhtipocapacitacion(cod_tipocapacitacion);


--
-- TOC entry 4033 (class 2606 OID 18839)
-- Name: tsgrhplancapacitacion plancapacitacion_estatus_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_estatus_fk FOREIGN KEY (cod_estatus) REFERENCES sgrh.tsgrhestatuscapacitacion(cod_estatus);


--
-- TOC entry 4032 (class 2606 OID 18834)
-- Name: tsgrhplancapacitacion plancapacitacion_lugar_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_lugar_fk FOREIGN KEY (cod_lugar) REFERENCES sgrh.tsgrhlugares(cod_lugar);


--
-- TOC entry 4028 (class 2606 OID 18814)
-- Name: tsgrhplancapacitacion plancapacitacion_modo_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_modo_fk FOREIGN KEY (cod_modo) REFERENCES sgrh.tsgrhmodo(cod_modo);


--
-- TOC entry 4030 (class 2606 OID 18824)
-- Name: tsgrhplancapacitacion plancapacitacion_proceso_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proceso_fk FOREIGN KEY (cod_proceso) REFERENCES sgrh.tsgrhprocesos(cod_proceso);


--
-- TOC entry 4031 (class 2606 OID 18829)
-- Name: tsgrhplancapacitacion plancapacitacion_proveedor_fk; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhplancapacitacion
    ADD CONSTRAINT plancapacitacion_proveedor_fk FOREIGN KEY (cod_proveedor) REFERENCES sgrh.tsgrhproveedores(cod_proveedor);


--
-- TOC entry 3840 (class 2606 OID 17821)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_empleado_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_empleado_fkey FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3841 (class 2606 OID 17826)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_encuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_encuesta_fkey FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta);


--
-- TOC entry 3842 (class 2606 OID 17831)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_pregunta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_pregunta_fkey FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta);


--
-- TOC entry 3843 (class 2606 OID 17836)
-- Name: tsgrhencuesta_participantes tsgrhencuesta_participantes_cod_respuesta_fkey; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhencuesta_participantes
    ADD CONSTRAINT tsgrhencuesta_participantes_cod_respuesta_fkey FOREIGN KEY (cod_respuesta) REFERENCES sgrh.tsgrhrespuestasenc(cod_respuesta);


--
-- TOC entry 3894 (class 2606 OID 17841)
-- Name: tsgrtcomentariosagenda fk_cod_agenda; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_agenda FOREIGN KEY (cod_agenda) REFERENCES sgrt.tsgrtagenda(cod_agenda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3913 (class 2606 OID 17846)
-- Name: tsgrtfaq fk_cod_categoria; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_categoria FOREIGN KEY (cod_categoriafaq) REFERENCES sgrt.tsgrtcategoriafaq(cod_categoriafaq) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3898 (class 2606 OID 17851)
-- Name: tsgrtcompromisos fk_cod_chat; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_chat FOREIGN KEY (cod_chat) REFERENCES sgrt.tsgrtchat(cod_chat);


--
-- TOC entry 3921 (class 2606 OID 17856)
-- Name: tsgrtlugares fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtlugares
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3908 (class 2606 OID 17861)
-- Name: tsgrtdepartamento fk_cod_correo; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_correo FOREIGN KEY (cod_correo) REFERENCES sgrt.tsgrtcorreo(cod_correo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3892 (class 2606 OID 17866)
-- Name: tsgrtcategoriafaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcategoriafaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3900 (class 2606 OID 17871)
-- Name: tsgrtcorreo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3909 (class 2606 OID 17876)
-- Name: tsgrtdepartamento fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3914 (class 2606 OID 17881)
-- Name: tsgrtfaq fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3916 (class 2606 OID 17886)
-- Name: tsgrtgrupo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3887 (class 2606 OID 17891)
-- Name: tsgrtayudatopico fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3926 (class 2606 OID 17896)
-- Name: tsgrtnota fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3931 (class 2606 OID 17901)
-- Name: tsgrtplantillacorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3934 (class 2606 OID 17906)
-- Name: tsgrtprioridad fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3941 (class 2606 OID 17911)
-- Name: tsgrtrespuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3937 (class 2606 OID 17916)
-- Name: tsgrtresppredefinida fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3954 (class 2606 OID 17921)
-- Name: tsgrtticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3883 (class 2606 OID 17926)
-- Name: tsgrtattchticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3922 (class 2606 OID 17931)
-- Name: tsgrtmsjticket fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3947 (class 2606 OID 17936)
-- Name: tsgrtreuniones fk_cod_creadorreunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_creadorreunion FOREIGN KEY (cod_creadorreunion) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3901 (class 2606 OID 17941)
-- Name: tsgrtcorreo fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3888 (class 2606 OID 17946)
-- Name: tsgrtayudatopico fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3938 (class 2606 OID 17951)
-- Name: tsgrtresppredefinida fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3955 (class 2606 OID 17956)
-- Name: tsgrtticket fk_cod_depto; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_depto FOREIGN KEY (cod_depto) REFERENCES sgrt.tsgrtdepartamento(cod_depto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3905 (class 2606 OID 17961)
-- Name: tsgrtdatossolicitud fk_cod_edosolicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_edosolicitud FOREIGN KEY (cod_edosolicitud) REFERENCES sgrt.tsgrtedosolicitudes(cod_edosolicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3906 (class 2606 OID 17966)
-- Name: tsgrtdatossolicitud fk_cod_elemento; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_elemento FOREIGN KEY (cod_elemento) REFERENCES sgrt.tsgrtelementos(cod_elemento) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3881 (class 2606 OID 17971)
-- Name: tsgrtasistentes fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3927 (class 2606 OID 17976)
-- Name: tsgrtnota fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3942 (class 2606 OID 17981)
-- Name: tsgrtrespuesta fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3951 (class 2606 OID 17986)
-- Name: tsgrtsolicitudservicios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3956 (class 2606 OID 17991)
-- Name: tsgrtticket fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3919 (class 2606 OID 17996)
-- Name: tsgrtinvitados fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3882 (class 2606 OID 18001)
-- Name: tsgrtasistentes fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtasistentes
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3896 (class 2606 OID 18006)
-- Name: tsgrtcomentariosreunion fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3895 (class 2606 OID 18011)
-- Name: tsgrtcomentariosagenda fk_cod_invitado; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosagenda
    ADD CONSTRAINT fk_cod_invitado FOREIGN KEY (cod_invitado) REFERENCES sgrt.tsgrtinvitados(cod_invitado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3948 (class 2606 OID 18016)
-- Name: tsgrtreuniones fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3943 (class 2606 OID 18021)
-- Name: tsgrtrespuesta fk_cod_mensaje; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_mensaje FOREIGN KEY (cod_mensaje) REFERENCES sgrt.tsgrtmsjticket(cod_mensaje) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3910 (class 2606 OID 18026)
-- Name: tsgrtdepartamento fk_cod_plantillacorreo; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_plantillacorreo FOREIGN KEY (cod_plantillacorreo) REFERENCES sgrt.tsgrtplantillacorreos(cod_plantillacorreo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3889 (class 2606 OID 18031)
-- Name: tsgrtayudatopico fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3957 (class 2606 OID 18036)
-- Name: tsgrtticket fk_cod_prioridad; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_prioridad FOREIGN KEY (cod_prioridad) REFERENCES sgrt.tsgrtprioridad(cod_prioridad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3949 (class 2606 OID 18041)
-- Name: tsgrtreuniones fk_cod_responsable; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_responsable FOREIGN KEY (cod_responsable) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3899 (class 2606 OID 18046)
-- Name: tsgrtcompromisos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcompromisos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3880 (class 2606 OID 18051)
-- Name: tsgrtarchivos fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtarchivos
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3920 (class 2606 OID 18056)
-- Name: tsgrtinvitados fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtinvitados
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3897 (class 2606 OID 18061)
-- Name: tsgrtcomentariosreunion fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcomentariosreunion
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3879 (class 2606 OID 18066)
-- Name: tsgrtagenda fk_cod_reunion; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtagenda
    ADD CONSTRAINT fk_cod_reunion FOREIGN KEY (cod_reunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3950 (class 2606 OID 18071)
-- Name: tsgrtreuniones fk_cod_reunionanterior; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtreuniones
    ADD CONSTRAINT fk_cod_reunionanterior FOREIGN KEY (cod_proximareunion) REFERENCES sgrt.tsgrtreuniones(cod_reunion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3952 (class 2606 OID 18076)
-- Name: tsgrtsolicitudservicios fk_cod_servicio; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_servicio FOREIGN KEY (cod_servicio) REFERENCES sgrt.tsgrtservicios(cod_servicio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3902 (class 2606 OID 18081)
-- Name: tsgrtcorreo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3911 (class 2606 OID 18086)
-- Name: tsgrtdepartamento fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3917 (class 2606 OID 18091)
-- Name: tsgrtgrupo fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3890 (class 2606 OID 18096)
-- Name: tsgrtayudatopico fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3928 (class 2606 OID 18101)
-- Name: tsgrtnota fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3932 (class 2606 OID 18106)
-- Name: tsgrtplantillacorreos fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3935 (class 2606 OID 18111)
-- Name: tsgrtprioridad fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3944 (class 2606 OID 18116)
-- Name: tsgrtrespuesta fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3939 (class 2606 OID 18121)
-- Name: tsgrtresppredefinida fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3958 (class 2606 OID 18126)
-- Name: tsgrtticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3884 (class 2606 OID 18131)
-- Name: tsgrtattchticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3923 (class 2606 OID 18136)
-- Name: tsgrtmsjticket fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3907 (class 2606 OID 18141)
-- Name: tsgrtdatossolicitud fk_cod_solicitud; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdatossolicitud
    ADD CONSTRAINT fk_cod_solicitud FOREIGN KEY (cod_solicitud) REFERENCES sgrt.tsgrtsolicitudservicios(cod_solicitud) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3929 (class 2606 OID 18146)
-- Name: tsgrtnota fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3945 (class 2606 OID 18151)
-- Name: tsgrtrespuesta fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3953 (class 2606 OID 18156)
-- Name: tsgrtsolicitudservicios fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtsolicitudservicios
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3885 (class 2606 OID 18161)
-- Name: tsgrtattchticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3924 (class 2606 OID 18166)
-- Name: tsgrtmsjticket fk_cod_ticket; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ticket FOREIGN KEY (cod_ticket) REFERENCES sgrt.tsgrtticket(cod_ticket) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3959 (class 2606 OID 18171)
-- Name: tsgrtticket fk_cod_topico; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_topico FOREIGN KEY (cod_topico) REFERENCES sgrt.tsgrtayudatopico(cod_topico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3903 (class 2606 OID 18176)
-- Name: tsgrtcorreo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3912 (class 2606 OID 18181)
-- Name: tsgrtdepartamento fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtdepartamento
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3915 (class 2606 OID 18186)
-- Name: tsgrtfaq fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtfaq
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3918 (class 2606 OID 18191)
-- Name: tsgrtgrupo fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtgrupo
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3891 (class 2606 OID 18196)
-- Name: tsgrtayudatopico fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtayudatopico
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3930 (class 2606 OID 18201)
-- Name: tsgrtnota fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtnota
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3933 (class 2606 OID 18206)
-- Name: tsgrtplantillacorreos fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtplantillacorreos
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3936 (class 2606 OID 18211)
-- Name: tsgrtprioridad fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtprioridad
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3946 (class 2606 OID 18216)
-- Name: tsgrtrespuesta fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtrespuesta
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3940 (class 2606 OID 18221)
-- Name: tsgrtresppredefinida fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtresppredefinida
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3960 (class 2606 OID 18226)
-- Name: tsgrtticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3886 (class 2606 OID 18231)
-- Name: tsgrtattchticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtattchticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3925 (class 2606 OID 18236)
-- Name: tsgrtmsjticket fk_cod_ultactualizacionpor; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtmsjticket
    ADD CONSTRAINT fk_cod_ultactualizacionpor FOREIGN KEY (cod_ultactualizacionpor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3904 (class 2606 OID 18241)
-- Name: tsgrtcorreo fk_cod_usuario; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtcorreo
    ADD CONSTRAINT fk_cod_usuario FOREIGN KEY (cod_usuario) REFERENCES sgco.tsgcousuarios(cod_usuario) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3893 (class 2606 OID 18246)
-- Name: tsgrtciudades fk_estadorep; Type: FK CONSTRAINT; Schema: sgrt; Owner: postgres
--

ALTER TABLE ONLY sgrt.tsgrtciudades
    ADD CONSTRAINT fk_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4003 (class 2606 OID 18251)
-- Name: tsisatprospectos fk_cod_administrador; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_administrador FOREIGN KEY (cod_administrador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3969 (class 2606 OID 18256)
-- Name: tsisatcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3988 (class 2606 OID 18261)
-- Name: tsisatfirmas fk_cod_autoriza; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_autoriza FOREIGN KEY (cod_autoriza) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3996 (class 2606 OID 18266)
-- Name: tsisatordenservicio fk_cod_ciudad; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_ciudad FOREIGN KEY (cod_ciudad) REFERENCES sgrt.tsgrtciudades(cod_ciudad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3970 (class 2606 OID 18271)
-- Name: tsisatcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3997 (class 2606 OID 18276)
-- Name: tsisatordenservicio fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3961 (class 2606 OID 18281)
-- Name: tsisatasignaciones fk_cod_cliente; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3989 (class 2606 OID 18286)
-- Name: tsisatfirmas fk_cod_contratacion; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_contratacion FOREIGN KEY (cod_contratacion) REFERENCES sgrh.tsgrhcontrataciones(cod_contratacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4010 (class 2606 OID 18291)
-- Name: tsisatvacantes fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3967 (class 2606 OID 18296)
-- Name: tsisatcartaaceptacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3971 (class 2606 OID 18301)
-- Name: tsisatcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3990 (class 2606 OID 18306)
-- Name: tsisatfirmas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3978 (class 2606 OID 18311)
-- Name: tsisatcotizaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3998 (class 2606 OID 18316)
-- Name: tsisatordenservicio fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3984 (class 2606 OID 18321)
-- Name: tsisatenviocorreos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3962 (class 2606 OID 18326)
-- Name: tsisatasignaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3982 (class 2606 OID 18331)
-- Name: tsisatentrevistas fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4004 (class 2606 OID 18336)
-- Name: tsisatprospectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4006 (class 2606 OID 18341)
-- Name: tsisatproyectos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3999 (class 2606 OID 18346)
-- Name: tsisatordenservicio fk_cod_estadorep; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_estadorep FOREIGN KEY (cod_estadorep) REFERENCES sgrt.tsgrtestados(cod_estadorep) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3972 (class 2606 OID 18351)
-- Name: tsisatcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4000 (class 2606 OID 18356)
-- Name: tsisatordenservicio fk_cod_gpy; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4011 (class 2606 OID 18361)
-- Name: tsisatvacantes fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatvacantes
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3968 (class 2606 OID 18366)
-- Name: tsisatcartaaceptacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaaceptacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3973 (class 2606 OID 18371)
-- Name: tsisatcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3991 (class 2606 OID 18376)
-- Name: tsisatfirmas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3979 (class 2606 OID 18381)
-- Name: tsisatcotizaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4001 (class 2606 OID 18386)
-- Name: tsisatordenservicio fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3985 (class 2606 OID 18391)
-- Name: tsisatenviocorreos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatenviocorreos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3963 (class 2606 OID 18396)
-- Name: tsisatasignaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3983 (class 2606 OID 18401)
-- Name: tsisatentrevistas fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatentrevistas
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4005 (class 2606 OID 18406)
-- Name: tsisatprospectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatprospectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4007 (class 2606 OID 18411)
-- Name: tsisatproyectos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3974 (class 2606 OID 18416)
-- Name: tsisatcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3966 (class 2606 OID 18421)
-- Name: tsisatcandidatos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcandidatos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3964 (class 2606 OID 18426)
-- Name: tsisatasignaciones fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4008 (class 2606 OID 18431)
-- Name: tsisatproyectos fk_cod_perfil; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3975 (class 2606 OID 18436)
-- Name: tsisatcartaasignacion fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3965 (class 2606 OID 18441)
-- Name: tsisatasignaciones fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatasignaciones
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4009 (class 2606 OID 18446)
-- Name: tsisatproyectos fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatproyectos
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3987 (class 2606 OID 18451)
-- Name: tsisatexperienciaslaborales fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatexperienciaslaborales
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3986 (class 2606 OID 18456)
-- Name: tsisatescolaridad fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatescolaridad
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3981 (class 2606 OID 18461)
-- Name: tsisatcursosycerticados fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcursosycerticados
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3995 (class 2606 OID 18466)
-- Name: tsisatidiomas fk_cod_prospecto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatidiomas
    ADD CONSTRAINT fk_cod_prospecto FOREIGN KEY (cod_prospecto) REFERENCES sisat.tsisatprospectos(cod_prospecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3980 (class 2606 OID 18471)
-- Name: tsisatcotizaciones fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcotizaciones
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4002 (class 2606 OID 18476)
-- Name: tsisatordenservicio fk_cod_puesto; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatordenservicio
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3992 (class 2606 OID 18481)
-- Name: tsisatfirmas fk_cod_puestoautoriza; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_puestoautoriza FOREIGN KEY (cod_puestoautoriza) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3993 (class 2606 OID 18486)
-- Name: tsisatfirmas fk_cod_puestosolicita; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_puestosolicita FOREIGN KEY (cod_puestosolicita) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3976 (class 2606 OID 18491)
-- Name: tsisatcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3977 (class 2606 OID 18496)
-- Name: tsisatcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3994 (class 2606 OID 18501)
-- Name: tsisatfirmas fk_cod_solicita; Type: FK CONSTRAINT; Schema: sisat; Owner: postgres
--

ALTER TABLE ONLY sisat.tsisatfirmas
    ADD CONSTRAINT fk_cod_solicita FOREIGN KEY (cod_solicita) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 4356 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE tsgrhasignacionesemp; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhasignacionesemp TO suite WITH GRANT OPTION;


-- Completed on 2019-04-25 23:20:57 CDT

--
-- PostgreSQL database dump complete
--

