﻿-- Table: sgrh.tsgrhreglogistica
-- SEQUENCE: sgrh.seq_reglogistica;

-- DROP TABLE sgrh.tsgrhreglogistica;
-- DROP SEQUENCE sgrh.seq_reglogistica;

CREATE SEQUENCE sgrh.seq_reglogistica
   START WITH 1
   INCREMENT BY 1
   NO MINVALUE
   NO MAXVALUE
   CACHE 1;

CREATE TABLE sgrh.tsgrhreglogistica
(
  cod_reglogistica integer NOT NULL DEFAULT nextval('sgrh.seq_reglogistica'::regclass),
  tim_horario integer NOT NULL,
  des_lugarevento varchar(200) NOT NULL,
  des_requerimientos varchar(200) NOT NULL,
  fec_inicio date NOT NULL,
  fec_termino date NOT NULL,
  cod_capacitacion integer NOT NULL,
  CONSTRAINT tsgrhreglogistica_pkey PRIMARY KEY (cod_reglogistica),
  CONSTRAINT fk_cod_capacitacion FOREIGN KEY (cod_capacitacion)
      REFERENCES sgrh.tsgrhplancapacitacion (cod_plancapacitacion) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tsgrhreglogistica_cod_capacitacion_key UNIQUE (cod_capacitacion)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sgrh.tsgrhreglogistica
  OWNER TO postgres;
