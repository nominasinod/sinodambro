CREATE SEQUENCE sgrh.seq_plancapacitacion
  START WITH 1
  
  INCREMENT BY 1
  
  NO MINVALUE
  
  NO MAXVALUE
  
  CACHE 1;

CREATE SEQUENCE sgrh.seq_tiposcapacitaciones
  START WITH 1
  
  INCREMENT BY 1
  
  NO MINVALUE
  
  NO MAXVALUE
  
  CACHE 1;

CREATE SEQUENCE sgrh.seq_proceso
  START WITH 1
  
  INCREMENT BY 1
  
  NO MINVALUE
  
  NO MAXVALUE
  
  CACHE 1;
  
  CREATE SEQUENCE sgrh.seq_planinvitados
  START WITH 1
  
  INCREMENT BY 1
  
  NO MINVALUE
  
  NO MAXVALUE
  
  CACHE 1;

CREATE TABLE sgrh.tsgrhplancapacitacion
(
    Cod_plancapacitacion integer NOT NULL DEFAULT nextval('sgrh.seq_plancapacitacion'),
    Des_nombre character varying (50) NOT NULL,
    Des_tipo character varying (10),
    Cod_capacitacion integer,
    Des_criterios character varying (200),
    Des_roles character varying (200),
    Cod_proceso integer,
    Des_instructor character varying (50),
    Des_proveedor character varying (50),
	Fec_fecha date,
	Des_lugar character varying (50),
	Cnu_numeroasistentes integer,
	Des_estado character varying(10) default '--',
	Des_defecto_nombre character varying (200),
	Des_defecto_tipo character varying (200),
	Des_defecto_capacitacion character varying (200),
	Des_defecto_criterios character varying (200),
	Des_defecto_roles character varying (200),
	Des_defecto_proceso character varying (200),
	Des_defecto_instructor character varying (200),
	Des_defecto_provedor character varying (200),
	Des_defecto_logistica character varying (200),
	Des_defecto_calendario character varying (200),
	fec_creacion date NOT NULL,
    fec_modificacion date,
    cod_creadopor integer not null,
    cod_modificadopor integer,
	
     CONSTRAINT tsgrhplancapacitacion_pkey PRIMARY KEY (Cod_plancapacitacion)
);



CREATE TABLE sgrh.tsgrhtiposcapacitaciones
(
    Cod_capacitacion integer NOT NULL DEFAULT nextval('sgrh.seq_tiposcapacitaciones'::regclass),
    Des_capacitacion character varying (30) NOT NULL,
     CONSTRAINT tsgrhtiposcapacitaciones_pkey PRIMARY KEY (Cod_capacitacion)
);

CREATE TABLE sgrh.tsgrhprocesocapacitacion
(
    Cod_proceso integer NOT NULL DEFAULT nextval('sgrh.seq_proceso'::regclass),
    Des_proceso character varying (30) NOT NULL,
     CONSTRAINT tsgrhproceso_pkey PRIMARY KEY (Cod_proceso)
);

CREATE TABLE sgrh.tsgrhplaninvitados
(
	Cod_planinvitados integer NOT NULL DEFAULT nextval('sgrh.seq_planinvitados'::regclass),
    Cod_plancapacitacion integer NOT NULL,
	Cod_empleado integer NOT NULL,
     CONSTRAINT tsgrhplaninvitados_pkey PRIMARY KEY (Cod_planinvitados)
);
  
 
  ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD CONSTRAINT FK_plancap_capacitacion
  FOREIGN KEY (Cod_capacitacion)
  REFERENCES sgrh.tsgrhtiposcapacitaciones (Cod_capacitacion)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

  ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD CONSTRAINT FK_plancap_proceso
  FOREIGN KEY (Cod_proceso)
  REFERENCES sgrh.tsgrhprocesocapacitacion (Cod_proceso)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
  
  
  ALTER TABLE sgrh.tsgrhplaninvitados
  ADD CONSTRAINT FK_invitados_empleado
  FOREIGN KEY (Cod_empleado)
  REFERENCES sgrh.tsgrhempleados (Cod_empleado)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
  
  ALTER TABLE sgrh.tsgrhplaninvitados
  ADD CONSTRAINT FK_invitados_plancapa
  FOREIGN KEY (Cod_plancapacitacion)
  REFERENCES sgrh.tsgrhplancapacitacion (Cod_plancapacitacion)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
  
  
   ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD  CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor)
        REFERENCES sgrh.tsgrhempleados (cod_empleado) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE;
		
    ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor)
        REFERENCES sgrh.tsgrhempleados (cod_empleado) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE;

  
	
  INSERT INTO sgrh.tsgrhprocesocapacitacion(des_proceso)
	VALUES ('PlanCapacitacionGN'),('PlanCapacitacionGPR'),('PlanCapacitacionGR'),('PlanCapacitacionAPE');
	
  INSERT INTO sgrh.tsgrhtiposcapacitaciones(des_capacitacion)
	VALUES ('Curso'),('Taller'),('Seminario'),('Autoestudio'),('Mentoreo'),('Diplomado'),('Congreso');
  