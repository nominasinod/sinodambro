--
-- Name: sgrh; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sgrh;

ALTER SCHEMA sgrh OWNER TO suite;

--
-- Name: SCHEMA sgrh; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sgrh IS 'ESQUEMA QUE CONTIENE LAS TABLAS DE SISTEMA DE GESTION DE RECURSOS HUMANOS';


--####################  VARIABLES ###################
--
-- Name: edo_encuesta; Type: TYPE; Schema: sgrh; Owner: suite
--

CREATE TYPE sgrh.edo_encuesta AS ENUM (
    '--',
    'En proceso',
    'Corregido',
    'Aceptado'
);

ALTER TYPE sgrh.edo_encuesta OWNER TO suite;


--####################  FUNCIONES   ###################
--
-- Name: crosstab_report_encuesta(integer); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.crosstab_report_encuesta(integer) RETURNS TABLE(pregunta character varying, resp1 character varying, resp2 character varying, resp3 character varying, resp4 character varying, resp5 character varying)
    LANGUAGE sql
    AS $_$        
            SELECT * FROM crosstab(
                'SELECT p.des_pregunta AS rowid, 
                        cr.cod_ponderacion as attribute, 
                        cr.des_respuesta as value
                FROM sgrh.tsgrhpreguntasenc p
                INNER JOIN sgrh.tsgrhencuesta e ON p.cod_encuesta = e.cod_encuesta
                LEFT JOIN sgrh.tsgrhrespuestasenc r ON p.cod_pregunta = r.cod_pregunta
                LEFT JOIN sgrh.tsgrhcatrespuestas cr ON r.cod_catrespuesta = cr.cod_catrespuesta
                WHERE e.cod_encuesta = ' || $1
			) 
            AS (
                pregunta VARCHAR(200), 
                resp1 VARCHAR(200), 
                resp2 VARCHAR(200), 
                resp3 VARCHAR(200), 
                resp4 VARCHAR(200), 
                resp5 VARCHAR(200)
            );
    $_$;

ALTER FUNCTION sgrh.crosstab_report_encuesta(integer) OWNER TO postgres;

--
-- Name: factualizarfecha(); Type: FUNCTION; Schema: sgrh; Owner: postgres
--

CREATE FUNCTION sgrh.factualizarfecha() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare begin
	new.fec_modificacion:=current_date;
	return new;

end;
$$;

ALTER FUNCTION sgrh.factualizarfecha() OWNER TO postgres;


--####################  SECUENCIAS  ###################
--
CREATE SEQUENCE sgrh.seq_plancapacitacion
  START WITH 1
  
  INCREMENT BY 1
  
  NO MINVALUE
  
  NO MAXVALUE
  
  CACHE 1;

CREATE SEQUENCE sgrh.seq_tiposcapacitaciones
  START WITH 1
  
  INCREMENT BY 1
  
  NO MINVALUE
  
  NO MAXVALUE
  
  CACHE 1;

CREATE SEQUENCE sgrh.seq_proceso
  START WITH 1
  
  INCREMENT BY 1
  
  NO MINVALUE
  
  NO MAXVALUE
  
  CACHE 1;


-- Name: seq_area; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_area
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_area OWNER TO suite;

--
-- Name: seq_capacitaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_capacitaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_capacitaciones OWNER TO suite;

--
-- Name: seq_cartaasignacion; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_cartaasignacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cartaasignacion OWNER TO suite;

--
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_cat_encuesta_participantes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_cat_encuesta_participantes OWNER TO postgres;

--
-- Name: seq_catrespuestas; Type: SEQUENCE; Schema: sgrh; Owner: postgres
--

CREATE SEQUENCE sgrh.seq_catrespuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_catrespuestas OWNER TO postgres;

--
-- Name: seq_clientes; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_clientes
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_clientes OWNER TO suite;

--
-- Name: seq_contrataciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_contrataciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contrataciones OWNER TO suite;

--
-- Name: seq_contratos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_contratos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_contratos OWNER TO suite;

--
-- Name: seq_empleado; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_empleado
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_empleado OWNER TO suite;

--
-- Name: seq_encuestas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_encuestas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_encuestas OWNER TO suite;

--
-- Name: seq_escolaridad; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_escolaridad
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_escolaridad OWNER TO suite;

--
-- Name: seq_evacontestadas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_evacontestadas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evacontestadas OWNER TO suite;

--
-- Name: seq_evaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_evaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_evaluaciones OWNER TO suite;

--
-- Name: seq_experiencialab; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_experiencialab
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_experiencialab OWNER TO suite;

--
-- Name: seq_factoreseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_factoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_factoreseva OWNER TO suite;

--
-- Name: seq_idiomas; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_idiomas
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_idiomas OWNER TO suite;

--
-- Name: seq_perfiles; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_perfiles
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_perfiles OWNER TO suite;

--
-- Name: seq_planesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_planesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_planesoperativos OWNER TO suite;

--
-- Name: seq_preguntasenc; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_preguntasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntasenc OWNER TO suite;

--
-- Name: seq_preguntaseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_preguntaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_preguntaseva OWNER TO suite;

--
-- Name: seq_puestos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_puestos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_puestos OWNER TO suite;

--
-- Name: seq_respuestasenc; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_respuestasenc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestasenc OWNER TO suite;

--
-- Name: seq_respuestaseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_respuestaseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_respuestaseva OWNER TO suite;

--
-- Name: seq_revplanesoperativos; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_revplanesoperativos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_revplanesoperativos OWNER TO suite;

--
-- Name: seq_subfactoreseva; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_subfactoreseva
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_subfactoreseva OWNER TO suite;

--
-- Name: seq_validaevaluaciones; Type: SEQUENCE; Schema: sgrh; Owner: suite
--

CREATE SEQUENCE sgrh.seq_validaevaluaciones
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgrh.seq_validaevaluaciones OWNER TO suite;



--####################  TABLAS   ###################
--

CREATE TABLE sgrh.tsgrhplancapacitacion
(
    Cod_plancapacitacion integer NOT NULL DEFAULT nextval('sgrh.seq_plancapacitacion'),
    Des_nombre character varying (50) NOT NULL,
    Des_tipoplancapa character varying (10),
    Cod_capacitacion integer,
    Des_criterios character varying (200),
    Des_roles character varying (200),
    Cod_proceso integer,
    Des_instructor character varying (50),
    Des_proveedor character varying (50),
	Fec_fecha date,
	Des_lugar character varying (50),
	Cnu_numeroasistentes integer,
	Des_estado character varying(10) default '--',
	Des_defecto_nombre character varying (200),
	Des_defecto_tipo character varying (200),
	Des_defecto_capacitacion character varying (200),
	Des_defecto_criterios character varying (200),
	Des_defecto_roles character varying (200),
	Des_defecto_proceso character varying (200),
	Des_defecto_instructor character varying (200),
	Des_defecto_provedor character varying (200),
	Des_defecto_logistica character varying (200),
	Des_defecto_calendario character varying (200),
	fec_creacion date NOT NULL,
    fec_modificacion date,
    cod_creadopor integer not null,
    cod_modificadopor integer,
	
     CONSTRAINT tsgrhplancapacitacion_pkey PRIMARY KEY (Cod_plancapacitacion)
);



CREATE TABLE sgrh.tsgrhtiposcapacitaciones
(
    Cod_capacitacion integer NOT NULL DEFAULT nextval('sgrh.seq_tiposcapacitaciones'::regclass),
    Des_capacitacion character varying (30) NOT NULL,
     CONSTRAINT tsgrhtiposcapacitaciones_pkey PRIMARY KEY (Cod_capacitacion)
);

CREATE TABLE sgrh.tsgrhprocesocapacitacion
(
    Cod_proceso integer NOT NULL DEFAULT nextval('sgrh.seq_proceso'::regclass),
    Des_proceso character varying (30) NOT NULL,
     CONSTRAINT tsgrhproceso_pkey PRIMARY KEY (Cod_proceso)
);

-- Name: tsgrhareas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhareas (
    cod_area integer DEFAULT nextval('sgrh.seq_area'::regclass) NOT NULL,
    des_nbarea character varying(50) NOT NULL,
    cod_acronimo character varying(5) NOT NULL,
    cnu_activo boolean NOT NULL,
    cod_sistemasuite integer
);


ALTER TABLE sgrh.tsgrhareas OWNER TO suite;

--
-- Name: tsgrhcapacitaciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcapacitaciones (
    cod_capacitacion integer DEFAULT nextval('sgrh.seq_capacitaciones'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_tipocurso character varying(40) NOT NULL,
    des_nbcurso character varying(50) NOT NULL,
    des_organismo character varying(50) NOT NULL,
    fec_termino date NOT NULL,
    des_duracion character varying(40) NOT NULL,
    bin_documento bytea
);


ALTER TABLE sgrh.tsgrhcapacitaciones OWNER TO suite;

--
-- Name: tsgrhcartaasignacion; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcartaasignacion (
    cod_asignacion integer DEFAULT nextval('sgrh.seq_cartaasignacion'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    cod_perfil integer NOT NULL,
    des_actividades character varying(200),
    des_lugarsalida character varying(100),
    des_lugarllegada character varying(100),
    fec_salida date,
    fec_llegada date,
    cod_transporte character varying(20),
    des_lugarhopedaje character varying(60),
    fec_hospedaje date,
    des_computadora character varying(150),
    cod_telefono character varying(16),
    des_accesorios character varying(150),
    des_nbresponsable character varying(50),
    des_nbpuesto character varying(50),
    des_lugarresp character varying(100),
    cod_telefonoresp character varying(16),
    tim_horario time without time zone,
    fec_iniciocontra date,
    fec_terminocontra date,
    imp_sueldomensual numeric(6,2),
    imp_nominaimss numeric(6,2),
    imp_honorarios numeric(6,2),
    imp_otros numeric(6,2),
    cod_rfc character varying(13),
    des_razonsocial character varying(45),
    des_correo character varying(50),
    cod_cpostal integer,
    des_direccionfact character varying(200),
    cod_cliente integer NOT NULL,
    cod_gpy integer,
    cod_rhta integer,
    cod_ape integer,
    cod_rys integer,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcartaasignacion OWNER TO suite;

--
-- Name: tsgrhcatencuestaparticipantes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcatencuestaparticipantes (
    cod_cat_participante integer DEFAULT nextval('sgrh.seq_cat_encuesta_participantes'::regclass) NOT NULL,
    cod_encuesta integer NOT NULL,
    cod_participante integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatencuestaparticipantes OWNER TO suite;

--
-- Name: tsgrhcatrespuestas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcatrespuestas (
    cod_catrespuesta integer DEFAULT nextval('sgrh.seq_catrespuestas'::regclass) NOT NULL,
    des_respuesta character varying(100) NOT NULL,
    cod_ponderacion integer NOT NULL
);


ALTER TABLE sgrh.tsgrhcatrespuestas OWNER TO suite;

--
-- Name: tsgrhclientes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhclientes (
    cod_cliente integer DEFAULT nextval('sgrh.seq_clientes'::regclass) NOT NULL,
    des_nbcliente character varying(90),
    des_direccioncte character varying(150) NOT NULL,
    des_nbcontactocte character varying(70) NOT NULL,
    des_correocte character varying(50) NOT NULL,
    cod_telefonocte character varying(16) DEFAULT NULL::character varying
);


ALTER TABLE sgrh.tsgrhclientes OWNER TO suite;

--
-- Name: tsgrhcontrataciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcontrataciones (
    cod_contratacion integer DEFAULT nextval('sgrh.seq_contrataciones'::regclass) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date,
    des_esquema character varying(30) NOT NULL,
    cod_salarioestmin numeric(6,2) NOT NULL,
    cod_salarioestmax numeric(6,2),
    tim_jornada time without time zone,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontrataciones OWNER TO suite;

--
-- Name: tsgrhcontratos; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhcontratos (
    cod_contrato integer DEFAULT nextval('sgrh.seq_contratos'::regclass) NOT NULL,
    des_nbconsultor character varying(45) NOT NULL,
    des_appaterno character varying(45) NOT NULL,
    des_apmaterno character varying(45) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhcontratos OWNER TO suite;

--
-- Name: tsgrhempleados; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhempleados (
    cod_empleado integer DEFAULT nextval('sgrh.seq_empleado'::regclass) NOT NULL,
    des_nombre character varying(45) NOT NULL,
    des_nombres character varying(60),
    des_apepaterno character varying(40) NOT NULL,
    des_apematerno character varying(40),
    des_direccion character varying(150) NOT NULL,
    fec_nacimiento date NOT NULL,
    des_lugarnacimiento character varying(50) NOT NULL,
    cod_edad integer NOT NULL,
    des_correo character varying(50),
    cod_tiposangre character varying(5) NOT NULL,
    cod_telefonocasa character varying(16),
    cod_telefonocelular character varying(16),
    cod_telemergencia character varying(16),
    bin_identificacion bytea,
    bin_pasaporte bytea,
    bin_visa bytea,
    cod_licenciamanejo character varying(20),
    fec_ingreso date NOT NULL,
    cod_rfc character varying(13),
    cod_nss character varying(20),
    cod_curp character varying(18) NOT NULL,
    bin_foto bytea,
    cod_tipofoto character varying(30),
    cod_extensionfoto character varying(5),
    cod_empleadoactivo boolean,
    cod_estatusempleado integer NOT NULL,
    cod_estadocivil integer NOT NULL,
    cod_rol integer,
    cod_puesto integer,
    cod_diasvacaciones integer,
    cod_sistemasuite integer,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_creadopor integer,
    cod_modificadopor integer,
    cod_area integer NOT NULL
);


ALTER TABLE sgrh.tsgrhempleados OWNER TO suite;

--
-- Name: tsgrhencuesta; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhencuesta (
    cod_encuesta integer DEFAULT nextval('sgrh.seq_encuestas'::regclass) NOT NULL,
    des_nbencuesta character varying(50) NOT NULL,
    cod_edoencuesta character varying(20) NOT NULL,
    fec_fechaencuesta date NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    des_elementosvalidar character varying(200),
    des_defectos character varying(200),
    des_introduccion character varying(200),
    cod_aceptado boolean,
    cod_edoeliminar boolean,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_area integer NOT NULL,
    CONSTRAINT tsgrhencuesta_cod_edoencuesta_check CHECK (((cod_edoencuesta)::text = ANY (ARRAY[('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhencuesta OWNER TO suite;

--
-- Name: tsgrhescolaridad; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhescolaridad (
    cod_escolaridad integer DEFAULT nextval('sgrh.seq_escolaridad'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbinstitucion character varying(70) NOT NULL,
    des_nivelestudios character varying(30) NOT NULL,
    cod_titulo boolean NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    bin_titulo bytea
);


ALTER TABLE sgrh.tsgrhescolaridad OWNER TO suite;

--
-- Name: tsgrhevacontestadas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhevacontestadas (
    cod_evacontestada integer DEFAULT nextval('sgrh.seq_evacontestadas'::regclass) NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_evaluador integer NOT NULL,
    cod_evaluado integer NOT NULL,
    cod_total integer NOT NULL,
    bin_reporte bytea
);


ALTER TABLE sgrh.tsgrhevacontestadas OWNER TO suite;

--
-- Name: tsgrhevaluaciones; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhevaluaciones (
    cod_evaluacion integer DEFAULT nextval('sgrh.seq_evaluaciones'::regclass) NOT NULL,
    des_nbevaluacion character varying(60) NOT NULL,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL,
    des_edoevaluacion character varying(30) DEFAULT '--'::character varying,
    cod_edoeliminar boolean,
    CONSTRAINT tsgrhevaluaciones_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhevaluaciones OWNER TO suite;

--
-- Name: tsgrhexperienciaslaborales; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhexperienciaslaborales (
    cod_experiencia integer DEFAULT nextval('sgrh.seq_experiencialab'::regclass) NOT NULL,
    cod_empleado integer NOT NULL,
    des_nbempresa character varying(50) NOT NULL,
    des_nbpuesto character varying(50) NOT NULL,
    fec_inicio date NOT NULL,
    fec_termino date NOT NULL,
    txt_actividades text NOT NULL,
    des_ubicacion character varying(70),
    des_nbcliente character varying(70),
    des_proyecto character varying(70),
    txt_logros character varying(300)
);


ALTER TABLE sgrh.tsgrhexperienciaslaborales OWNER TO suite;

--
-- Name: tsgrhfactoreseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhfactoreseva (
    cod_factor integer DEFAULT nextval('sgrh.seq_factoreseva'::regclass) NOT NULL,
    des_nbfactor character varying(60) NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhfactoreseva OWNER TO suite;

--
-- Name: tsgrhidiomas; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhidiomas (
    cod_idioma integer DEFAULT nextval('sgrh.seq_idiomas'::regclass) NOT NULL,
    des_nbidioma character varying(45) NOT NULL,
    por_dominiooral integer,
    por_dominioescrito integer,
    cod_empleado integer
);


ALTER TABLE sgrh.tsgrhidiomas OWNER TO suite;

--
-- Name: tsgrhperfiles; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhperfiles (
    cod_perfil integer DEFAULT nextval('sgrh.seq_perfiles'::regclass) NOT NULL,
    des_perfil character varying(100) NOT NULL
);


ALTER TABLE sgrh.tsgrhperfiles OWNER TO suite;

--
-- Name: tsgrhplanoperativo; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhplanoperativo (
    cod_planoperativo integer DEFAULT nextval('sgrh.seq_planesoperativos'::regclass) NOT NULL,
    des_nbplan character varying(100) NOT NULL,
    cod_version character varying(5) NOT NULL,
    cod_anio integer NOT NULL,
    cod_estatus character varying(20) NOT NULL,
    bin_planoperativo bytea,
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhplanoperativo OWNER TO suite;

--
-- Name: tsgrhpreguntasenc; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpreguntasenc (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntasenc'::regclass) NOT NULL,
    des_pregunta character varying(200) NOT NULL,
    cod_tipopregunta boolean,
    cod_edoeliminar boolean,
    cod_encuesta integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntasenc OWNER TO suite;

--
-- Name: tsgrhpreguntaseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpreguntaseva (
    cod_pregunta integer DEFAULT nextval('sgrh.seq_preguntaseva'::regclass) NOT NULL,
    des_pregunta character varying(100) NOT NULL,
    cod_edoeliminar boolean NOT NULL,
    cod_evaluacion integer NOT NULL,
    cod_subfactor integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpreguntaseva OWNER TO suite;

--
-- Name: tsgrhpuestos; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhpuestos (
    cod_puesto integer DEFAULT nextval('sgrh.seq_puestos'::regclass) NOT NULL,
    des_puesto character varying(100) NOT NULL,
    cod_area integer NOT NULL
);


ALTER TABLE sgrh.tsgrhpuestos OWNER TO suite;

--
-- Name: tsgrhrespuestasenc; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrespuestasenc (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestasenc'::regclass) NOT NULL,
    cod_catrespuesta integer,
    cod_pregunta integer NOT NULL,
    cod_edoeliminar boolean
);


ALTER TABLE sgrh.tsgrhrespuestasenc OWNER TO suite;

--
-- Name: tsgrhrespuestaseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrespuestaseva (
    cod_respuesta integer DEFAULT nextval('sgrh.seq_respuestaseva'::regclass) NOT NULL,
    des_respuesta character varying(200) NOT NULL,
    cod_pregunta integer NOT NULL,
    cod_evacontestada integer NOT NULL
);


ALTER TABLE sgrh.tsgrhrespuestaseva OWNER TO suite;

--
-- Name: tsgrhrevplanoperativo; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhrevplanoperativo (
    cod_revplanoperativo integer DEFAULT nextval('sgrh.seq_revplanesoperativos'::regclass) NOT NULL,
    fec_creacion date DEFAULT CURRENT_DATE NOT NULL,
    cod_lugar integer NOT NULL,
    tim_duracion time without time zone,
    cod_participante1 integer,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_participante5 integer,
    cod_planoperativo integer,
    des_puntosatratar character varying(250),
    des_acuerdosobtenidos character varying(500),
    cod_creadopor integer NOT NULL,
    cod_modificadopor integer NOT NULL,
    fec_modificacion date DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE sgrh.tsgrhrevplanoperativo OWNER TO suite;

--
-- Name: tsgrhsubfactoreseva; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhsubfactoreseva (
    cod_subfactor integer DEFAULT nextval('sgrh.seq_subfactoreseva'::regclass) NOT NULL,
    des_nbsubfactor character varying(60) NOT NULL,
    cod_factor integer NOT NULL,
    cod_edoeliminar boolean NOT NULL
);


ALTER TABLE sgrh.tsgrhsubfactoreseva OWNER TO suite;

--
-- Name: tsgrhvalidaevaluaciondes; Type: TABLE; Schema: sgrh; Owner: suite
--

CREATE TABLE sgrh.tsgrhvalidaevaluaciondes (
    cod_validacion integer DEFAULT nextval('sgrh.seq_validaevaluaciones'::regclass) NOT NULL,
    des_edoevaluacion character varying(30) NOT NULL,
    fec_validacion date,
    cod_lugar integer,
    tim_duracion time without time zone,
    des_defectosevalucacion character varying(1000),
    cod_edoeliminar boolean,
    cod_evaluacion integer,
    cod_participante1 integer NOT NULL,
    cod_participante2 integer,
    cod_participante3 integer,
    cod_participante4 integer,
    cod_evaluador integer,
    cod_evaluado integer,
    CONSTRAINT tsgrhvalidaevaluaciondes_des_edoevaluacion_check CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
);


ALTER TABLE sgrh.tsgrhvalidaevaluaciondes OWNER TO suite;

--
-- Name: tsgrhasignacionesemp; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhasignacionesemp (
    cod_asignacion integer NOT NULL,
    cod_empleado integer NOT NULL,
    cod_puesto integer NOT NULL,
    cod_asignadopor integer NOT NULL,
    cod_modificadopor integer,
    status character varying(10) NOT NULL,
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhasignacionesemp OWNER TO postgres;

--
-- Name: tsgrhevacapacitacionesemp; Type: TABLE; Schema: sgrh; Owner: postgres
--

CREATE TABLE sgrh.tsgrhevacapacitacionesemp (
    cod_evaluacioncap integer NOT NULL,
    cod_plancapacitacion integer NOT NULL,
    cod_empleado integer,
    des_estado character varying(50) NOT NULL,
    des_evaluacion character varying(50),
    fec_creacion date NOT NULL,
    fec_modificacion date
);


ALTER TABLE sgrh.tsgrhevacapacitacionesemp OWNER TO postgres;




--###############	VALORES DE SECUENCIA   ###################
--
-- Name: seq_area; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_area', 5, true);


--
-- Name: seq_capacitaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_capacitaciones', 1, false);


--
-- Name: seq_cartaasignacion; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_cartaasignacion', 1, false);


--
-- Name: seq_cat_encuesta_participantes; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_cat_encuesta_participantes', 9, true);


--
-- Name: seq_catrespuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: postgres
--

SELECT pg_catalog.setval('sgrh.seq_catrespuestas', 8, true);


--
-- Name: seq_clientes; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_clientes', 1, false);


--
-- Name: seq_contrataciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_contrataciones', 1, false);


--
-- Name: seq_contratos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_contratos', 1, false);


--
-- Name: seq_empleado; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_empleado', 13, true);


--
-- Name: seq_encuestas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_encuestas', 7, true);


--
-- Name: seq_escolaridad; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_escolaridad', 1, false);


--
-- Name: seq_evacontestadas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_evacontestadas', 1, false);


--
-- Name: seq_evaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_evaluaciones', 1, false);


--
-- Name: seq_experiencialab; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_experiencialab', 1, false);


--
-- Name: seq_factoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_factoreseva', 1, false);


--
-- Name: seq_idiomas; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_idiomas', 1, false);


--
-- Name: seq_perfiles; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_perfiles', 1, false);


--
-- Name: seq_planesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_planesoperativos', 1, false);


--
-- Name: seq_preguntasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_preguntasenc', 106, true);


--
-- Name: seq_preguntaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_preguntaseva', 1, false);


--
-- Name: seq_puestos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_puestos', 4, true);


--
-- Name: seq_respuestasenc; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_respuestasenc', 411, true);


--
-- Name: seq_respuestaseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_respuestaseva', 1, false);


--
-- Name: seq_revplanesoperativos; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_revplanesoperativos', 1, false);


--
-- Name: seq_subfactoreseva; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_subfactoreseva', 1, false);


--
-- Name: seq_validaevaluaciones; Type: SEQUENCE SET; Schema: sgrh; Owner: suite
--

SELECT pg_catalog.setval('sgrh.seq_validaevaluaciones', 1, false);



--###############	RESTRICCIONES   ###################

--
-- Name: tsgrhcatrespuestas catrespuestas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcatrespuestas
    ADD CONSTRAINT catrespuestas_pkey PRIMARY KEY (cod_catrespuesta);

--
-- TOC entry 3207 (class 2606 OID 74724)
-- Name: tsgrhasignacionesemp pk_cod_asignacion_asignacionesempleados; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT pk_cod_asignacion_asignacionesempleados PRIMARY KEY (cod_asignacion);



--###############	LLAVES PRIMARIAS   ###################
--
-- Name: tsgrhcatencuestaparticipantes tsgrh_cat_encuesta_participantes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcatencuestaparticipantes
    ADD CONSTRAINT tsgrh_cat_encuesta_participantes_pkey PRIMARY KEY (cod_cat_participante);


--
-- Name: tsgrhareas tsgrhareas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT tsgrhareas_pkey PRIMARY KEY (cod_area);


--
-- Name: tsgrhcapacitaciones tsgrhcapacitaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT tsgrhcapacitaciones_pkey PRIMARY KEY (cod_capacitacion);


--
-- Name: tsgrhcartaasignacion tsgrhcartaasignacion_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT tsgrhcartaasignacion_pkey PRIMARY KEY (cod_asignacion);


--
-- Name: tsgrhclientes tsgrhclientes_des_correocte_key; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_des_correocte_key UNIQUE (des_correocte);


--
-- Name: tsgrhclientes tsgrhclientes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhclientes
    ADD CONSTRAINT tsgrhclientes_pkey PRIMARY KEY (cod_cliente);


--
-- Name: tsgrhcontrataciones tsgrhcontrataciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT tsgrhcontrataciones_pkey PRIMARY KEY (cod_contratacion);


--
-- Name: tsgrhcontratos tsgrhcontratos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT tsgrhcontratos_pkey PRIMARY KEY (cod_contrato);


--
-- Name: tsgrhempleados tsgrhempleados_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT tsgrhempleados_pkey PRIMARY KEY (cod_empleado);


--
-- Name: tsgrhencuesta tsgrhencuesta_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT tsgrhencuesta_pkey PRIMARY KEY (cod_encuesta);


--
-- Name: tsgrhescolaridad tsgrhescolaridad_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT tsgrhescolaridad_pkey PRIMARY KEY (cod_escolaridad);


--
-- Name: tsgrhevacontestadas tsgrhevacontestadas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT tsgrhevacontestadas_pkey PRIMARY KEY (cod_evacontestada);


--
-- Name: tsgrhevaluaciones tsgrhevaluaciones_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT tsgrhevaluaciones_pkey PRIMARY KEY (cod_evaluacion);


--
-- Name: tsgrhexperienciaslaborales tsgrhexperienciaslaborales_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT tsgrhexperienciaslaborales_pkey PRIMARY KEY (cod_experiencia);


--
-- Name: tsgrhfactoreseva tsgrhfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhfactoreseva
    ADD CONSTRAINT tsgrhfactoreseva_pkey PRIMARY KEY (cod_factor);


--
-- Name: tsgrhidiomas tsgrhidiomas_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhidiomas
    ADD CONSTRAINT tsgrhidiomas_pkey PRIMARY KEY (cod_idioma);


--
-- Name: tsgrhperfiles tsgrhperfiles_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhperfiles
    ADD CONSTRAINT tsgrhperfiles_pkey PRIMARY KEY (cod_perfil);


--
-- Name: tsgrhplanoperativo tsgrhplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT tsgrhplanoperativo_pkey PRIMARY KEY (cod_planoperativo);


--
-- Name: tsgrhpreguntasenc tsgrhpreguntasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT tsgrhpreguntasenc_pkey PRIMARY KEY (cod_pregunta);


--
-- Name: tsgrhpreguntaseva tsgrhpreguntaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT tsgrhpreguntaseva_pkey PRIMARY KEY (cod_pregunta);


--
-- Name: tsgrhpuestos tsgrhpuestos_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT tsgrhpuestos_pkey PRIMARY KEY (cod_puesto);


--
-- Name: tsgrhrespuestasenc tsgrhrespuestasenc_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT tsgrhrespuestasenc_pkey PRIMARY KEY (cod_respuesta);


--
-- Name: tsgrhrespuestaseva tsgrhrespuestaseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT tsgrhrespuestaseva_pkey PRIMARY KEY (cod_respuesta);


--
-- Name: tsgrhrevplanoperativo tsgrhrevplanoperativo_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT tsgrhrevplanoperativo_pkey PRIMARY KEY (cod_revplanoperativo);


--
-- Name: tsgrhsubfactoreseva tsgrhsubfactoreseva_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT tsgrhsubfactoreseva_pkey PRIMARY KEY (cod_subfactor);


--
-- Name: tsgrhvalidaevaluaciondes tsgrhvalidaevaluaciondes_pkey; Type: CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT tsgrhvalidaevaluaciondes_pkey PRIMARY KEY (cod_validacion);

--
-- TOC entry 3207 (class 2606 OID 82929)
-- Name: tsgrhevacapacitacionesemp pk_cod_evaluacioncap_evacapacitacionesemp; Type: CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacionesemp
    ADD CONSTRAINT pk_cod_evaluacioncap_evacapacitacionesemp PRIMARY KEY (cod_evaluacioncap);



--####################  INDICES   ###################



--####################  TRIGGER   ###################
--
-- Name: tsgrhencuesta tg_actualizarfecha; Type: TRIGGER; Schema: sgrh; Owner: suite
--

CREATE TRIGGER tg_actualizarfecha BEFORE UPDATE ON sgrh.tsgrhencuesta FOR EACH ROW EXECUTE PROCEDURE sgrh.factualizarfecha();


--###############	LLAVES FORANEAS   ###################

--
-- Name: tsgrhcartaasignacion fk_cod_ape; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_ape FOREIGN KEY (cod_ape) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhempleados fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhpuestos fk_cod_area; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpuestos
    ADD CONSTRAINT fk_cod_area FOREIGN KEY (cod_area) REFERENCES sgrh.tsgrhareas(cod_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrespuestasenc fk_cod_catrespuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_catrespuesta FOREIGN KEY (cod_catrespuesta) REFERENCES sgrh.tsgrhcatrespuestas(cod_catrespuesta);


--
-- Name: tsgrhcartaasignacion fk_cod_cliente; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_cliente FOREIGN KEY (cod_cliente) REFERENCES sgrh.tsgrhclientes(cod_cliente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhempleados fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcontratos fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcontrataciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhencuesta fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhevaluaciones fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcartaasignacion fk_cod_creadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhexperienciaslaborales fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhexperienciaslaborales
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhescolaridad fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhescolaridad
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcapacitaciones fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcapacitaciones
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcartaasignacion fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcatencuestaparticipantes fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcatencuestaparticipantes
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_participante) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhpreguntasenc fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntasenc
    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcatencuestaparticipantes fk_cod_encuesta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcatencuestaparticipantes
    ADD CONSTRAINT fk_cod_encuesta FOREIGN KEY (cod_encuesta) REFERENCES sgrh.tsgrhencuesta(cod_encuesta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrespuestaseva fk_cod_evacontestada; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_evacontestada FOREIGN KEY (cod_evacontestada) REFERENCES sgrh.tsgrhevacontestadas(cod_evacontestada) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhpreguntaseva fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhevacontestadas fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluacion; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluacion FOREIGN KEY (cod_evaluacion) REFERENCES sgrh.tsgrhevaluaciones(cod_evaluacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhevacontestadas fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluado; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluado FOREIGN KEY (cod_evaluado) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhevacontestadas fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevacontestadas
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_evaluador; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_evaluador FOREIGN KEY (cod_evaluador) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhsubfactoreseva fk_cod_factor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhsubfactoreseva
    ADD CONSTRAINT fk_cod_factor FOREIGN KEY (cod_factor) REFERENCES sgrh.tsgrhfactoreseva(cod_factor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcartaasignacion fk_cod_gpy; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_gpy FOREIGN KEY (cod_gpy) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhencuesta fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_lugar; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_lugar FOREIGN KEY (cod_lugar) REFERENCES sgrt.tsgrtlugares(cod_lugar) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhempleados fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcontratos fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontratos
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcontrataciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcontrataciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhencuesta fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhencuesta
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhevaluaciones fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhevaluaciones
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcartaasignacion fk_cod_modificadopor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_participante1; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante1 FOREIGN KEY (cod_participante1) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_participante2; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante2 FOREIGN KEY (cod_participante2) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_participante3; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante3 FOREIGN KEY (cod_participante3) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhvalidaevaluaciondes fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhvalidaevaluaciondes
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_participante4; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante4 FOREIGN KEY (cod_participante4) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_participante5; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_participante5 FOREIGN KEY (cod_participante5) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcartaasignacion fk_cod_perfil; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_perfil FOREIGN KEY (cod_perfil) REFERENCES sgrh.tsgrhperfiles(cod_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrevplanoperativo fk_cod_planoperativo; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrevplanoperativo
    ADD CONSTRAINT fk_cod_planoperativo FOREIGN KEY (cod_planoperativo) REFERENCES sgrh.tsgrhplanoperativo(cod_planoperativo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrespuestasenc fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestasenc
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntasenc(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhrespuestaseva fk_cod_pregunta; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhrespuestaseva
    ADD CONSTRAINT fk_cod_pregunta FOREIGN KEY (cod_pregunta) REFERENCES sgrh.tsgrhpreguntaseva(cod_pregunta) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhempleados fk_cod_puesto; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_puesto FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcartaasignacion fk_cod_rhat; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rhat FOREIGN KEY (cod_rhta) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhcartaasignacion fk_cod_rys; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhcartaasignacion
    ADD CONSTRAINT fk_cod_rys FOREIGN KEY (cod_rys) REFERENCES sgrh.tsgrhempleados(cod_empleado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhareas fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhareas
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhempleados fk_cod_sistemasuite; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhempleados
    ADD CONSTRAINT fk_cod_sistemasuite FOREIGN KEY (cod_sistemasuite) REFERENCES sgco.tsgcosistemas(cod_sistema) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tsgrhpreguntaseva fk_cod_subfactor; Type: FK CONSTRAINT; Schema: sgrh; Owner: suite
--

ALTER TABLE ONLY sgrh.tsgrhpreguntaseva
    ADD CONSTRAINT fk_cod_subfactor FOREIGN KEY (cod_subfactor) REFERENCES sgrh.tsgrhsubfactoreseva(cod_subfactor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3208 (class 2606 OID 74735)
-- Name: tsgrhasignacionesemp fk_codasignadopor_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codasignadopor_asignacionesempleados FOREIGN KEY (cod_asignadopor) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3209 (class 2606 OID 74725)
-- Name: tsgrhasignacionesemp fk_codprospecto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codprospecto_asignacionesempleados FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3210 (class 2606 OID 74730)
-- Name: tsgrhasignacionesemp fk_codpuesto_asignacionesempleados; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhasignacionesemp
    ADD CONSTRAINT fk_codpuesto_asignacionesempleados FOREIGN KEY (cod_puesto) REFERENCES sgrh.tsgrhpuestos(cod_puesto);


--
-- TOC entry 3208 (class 2606 OID 82935)
-- Name: tsgrhevacapacitacionesemp fk_codempleado_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacionesemp
    ADD CONSTRAINT fk_codempleado_evacapacitacionesemp FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--
-- TOC entry 3209 (class 2606 OID 82930)
-- Name: tsgrhevacapacitacionesemp fk_plancapacitacion_evacapacitacionesemp; Type: FK CONSTRAINT; Schema: sgrh; Owner: postgres
--

ALTER TABLE ONLY sgrh.tsgrhevacapacitacionesemp
    ADD CONSTRAINT fk_plancapacitacion_evacapacitacionesemp FOREIGN KEY (cod_plancapacitacion) REFERENCES sgrh.tsgrhplancapacitacion(cod_plancapacitacion);





--###############	PRIVILEGIOS   ###################
--
-- Name: SCHEMA sgrh; Type: ACL; Schema: -; Owner: suite
--

REVOKE ALL ON SCHEMA sgrh FROM suite;
GRANT ALL ON SCHEMA sgrh TO suite WITH GRANT OPTION;

--
-- Name: FUNCTION factualizarfecha(); Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON FUNCTION sgrh.factualizarfecha() TO suite WITH GRANT OPTION;


--
-- Name: SEQUENCE seq_cat_encuesta_participantes; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_cat_encuesta_participantes TO suite WITH GRANT OPTION;


--
-- Name: SEQUENCE seq_catrespuestas; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE sgrh.seq_catrespuestas TO suite WITH GRANT OPTION;


--
-- Name: TABLE tsgrhcatencuestaparticipantes; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhcatencuestaparticipantes FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhcatencuestaparticipantes TO suite WITH GRANT OPTION;


--
-- Name: TABLE tsgrhcatrespuestas; Type: ACL; Schema: sgrh; Owner: suite
--

REVOKE ALL ON TABLE sgrh.tsgrhcatrespuestas FROM suite;
GRANT ALL ON TABLE sgrh.tsgrhcatrespuestas TO suite WITH GRANT OPTION;

--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: sgrh; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh REVOKE ALL ON SEQUENCES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh GRANT SELECT,USAGE ON SEQUENCES  TO suite WITH GRANT OPTION;


--
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: sgrh; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh REVOKE ALL ON TYPES  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh REVOKE ALL ON TYPES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh GRANT ALL ON TYPES  TO suite WITH GRANT OPTION;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: sgrh; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh REVOKE ALL ON FUNCTIONS  FROM PUBLIC;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh REVOKE ALL ON FUNCTIONS  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh GRANT ALL ON FUNCTIONS  TO suite WITH GRANT OPTION;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: sgrh; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA sgrh GRANT ALL ON TABLES  TO suite WITH GRANT OPTION;

--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 368
-- Name: TABLE tsgrhasignacionesemp; Type: ACL; Schema: sgrh; Owner: postgres

ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD CONSTRAINT FK_plancap_capacitacion
  FOREIGN KEY (Cod_capacitacion)
  REFERENCES sgrh.tsgrhtiposcapacitaciones (Cod_capacitacion)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

  ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD CONSTRAINT FK_plancap_proceso
  FOREIGN KEY (Cod_proceso)
  REFERENCES sgrh.tsgrhprocesocapacitacion (Cod_proceso)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
  
  
  ALTER TABLE sgrh.tsgrhplaninvitados
  ADD CONSTRAINT FK_invitados_empleado
  FOREIGN KEY (Cod_empleado)
  REFERENCES sgrh.tsgrhempleados (Cod_empleado)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
  
  ALTER TABLE sgrh.tsgrhplaninvitados
  ADD CONSTRAINT FK_invitados_plancapa
  FOREIGN KEY (Cod_plancapacitacion)
  REFERENCES sgrh.tsgrhplancapacitacion (Cod_plancapacitacion)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
  
  
   ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD  CONSTRAINT fk_cod_creadopor FOREIGN KEY (cod_creadopor)
        REFERENCES sgrh.tsgrhempleados (cod_empleado) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE;
		
    ALTER TABLE sgrh.tsgrhplancapacitacion
  ADD CONSTRAINT fk_cod_modificadopor FOREIGN KEY (cod_modificadopor)
        REFERENCES sgrh.tsgrhempleados (cod_empleado) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE;

  
	
  INSERT INTO sgrh.tsgrhprocesocapacitacion(des_proceso)
	VALUES ('PlanCapacitacionGN'),('PlanCapacitacionGPR'),('PlanCapacitacionGR'),('PlanCapacitacionAPE');
	
  INSERT INTO sgrh.tsgrhtiposcapacitaciones(des_capacitacion)
	VALUES ('Curso'),('Taller'),('Seminario'),('Autoestudio'),('Mentoreo'),('Diplomado'),('Congreso');
--

GRANT ALL ON TABLE sgrh.tsgrhasignacionesemp TO suite WITH GRANT OPTION;

--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 369
-- Name: TABLE tsgrhevacapacitacionesemp; Type: ACL; Schema: sgrh; Owner: postgres
--

GRANT ALL ON TABLE sgrh.tsgrhevacapacitacionesemp TO suite WITH GRANT OPTION;


