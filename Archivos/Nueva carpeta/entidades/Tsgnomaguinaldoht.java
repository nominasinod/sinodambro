/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbn.sinod.model.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mi Pe
 */
@Entity
@Table(name = "tsgnomaguinaldoht", catalog = "suite", schema = "sgnom")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tsgnomaguinaldoht.findAll", query = "SELECT t FROM Tsgnomaguinaldoht t")})
public class Tsgnomaguinaldoht implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cod_aguinaldohtid")
    private Integer codAguinaldohtid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "imp_aguinaldo")
    private BigDecimal impAguinaldo;
    @Basic(optional = false)
    @Column(name = "cod_tipoaguinaldo")
    private Character codTipoaguinaldo;
    @Basic(optional = false)
    @Column(name = "bol_estatus")
    private boolean bolEstatus;
    @Column(name = "xml_desgloce")
    private Serializable xmlDesgloce;
    @JoinColumn(name = "cod_empquincenahtid_fk", referencedColumnName = "cod_empquincenahtid")
    @ManyToOne(optional = false)
    private Tsgnomempquincenaht codEmpquincenahtidFk;

    public Tsgnomaguinaldoht() {
    }

    public Tsgnomaguinaldoht(Integer codAguinaldohtid) {
        this.codAguinaldohtid = codAguinaldohtid;
    }

    public Tsgnomaguinaldoht(Integer codAguinaldohtid, BigDecimal impAguinaldo, Character codTipoaguinaldo, boolean bolEstatus) {
        this.codAguinaldohtid = codAguinaldohtid;
        this.impAguinaldo = impAguinaldo;
        this.codTipoaguinaldo = codTipoaguinaldo;
        this.bolEstatus = bolEstatus;
    }

    public Integer getCodAguinaldohtid() {
        return codAguinaldohtid;
    }

    public void setCodAguinaldohtid(Integer codAguinaldohtid) {
        this.codAguinaldohtid = codAguinaldohtid;
    }

    public BigDecimal getImpAguinaldo() {
        return impAguinaldo;
    }

    public void setImpAguinaldo(BigDecimal impAguinaldo) {
        this.impAguinaldo = impAguinaldo;
    }

    public Character getCodTipoaguinaldo() {
        return codTipoaguinaldo;
    }

    public void setCodTipoaguinaldo(Character codTipoaguinaldo) {
        this.codTipoaguinaldo = codTipoaguinaldo;
    }

    public boolean getBolEstatus() {
        return bolEstatus;
    }

    public void setBolEstatus(boolean bolEstatus) {
        this.bolEstatus = bolEstatus;
    }

    public Serializable getXmlDesgloce() {
        return xmlDesgloce;
    }

    public void setXmlDesgloce(Serializable xmlDesgloce) {
        this.xmlDesgloce = xmlDesgloce;
    }

    public Tsgnomempquincenaht getCodEmpquincenahtidFk() {
        return codEmpquincenahtidFk;
    }

    public void setCodEmpquincenahtidFk(Tsgnomempquincenaht codEmpquincenahtidFk) {
        this.codEmpquincenahtidFk = codEmpquincenahtidFk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codAguinaldohtid != null ? codAguinaldohtid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tsgnomaguinaldoht)) {
            return false;
        }
        Tsgnomaguinaldoht other = (Tsgnomaguinaldoht) object;
        if ((this.codAguinaldohtid == null && other.codAguinaldohtid != null) || (this.codAguinaldohtid != null && !this.codAguinaldohtid.equals(other.codAguinaldohtid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mbn.sinod.model.entidades.Tsgnomaguinaldoht[ codAguinaldohtid=" + codAguinaldohtid + " ]";
    }
    
}
