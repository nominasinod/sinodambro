
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'Functions that manipulate whole tables, including crosstab';




--####################  SCHEMA   ###################
--
-- Name: sgco; Type: SCHEMA; Schema: -; Owner: suite
--

CREATE SCHEMA sgco;


ALTER SCHEMA sgco OWNER TO suite;

--
-- Name: SCHEMA sgco; Type: COMMENT; Schema: -; Owner: suite
--

COMMENT ON SCHEMA sgco IS 'Sistema de Gestion de Conocimiento de la Organizacion.';


--####################  FUNCIONES   ###################


--####################  VARIABLES ###################



--####################  SECUENCIAS  ###################
--
-- Name: seq_sistema; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_sistema
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_sistema OWNER TO suite;

--
-- Name: seq_tipousuario; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_tipousuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_tipousuario OWNER TO suite;

--
-- Name: seq_usuarios; Type: SEQUENCE; Schema: sgco; Owner: suite
--

CREATE SEQUENCE sgco.seq_usuarios
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sgco.seq_usuarios OWNER TO suite;

SET default_tablespace = '';

SET default_with_oids = false;


--####################  TABLAS   ###################

--
-- Name: tsgcosistemas; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcosistemas (
    cod_sistema integer NOT NULL,
    des_nbsistema character varying(50) NOT NULL,
    des_descripcion character varying(300)
);


ALTER TABLE sgco.tsgcosistemas OWNER TO suite;

--
-- Name: tsgcotipousuario; Type: TABLE; Schema: sgco; Owner: suite
--

CREATE TABLE sgco.tsgcotipousuario (
    cod_tipousuario integer NOT NULL,
    cod_usuario integer NOT NULL,
    cod_sistema integer NOT NULL,
    cod_rol character varying(35) NOT NULL
);


ALTER TABLE sgco.tsgcotipousuario OWNER TO suite;

--
-- Name: tsgcousuarios; Type: TABLE; Schema: sgco; Owner: suite
--
CREATE TABLE sgco.tsgcousuarios (
    cod_usuario integer NOT NULL,
    cod_empleado integer NOT NULL,
    des_correo character varying(60) NOT NULL,
    des_contrasenacorreo character varying(50) NOT NULL,
    cod_usuariosistema character varying(30) NOT NULL,
    des_contrasenasistema character varying(30) NOT NULL
);


ALTER TABLE sgco.tsgcousuarios OWNER TO suite;


--###############	VALORES DE SECUENCIA   ###################
--
-- Name: seq_sistema; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_sistema', 1, false);


--
-- Name: seq_tipousuario; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_tipousuario', 1, false);


--
-- Name: seq_usuarios; Type: SEQUENCE SET; Schema: sgco; Owner: suite
--

SELECT pg_catalog.setval('sgco.seq_usuarios', 1, false);



--###############	RESTRICCIONES   ###################
--
-- Name: tsgcosistemas cod_agenda; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
	ADD CONSTRAINT cod_agenda UNIQUE (cod_sistema);


--
-- Name: tsgcotipousuario cod_archivo; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT cod_archivo UNIQUE (cod_tipousuario);

--
-- Name: tsgcousuarios cod_asistente; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT cod_asistente UNIQUE (cod_usuario);



--###############	LLAVES PRIMARIAS   ###################
--
-- Name: tsgcosistemas tsgcosistemas_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcosistemas
    ADD CONSTRAINT tsgcosistemas_pkey PRIMARY KEY (cod_sistema);

--
-- Name: tsgcotipousuario tsgcotipousuario_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcotipousuario
    ADD CONSTRAINT tsgcotipousuario_pkey PRIMARY KEY (cod_tipousuario);

--
-- Name: tsgcousuarios tsgcousuarios_pkey; Type: CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT tsgcousuarios_pkey PRIMARY KEY (cod_usuario);



--####################  INDICES   ###################



--####################  TRIGGER   ###################



--###############	LLAVES FORANEAS   ###################
--
-- Name: tsgcousuarios fk_cod_empleado; Type: FK CONSTRAINT; Schema: sgco; Owner: suite
--

ALTER TABLE ONLY sgco.tsgcousuarios
    ADD CONSTRAINT fk_cod_empleado FOREIGN KEY (cod_empleado) REFERENCES sgrh.tsgrhempleados(cod_empleado);


--###############	PRIVILEGIOS   ###################
--
-- Name: SCHEMA sgco; Type: ACL; Schema: -; Owner: suite
--

REVOKE ALL ON SCHEMA sgco FROM suite;
GRANT ALL ON SCHEMA sgco TO suite WITH GRANT OPTION;

--
-- Name: SEQUENCE seq_sistema; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON SEQUENCE sgco.seq_sistema FROM suite;
GRANT UPDATE ON SEQUENCE sgco.seq_sistema TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_sistema TO suite WITH GRANT OPTION;


--
-- Name: SEQUENCE seq_tipousuario; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON SEQUENCE sgco.seq_tipousuario FROM suite;
GRANT UPDATE ON SEQUENCE sgco.seq_tipousuario TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_tipousuario TO suite WITH GRANT OPTION;


--
-- Name: SEQUENCE seq_usuarios; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON SEQUENCE sgco.seq_usuarios FROM suite;
GRANT UPDATE ON SEQUENCE sgco.seq_usuarios TO suite;
GRANT SELECT,USAGE ON SEQUENCE sgco.seq_usuarios TO suite WITH GRANT OPTION;


--
-- Name: TABLE tsgcosistemas; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON TABLE sgco.tsgcosistemas FROM suite;
GRANT ALL ON TABLE sgco.tsgcosistemas TO suite WITH GRANT OPTION;


--
-- Name: TABLE tsgcotipousuario; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON TABLE sgco.tsgcotipousuario FROM suite;
GRANT ALL ON TABLE sgco.tsgcotipousuario TO suite WITH GRANT OPTION;


--
-- Name: TABLE tsgcousuarios; Type: ACL; Schema: sgco; Owner: suite
--

REVOKE ALL ON TABLE sgco.tsgcousuarios FROM suite;
GRANT ALL ON TABLE sgco.tsgcousuarios TO suite WITH GRANT OPTION;
