CREATE TABLE "sgnom"."tsgnomalguinaldo" (
"cod_aguinaldoid" int4 NOT NULL,
"imp_aguinaldo" numeric(10,2) NOT NULL,
"cod_tipoaguinaldo" char(1) COLLATE "default" NOT NULL,
"cod_empquincenaid_fk" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
"xml_desgloce" xml,
CONSTRAINT "nom_aguinaldo_id" PRIMARY KEY ("cod_aguinaldoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomalguinaldo" IS 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';
ALTER TABLE "sgnom"."tsgnomalguinaldo" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomargumento" (
"cod_argumentoid" int4 NOT NULL,
"cod_nbargumento" varchar(30) COLLATE "default" NOT NULL,
"cod_clavearg" varchar(5) COLLATE "default" NOT NULL,
"imp_valorconst" numeric(10,2),
"des_funcionbd" varchar(60) COLLATE "default",
"cod_tipoargumento" char(1) COLLATE "default",
"bol_estatus" bool NOT NULL,
"txt_descripcion" text COLLATE "default" NOT NULL,
"aud_ucrea" int4 NOT NULL,
"aud_uactualizacion" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualizacion" date,
CONSTRAINT "nom_cat_argumento_id" PRIMARY KEY ("cod_argumentoid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomargumento" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnombitacora" (
"cod_bitacoraid" int4 NOT NULL,
"xml_bitacora" xml NOT NULL,
"cod_tablaid_fk" int4 NOT NULL,
CONSTRAINT "nom_bitacora_id" PRIMARY KEY ("cod_bitacoraid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnombitacora" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcabecera" (
"cod_cabeceraid" int4 NOT NULL,
"cod_nbnomina" varchar(40) COLLATE "default" NOT NULL,
"fec_creacion" date NOT NULL,
"fec_ejecucion" date,
"fec_cierre" date,
"imp_totpercepcion" numeric(10,2),
"imp_totdeduccion" numeric(10,2),
"imp_totalemp" numeric(10,2),
"cod_quincenaid_fk" int4 NOT NULL,
"cod_tiponominaid_fk" int4 NOT NULL,
"cod_estatusnomid_fk" int4 NOT NULL,
"aud_ucrea" int4 NOT NULL,
"aud_uactualiza" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualiza" date,
"cnu_totalemp" int4,
CONSTRAINT "nom_cabecera_id" PRIMARY KEY ("cod_cabeceraid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcabecera" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcabeceraht" (
"cod_cabeceraid" int4 NOT NULL,
"cod_nbnomina" varchar(40) COLLATE "default" NOT NULL,
"fec_creacion" date NOT NULL,
"fec_ejecucion" date,
"fec_cierre" date,
"imp_totpercepcion" numeric(10,2),
"imp_totdeduccion" numeric(10,2),
"imp_totalemp" numeric(10,2),
"cod_quincenaid_fk" int4 NOT NULL,
"cod_tiponominaid_fk" int4 NOT NULL,
"cod_estatusnomid_fk" int4 NOT NULL,
"aud_ucrea" int4 NOT NULL,
"aud_uactualiza" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualiza" date,
"cnu_totalemp" int4,
CONSTRAINT "nom_cabecera_copia_id" PRIMARY KEY ("cod_cabeceraid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcabeceraht" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcalculo" (
"cod_calculoid" int4 NOT NULL,
"cod_tpcalculo" varchar(25) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tipo_calculo_id" PRIMARY KEY ("cod_calculoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomcalculo" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomcalculo" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcatincidencia" (
"cod_catincidenciaid" int4 NOT NULL,
"cod_claveincidencia" varchar(5) COLLATE "default",
"cod_nbincidencia" varchar(20) COLLATE "default",
"cod_perfilincidencia" varchar(25) COLLATE "default",
"bol_estatus" bool,
"cod_tipoincidencia" char(1) COLLATE "default",
"imp_monto" numeric(10,2),
CONSTRAINT "cat_incidencia_id" PRIMARY KEY ("cod_catincidenciaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomcatincidencia" IS 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';
ALTER TABLE "sgnom"."tsgnomcatincidencia" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomclasificador" (
"cod_clasificadorid" int4 NOT NULL,
"cod_tpclasificador" varchar(20) COLLATE "default",
"bol_estatus" bool,
CONSTRAINT "nom_cat_tipo_clasificador_id" PRIMARY KEY ("cod_clasificadorid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomclasificador" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcncptoquinc" (
"cod_cncptoquincid" int4 NOT NULL,
"cod_empquincenaid_fk" int4 NOT NULL,
"cod_conceptoid_fk" int4 NOT NULL,
"imp_concepto" numeric(10,2) NOT NULL,
"imp_gravado" numeric(10,2),
"imp_exento" numeric(10,2),
"xml_desgloce" xml,
CONSTRAINT "nom_conceptos_quincena_id" PRIMARY KEY ("cod_cncptoquincid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcncptoquinc" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcncptoquincht" (
"cod_cncptoquinchtid" int4 NOT NULL,
"cod_empquincenaid_fk" int4 NOT NULL,
"cod_conceptoid_fk" int4 NOT NULL,
"imp_concepto" numeric(10,2) NOT NULL,
"imp_gravado" numeric(10,2),
"imp_exento" numeric(10,2),
"xml_desgloce" xml,
CONSTRAINT "nom_conceptos_quincena_copia_id" PRIMARY KEY ("cod_cncptoquinchtid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcncptoquincht" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomconcepto" (
"cod_conceptoid" int4 NOT NULL,
"cod_nbconcepto" varchar(20) COLLATE "default" NOT NULL,
"cod_claveconcepto" varchar(4) COLLATE "default" NOT NULL,
"cnu_prioricalculo" int4 NOT NULL,
"cnu_articulo" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
"cod_formulaid_fk" int4,
"cod_tipoconceptoid_fk" int4 NOT NULL,
"cod_calculoid_fk" int4 NOT NULL,
"cod_conceptosatid_fk" int4 NOT NULL,
"cod_frecuenciapago" varchar(20) COLLATE "default" NOT NULL,
"cod_partidaprep" int4 NOT NULL,
"cnu_cuentacontable" int4 NOT NULL,
"cod_gravado" char(1) COLLATE "default",
"cod_excento" char(1) COLLATE "default",
"bol_aplicaisn" bool,
"bol_retroactividad" bool NOT NULL,
"cnu_topeex" int4,
"cod_clasificadorid_fk" int4 NOT NULL,
"aud_ualta" int4 NOT NULL,
"aud_uactualizacion" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualizacion" date,
"cod_tiponominaid_fk" int4,
CONSTRAINT "nom_cat_concepto_id" PRIMARY KEY ("cod_conceptoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomconcepto" IS 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';
ALTER TABLE "sgnom"."tsgnomconcepto" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomconceptosat" (
"cod_conceptosatid" int4 NOT NULL,
"des_conceptosat" varchar(51) COLLATE "default" NOT NULL,
"des_descconcepto" varchar(51) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_concepto_sat_id" PRIMARY KEY ("cod_conceptosatid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomconceptosat" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomconceptosat" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomconfpago" (
"cod_confpagoid" int4 NOT NULL,
"bol_pagoempleado" bool,
"bol_pagorh" bool,
"bol_pagofinanzas" bool,
"cod_empquincenaid_fk" int4,
CONSTRAINT "nom_conf_pago_pkey" PRIMARY KEY ("cod_confpagoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomconfpago" IS 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';
ALTER TABLE "sgnom"."tsgnomconfpago" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomejercicio" (
"cod_ejercicioid" int4 NOT NULL,
"cnu_valorejercicio" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_ejercicio_id" PRIMARY KEY ("cod_ejercicioid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomejercicio" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomempleados" (
"cod_empleadoid" int4 NOT NULL,
"fec_ingreso" date NOT NULL,
"fec_salida" date,
"bol_estatus" bool NOT NULL,
"cod_empleado_fk" int4 NOT NULL,
"imp_sueldoimss" numeric(10,2),
"imp_honorarios" numeric(10,2),
"imp_finiquito" numeric(10,2),
"cod_tipoimss" char(1) COLLATE "default",
"cod_tipohonorarios" char(1) COLLATE "default",
"cod_banco" varchar(50) COLLATE "default",
"cod_sucursal" int4,
"cod_cuenta" int4,
"cod_clabe" varchar(18) COLLATE "default",
"txt_descripcionbaja" text COLLATE "default",
"aud_codcreadopor" int4 NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_codmodificadopor" int4,
"aud_fecmodificacion" date,
CONSTRAINT "nom_empleado_id" PRIMARY KEY ("cod_empleadoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomempleados" IS 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';
ALTER TABLE "sgnom"."tsgnomempleados" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomempquincena" (
"cod_empquincenaid" int4 NOT NULL,
"cod_empleadoid_fk" int4 NOT NULL,
"cod_cabeceraid_fk" int4 NOT NULL,
"imp_totpercepcion" numeric(10,2) NOT NULL,
"imp_totdeduccion" numeric(10,2) NOT NULL,
"imp_totalemp" numeric(10,2) NOT NULL,
"bol_estatusemp" bool NOT NULL,
CONSTRAINT "nom_empleado_quincena_id" PRIMARY KEY ("cod_empquincenaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomempquincena" IS 'bol_estatusemp

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomempquincena" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomempquincenaht" (
"cod_empquincenahtid" int4 NOT NULL,
"cod_empleadoid_fk" int4 NOT NULL,
"cod_cabeceraid_fk" int4 NOT NULL,
"imp_totpercepcion" numeric(10,2) NOT NULL,
"imp_totdeduccion" numeric(10,2) NOT NULL,
"imp_totalemp" numeric(10,2) NOT NULL,
"bol_estatusemp" bool NOT NULL,
CONSTRAINT "nom_empleado_quincena_copia_id" PRIMARY KEY ("cod_empquincenahtid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomempquincenaht" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomestatusnom" (
"cod_estatusnomid" int4 NOT NULL,
"cod_estatusnomina" varchar(15) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_estatus_nomina_id" PRIMARY KEY ("cod_estatusnomid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomestatusnom" IS 'cod_estatusnomid 

estatus

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';
ALTER TABLE "sgnom"."tsgnomestatusnom" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomformula" (
"cod_formulaid" int4 NOT NULL,
"des_nbformula" varchar(60) COLLATE "default" NOT NULL,
"des_formula" varchar(250) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_formula_id" PRIMARY KEY ("cod_formulaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomformula" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomformula" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomfuncion" (
"cod_funcionid" int4 NOT NULL,
"cod_nbfuncion" varchar(15) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_funcion_id" PRIMARY KEY ("cod_funcionid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomfuncion" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomhisttabla" (
"cod_tablaid" int4 NOT NULL,
"cod_nbtabla" varchar(18) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tabla_id" PRIMARY KEY ("cod_tablaid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomhisttabla" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomincidencia" (
"cod_incidenciaid" int4 NOT NULL,
"cod_catincidenciaid_fk" int4 NOT NULL,
"cnu_cantidad" int2,
"des_actividad" varchar(100) COLLATE "default",
"txt_comentarios" text COLLATE "default",
"cod_empreporta_fk" int4,
"cod_empautoriza_fk" int4,
"imp_monto" numeric(10,2),
"xml_detcantidad" xml,
"bol_estatus" bool,
"cod_quincenaid_fk" int4 NOT NULL,
"bol_validacion" bool,
"aud_feccrea" date NOT NULL,
"aud_fecvalidacion" date,
"aud_codmodificadopor" int4,
"aud_fecmodificadopor" date,
CONSTRAINT "nom_incidencia_id" PRIMARY KEY ("cod_incidenciaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomincidencia" IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';
ALTER TABLE "sgnom"."tsgnomincidencia" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnommanterceros" (
"cod_mantercerosid" int4 NOT NULL,
"cod_conceptoid_fk" int4,
"imp_monto" numeric(10,2),
"cod_quincenainicio_fk" int4,
"cod_quincenafin_fk" int4,
"cod_empleadoid_fk" int4,
"cod_frecuenciapago" varchar(20) COLLATE "default",
"bol_estatus" bool,
"aud_ucrea" int4 NOT NULL,
"aud_uactualizacion" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualizacion" varchar(255) COLLATE "default",
CONSTRAINT "nom_manuales_terceros_pkey" PRIMARY KEY ("cod_mantercerosid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnommanterceros" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomquincena" (
"cod_quincenaid" int4 NOT NULL,
"des_quincena" varchar(70) COLLATE "default" NOT NULL,
"fec_inicio" date NOT NULL,
"fec_fin" date NOT NULL,
"fec_pago" date NOT NULL,
"fec_dispersion" date NOT NULL,
"cnu_numquincena" int4 NOT NULL,
"cod_ejercicioid_fk" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_quincena_id" PRIMARY KEY ("cod_quincenaid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomquincena" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomtipoconcepto" (
"cod_tipoconceptoid" int4 NOT NULL,
"cod_tipoconcepto" varchar(25) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tipo_conepto_id" PRIMARY KEY ("cod_tipoconceptoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomtipoconcepto" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomtipoconcepto" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomtiponomina" (
"cod_tiponominaid" int4 NOT NULL,
"cod_nomina" varchar(30) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tipo_nomina_id" PRIMARY KEY ("cod_tiponominaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomtiponomina" IS 'bol_estatus (activa, inactiva)';
ALTER TABLE "sgnom"."tsgnomtiponomina" OWNER TO "suite";


ALTER TABLE "sgnom"."tsgnomalguinaldo" ADD CONSTRAINT "nom_empleados_quincena_fk_aguinaldos" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnombitacora" ADD CONSTRAINT "nom_cat_tabla_id_fk_nom_cat_tablas" FOREIGN KEY ("cod_tablaid_fk") REFERENCES "sgnom"."tsgnomhisttabla" ("cod_tablaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "nom_cat_estatus_nomina_id_fk_cabeceras" FOREIGN KEY ("cod_estatusnomid_fk") REFERENCES "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "nom_cat_quincena_id_fk_cabeceras" FOREIGN KEY ("cod_quincenaid_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "nom_cat_tipo_nomina_id_fk_cabeceras" FOREIGN KEY ("cod_tiponominaid_fk") REFERENCES "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabeceraht" ADD CONSTRAINT "nom_cat_estatus_nomina_id_copia_fk_cabeceras" FOREIGN KEY ("cod_estatusnomid_fk") REFERENCES "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabeceraht" ADD CONSTRAINT "nom_cat_quincena_id_copia_fk_cabeceras" FOREIGN KEY ("cod_quincenaid_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabeceraht" ADD CONSTRAINT "nom_cat_tipo_nomina_id_copia_fk_cabeceras" FOREIGN KEY ("cod_tiponominaid_fk") REFERENCES "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquinc" ADD CONSTRAINT "concepto_quincena_id_fk_conceptos_quincena" FOREIGN KEY ("cod_conceptoid_fk") REFERENCES "sgnom"."tsgnomconcepto" ("cod_conceptoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquinc" ADD CONSTRAINT "empleado_concepto_id_fk_conceptos_quincena" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquincht" ADD CONSTRAINT "concepto_quincena_id_copia_fk_conceptos_quincena" FOREIGN KEY ("cod_conceptoid_fk") REFERENCES "sgnom"."tsgnomconcepto" ("cod_conceptoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquincht" ADD CONSTRAINT "empleado_concepto_id_copia_fk_conceptos_quincena" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_cat_clasificador_id_fk_cat_conceptos" FOREIGN KEY ("cod_clasificadorid_fk") REFERENCES "sgnom"."tsgnomclasificador" ("cod_clasificadorid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_concepto_sat_id_fk_cat_conceptos" FOREIGN KEY ("cod_conceptosatid_fk") REFERENCES "sgnom"."tsgnomconceptosat" ("cod_conceptosatid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_formula_id_fk_cat_conceptos" FOREIGN KEY ("cod_formulaid_fk") REFERENCES "sgnom"."tsgnomformula" ("cod_formulaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_tipo_calculo_id_fk_cat_conceptos" FOREIGN KEY ("cod_calculoid_fk") REFERENCES "sgnom"."tsgnomcalculo" ("cod_calculoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_tipo_concepto_id_fk_cat_conceptos" FOREIGN KEY ("cod_tipoconceptoid_fk") REFERENCES "sgnom"."tsgnomtipoconcepto" ("cod_tipoconceptoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_tipo_nomina_id_fk_cat_conceptos" FOREIGN KEY ("cod_tiponominaid_fk") REFERENCES "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconfpago" ADD CONSTRAINT "nom_empleados_quincena_fk_conf_pago" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincena" ADD CONSTRAINT "nom_cabecera_id_fk_empleados_quincena" FOREIGN KEY ("cod_cabeceraid_fk") REFERENCES "sgnom"."tsgnomcabecera" ("cod_cabeceraid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincena" ADD CONSTRAINT "nom_empleado_quincena_id_fk_empleados_quincena" FOREIGN KEY ("cod_empleadoid_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincenaht" ADD CONSTRAINT "nom_cabecera_id_fk_empleados_quincena_copia" FOREIGN KEY ("cod_cabeceraid_fk") REFERENCES "sgnom"."tsgnomcabecera" ("cod_cabeceraid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincenaht" ADD CONSTRAINT "nom_empleado_quincena_id_fk_empleados_quincena_copia" FOREIGN KEY ("cod_empleadoid_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_cat_incidencia_id_fk_incidencias" FOREIGN KEY ("cod_catincidenciaid_fk") REFERENCES "sgnom"."tsgnomcatincidencia" ("cod_catincidenciaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_cat_quincena_id_fk_incidencias" FOREIGN KEY ("cod_quincenaid_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_emp_autoriza_fk_incidencias" FOREIGN KEY ("cod_empautoriza_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_emp_reporta_fk_incidencias" FOREIGN KEY ("cod_empreporta_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_cat_conceptos_fk_manuales_terceros" FOREIGN KEY ("cod_conceptoid_fk") REFERENCES "sgnom"."tsgnomconcepto" ("cod_conceptoid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_cat_quincenas_fk_manuales_terceros_fin" FOREIGN KEY ("cod_quincenafin_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_cat_quincenas_fk_manuales_terceros_inicio" FOREIGN KEY ("cod_quincenainicio_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_empleados_fk_manuales_terceros" FOREIGN KEY ("cod_empleadoid_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnomquincena" ADD CONSTRAINT "nom_cat_ejercicio_id_fk_cat_quincenas" FOREIGN KEY ("cod_ejercicioid_fk") REFERENCES "sgnom"."tsgnomejercicio" ("cod_ejercicioid") ON DELETE NO ACTION ON UPDATE CASCADE;

