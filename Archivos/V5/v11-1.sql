CREATE TABLE "sgrh"."tsgrhareas" (
"cod_area" int4 NOT NULL DEFAULT nextval('sgrh.seq_area'::regclass),
"des_nbarea" varchar(50) COLLATE "default" NOT NULL,
"cod_acronimo" varchar(5) COLLATE "default" NOT NULL,
"cnu_activo" bool NOT NULL,
"cod_sistemasuite" int4,
"cod_creadopor" int4,
"cod_modificadopor" int4,
"fec_creacion" date,
"fec_modificacion" date,
CONSTRAINT "tsgrhareas_pkey" PRIMARY KEY ("cod_area") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhareas" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhasignacionesemp" (
"cod_asignacion" int4 NOT NULL,
"cod_empleado" int4 NOT NULL,
"cod_puesto" int4 NOT NULL,
"cod_asignadopor" int4 NOT NULL,
"cod_modificadopor" int4,
"status" varchar(10) COLLATE "default" NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date,
CONSTRAINT "pk_cod_asignacion_asignacionesempleados" PRIMARY KEY ("cod_asignacion") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhasignacionesemp" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhcapacitaciones" (
"cod_capacitacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_capacitaciones'::regclass),
"cod_empleado" int4 NOT NULL,
"cod_tipocurso" varchar(40) COLLATE "default" NOT NULL,
"des_nbcurso" varchar(50) COLLATE "default" NOT NULL,
"des_organismo" varchar(50) COLLATE "default" NOT NULL,
"fec_termino" date NOT NULL,
"des_duracion" varchar(40) COLLATE "default" NOT NULL,
"bin_documento" bytea,
CONSTRAINT "tsgrhcapacitaciones_pkey" PRIMARY KEY ("cod_capacitacion") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhcapacitaciones" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhcartaasignacion" (
"cod_asignacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_cartaasignacion'::regclass),
"cod_empleado" int4 NOT NULL,
"cod_perfil" int4 NOT NULL,
"des_actividades" varchar(200) COLLATE "default",
"des_lugarsalida" varchar(100) COLLATE "default",
"des_lugarllegada" varchar(100) COLLATE "default",
"fec_salida" date,
"fec_llegada" date,
"cod_transporte" varchar(20) COLLATE "default",
"des_lugarhopedaje" varchar(60) COLLATE "default",
"fec_hospedaje" date,
"des_computadora" varchar(150) COLLATE "default",
"cod_telefono" varchar(16) COLLATE "default",
"des_accesorios" varchar(150) COLLATE "default",
"des_nbresponsable" varchar(50) COLLATE "default",
"des_nbpuesto" varchar(50) COLLATE "default",
"des_lugarresp" varchar(100) COLLATE "default",
"cod_telefonoresp" varchar(16) COLLATE "default",
"tim_horario" time(6),
"fec_iniciocontra" date,
"fec_terminocontra" date,
"imp_sueldomensual" numeric(6,2),
"imp_nominaimss" numeric(6,2),
"imp_honorarios" numeric(6,2),
"imp_otros" numeric(6,2),
"cod_rfc" varchar(13) COLLATE "default",
"des_razonsocial" varchar(45) COLLATE "default",
"des_correo" varchar(50) COLLATE "default",
"cod_cpostal" int4,
"des_direccionfact" varchar(200) COLLATE "default",
"cod_cliente" int4 NOT NULL,
"cod_gpy" int4,
"cod_rhta" int4,
"cod_ape" int4,
"cod_rys" int4,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
CONSTRAINT "tsgrhcartaasignacion_pkey" PRIMARY KEY ("cod_asignacion") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhcatrespuestas" (
"cod_catrespuesta" int4 NOT NULL DEFAULT nextval('sgrh.seq_catrespuestas'::regclass),
"des_respuesta" varchar(100) COLLATE "default" NOT NULL,
"cod_ponderacion" int4 NOT NULL,
CONSTRAINT "catrespuestas_pkey" PRIMARY KEY ("cod_catrespuesta") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhcatrespuestas" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhclientes" (
"cod_cliente" int4 NOT NULL DEFAULT nextval('sgrh.seq_clientes'::regclass),
"des_nbcliente" varchar(90) COLLATE "default",
"des_direccioncte" varchar(150) COLLATE "default" NOT NULL,
"des_nbcontactocte" varchar(70) COLLATE "default" NOT NULL,
"des_correocte" varchar(50) COLLATE "default" NOT NULL,
"cod_telefonocte" varchar(16) COLLATE "default" DEFAULT NULL::character varying,
CONSTRAINT "tsgrhclientes_pkey" PRIMARY KEY ("cod_cliente") ,
CONSTRAINT "tsgrhclientes_des_correocte_key" UNIQUE ("des_correocte")
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhclientes" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhcontrataciones" (
"cod_contratacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_contrataciones'::regclass),
"fec_inicio" date NOT NULL,
"fec_termino" date,
"des_esquema" varchar(30) COLLATE "default" NOT NULL,
"cod_salarioestmin" numeric(6,2) NOT NULL,
"cod_salarioestmax" numeric(6,2),
"tim_jornada" time(6),
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
CONSTRAINT "tsgrhcontrataciones_pkey" PRIMARY KEY ("cod_contratacion") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhcontrataciones" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhcontratos" (
"cod_contrato" int4 NOT NULL DEFAULT nextval('sgrh.seq_contratos'::regclass),
"des_nbconsultor" varchar(45) COLLATE "default" NOT NULL,
"des_appaterno" varchar(45) COLLATE "default" NOT NULL,
"des_apmaterno" varchar(45) COLLATE "default" NOT NULL,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
CONSTRAINT "tsgrhcontratos_pkey" PRIMARY KEY ("cod_contrato") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhcontratos" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhempleados" (
"cod_empleado" int4 NOT NULL DEFAULT nextval('sgrh.seq_empleado'::regclass),
"des_nombre" varchar(45) COLLATE "default" NOT NULL,
"des_nombres" varchar(60) COLLATE "default",
"des_apepaterno" varchar(40) COLLATE "default" NOT NULL,
"des_apematerno" varchar(40) COLLATE "default",
"des_direccion" varchar(150) COLLATE "default" NOT NULL,
"fec_nacimiento" date NOT NULL,
"des_lugarnacimiento" varchar(50) COLLATE "default" NOT NULL,
"cod_edad" int4 NOT NULL,
"des_correo" varchar(50) COLLATE "default",
"cod_tiposangre" varchar(5) COLLATE "default" NOT NULL,
"cod_telefonocasa" varchar(16) COLLATE "default",
"cod_telefonocelular" varchar(16) COLLATE "default",
"cod_telemergencia" varchar(16) COLLATE "default",
"bin_identificacion" bytea,
"bin_pasaporte" bytea,
"bin_visa" bytea,
"cod_licenciamanejo" varchar(20) COLLATE "default",
"fec_ingreso" date NOT NULL,
"cod_rfc" varchar(13) COLLATE "default",
"cod_nss" varchar(20) COLLATE "default",
"cod_curp" varchar(18) COLLATE "default" NOT NULL,
"bin_foto" bytea,
"cod_tipofoto" varchar(30) COLLATE "default",
"cod_extensionfoto" varchar(5) COLLATE "default",
"cod_empleadoactivo" bool,
"cod_estatusempleado" int4 NOT NULL,
"cod_estadocivil" int4 NOT NULL,
"cod_rol" int4,
"cod_puesto" int4,
"cod_diasvacaciones" int4,
"cod_sistemasuite" int4,
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
"cod_creadopor" int4,
"cod_modificadopor" int4,
"cod_area" int4 NOT NULL,
CONSTRAINT "tsgrhempleados_pkey" PRIMARY KEY ("cod_empleado") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhempleados" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhencuesta" (
"cod_encuesta" int4 NOT NULL DEFAULT nextval('sgrh.seq_encuestas'::regclass),
"des_nbencuesta" varchar(50) COLLATE "default" NOT NULL,
"cod_edoencuesta" varchar(20) COLLATE "default" NOT NULL,
"fec_fechaencuesta" date NOT NULL,
"cod_lugar" int4 NOT NULL,
"tim_duracion" time(6),
"des_elementosvalidar" varchar(200) COLLATE "default",
"des_defectos" varchar(200) COLLATE "default",
"des_introduccion" varchar(200) COLLATE "default",
"cod_aceptado" bool,
"cod_edoeliminar" bool,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
"cod_area" int4 NOT NULL,
CONSTRAINT "tsgrhencuesta_pkey" PRIMARY KEY ("cod_encuesta") ,
CONSTRAINT "tsgrhencuesta_cod_edoencuesta_check" CHECK (((cod_edoencuesta)::text = ANY (ARRAY[('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
)
WITHOUT OIDS;
CREATE TRIGGER "tg_actualizarfecha" BEFORE UPDATE ON "sgrh"."tsgrhencuesta" FOR EACH ROW EXECUTE PROCEDURE "sgrh"."factualizarfecha"();
ALTER TABLE "sgrh"."tsgrhencuesta" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhencuesta_participantes" (
"cod_participantenc" int4 NOT NULL DEFAULT nextval('sgrh.seq_cat_encuesta_participantes'::regclass),
"cod_empleado" int4 NOT NULL,
"cod_encuesta" int4 NOT NULL,
"cod_pregunta" int4 NOT NULL,
"cod_respuesta" int4,
"respuesta_abierta" text COLLATE "default",
CONSTRAINT "tsgrhencuesta_participantes_pkey" PRIMARY KEY ("cod_participantenc") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhencuesta_participantes" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhescolaridad" (
"cod_escolaridad" int4 NOT NULL DEFAULT nextval('sgrh.seq_escolaridad'::regclass),
"cod_empleado" int4 NOT NULL,
"des_nbinstitucion" varchar(70) COLLATE "default" NOT NULL,
"des_nivelestudios" varchar(30) COLLATE "default" NOT NULL,
"cod_titulo" bool NOT NULL,
"fec_inicio" date NOT NULL,
"fec_termino" date NOT NULL,
"bin_titulo" bytea,
CONSTRAINT "tsgrhescolaridad_pkey" PRIMARY KEY ("cod_escolaridad") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhescolaridad" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhestatuscapacitacion" (
"cod_estatus" int4 NOT NULL DEFAULT nextval('sgrh.seq_estatus'::regclass),
"des_estatus" varchar(50) COLLATE "default" NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_estatus_pk" PRIMARY KEY ("cod_estatus") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhestatuscapacitacion" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhevacapacitacion" (
"cod_evacapacitacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_evacapacitacion'::regclass),
"cod_plancapacitacion" int4 NOT NULL,
"cod_empleado" int4,
"des_estado" varchar(50) COLLATE "default" NOT NULL,
"des_evaluacion" varchar(50) COLLATE "default",
"auf_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4 NOT NULL,
CONSTRAINT "pk_cod_evaluacioncap_evacapacitacionesemp" PRIMARY KEY ("cod_evacapacitacion") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhevacapacitacion" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhevacontestadas" (
"cod_evacontestada" int4 NOT NULL DEFAULT nextval('sgrh.seq_evacontestadas'::regclass),
"cod_evaluacion" int4 NOT NULL,
"cod_evaluador" int4 NOT NULL,
"cod_evaluado" int4 NOT NULL,
"cod_total" int4 NOT NULL,
"bin_reporte" bytea,
CONSTRAINT "tsgrhevacontestadas_pkey" PRIMARY KEY ("cod_evacontestada") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhevacontestadas" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhevaluaciones" (
"cod_evaluacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_evaluaciones'::regclass),
"des_nbevaluacion" varchar(60) COLLATE "default" NOT NULL,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
"des_edoevaluacion" varchar(30) COLLATE "default" DEFAULT '--'::character varying,
"cod_edoeliminar" bool,
CONSTRAINT "tsgrhevaluaciones_pkey" PRIMARY KEY ("cod_evaluacion") ,
CONSTRAINT "tsgrhevaluaciones_des_edoevaluacion_check" CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhevaluaciones" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhexperienciaslaborales" (
"cod_experiencia" int4 NOT NULL DEFAULT nextval('sgrh.seq_experiencialab'::regclass),
"cod_empleado" int4 NOT NULL,
"des_nbempresa" varchar(50) COLLATE "default" NOT NULL,
"des_nbpuesto" varchar(50) COLLATE "default" NOT NULL,
"fec_inicio" date NOT NULL,
"fec_termino" date NOT NULL,
"txt_actividades" text COLLATE "default" NOT NULL,
"des_ubicacion" varchar(70) COLLATE "default",
"des_nbcliente" varchar(70) COLLATE "default",
"des_proyecto" varchar(70) COLLATE "default",
"txt_logros" varchar(300) COLLATE "default",
CONSTRAINT "tsgrhexperienciaslaborales_pkey" PRIMARY KEY ("cod_experiencia") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhexperienciaslaborales" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhfactoreseva" (
"cod_factor" int4 NOT NULL DEFAULT nextval('sgrh.seq_factoreseva'::regclass),
"des_nbfactor" varchar(60) COLLATE "default" NOT NULL,
"cod_edoeliminar" bool NOT NULL,
CONSTRAINT "tsgrhfactoreseva_pkey" PRIMARY KEY ("cod_factor") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhfactoreseva" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhidiomas" (
"cod_idioma" int4 NOT NULL DEFAULT nextval('sgrh.seq_idiomas'::regclass),
"des_nbidioma" varchar(45) COLLATE "default" NOT NULL,
"por_dominiooral" int4,
"por_dominioescrito" int4,
"cod_empleado" int4,
CONSTRAINT "tsgrhidiomas_pkey" PRIMARY KEY ("cod_idioma") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhidiomas" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhlogistica" (
"cod_logistica" int4 NOT NULL DEFAULT nextval('sgrh.seq_logistica'::regclass),
"tim_horario" int4 NOT NULL,
"des_requerimientos" varchar(200) COLLATE "default",
"fec_fecinicio" date NOT NULL,
"fec_termino" date NOT NULL,
"cod_capacitacion" int4 NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date NOT NULL,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4 NOT NULL,
CONSTRAINT "tsgrhreglogistica_pkey" PRIMARY KEY ("cod_logistica") ,
CONSTRAINT "tsgrhreglogistica_cod_capacitacion_key" UNIQUE ("cod_capacitacion")
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhlogistica" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhlugares" (
"cod_lugar" int4 NOT NULL DEFAULT nextval('sgrh.seq_lugar'::regclass),
"des_lugar" varchar(50) COLLATE "default" NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_lugar_pk" PRIMARY KEY ("cod_lugar") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhlugares" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhmodo" (
"cod_modo" int4 NOT NULL DEFAULT nextval('sgrh.seq_modo'::regclass),
"des_nbmodo" varchar(50) COLLATE "default" NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_capacitacion_pk" PRIMARY KEY ("cod_modo") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhmodo" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhperfiles" (
"cod_perfil" int4 NOT NULL DEFAULT nextval('sgrh.seq_perfiles'::regclass),
"des_perfil" varchar(100) COLLATE "default" NOT NULL,
CONSTRAINT "tsgrhperfiles_pkey" PRIMARY KEY ("cod_perfil") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhperfiles" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhplancapacitacion" (
"cod_plancapacitacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_plancapacitacion'::regclass),
"des_nombre" varchar(50) COLLATE "default" NOT NULL,
"cod_modo" int4 NOT NULL,
"cod_tipocapacitacion" int4 NOT NULL,
"des_criterios" varchar(200) COLLATE "default" NOT NULL,
"cod_proceso" int4 NOT NULL,
"des_instructor" varchar(50) COLLATE "default" NOT NULL,
"cod_proveedor" int4 NOT NULL,
"cod_estatus" int4 NOT NULL,
"des_comentarios" varchar(200) COLLATE "default",
"des_evaluacion" varchar(50) COLLATE "default",
"cod_lugar" int4 NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date NOT NULL,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4 NOT NULL,
CONSTRAINT "tsgrhplancapacitacion_pkey" PRIMARY KEY ("cod_plancapacitacion") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhplanoperativo" (
"cod_planoperativo" int4 NOT NULL DEFAULT nextval('sgrh.seq_planesoperativos'::regclass),
"des_nbplan" varchar(100) COLLATE "default" NOT NULL,
"cod_version" varchar(5) COLLATE "default" NOT NULL,
"cod_anio" int4 NOT NULL,
"cod_estatus" varchar(20) COLLATE "default" NOT NULL,
"bin_planoperativo" bytea,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
CONSTRAINT "tsgrhplanoperativo_pkey" PRIMARY KEY ("cod_planoperativo") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhplanoperativo" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhpreguntasenc" (
"cod_pregunta" int4 NOT NULL DEFAULT nextval('sgrh.seq_preguntasenc'::regclass),
"des_pregunta" varchar(200) COLLATE "default" NOT NULL,
"cod_tipopregunta" bool,
"cod_edoeliminar" bool,
"cod_encuesta" int4 NOT NULL,
CONSTRAINT "tsgrhpreguntasenc_pkey" PRIMARY KEY ("cod_pregunta") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhpreguntasenc" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhpreguntaseva" (
"cod_pregunta" int4 NOT NULL DEFAULT nextval('sgrh.seq_preguntaseva'::regclass),
"des_pregunta" varchar(100) COLLATE "default" NOT NULL,
"cod_edoeliminar" bool NOT NULL,
"cod_evaluacion" int4 NOT NULL,
"cod_subfactor" int4 NOT NULL,
CONSTRAINT "tsgrhpreguntaseva_pkey" PRIMARY KEY ("cod_pregunta") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhpreguntaseva" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhprocesos" (
"cod_proceso" int4 NOT NULL DEFAULT nextval('sgrh.seq_proceso'::regclass),
"des_nbproceso" varchar(50) COLLATE "default" NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_procesos_pk" PRIMARY KEY ("cod_proceso") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhprocesos" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhproveedores" (
"cod_proveedor" int4 NOT NULL DEFAULT nextval('sgrh.seq_proveedor'::regclass),
"des_nbproveedor" varchar(50) COLLATE "default" NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_proveedor_pk" PRIMARY KEY ("cod_proveedor") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhproveedores" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhpuestos" (
"cod_puesto" int4 NOT NULL DEFAULT nextval('sgrh.seq_puestos'::regclass),
"des_puesto" varchar(100) COLLATE "default" NOT NULL,
"cod_area" int4 NOT NULL,
"cod_acronimo" varchar(5) COLLATE "default",
CONSTRAINT "tsgrhpuestos_pkey" PRIMARY KEY ("cod_puesto") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhpuestos" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhrelacionroles" (
"cod_plancapacitacion" int4 NOT NULL,
"cod_rolempleado" int4 NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_relacion_pk" PRIMARY KEY ("cod_plancapacitacion", "cod_rolempleado") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhrelacionroles" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhrespuestasenc" (
"cod_respuesta" int4 NOT NULL DEFAULT nextval('sgrh.seq_respuestasenc'::regclass),
"cod_catrespuesta" int4,
"cod_pregunta" int4 NOT NULL,
"cod_edoeliminar" bool,
CONSTRAINT "tsgrhrespuestasenc_pkey" PRIMARY KEY ("cod_respuesta") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhrespuestasenc" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhrespuestaseva" (
"cod_respuesta" int4 NOT NULL DEFAULT nextval('sgrh.seq_respuestaseva'::regclass),
"des_respuesta" varchar(200) COLLATE "default" NOT NULL,
"cod_pregunta" int4 NOT NULL,
"cod_evacontestada" int4 NOT NULL,
CONSTRAINT "tsgrhrespuestaseva_pkey" PRIMARY KEY ("cod_respuesta") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhrespuestaseva" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhrevplanoperativo" (
"cod_revplanoperativo" int4 NOT NULL DEFAULT nextval('sgrh.seq_revplanesoperativos'::regclass),
"fec_creacion" date NOT NULL DEFAULT CURRENT_DATE,
"cod_lugar" int4 NOT NULL,
"tim_duracion" time(6),
"cod_participante1" int4,
"cod_participante2" int4,
"cod_participante3" int4,
"cod_participante4" int4,
"cod_participante5" int4,
"cod_planoperativo" int4,
"des_puntosatratar" varchar(250) COLLATE "default",
"des_acuerdosobtenidos" varchar(500) COLLATE "default",
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_modificacion" date NOT NULL DEFAULT CURRENT_DATE,
CONSTRAINT "tsgrhrevplanoperativo_pkey" PRIMARY KEY ("cod_revplanoperativo") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhrolempleado" (
"cod_rolempleado" int4 NOT NULL DEFAULT nextval('sgrh.seq_rolempleado'::regclass),
"des_nbrol" varchar(50) COLLATE "default" NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_rolempleado_pk" PRIMARY KEY ("cod_rolempleado") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhrolempleado" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhsubfactoreseva" (
"cod_subfactor" int4 NOT NULL DEFAULT nextval('sgrh.seq_subfactoreseva'::regclass),
"des_nbsubfactor" varchar(60) COLLATE "default" NOT NULL,
"cod_factor" int4 NOT NULL,
"cod_edoeliminar" bool NOT NULL,
CONSTRAINT "tsgrhsubfactoreseva_pkey" PRIMARY KEY ("cod_subfactor") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhsubfactoreseva" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhtipocapacitacion" (
"cod_tipocapacitacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_tipocapacitacion'::regclass),
"des_nbtipocapacitacion" varchar(50) COLLATE "default" NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_fecmodificacion" date,
"aud_creadopor" int4 NOT NULL,
"aud_modificadopor" int4,
CONSTRAINT "cod_tipocapacitacion_pk" PRIMARY KEY ("cod_tipocapacitacion") 
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhtipocapacitacion" OWNER TO "postgres";

CREATE TABLE "sgrh"."tsgrhvalidaevaluaciondes" (
"cod_validacion" int4 NOT NULL DEFAULT nextval('sgrh.seq_validaevaluaciones'::regclass),
"des_edoevaluacion" varchar(30) COLLATE "default" NOT NULL,
"fec_validacion" date,
"cod_lugar" int4,
"tim_duracion" time(6),
"des_defectosevalucacion" varchar(1000) COLLATE "default",
"cod_edoeliminar" bool,
"cod_evaluacion" int4,
"cod_participante1" int4 NOT NULL,
"cod_participante2" int4,
"cod_participante3" int4,
"cod_participante4" int4,
"cod_evaluador" int4,
"cod_evaluado" int4,
CONSTRAINT "tsgrhvalidaevaluaciondes_pkey" PRIMARY KEY ("cod_validacion") ,
CONSTRAINT "tsgrhvalidaevaluaciondes_des_edoevaluacion_check" CHECK (((des_edoevaluacion)::text = ANY (ARRAY[('--'::character varying)::text, ('En Proceso'::character varying)::text, ('Corregido'::character varying)::text, ('Aceptado'::character varying)::text])))
)
WITHOUT OIDS;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" OWNER TO "postgres";

CREATE TABLE "sgnom"."tsgnomalguinaldo" (
"cod_aguinaldoid" int4 NOT NULL,
"imp_aguinaldo" numeric(10,2) NOT NULL,
"cod_tipoaguinaldo" char(1) COLLATE "default" NOT NULL,
"cod_empquincenaid_fk" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
"xml_desgloce" xml,
CONSTRAINT "nom_aguinaldo_id" PRIMARY KEY ("cod_aguinaldoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomalguinaldo" IS 'cod_tipoaguinaldo

(

i = imss,

h = honorarios

)';
ALTER TABLE "sgnom"."tsgnomalguinaldo" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomargumento" (
"cod_argumentoid" int4 NOT NULL,
"cod_nbargumento" varchar(30) COLLATE "default" NOT NULL,
"cod_clavearg" varchar(5) COLLATE "default" NOT NULL,
"imp_valorconst" numeric(10,2),
"des_funcionbd" varchar(60) COLLATE "default",
"cod_tipoargumento" char(1) COLLATE "default",
"bol_estatus" bool NOT NULL,
"txt_descripcion" text COLLATE "default" NOT NULL,
"aud_ucrea" int4 NOT NULL,
"aud_uactualizacion" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualizacion" date,
CONSTRAINT "nom_cat_argumento_id" PRIMARY KEY ("cod_argumentoid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomargumento" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnombitacora" (
"cod_bitacoraid" int4 NOT NULL,
"xml_bitacora" xml NOT NULL,
"cod_tablaid_fk" int4 NOT NULL,
CONSTRAINT "nom_bitacora_id" PRIMARY KEY ("cod_bitacoraid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnombitacora" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcabecera" (
"cod_cabeceraid" int4 NOT NULL,
"cod_nbnomina" varchar(40) COLLATE "default" NOT NULL,
"fec_creacion" date NOT NULL,
"fec_ejecucion" date,
"fec_cierre" date,
"imp_totpercepcion" numeric(10,2),
"imp_totdeduccion" numeric(10,2),
"imp_totalemp" numeric(10,2),
"cod_quincenaid_fk" int4 NOT NULL,
"cod_tiponominaid_fk" int4 NOT NULL,
"cod_estatusnomid_fk" int4 NOT NULL,
"aud_ucrea" int4 NOT NULL,
"aud_uactualiza" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualiza" date,
"cnu_totalemp" int4,
CONSTRAINT "nom_cabecera_id" PRIMARY KEY ("cod_cabeceraid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcabecera" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcabeceraht" (
"cod_cabeceraid" int4 NOT NULL,
"cod_nbnomina" varchar(40) COLLATE "default" NOT NULL,
"fec_creacion" date NOT NULL,
"fec_ejecucion" date,
"fec_cierre" date,
"imp_totpercepcion" numeric(10,2),
"imp_totdeduccion" numeric(10,2),
"imp_totalemp" numeric(10,2),
"cod_quincenaid_fk" int4 NOT NULL,
"cod_tiponominaid_fk" int4 NOT NULL,
"cod_estatusnomid_fk" int4 NOT NULL,
"aud_ucrea" int4 NOT NULL,
"aud_uactualiza" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualiza" date,
"cnu_totalemp" int4,
CONSTRAINT "nom_cabecera_copia_id" PRIMARY KEY ("cod_cabeceraid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcabeceraht" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcalculo" (
"cod_calculoid" int4 NOT NULL,
"cod_tpcalculo" varchar(25) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tipo_calculo_id" PRIMARY KEY ("cod_calculoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomcalculo" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomcalculo" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcatincidencia" (
"cod_catincidenciaid" int4 NOT NULL,
"cod_claveincidencia" varchar(5) COLLATE "default",
"cod_nbincidencia" varchar(20) COLLATE "default",
"cod_perfilincidencia" varchar(25) COLLATE "default",
"bol_estatus" bool,
"cod_tipoincidencia" char(1) COLLATE "default",
"imp_monto" numeric(10,2),
CONSTRAINT "cat_incidencia_id" PRIMARY KEY ("cod_catincidenciaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomcatincidencia" IS 'cod_tipoincidencia

(

1 = horas

2 = dias

3 = actividad

)';
ALTER TABLE "sgnom"."tsgnomcatincidencia" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomclasificador" (
"cod_clasificadorid" int4 NOT NULL,
"cod_tpclasificador" varchar(20) COLLATE "default",
"bol_estatus" bool,
CONSTRAINT "nom_cat_tipo_clasificador_id" PRIMARY KEY ("cod_clasificadorid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomclasificador" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcncptoquinc" (
"cod_cncptoquincid" int4 NOT NULL,
"cod_empquincenaid_fk" int4 NOT NULL,
"cod_conceptoid_fk" int4 NOT NULL,
"imp_concepto" numeric(10,2) NOT NULL,
"imp_gravado" numeric(10,2),
"imp_exento" numeric(10,2),
"xml_desgloce" xml,
CONSTRAINT "nom_conceptos_quincena_id" PRIMARY KEY ("cod_cncptoquincid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcncptoquinc" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomcncptoquincht" (
"cod_cncptoquinchtid" int4 NOT NULL,
"cod_empquincenaid_fk" int4 NOT NULL,
"cod_conceptoid_fk" int4 NOT NULL,
"imp_concepto" numeric(10,2) NOT NULL,
"imp_gravado" numeric(10,2),
"imp_exento" numeric(10,2),
"xml_desgloce" xml,
CONSTRAINT "nom_conceptos_quincena_copia_id" PRIMARY KEY ("cod_cncptoquinchtid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomcncptoquincht" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomconcepto" (
"cod_conceptoid" int4 NOT NULL,
"cod_nbconcepto" varchar(20) COLLATE "default" NOT NULL,
"cod_claveconcepto" varchar(4) COLLATE "default" NOT NULL,
"cnu_prioricalculo" int4 NOT NULL,
"cnu_articulo" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
"cod_formulaid_fk" int4,
"cod_tipoconceptoid_fk" int4 NOT NULL,
"cod_calculoid_fk" int4 NOT NULL,
"cod_conceptosatid_fk" int4 NOT NULL,
"cod_frecuenciapago" varchar(20) COLLATE "default" NOT NULL,
"cod_partidaprep" int4 NOT NULL,
"cnu_cuentacontable" int4 NOT NULL,
"cod_gravado" char(1) COLLATE "default",
"cod_excento" char(1) COLLATE "default",
"bol_aplicaisn" bool,
"bol_retroactividad" bool NOT NULL,
"cnu_topeex" int4,
"cod_clasificadorid_fk" int4 NOT NULL,
"aud_ualta" int4 NOT NULL,
"aud_uactualizacion" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualizacion" date,
"cod_tiponominaid_fk" int4,
CONSTRAINT "nom_cat_concepto_id" PRIMARY KEY ("cod_conceptoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomconcepto" IS 'bol_estatus

(

activo, inactivo

)



gravado

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



excento

(

activo, inactivo

)





validar tope_ex';
ALTER TABLE "sgnom"."tsgnomconcepto" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomconceptosat" (
"cod_conceptosatid" int4 NOT NULL,
"des_conceptosat" varchar(51) COLLATE "default" NOT NULL,
"des_descconcepto" varchar(51) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_concepto_sat_id" PRIMARY KEY ("cod_conceptosatid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomconceptosat" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomconceptosat" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomconfpago" (
"cod_confpagoid" int4 NOT NULL,
"bol_pagoempleado" bool,
"bol_pagorh" bool,
"bol_pagofinanzas" bool,
"cod_empquincenaid_fk" int4,
CONSTRAINT "nom_conf_pago_pkey" PRIMARY KEY ("cod_confpagoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomconfpago" IS 'bol_pagoempleado

(

confirmado, pendiente 

)



pago_rh

(

autorizado, pendiente

)



pago_fnzas

(

autorizado, pendiente

)';
ALTER TABLE "sgnom"."tsgnomconfpago" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomejercicio" (
"cod_ejercicioid" int4 NOT NULL,
"cnu_valorejercicio" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_ejercicio_id" PRIMARY KEY ("cod_ejercicioid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomejercicio" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomempleados" (
"cod_empleadoid" int4 NOT NULL,
"fec_ingreso" date NOT NULL,
"fec_salida" date,
"bol_estatus" bool NOT NULL,
"cod_empleado_fk" int4 NOT NULL,
"imp_sueldoimss" numeric(10,2),
"imp_honorarios" numeric(10,2),
"imp_finiquito" numeric(10,2),
"cod_tipoimss" char(1) COLLATE "default",
"cod_tipohonorarios" char(1) COLLATE "default",
"cod_banco" varchar(50) COLLATE "default",
"cod_sucursal" int4,
"cod_cuenta" int4,
"cod_clabe" varchar(18) COLLATE "default",
"txt_descripcionbaja" text COLLATE "default",
"aud_codcreadopor" int4 NOT NULL,
"aud_feccreacion" date NOT NULL,
"aud_codmodificadopor" int4,
"aud_fecmodificacion" date,
CONSTRAINT "nom_empleado_id" PRIMARY KEY ("cod_empleadoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomempleados" IS 'cod_empleado_fk hace referencia al schema sgrh en la tabla tsgrhempleados';
ALTER TABLE "sgnom"."tsgnomempleados" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomempquincena" (
"cod_empquincenaid" int4 NOT NULL,
"cod_empleadoid_fk" int4 NOT NULL,
"cod_cabeceraid_fk" int4 NOT NULL,
"imp_totpercepcion" numeric(10,2) NOT NULL,
"imp_totdeduccion" numeric(10,2) NOT NULL,
"imp_totalemp" numeric(10,2) NOT NULL,
"bol_estatusemp" bool NOT NULL,
CONSTRAINT "nom_empleado_quincena_id" PRIMARY KEY ("cod_empquincenaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomempquincena" IS 'bol_estatusemp

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomempquincena" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomempquincenaht" (
"cod_empquincenahtid" int4 NOT NULL,
"cod_empleadoid_fk" int4 NOT NULL,
"cod_cabeceraid_fk" int4 NOT NULL,
"imp_totpercepcion" numeric(10,2) NOT NULL,
"imp_totdeduccion" numeric(10,2) NOT NULL,
"imp_totalemp" numeric(10,2) NOT NULL,
"bol_estatusemp" bool NOT NULL,
CONSTRAINT "nom_empleado_quincena_copia_id" PRIMARY KEY ("cod_empquincenahtid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomempquincenaht" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomestatusnom" (
"cod_estatusnomid" int4 NOT NULL,
"cod_estatusnomina" varchar(15) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_estatus_nomina_id" PRIMARY KEY ("cod_estatusnomid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomestatusnom" IS 'cod_estatusnomid 

estatus

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';
ALTER TABLE "sgnom"."tsgnomestatusnom" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomformula" (
"cod_formulaid" int4 NOT NULL,
"des_nbformula" varchar(60) COLLATE "default" NOT NULL,
"des_formula" varchar(250) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_formula_id" PRIMARY KEY ("cod_formulaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomformula" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomformula" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomfuncion" (
"cod_funcionid" int4 NOT NULL,
"cod_nbfuncion" varchar(15) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_funcion_id" PRIMARY KEY ("cod_funcionid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomfuncion" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomhisttabla" (
"cod_tablaid" int4 NOT NULL,
"cod_nbtabla" varchar(18) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tabla_id" PRIMARY KEY ("cod_tablaid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomhisttabla" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomincidencia" (
"cod_incidenciaid" int4 NOT NULL,
"cod_catincidenciaid_fk" int4 NOT NULL,
"cnu_cantidad" int2,
"des_actividad" varchar(100) COLLATE "default",
"txt_comentarios" text COLLATE "default",
"cod_empreporta_fk" int4,
"cod_empautoriza_fk" int4,
"imp_monto" numeric(10,2),
"xml_detcantidad" xml,
"bol_estatus" bool,
"cod_quincenaid_fk" int4 NOT NULL,
"bol_validacion" bool,
"aud_feccrea" date NOT NULL,
"aud_fecvalidacion" date,
"aud_codmodificadopor" int4,
"aud_fecmodificadopor" date,
CONSTRAINT "nom_incidencia_id" PRIMARY KEY ("cod_incidenciaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomincidencia" IS 'bol_estatus: (activo, inactivo) (1, 0)

validacion: (validar, denegar) (1, 0) 



';
ALTER TABLE "sgnom"."tsgnomincidencia" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnommanterceros" (
"cod_mantercerosid" int4 NOT NULL,
"cod_conceptoid_fk" int4,
"imp_monto" numeric(10,2),
"cod_quincenainicio_fk" int4,
"cod_quincenafin_fk" int4,
"cod_empleadoid_fk" int4,
"cod_frecuenciapago" varchar(20) COLLATE "default",
"bol_estatus" bool,
"aud_ucrea" int4 NOT NULL,
"aud_uactualizacion" int4,
"aud_feccrea" date NOT NULL,
"aud_fecactualizacion" varchar(255) COLLATE "default",
CONSTRAINT "nom_manuales_terceros_pkey" PRIMARY KEY ("cod_mantercerosid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnommanterceros" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomquincena" (
"cod_quincenaid" int4 NOT NULL,
"des_quincena" varchar(70) COLLATE "default" NOT NULL,
"fec_inicio" date NOT NULL,
"fec_fin" date NOT NULL,
"fec_pago" date NOT NULL,
"fec_dispersion" date NOT NULL,
"cnu_numquincena" int4 NOT NULL,
"cod_ejercicioid_fk" int4 NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_quincena_id" PRIMARY KEY ("cod_quincenaid") 
)
WITHOUT OIDS;
ALTER TABLE "sgnom"."tsgnomquincena" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomtipoconcepto" (
"cod_tipoconceptoid" int4 NOT NULL,
"cod_tipoconcepto" varchar(25) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tipo_conepto_id" PRIMARY KEY ("cod_tipoconceptoid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomtipoconcepto" IS 'bol_estatus

(

activo, inactivo

)';
ALTER TABLE "sgnom"."tsgnomtipoconcepto" OWNER TO "suite";

CREATE TABLE "sgnom"."tsgnomtiponomina" (
"cod_tiponominaid" int4 NOT NULL,
"cod_nomina" varchar(30) COLLATE "default" NOT NULL,
"bol_estatus" bool NOT NULL,
CONSTRAINT "nom_cat_tipo_nomina_id" PRIMARY KEY ("cod_tiponominaid") 
)
WITHOUT OIDS;
COMMENT ON TABLE "sgnom"."tsgnomtiponomina" IS 'bol_estatus (activa, inactiva)';
ALTER TABLE "sgnom"."tsgnomtiponomina" OWNER TO "suite";

CREATE TABLE "sgco"."tsgcosistemas" (
"cod_sistema" int4 NOT NULL,
"des_nbsistema" varchar(50) COLLATE "default" NOT NULL,
"des_descripcion" varchar(300) COLLATE "default",
CONSTRAINT "tsgcosistemas_pkey" PRIMARY KEY ("cod_sistema") ,
CONSTRAINT "cod_agenda" UNIQUE ("cod_sistema")
)
WITHOUT OIDS;
ALTER TABLE "sgco"."tsgcosistemas" OWNER TO "postgres";

CREATE TABLE "sgco"."tsgcotipousuario" (
"cod_tipousuario" int4 NOT NULL,
"cod_usuario" int4 NOT NULL,
"cod_sistema" int4 NOT NULL,
"cod_rol" varchar(35) COLLATE "default" NOT NULL,
CONSTRAINT "tsgcotipousuario_pkey" PRIMARY KEY ("cod_tipousuario") ,
CONSTRAINT "cod_archivo" UNIQUE ("cod_tipousuario")
)
WITHOUT OIDS;
ALTER TABLE "sgco"."tsgcotipousuario" OWNER TO "postgres";

CREATE TABLE "sgco"."tsgcousuarios" (
"cod_usuario" int4 NOT NULL,
"cod_empleado" int4 NOT NULL,
"des_correo" varchar(60) COLLATE "default" NOT NULL,
"des_contrasenacorreo" varchar(50) COLLATE "default" NOT NULL,
"cod_usuariosistema" varchar(30) COLLATE "default" NOT NULL,
"des_contrasenasistema" varchar(30) COLLATE "default" NOT NULL,
CONSTRAINT "tsgcousuarios_pkey" PRIMARY KEY ("cod_usuario") ,
CONSTRAINT "cod_asistente" UNIQUE ("cod_usuario")
)
WITHOUT OIDS;
ALTER TABLE "sgco"."tsgcousuarios" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtagenda" (
"cod_agenda" int4 NOT NULL DEFAULT nextval('sgrt.seq_agenda'::regclass),
"des_texto" varchar(200) COLLATE "default" NOT NULL,
"cnu_tratado" int2 DEFAULT '0'::smallint,
"cod_reunion" int4 NOT NULL,
CONSTRAINT "tsgrtagenda_pkey" PRIMARY KEY ("cod_agenda") ,
CONSTRAINT "cod_agenda" UNIQUE ("cod_agenda")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtagenda" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtarchivos" (
"cod_archivo" int4 NOT NULL DEFAULT nextval('sgrt.seq_archivo'::regclass),
"bin_archivo" bytea,
"cod_reunion" int4 NOT NULL,
CONSTRAINT "tsgrtarchivos_pkey" PRIMARY KEY ("cod_archivo") ,
CONSTRAINT "cod_archivo" UNIQUE ("cod_archivo"),
CONSTRAINT "cod_reunionarchivos" UNIQUE ("cod_reunion")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtarchivos" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtasistentes" (
"cod_asistente" int4 NOT NULL DEFAULT nextval('sgrt.seq_asistente'::regclass),
"cod_reunion" int4 NOT NULL,
"cod_empleado" int4,
"cnu_asiste" int2 NOT NULL,
"cod_invitado" int4,
CONSTRAINT "tsgrtasistentes_pkey" PRIMARY KEY ("cod_asistente") ,
CONSTRAINT "cod_asistente" UNIQUE ("cod_asistente")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtasistentes" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtattchticket" (
"cod_attach" int4 NOT NULL DEFAULT nextval('sgrt.seq_attach'::regclass),
"cod_ticket" int4 NOT NULL,
"cod_tamano" varchar(20) COLLATE "default" NOT NULL,
"des_nombre" varchar(128) COLLATE "default" NOT NULL,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
"bin_attach" bytea,
CONSTRAINT "tsgrtattchticket_pkey" PRIMARY KEY ("cod_attach") ,
CONSTRAINT "cod_attach" UNIQUE ("cod_attach"),
CONSTRAINT "cod_ticketattach" UNIQUE ("cod_ticket")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtattchticket" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtayudatopico" (
"cod_topico" int4 NOT NULL DEFAULT nextval('sgrt.seq_topico'::regclass),
"cnu_activo" int2 NOT NULL,
"cnu_autorespuesta" int2 NOT NULL,
"cod_prioridad" int2 NOT NULL,
"cod_depto" int4 NOT NULL,
"des_topico" varchar(36) COLLATE "default" NOT NULL,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtayudatopico_pkey" PRIMARY KEY ("cod_topico") ,
CONSTRAINT "cod_topico" UNIQUE ("cod_topico"),
CONSTRAINT "tsgrtayudatopico_des_topico_key" UNIQUE ("des_topico")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtayudatopico" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtcategoriafaq" (
"cod_categoriafaq" int4 NOT NULL DEFAULT nextval('sgrt.seq_categoriafaq'::regclass),
"cnu_tipo" int2 NOT NULL,
"des_categoria" varchar(255) COLLATE "default" NOT NULL,
"des_descripcion" varchar(255) COLLATE "default" NOT NULL,
"des_notas" varchar(255) COLLATE "default" NOT NULL,
"tim_ultactualiza" timestamp(6) NOT NULL,
"fec_creacion" timestamp(6) NOT NULL,
"fec_ultactualizadopor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
CONSTRAINT "tsgrtcategoriafaq_pkey" PRIMARY KEY ("cod_categoriafaq") ,
CONSTRAINT "cod_categoriafaq" UNIQUE ("cod_categoriafaq")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtcategoriafaq" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtchat" (
"cod_chat" int4 NOT NULL DEFAULT nextval('sgrt.seq_chat'::regclass),
"chat" varchar COLLATE "default" NOT NULL,
CONSTRAINT "tsgrtchat_pkey" PRIMARY KEY ("cod_chat") 
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtchat" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtciudades" (
"cod_ciudad" int4 NOT NULL DEFAULT nextval('sgrt.seq_ciudad'::regclass),
"des_nbciudad" varchar(100) COLLATE "default" NOT NULL,
"cod_estadorep" int4 NOT NULL,
CONSTRAINT "tsgrtciudades_pkey" PRIMARY KEY ("cod_ciudad") 
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtciudades" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtcomentariosagenda" (
"cod_comentsagenda" int4 NOT NULL DEFAULT nextval('sgrt.seq_comentsagenda'::regclass),
"des_comentario" varchar(500) COLLATE "default" NOT NULL,
"cod_agenda" int4 NOT NULL,
"cod_invitado" int4 NOT NULL,
CONSTRAINT "tsgrtcomentariosagenda_pkey" PRIMARY KEY ("cod_comentsagenda") ,
CONSTRAINT "cod_comentsagenda" UNIQUE ("cod_comentsagenda")
)
WITHOUT OIDS;
CREATE INDEX "fki_fk_cod_invitados" ON "sgrt"."tsgrtcomentariosagenda" USING btree ("cod_invitado" "pg_catalog"."int4_ops" ASC NULLS LAST);
ALTER TABLE "sgrt"."tsgrtcomentariosagenda" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtcomentariosreunion" (
"cod_commentsreunion" int4 NOT NULL DEFAULT nextval('sgrt.seq_comentsreunion'::regclass),
"des_comentario" varchar(500) COLLATE "default" NOT NULL,
"cod_invitado" int4 NOT NULL,
"cod_reunion" int4 NOT NULL,
CONSTRAINT "tsgrtcomentariosreunion_pkey" PRIMARY KEY ("cod_commentsreunion") 
)
WITHOUT OIDS;
CREATE INDEX "fki_fk_cod_invitado" ON "sgrt"."tsgrtcomentariosreunion" USING btree ("cod_invitado" "pg_catalog"."int4_ops" ASC NULLS LAST);
ALTER TABLE "sgrt"."tsgrtcomentariosreunion" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtcompromisos" (
"cod_compromiso" int4 NOT NULL DEFAULT nextval('sgrt.seq_compromiso'::regclass),
"des_descripcion" varchar(200) COLLATE "default" NOT NULL,
"fec_solicitud" date NOT NULL,
"fec_compromiso" date NOT NULL,
"cod_reunion" int4 NOT NULL,
"cod_validador" int4 NOT NULL,
"cod_verificador" int4 NOT NULL,
"cod_estado" int4,
"des_valor" varchar(45) COLLATE "default",
"cod_ejecutor" int4 NOT NULL,
"cod_tipoejecutor" varchar(10) COLLATE "default",
"cnu_revisado" int2,
"cod_estatus" "sgrt"."estatus_compromiso" NOT NULL DEFAULT 'Pendiente'::sgrt.estatus_compromiso,
"cod_tipocompromiso" "sgrt"."tipo_compromiso" NOT NULL DEFAULT 'Pendiente'::sgrt.tipo_compromiso,
"cod_chat" int4,
"fec_entrega" date,
CONSTRAINT "tsgrtcompromisos_pkey" PRIMARY KEY ("cod_compromiso") ,
CONSTRAINT "unique_cod_chat" UNIQUE ("cod_chat")
)
WITHOUT OIDS;
CREATE INDEX "fki_fk_cod_chat" ON "sgrt"."tsgrtcompromisos" USING btree ("cod_chat" "pg_catalog"."int4_ops" ASC NULLS LAST);
ALTER TABLE "sgrt"."tsgrtcompromisos" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtcorreo" (
"cod_correo" int4 NOT NULL DEFAULT nextval('sgrt.seq_correo'::regclass),
"cnu_autorespuesta" int2 NOT NULL,
"cod_prioridad" int2 NOT NULL,
"cod_depto" int4,
"des_nbusuario" varchar(32) COLLATE "default" NOT NULL,
"des_correo" varchar(50) COLLATE "default" NOT NULL,
"des_nombre" varchar(70) COLLATE "default" NOT NULL,
"des_contrasena" varchar(30) COLLATE "default" NOT NULL,
"cnu_activo" int2 NOT NULL,
"des_dirhost" varchar(125) COLLATE "default" NOT NULL,
"cod_protocolo" "sgrt"."protocolo" NOT NULL DEFAULT 'POP'::sgrt.protocolo,
"cod_encriptacion" "sgrt"."encriptacion" NOT NULL DEFAULT 'NONE'::sgrt.encriptacion,
"cod_puerto" int4,
"cnu_frecsinc" int2 NOT NULL,
"cnu_nummaxcorreo" int2 NOT NULL,
"cnu_eliminar" int2 NOT NULL,
"cnu_errores" int2 NOT NULL,
"fec_ulterror" timestamp(6),
"fec_ultsincr" timestamp(6),
"cnu_smtpactivo" int2,
"des_smtphost" varchar(125) COLLATE "default" NOT NULL,
"cod_smtpport" int4,
"cnu_smtpsecure" int2 NOT NULL,
"cnu_smtpauth" int2 NOT NULL,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
"cod_usuario" int4 NOT NULL,
CONSTRAINT "tsgrtcorreo_pkey" PRIMARY KEY ("cod_correo") ,
CONSTRAINT "cod_correo" UNIQUE ("cod_correo"),
CONSTRAINT "tsgrtcorreo_des_correo_key" UNIQUE ("des_correo")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtcorreo" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtdatossolicitud" (
"cod_datosolicitud" int4 NOT NULL,
"cod_elemento" int4 NOT NULL,
"des_descripcion" varchar(45) COLLATE "default" NOT NULL,
"cod_solicitud" int4 NOT NULL,
"cod_edosolicitud" int4 NOT NULL,
CONSTRAINT "tsgrtdatossolicitud_pkey" PRIMARY KEY ("cod_datosolicitud") ,
CONSTRAINT "cod_datosolicitud" UNIQUE ("cod_datosolicitud")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtdatossolicitud" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtdepartamento" (
"cod_depto" int4 NOT NULL DEFAULT nextval('sgrt.seq_depto'::regclass),
"cod_plantillacorreo" int4 NOT NULL,
"cod_correo" int4 NOT NULL,
"cod_manager" int4 NOT NULL DEFAULT 0,
"des_nombre" varchar(50) COLLATE "default" NOT NULL,
"cod_ncorto" varchar(10) COLLATE "default" NOT NULL,
"des_firma" text COLLATE "default" NOT NULL,
"cnu_publico" int2 NOT NULL,
"cod_sistemasuite" int4 NOT NULL DEFAULT 1,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtdepartamento_pkey" PRIMARY KEY ("cod_depto") ,
CONSTRAINT "cod_depto" UNIQUE ("cod_depto"),
CONSTRAINT "tsgrtdepartamento_des_nombre_key" UNIQUE ("des_nombre")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtdepartamento" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtedosolicitudes" (
"cod_edosolicitud" int4 NOT NULL,
"cod_nbedosolicitud" int4 NOT NULL,
CONSTRAINT "tsgrtedosolicitudes_pkey" PRIMARY KEY ("cod_edosolicitud") ,
CONSTRAINT "cod_edosolicitud" UNIQUE ("cod_edosolicitud")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtedosolicitudes" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtelementos" (
"cod_elemento" int4 NOT NULL DEFAULT nextval('sgrt.seq_elemento'::regclass),
"des_nombre" varchar(50) COLLATE "default" NOT NULL,
"cnu_activo" int2 NOT NULL,
CONSTRAINT "tsgrtelementos_pkey" PRIMARY KEY ("cod_elemento") ,
CONSTRAINT "cod_elemento" UNIQUE ("cod_elemento")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtelementos" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtestados" (
"cod_estadorep" int4 NOT NULL DEFAULT nextval('sgrt.seq_estadorep'::regclass),
"des_nbestado" varchar(60) COLLATE "default" NOT NULL,
CONSTRAINT "tsgrtestados_pkey" PRIMARY KEY ("cod_estadorep") ,
CONSTRAINT "cod_estadorep" UNIQUE ("cod_estadorep")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtestados" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtfaq" (
"cod_faq" int4 NOT NULL DEFAULT nextval('sgrt.seq_faq'::regclass),
"cod_categoriafaq" int4 NOT NULL,
"des_pregunta" varchar(255) COLLATE "default" NOT NULL,
"cnu_activo" int2 NOT NULL,
"des_respuesta" varchar(255) COLLATE "default" NOT NULL,
"des_notasint" varchar(255) COLLATE "default" NOT NULL,
"fec_ultactualizacion" timestamp(6) NOT NULL,
"fec_creacion" timestamp(6) NOT NULL,
"cod_creadopor" int4 NOT NULL,
"cod_ultactualizacionpor" int4 NOT NULL,
CONSTRAINT "tsgrtfaq_pkey" PRIMARY KEY ("cod_faq") ,
CONSTRAINT "cod_categofaq" UNIQUE ("cod_categoriafaq"),
CONSTRAINT "cod_faq" UNIQUE ("cod_faq")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtfaq" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtgrupo" (
"cod_grupo" int4 NOT NULL DEFAULT nextval('sgrt.seq_grupo'::regclass),
"cnu_activo" int2 NOT NULL,
"des_nombre" varchar(50) COLLATE "default" NOT NULL,
"cnu_crear" int2 NOT NULL,
"cnu_editar" int2 NOT NULL,
"cnu_borrar" int2 NOT NULL,
"cnu_cerrar" int2 NOT NULL,
"cnu_transferir" int2 NOT NULL,
"cnu_prohibir" int2 NOT NULL,
"cnu_administrar" int2 NOT NULL,
"cod_sistemasuite" int4 NOT NULL DEFAULT 1,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtgrupo_pkey" PRIMARY KEY ("cod_grupo") ,
CONSTRAINT "cod_grupo" UNIQUE ("cod_grupo")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtgrupo" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtinvitados" (
"cod_invitado" int4 NOT NULL DEFAULT nextval('sgrt.seq_invitado'::regclass),
"cod_reunion" int4 NOT NULL,
"des_nombre" varchar COLLATE "default",
"des_correo" varchar COLLATE "default",
"cnu_invitacionenv" int2 NOT NULL,
"cnu_asiste" int2 NOT NULL,
"cod_empleado" int4,
"des_empresa" varchar COLLATE "default",
CONSTRAINT "tsgrtinvitados_pkey" PRIMARY KEY ("cod_invitado") ,
CONSTRAINT "cod_invitado" UNIQUE ("cod_invitado")
)
WITHOUT OIDS;
CREATE INDEX "fki_fk_cod_empleado" ON "sgrt"."tsgrtinvitados" USING btree ("cod_empleado" "pg_catalog"."int4_ops" ASC NULLS LAST);
ALTER TABLE "sgrt"."tsgrtinvitados" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtlugares" (
"cod_lugar" int4 NOT NULL DEFAULT nextval('sgrt.seq_lugar'::regclass),
"des_nombre" varchar(60) COLLATE "default" NOT NULL,
"cod_ciudad" int4 NOT NULL,
CONSTRAINT "tsgrtlugares_pkey" PRIMARY KEY ("cod_lugar") ,
CONSTRAINT "cod_lugar" UNIQUE ("cod_lugar")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtlugares" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtmsjticket" (
"cod_mensaje" int4 NOT NULL DEFAULT nextval('sgrt.seq_mensaje'::regclass),
"cod_ticket" int4 NOT NULL,
"cod_usuario" int4 NOT NULL DEFAULT 0,
"des_mensaje" text COLLATE "default" NOT NULL,
"cod_fuente" varchar(16) COLLATE "default" DEFAULT NULL::character varying,
"cod_sistemasuite" int4 NOT NULL DEFAULT 1,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtmsjticket_pkey" PRIMARY KEY ("cod_mensaje") ,
CONSTRAINT "cod_mensaje" UNIQUE ("cod_mensaje")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtmsjticket" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtnota" (
"cod_nota" int4 NOT NULL DEFAULT nextval('sgrt.seq_nota'::regclass),
"cod_ticket" int4 NOT NULL,
"cod_empleado" int4 NOT NULL,
"des_fuente" varchar(32) COLLATE "default" NOT NULL,
"des_titulo" varchar(255) COLLATE "default" NOT NULL DEFAULT 'Nota INTEGERerna Generica'::character varying,
"des_nota" text COLLATE "default" NOT NULL,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtnota_pkey" PRIMARY KEY ("cod_nota") ,
CONSTRAINT "cod_nota" UNIQUE ("cod_nota")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtnota" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtplantillacorreos" (
"cod_plantillacorreo" int4 NOT NULL DEFAULT nextval('sgrt.seq_plantillacorreo'::regclass),
"des_nombre" varchar(32) COLLATE "default" NOT NULL,
"des_notas" text COLLATE "default",
"cod_tipodestinario" "sgrt"."destinatario" DEFAULT 'USR'::sgrt.destinatario,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
"des_asunto" varchar(45) COLLATE "default",
"des_cuerpo" varchar(255) COLLATE "default",
CONSTRAINT "tsgrtplantillacorreos_pkey" PRIMARY KEY ("cod_plantillacorreo") ,
CONSTRAINT "cod_plantillacorreo" UNIQUE ("cod_plantillacorreo")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtplantillacorreos" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtprioridad" (
"cod_prioridad" int4 NOT NULL DEFAULT nextval('sgrt.seq_prioridad'::regclass),
"des_nombre" varchar(60) COLLATE "default" NOT NULL,
"des_descripcion" varchar(30) COLLATE "default" NOT NULL,
"cod_color" varchar(7) COLLATE "default" NOT NULL,
"cnu_valprioridad" int2 NOT NULL,
"cnu_publica" int2 NOT NULL,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtprioridad_pkey" PRIMARY KEY ("cod_prioridad") ,
CONSTRAINT "cod_prioridad" UNIQUE ("cod_prioridad"),
CONSTRAINT "tsgrtprioridad_des_nombre_key" UNIQUE ("des_nombre")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtprioridad" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtresppredefinida" (
"cod_respuesta" int4 NOT NULL DEFAULT nextval('sgrt.seq_resp'::regclass),
"cod_depto" int4 NOT NULL,
"cnu_activo" int2 NOT NULL,
"des_titulo" varchar(125) COLLATE "default" NOT NULL,
"des_respuesta" text COLLATE "default" NOT NULL,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtresppredefinida_pkey" PRIMARY KEY ("cod_respuesta") ,
CONSTRAINT "cod_respuestapredf" UNIQUE ("cod_respuesta"),
CONSTRAINT "tsgrtresppredefinida_des_titulo_key" UNIQUE ("des_titulo")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtresppredefinida" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtrespuesta" (
"cod_respuesta" int4 NOT NULL DEFAULT nextval('sgrt.seq_respuesta'::regclass),
"cod_mensaje" int4 NOT NULL,
"cod_ticket" int4 NOT NULL,
"cod_empleado" int4 NOT NULL,
"des_respuesta" text COLLATE "default" NOT NULL,
"cod_sistemasuite" int4 NOT NULL,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
CONSTRAINT "tsgrtrespuesta_pkey" PRIMARY KEY ("cod_respuesta") ,
CONSTRAINT "cod_respuesta" UNIQUE ("cod_respuesta")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtrespuesta" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtreuniones" (
"cod_reunion" int4 NOT NULL DEFAULT nextval('sgrt.seq_reunion'::regclass),
"des_nombre" varchar(45) COLLATE "default" NOT NULL,
"fec_fecha" date NOT NULL,
"des_objetivo" varchar(700) COLLATE "default" NOT NULL,
"cod_lugar" int4 NOT NULL,
"cod_responsable" int4 NOT NULL,
"cod_proximareunion" int4,
"cod_creadorreunion" int4 NOT NULL,
"tim_duracion" time(6),
"tim_hora" time(6),
CONSTRAINT "tsgrtreuniones_pkey" PRIMARY KEY ("cod_reunion") ,
CONSTRAINT "cod_reuniones" UNIQUE ("cod_reunion")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtreuniones" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtservicios" (
"cod_servicio" int4 NOT NULL DEFAULT nextval('sgrt.seq_servicio'::regclass),
"des_nombre_servicio" varchar(45) COLLATE "default" NOT NULL,
"des_descripcion" varchar(100) COLLATE "default" NOT NULL,
"fec_contratacion" date NOT NULL,
CONSTRAINT "tsgrtservicios_pkey" PRIMARY KEY ("cod_servicio") ,
CONSTRAINT "cod_servicio" UNIQUE ("cod_servicio")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtservicios" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtsolicitudservicios" (
"cod_solicitud" int4 NOT NULL DEFAULT nextval('sgrt.seq_solicitud'::regclass),
"cod_empleado" int4 NOT NULL,
"cod_ticket" int4 NOT NULL,
"cod_servicio" int4 NOT NULL,
CONSTRAINT "tsgrtsolicitudservicios_pkey" PRIMARY KEY ("cod_solicitud") ,
CONSTRAINT "cod_solicitud" UNIQUE ("cod_solicitud")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtsolicitudservicios" OWNER TO "postgres";

CREATE TABLE "sgrt"."tsgrtticket" (
"cod_ticket" int4 NOT NULL DEFAULT nextval('sgrt.seq_ticket'::regclass),
"des_folio" varchar(45) COLLATE "default" NOT NULL,
"cod_reunion" int4,
"cod_acuerdo" int4,
"cod_responsable" int4,
"cod_validador" int4,
"cod_depto" int4 NOT NULL,
"cod_prioridad" int2 NOT NULL,
"cod_topico" int4 NOT NULL,
"cod_empleado" int4 NOT NULL,
"des_correo" varchar(50) COLLATE "default" NOT NULL,
"des_nombre" varchar(50) COLLATE "default" NOT NULL,
"des_tema" varchar(64) COLLATE "default" NOT NULL DEFAULT '[Sin Asunto]'::character varying,
"des_temaayuda" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
"cod_telefono" varchar(16) COLLATE "default" DEFAULT NULL::character varying,
"cod_extension" varchar(8) COLLATE "default" DEFAULT NULL::character varying,
"cod_estadot" "sgrt"."edoticket" NOT NULL DEFAULT 'Abierto'::sgrt.edoticket,
"cod_origent" "sgrt"."origencontac" NOT NULL DEFAULT 'Otro'::sgrt.origencontac,
"cnu_expirado" int2 NOT NULL,
"cnu_atendido" int2 NOT NULL,
"fec_exp" timestamp(6),
"fec_reap" timestamp(6),
"fec_cierre" timestamp(6),
"fec_ultimomsg" timestamp(6),
"fec_ultimaresp" timestamp(6),
"cod_sistemasuite" int4 NOT NULL DEFAULT 1,
"fec_ultactualizacion" timestamp(6),
"cod_ultactualizacionpor" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"fec_creacion" timestamp(6),
"cod_ejecutor" int4,
CONSTRAINT "tsgrtticket_pkey" PRIMARY KEY ("cod_ticket") ,
CONSTRAINT "cod_deptoticket" UNIQUE ("cod_depto"),
CONSTRAINT "cod_empleado" UNIQUE ("cod_empleado"),
CONSTRAINT "cod_ticket" UNIQUE ("cod_ticket"),
CONSTRAINT "tsgrtticket_des_correo_key" UNIQUE ("des_correo"),
CONSTRAINT "tsgrtticket_des_folio_key" UNIQUE ("des_folio")
)
WITHOUT OIDS;
ALTER TABLE "sgrt"."tsgrtticket" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatasignaciones" (
"cod_asignacion" int4 NOT NULL DEFAULT nextval('sisat.seq_asignaciones'::regclass),
"cod_prospecto" int4 NOT NULL,
"cod_perfil" int4 NOT NULL,
"cod_cliente" int4 NOT NULL,
"des_correocte" varchar(40) COLLATE "default" NOT NULL,
"cod_telefonocte" varchar(16) COLLATE "default" NOT NULL,
"des_direccioncte" varchar(200) COLLATE "default",
"cod_empleado" int4,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatasignaciones_pkey" PRIMARY KEY ("cod_asignacion") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatasignaciones" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatcandidatos" (
"cod_candidato" int4 NOT NULL DEFAULT nextval('sisat.seq_candidatos'::regclass),
"des_nombre" varchar(50) COLLATE "default" NOT NULL,
"cod_perfil" int4 NOT NULL,
"imp_sueldo" numeric(6,2),
"imp_sueldodia" numeric(6,2),
"imp_nominaimss" numeric(6,2),
"imp_honorarios" numeric(6,2),
"imp_cargasocial" numeric(6,2),
"imp_prestaciones" numeric(6,2),
"imp_viaticos" numeric(6,2),
"imp_subtotalcandidato" numeric(6,2),
"imp_costoadmin" numeric(6,2),
"cnu_financiamiento" int2,
"imp_isr" numeric(6,2),
"imp_financiamiento" numeric(6,2),
"imp_adicionales" numeric(6,2),
"imp_subtotaladmin1" numeric(6,2),
"imp_comisiones" numeric(6,2),
"imp_otrosgastos" numeric(6,2),
"imp_subtotaladmin2" numeric(6,2),
"imp_total" numeric(6,2),
"imp_iva" numeric(6,2),
"por_utilidad" numeric(4,2),
"imp_utilidad" numeric(6,2),
"imp_tarifa" numeric(6,2),
CONSTRAINT "tsisatcandidatos_pkey" PRIMARY KEY ("cod_candidato") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatcandidatos" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatcartaaceptacion" (
"cod_aceptacion" int4 NOT NULL DEFAULT nextval('sisat.seq_aceptaciones'::regclass),
"des_objetivo" varchar(200) COLLATE "default" NOT NULL,
"txt_oferta" text COLLATE "default",
"des_esquema" varchar(30) COLLATE "default" NOT NULL,
"tim_jornada" time(6),
"txt_especificaciones" text COLLATE "default",
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatcartaaceptacion_pkey" PRIMARY KEY ("cod_aceptacion") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatcartaaceptacion" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatcartaasignacion" (
"cod_asignacion" int4 NOT NULL DEFAULT nextval('sisat.seq_cartaasignaciones'::regclass),
"cod_prospecto" int4 NOT NULL,
"cod_perfil" int4 NOT NULL,
"des_actividades" varchar(200) COLLATE "default",
"des_lugarsalida" varchar(100) COLLATE "default",
"des_lugarllegada" varchar(100) COLLATE "default",
"fec_salida" date,
"fec_llegada" date,
"cod_transporte" varchar(20) COLLATE "default",
"des_lugarhopedaje" varchar(60) COLLATE "default",
"fec_hospedaje" date,
"des_computadora" varchar(150) COLLATE "default",
"cod_telefono" varchar(16) COLLATE "default",
"des_accesorios" varchar(150) COLLATE "default",
"des_nbresponsable" varchar(50) COLLATE "default",
"des_nbpuesto" varchar(50) COLLATE "default",
"des_lugarresp" varchar(100) COLLATE "default",
"cod_telefonoresp" varchar(16) COLLATE "default",
"tim_horario" time(6),
"fec_iniciocontra" date,
"fec_terminocontra" date,
"imp_sueldomensual" numeric(6,2),
"imp_nominaimss" numeric(6,2),
"imp_honorarios" numeric(6,2),
"imp_otros" numeric(6,2),
"cod_rfc" varchar(13) COLLATE "default",
"des_razonsocial" varchar(45) COLLATE "default",
"des_correo" varchar(50) COLLATE "default",
"cod_cpostal" int4,
"des_direccionfact" varchar(200) COLLATE "default",
"cod_cliente" int4 NOT NULL,
"cod_gpy" int4,
"cod_rhta" int4,
"cod_ape" int4,
"cod_rys" int4,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatcartaasignacion_pkey" PRIMARY KEY ("cod_asignacion") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatcartaasignacion" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatcotizaciones" (
"cod_cotizacion" int4 NOT NULL DEFAULT nextval('sisat.seq_cotizaciones'::regclass),
"des_nbciudad" varchar(20) COLLATE "default",
"des_nbestado" varchar(20) COLLATE "default",
"fec_fecha" date,
"des_nbcontacto" varchar(50) COLLATE "default",
"cod_puesto" int4,
"des_compania" varchar(50) COLLATE "default",
"des_nbservicio" varchar(50) COLLATE "default",
"cnu_cantidad" int2,
"txt_concepto" text COLLATE "default",
"imp_inversionhr" numeric(6,2),
"txt_condicionescomer" text COLLATE "default",
"des_nbatentamente" varchar(60) COLLATE "default",
"des_correoatentamente" varchar(50) COLLATE "default",
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatcotizaciones_pkey" PRIMARY KEY ("cod_cotizacion") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatcotizaciones" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatcursosycerticados" (
"cod_curso" int4 NOT NULL DEFAULT nextval('sisat.seq_cursos'::regclass),
"cod_prospecto" int4 NOT NULL,
"des_curso" varchar(100) COLLATE "default" NOT NULL,
"des_institucion" varchar(70) COLLATE "default" NOT NULL,
"fec_termino" date NOT NULL,
CONSTRAINT "tsisatcursosycerticados_pkey" PRIMARY KEY ("cod_curso") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatcursosycerticados" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatentrevistas" (
"cod_entrevista" int4 NOT NULL DEFAULT nextval('sisat.seq_entrevistas'::regclass),
"des_nbentrevistador" varchar(90) COLLATE "default" NOT NULL,
"des_puesto" varchar(50) COLLATE "default" NOT NULL,
"des_correoent" varchar(40) COLLATE "default" NOT NULL,
"cod_telefonoent" varchar(16) COLLATE "default" NOT NULL,
"des_direccionent" varchar(200) COLLATE "default",
"tim_horarioent" time(6),
"fec_fechaent" date NOT NULL,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatentrevistas_pkey" PRIMARY KEY ("cod_entrevista") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatentrevistas" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatenviocorreos" (
"cod_envio" int4 NOT NULL DEFAULT nextval('sisat.seq_envios'::regclass),
"des_destinatario" varchar(90) COLLATE "default" NOT NULL,
"des_asunto" varchar(50) COLLATE "default" NOT NULL,
"des_mensaje" varchar(200) COLLATE "default" NOT NULL,
"bin_adjunto" bytea,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatenviocorreos_pkey" PRIMARY KEY ("cod_envio") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatenviocorreos" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatescolaridad" (
"cod_escolaridad" int4 NOT NULL DEFAULT nextval('sisat.seq_escolaridad'::regclass),
"cod_prospecto" int4 NOT NULL,
"des_escolaridad" varchar(45) COLLATE "default" NOT NULL,
"des_escuela" varchar(70) COLLATE "default" NOT NULL,
"fec_inicio" date NOT NULL,
"fec_termino" date NOT NULL,
"cod_estatus" varchar(20) COLLATE "default",
CONSTRAINT "tsisatescolaridad_pkey" PRIMARY KEY ("cod_escolaridad") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatescolaridad" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatexperienciaslaborales" (
"cod_experiencia" int4 NOT NULL DEFAULT nextval('sisat.seq_experiencias'::regclass),
"cod_prospecto" int4 NOT NULL,
"des_empresa" varchar(50) COLLATE "default" NOT NULL,
"des_puesto" varchar(40) COLLATE "default" NOT NULL,
"fec_inicio" date,
"fec_termino" date,
"des_ubicacion" varchar(70) COLLATE "default",
"txt_funciones" text COLLATE "default",
"des_nbcliente" varchar(70) COLLATE "default",
"des_proyecto" varchar(70) COLLATE "default",
"txt_logros" varchar(300) COLLATE "default",
CONSTRAINT "tsisatexperienciaslaborales_pkey" PRIMARY KEY ("cod_experiencia") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatexperienciaslaborales" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatfirmas" (
"cod_firma" int4 NOT NULL DEFAULT nextval('sisat.seq_firmas'::regclass),
"cod_solicita" int4,
"cod_puestosolicita" int4,
"cod_autoriza" int4,
"cod_puestoautoriza" int4,
"cod_contratacion" int4 NOT NULL,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatfirmas_pkey" PRIMARY KEY ("cod_firma") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatfirmas" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisathabilidades" (
"cod_habilidad" int4 NOT NULL DEFAULT nextval('sisat.seq_habilidades'::regclass),
"cod_prospecto" int4 NOT NULL,
"des_habilidad1" varchar(50) COLLATE "default",
"des_habilidad2" varchar(50) COLLATE "default",
"des_habilidad3" varchar(50) COLLATE "default",
"des_habilidad4" varchar(50) COLLATE "default",
"des_habilidad5" varchar(50) COLLATE "default",
"des_habilidad6" varchar(50) COLLATE "default",
"des_habilidad7" varchar(50) COLLATE "default",
"des_habilidad8" varchar(50) COLLATE "default",
"des_habilidad9" varchar(50) COLLATE "default",
"des_habilidad10" varchar(50) COLLATE "default",
"des_habilidad11" varchar(50) COLLATE "default",
"des_habilidad12" varchar(50) COLLATE "default",
"des_habilidad13" varchar(50) COLLATE "default",
"des_habilidad14" varchar(50) COLLATE "default",
"des_habilidad15" varchar(50) COLLATE "default",
"des_habilidad16" varchar(50) COLLATE "default",
"des_habilidad17" varchar(50) COLLATE "default",
"des_habilidad18" varchar(50) COLLATE "default",
"des_habilidad19" varchar(50) COLLATE "default",
"des_habilidad20" varchar(50) COLLATE "default",
"des_habilidad21" varchar(50) COLLATE "default",
"des_habilidad22" varchar(50) COLLATE "default",
"des_habilidad23" varchar(50) COLLATE "default",
"des_habilidad24" varchar(50) COLLATE "default",
"des_habilidad25" varchar(50) COLLATE "default",
"des_habilidad26" varchar(50) COLLATE "default",
"des_habilidad27" varchar(50) COLLATE "default",
"des_habilidad28" varchar(50) COLLATE "default",
"des_habilidad29" varchar(50) COLLATE "default",
"des_habilidad30" varchar(50) COLLATE "default",
"des_dominio1" int4 DEFAULT 0,
"des_dominio2" int4 DEFAULT 0,
"des_dominio3" int4 DEFAULT 0,
"des_dominio4" int4 DEFAULT 0,
"des_dominio5" int4 DEFAULT 0,
"des_dominio6" int4 DEFAULT 0,
"des_dominio7" int4 DEFAULT 0,
"des_dominio8" int4 DEFAULT 0,
"des_dominio9" int4 DEFAULT 0,
"des_dominio10" int4 DEFAULT 0,
"des_dominio11" int4 DEFAULT 0,
"des_dominio12" int4 DEFAULT 0,
"des_dominio13" int4 DEFAULT 0,
"des_dominio14" int4 DEFAULT 0,
"des_dominio15" int4 DEFAULT 0,
"des_dominio16" int4 DEFAULT 0,
"des_dominio17" int4 DEFAULT 0,
"des_dominio18" int4 DEFAULT 0,
"des_dominio19" int4 DEFAULT 0,
"des_dominio20" int4 DEFAULT 0,
"des_dominio21" int4 DEFAULT 0,
"des_dominio22" int4 DEFAULT 0,
"des_dominio23" int4 DEFAULT 0,
"des_dominio24" int4 DEFAULT 0,
"des_dominio25" int4 DEFAULT 0,
"des_dominio26" int4 DEFAULT 0,
"des_dominio27" int4 DEFAULT 0,
"des_dominio28" int4 DEFAULT 0,
"des_dominio29" int4 DEFAULT 0,
"des_dominio30" int4 DEFAULT 0,
CONSTRAINT "tsisathabilidades_pkey" PRIMARY KEY ("cod_habilidad") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisathabilidades" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatidiomas" (
"cod_idioma" int4 NOT NULL DEFAULT nextval('sisat.seq_idiomas'::regclass),
"cod_prospecto" int4 NOT NULL,
"cod_nbidioma" varchar(20) COLLATE "default" NOT NULL,
"cod_nivel" varchar(20) COLLATE "default" NOT NULL,
"des_certificado" varchar(40) COLLATE "default",
CONSTRAINT "tsisatidiomas_pkey" PRIMARY KEY ("cod_idioma") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatidiomas" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatordenservicio" (
"cod_ordenservicio" int4 NOT NULL DEFAULT nextval('sisat.seq_ordenservicios'::regclass),
"cod_estadorep" int4 NOT NULL,
"cod_ciudad" int4 NOT NULL,
"fec_fecha" date NOT NULL,
"des_nbcontacto" varchar(50) COLLATE "default",
"cod_puesto" int4,
"des_nbcompania" varchar(50) COLLATE "default",
"des_nbservicio" varchar(60) COLLATE "default",
"cnu_cantidad" int2,
"txt_concepto" text COLLATE "default",
"imp_inversionhr" numeric(6,2),
"txt_condicionescomer" text COLLATE "default",
"des_ubcnconsultor" varchar(100) COLLATE "default",
"fec_finservicio" date,
"cod_gpy" int4 NOT NULL,
"des_correogpy" varchar(50) COLLATE "default" NOT NULL,
"cod_cliente" int4 NOT NULL,
"des_correoclte" varchar(50) COLLATE "default" NOT NULL,
"des_empresaclte" varchar(50) COLLATE "default",
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatordenservicio_pkey" PRIMARY KEY ("cod_ordenservicio") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatordenservicio" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatprospectos" (
"cod_prospecto" int4 NOT NULL DEFAULT nextval('sisat.seq_prospectos'::regclass),
"des_nombre" varchar(45) COLLATE "default" NOT NULL,
"des_nombres" varchar(60) COLLATE "default",
"des_appaterno" varchar(40) COLLATE "default" NOT NULL,
"des_apmaterno" varchar(40) COLLATE "default",
"des_lugarnacimiento" varchar(50) COLLATE "default" NOT NULL,
"fec_nacimiento" date NOT NULL,
"cod_edad" int4 NOT NULL,
"cod_edocivil" varchar(15) COLLATE "default" NOT NULL,
"des_nbpadre" varchar(70) COLLATE "default",
"des_nbmadre" varchar(70) COLLATE "default",
"cod_numhermanos" int4,
"des_nbcalle" varchar(60) COLLATE "default",
"cod_numcasa" int4,
"des_colonia" varchar(60) COLLATE "default",
"des_localidad" varchar(60) COLLATE "default",
"des_municipio" varchar(60) COLLATE "default",
"des_estado" varchar(60) COLLATE "default",
"cod_cpostal" int4,
"cod_tiposangre" varchar(5) COLLATE "default",
"des_emailmbn" varchar(40) COLLATE "default",
"des_emailpersonal" varchar(40) COLLATE "default",
"des_pasatiempo" varchar(200) COLLATE "default",
"cod_telefonocasa" varchar(16) COLLATE "default" DEFAULT NULL::character varying,
"cod_telefonomovil" varchar(16) COLLATE "default" DEFAULT NULL::character varying,
"cod_rfc" varchar(13) COLLATE "default",
"cod_nss" varchar(20) COLLATE "default",
"cod_curp" varchar(18) COLLATE "default" NOT NULL,
"des_nacionalidad" varchar(30) COLLATE "default",
"cod_administrador" int4,
"fec_fechacoment" date,
"txt_comentarios" text COLLATE "default",
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatprospectos_pkey" PRIMARY KEY ("cod_prospecto") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatprospectos" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatproyectos" (
"cod_proyecto" int4 NOT NULL DEFAULT nextval('sisat.seq_proyectos'::regclass),
"cod_prospecto" int4 NOT NULL,
"cod_perfil" int4 NOT NULL,
"des_nbcliente" varchar(50) COLLATE "default" NOT NULL,
"des_nbresponsable" varchar(50) COLLATE "default" NOT NULL,
"des_correo" varchar(50) COLLATE "default" NOT NULL,
"cod_telefono" varchar(16) COLLATE "default" NOT NULL,
"des_direccion" varchar(200) COLLATE "default" NOT NULL,
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatproyectos_pkey" PRIMARY KEY ("cod_proyecto") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatproyectos" OWNER TO "postgres";

CREATE TABLE "sisat"."tsisatvacantes" (
"cod_vacante" int4 NOT NULL DEFAULT nextval('sisat.seq_vacantes'::regclass),
"des_rqvacante" varchar(200) COLLATE "default" NOT NULL,
"cnu_anexperiencia" int2,
"txt_experiencia" text COLLATE "default",
"des_escolaridad" varchar(50) COLLATE "default",
"txt_herramientas" text COLLATE "default",
"txt_habilidades" text COLLATE "default",
"des_lugartrabajo" varchar(100) COLLATE "default",
"imp_sueldo" numeric(6,2),
"cod_creadopor" int4 NOT NULL,
"cod_modificadopor" int4 NOT NULL,
"fec_creacion" date NOT NULL,
"fec_modificacion" date NOT NULL,
CONSTRAINT "tsisatvacantes_pkey" PRIMARY KEY ("cod_vacante") 
)
WITHOUT OIDS;
ALTER TABLE "sisat"."tsisatvacantes" OWNER TO "postgres";


ALTER TABLE "sgrh"."tsgrhareas" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhasignacionesemp" ADD CONSTRAINT "fk_codasignadopor_asignacionesempleados" FOREIGN KEY ("cod_asignadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhasignacionesemp" ADD CONSTRAINT "fk_codprospecto_asignacionesempleados" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhasignacionesemp" ADD CONSTRAINT "fk_codpuesto_asignacionesempleados" FOREIGN KEY ("cod_puesto") REFERENCES "sgrh"."tsgrhpuestos" ("cod_puesto") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhcapacitaciones" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_ape" FOREIGN KEY ("cod_ape") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_cliente" FOREIGN KEY ("cod_cliente") REFERENCES "sgrh"."tsgrhclientes" ("cod_cliente") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_gpy" FOREIGN KEY ("cod_gpy") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_perfil" FOREIGN KEY ("cod_perfil") REFERENCES "sgrh"."tsgrhperfiles" ("cod_perfil") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_rhat" FOREIGN KEY ("cod_rhta") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcartaasignacion" ADD CONSTRAINT "fk_cod_rys" FOREIGN KEY ("cod_rys") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcontrataciones" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcontrataciones" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcontratos" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhcontratos" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhempleados" ADD CONSTRAINT "fk_cod_area" FOREIGN KEY ("cod_area") REFERENCES "sgrh"."tsgrhareas" ("cod_area") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhempleados" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhempleados" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhempleados" ADD CONSTRAINT "fk_cod_puesto" FOREIGN KEY ("cod_puesto") REFERENCES "sgrh"."tsgrhpuestos" ("cod_puesto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhempleados" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhencuesta" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhencuesta" ADD CONSTRAINT "fk_cod_lugar" FOREIGN KEY ("cod_lugar") REFERENCES "sgrt"."tsgrtlugares" ("cod_lugar") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhencuesta" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhencuesta_participantes" ADD CONSTRAINT "tsgrhencuesta_participantes_cod_empleado_fkey" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhencuesta_participantes" ADD CONSTRAINT "tsgrhencuesta_participantes_cod_encuesta_fkey" FOREIGN KEY ("cod_encuesta") REFERENCES "sgrh"."tsgrhencuesta" ("cod_encuesta") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhencuesta_participantes" ADD CONSTRAINT "tsgrhencuesta_participantes_cod_pregunta_fkey" FOREIGN KEY ("cod_pregunta") REFERENCES "sgrh"."tsgrhpreguntasenc" ("cod_pregunta") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhencuesta_participantes" ADD CONSTRAINT "tsgrhencuesta_participantes_cod_respuesta_fkey" FOREIGN KEY ("cod_respuesta") REFERENCES "sgrh"."tsgrhrespuestasenc" ("cod_respuesta") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhescolaridad" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhestatuscapacitacion" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhestatuscapacitacion" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhevacapacitacion" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhevacapacitacion" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhevacapacitacion" ADD CONSTRAINT "fk_codempleado_evacapacitacionesemp" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhevacapacitacion" ADD CONSTRAINT "fk_plancapacitacion_evacapacitacionesemp" FOREIGN KEY ("cod_plancapacitacion") REFERENCES "sgrh"."tsgrhplancapacitacion" ("cod_plancapacitacion") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhevacontestadas" ADD CONSTRAINT "fk_cod_evaluacion" FOREIGN KEY ("cod_evaluacion") REFERENCES "sgrh"."tsgrhevaluaciones" ("cod_evaluacion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhevacontestadas" ADD CONSTRAINT "fk_cod_evaluado" FOREIGN KEY ("cod_evaluado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhevacontestadas" ADD CONSTRAINT "fk_cod_evaluador" FOREIGN KEY ("cod_evaluador") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhevaluaciones" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhevaluaciones" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhexperienciaslaborales" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhlogistica" ADD CONSTRAINT "cod_capacitacion_fk" FOREIGN KEY ("cod_capacitacion") REFERENCES "sgrh"."tsgrhplancapacitacion" ("cod_plancapacitacion") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhlogistica" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhlogistica" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhlugares" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhlugares" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhmodo" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhmodo" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "plancap_tipocapacitacion_fk" FOREIGN KEY ("cod_tipocapacitacion") REFERENCES "sgrh"."tsgrhtipocapacitacion" ("cod_tipocapacitacion") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "plancapacitacion_estatus_fk" FOREIGN KEY ("cod_estatus") REFERENCES "sgrh"."tsgrhestatuscapacitacion" ("cod_estatus") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "plancapacitacion_lugar_fk" FOREIGN KEY ("cod_lugar") REFERENCES "sgrh"."tsgrhlugares" ("cod_lugar") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "plancapacitacion_modo_fk" FOREIGN KEY ("cod_modo") REFERENCES "sgrh"."tsgrhmodo" ("cod_modo") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "plancapacitacion_proceso_fk" FOREIGN KEY ("cod_proceso") REFERENCES "sgrh"."tsgrhprocesos" ("cod_proceso") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplancapacitacion" ADD CONSTRAINT "plancapacitacion_proveedor_fk" FOREIGN KEY ("cod_proveedor") REFERENCES "sgrh"."tsgrhproveedores" ("cod_proveedor") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhplanoperativo" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhplanoperativo" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhpreguntasenc" ADD CONSTRAINT "fk_cod_encuesta" FOREIGN KEY ("cod_encuesta") REFERENCES "sgrh"."tsgrhencuesta" ("cod_encuesta") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhpreguntaseva" ADD CONSTRAINT "fk_cod_evaluacion" FOREIGN KEY ("cod_evaluacion") REFERENCES "sgrh"."tsgrhevaluaciones" ("cod_evaluacion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhpreguntaseva" ADD CONSTRAINT "fk_cod_subfactor" FOREIGN KEY ("cod_subfactor") REFERENCES "sgrh"."tsgrhsubfactoreseva" ("cod_subfactor") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhprocesos" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhprocesos" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhproveedores" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhproveedores" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhpuestos" ADD CONSTRAINT "fk_cod_area" FOREIGN KEY ("cod_area") REFERENCES "sgrh"."tsgrhareas" ("cod_area") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrelacionroles" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhrelacionroles" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhrelacionroles" ADD CONSTRAINT "cod_plancapacitacion_fk" FOREIGN KEY ("cod_plancapacitacion") REFERENCES "sgrh"."tsgrhplancapacitacion" ("cod_plancapacitacion") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhrelacionroles" ADD CONSTRAINT "cod_rolempleado_fk" FOREIGN KEY ("cod_rolempleado") REFERENCES "sgrh"."tsgrhrolempleado" ("cod_rolempleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhrespuestasenc" ADD CONSTRAINT "fk_cod_catrespuesta" FOREIGN KEY ("cod_catrespuesta") REFERENCES "sgrh"."tsgrhcatrespuestas" ("cod_catrespuesta") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhrespuestasenc" ADD CONSTRAINT "fk_cod_pregunta" FOREIGN KEY ("cod_pregunta") REFERENCES "sgrh"."tsgrhpreguntasenc" ("cod_pregunta") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrespuestaseva" ADD CONSTRAINT "fk_cod_evacontestada" FOREIGN KEY ("cod_evacontestada") REFERENCES "sgrh"."tsgrhevacontestadas" ("cod_evacontestada") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrespuestaseva" ADD CONSTRAINT "fk_cod_pregunta" FOREIGN KEY ("cod_pregunta") REFERENCES "sgrh"."tsgrhpreguntaseva" ("cod_pregunta") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_lugar" FOREIGN KEY ("cod_lugar") REFERENCES "sgrt"."tsgrtlugares" ("cod_lugar") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_participante1" FOREIGN KEY ("cod_participante1") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_participante2" FOREIGN KEY ("cod_participante2") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_participante3" FOREIGN KEY ("cod_participante3") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_participante4" FOREIGN KEY ("cod_participante4") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_participante5" FOREIGN KEY ("cod_participante5") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrevplanoperativo" ADD CONSTRAINT "fk_cod_planoperativo" FOREIGN KEY ("cod_planoperativo") REFERENCES "sgrh"."tsgrhplanoperativo" ("cod_planoperativo") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhrolempleado" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhrolempleado" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhsubfactoreseva" ADD CONSTRAINT "fk_cod_factor" FOREIGN KEY ("cod_factor") REFERENCES "sgrh"."tsgrhfactoreseva" ("cod_factor") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhtipocapacitacion" ADD CONSTRAINT "cod_creadopor_fk" FOREIGN KEY ("aud_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhtipocapacitacion" ADD CONSTRAINT "cod_modificadopor_fk" FOREIGN KEY ("aud_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_evaluacion" FOREIGN KEY ("cod_evaluacion") REFERENCES "sgrh"."tsgrhevaluaciones" ("cod_evaluacion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_evaluado" FOREIGN KEY ("cod_evaluado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_evaluador" FOREIGN KEY ("cod_evaluador") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_lugar" FOREIGN KEY ("cod_lugar") REFERENCES "sgrt"."tsgrtlugares" ("cod_lugar") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_participante1" FOREIGN KEY ("cod_participante1") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_participante2" FOREIGN KEY ("cod_participante2") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_participante3" FOREIGN KEY ("cod_participante3") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrh"."tsgrhvalidaevaluaciondes" ADD CONSTRAINT "fk_cod_participante4" FOREIGN KEY ("cod_participante4") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomalguinaldo" ADD CONSTRAINT "nom_empleados_quincena_fk_aguinaldos" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnombitacora" ADD CONSTRAINT "nom_cat_tabla_id_fk_nom_cat_tablas" FOREIGN KEY ("cod_tablaid_fk") REFERENCES "sgnom"."tsgnomhisttabla" ("cod_tablaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "nom_cat_estatus_nomina_id_fk_cabeceras" FOREIGN KEY ("cod_estatusnomid_fk") REFERENCES "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "nom_cat_quincena_id_fk_cabeceras" FOREIGN KEY ("cod_quincenaid_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "nom_cat_tipo_nomina_id_fk_cabeceras" FOREIGN KEY ("cod_tiponominaid_fk") REFERENCES "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabeceraht" ADD CONSTRAINT "nom_cat_estatus_nomina_id_copia_fk_cabeceras" FOREIGN KEY ("cod_estatusnomid_fk") REFERENCES "sgnom"."tsgnomestatusnom" ("cod_estatusnomid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabeceraht" ADD CONSTRAINT "nom_cat_quincena_id_copia_fk_cabeceras" FOREIGN KEY ("cod_quincenaid_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcabeceraht" ADD CONSTRAINT "nom_cat_tipo_nomina_id_copia_fk_cabeceras" FOREIGN KEY ("cod_tiponominaid_fk") REFERENCES "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquinc" ADD CONSTRAINT "concepto_quincena_id_fk_conceptos_quincena" FOREIGN KEY ("cod_conceptoid_fk") REFERENCES "sgnom"."tsgnomconcepto" ("cod_conceptoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquinc" ADD CONSTRAINT "empleado_concepto_id_fk_conceptos_quincena" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquincht" ADD CONSTRAINT "concepto_quincena_id_copia_fk_conceptos_quincena" FOREIGN KEY ("cod_conceptoid_fk") REFERENCES "sgnom"."tsgnomconcepto" ("cod_conceptoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomcncptoquincht" ADD CONSTRAINT "empleado_concepto_id_copia_fk_conceptos_quincena" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_cat_clasificador_id_fk_cat_conceptos" FOREIGN KEY ("cod_clasificadorid_fk") REFERENCES "sgnom"."tsgnomclasificador" ("cod_clasificadorid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_concepto_sat_id_fk_cat_conceptos" FOREIGN KEY ("cod_conceptosatid_fk") REFERENCES "sgnom"."tsgnomconceptosat" ("cod_conceptosatid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_formula_id_fk_cat_conceptos" FOREIGN KEY ("cod_formulaid_fk") REFERENCES "sgnom"."tsgnomformula" ("cod_formulaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_tipo_calculo_id_fk_cat_conceptos" FOREIGN KEY ("cod_calculoid_fk") REFERENCES "sgnom"."tsgnomcalculo" ("cod_calculoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_tipo_concepto_id_fk_cat_conceptos" FOREIGN KEY ("cod_tipoconceptoid_fk") REFERENCES "sgnom"."tsgnomtipoconcepto" ("cod_tipoconceptoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "nom_tipo_nomina_id_fk_cat_conceptos" FOREIGN KEY ("cod_tiponominaid_fk") REFERENCES "sgnom"."tsgnomtiponomina" ("cod_tiponominaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomconfpago" ADD CONSTRAINT "nom_empleados_quincena_fk_conf_pago" FOREIGN KEY ("cod_empquincenaid_fk") REFERENCES "sgnom"."tsgnomempquincena" ("cod_empquincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincena" ADD CONSTRAINT "nom_cabecera_id_fk_empleados_quincena" FOREIGN KEY ("cod_cabeceraid_fk") REFERENCES "sgnom"."tsgnomcabecera" ("cod_cabeceraid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincena" ADD CONSTRAINT "nom_empleado_quincena_id_fk_empleados_quincena" FOREIGN KEY ("cod_empleadoid_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincenaht" ADD CONSTRAINT "nom_cabecera_id_fk_empleados_quincena_copia" FOREIGN KEY ("cod_cabeceraid_fk") REFERENCES "sgnom"."tsgnomcabecera" ("cod_cabeceraid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempquincenaht" ADD CONSTRAINT "nom_empleado_quincena_id_fk_empleados_quincena_copia" FOREIGN KEY ("cod_empleadoid_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_cat_incidencia_id_fk_incidencias" FOREIGN KEY ("cod_catincidenciaid_fk") REFERENCES "sgnom"."tsgnomcatincidencia" ("cod_catincidenciaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_cat_quincena_id_fk_incidencias" FOREIGN KEY ("cod_quincenaid_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_emp_autoriza_fk_incidencias" FOREIGN KEY ("cod_empautoriza_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "nom_emp_reporta_fk_incidencias" FOREIGN KEY ("cod_empreporta_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_cat_conceptos_fk_manuales_terceros" FOREIGN KEY ("cod_conceptoid_fk") REFERENCES "sgnom"."tsgnomconcepto" ("cod_conceptoid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_cat_quincenas_fk_manuales_terceros_fin" FOREIGN KEY ("cod_quincenafin_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_cat_quincenas_fk_manuales_terceros_inicio" FOREIGN KEY ("cod_quincenainicio_fk") REFERENCES "sgnom"."tsgnomquincena" ("cod_quincenaid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "nom_empleados_fk_manuales_terceros" FOREIGN KEY ("cod_empleadoid_fk") REFERENCES "sgnom"."tsgnomempleados" ("cod_empleadoid") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."tsgnomquincena" ADD CONSTRAINT "nom_cat_ejercicio_id_fk_cat_quincenas" FOREIGN KEY ("cod_ejercicioid_fk") REFERENCES "sgnom"."tsgnomejercicio" ("cod_ejercicioid") ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgco"."tsgcousuarios" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrt"."tsgrtagenda" ADD CONSTRAINT "fk_cod_reunion" FOREIGN KEY ("cod_reunion") REFERENCES "sgrt"."tsgrtreuniones" ("cod_reunion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtarchivos" ADD CONSTRAINT "fk_cod_reunion" FOREIGN KEY ("cod_reunion") REFERENCES "sgrt"."tsgrtreuniones" ("cod_reunion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtasistentes" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtasistentes" ADD CONSTRAINT "fk_cod_invitado" FOREIGN KEY ("cod_invitado") REFERENCES "sgrt"."tsgrtinvitados" ("cod_invitado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtattchticket" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtattchticket" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtattchticket" ADD CONSTRAINT "fk_cod_ticket" FOREIGN KEY ("cod_ticket") REFERENCES "sgrt"."tsgrtticket" ("cod_ticket") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtattchticket" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtayudatopico" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtayudatopico" ADD CONSTRAINT "fk_cod_depto" FOREIGN KEY ("cod_depto") REFERENCES "sgrt"."tsgrtdepartamento" ("cod_depto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtayudatopico" ADD CONSTRAINT "fk_cod_prioridad" FOREIGN KEY ("cod_prioridad") REFERENCES "sgrt"."tsgrtprioridad" ("cod_prioridad") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtayudatopico" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtayudatopico" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcategoriafaq" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtciudades" ADD CONSTRAINT "fk_estadorep" FOREIGN KEY ("cod_estadorep") REFERENCES "sgrt"."tsgrtestados" ("cod_estadorep") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcomentariosagenda" ADD CONSTRAINT "fk_cod_agenda" FOREIGN KEY ("cod_agenda") REFERENCES "sgrt"."tsgrtagenda" ("cod_agenda") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcomentariosagenda" ADD CONSTRAINT "fk_cod_invitado" FOREIGN KEY ("cod_invitado") REFERENCES "sgrt"."tsgrtinvitados" ("cod_invitado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcomentariosreunion" ADD CONSTRAINT "fk_cod_invitado" FOREIGN KEY ("cod_invitado") REFERENCES "sgrt"."tsgrtinvitados" ("cod_invitado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcomentariosreunion" ADD CONSTRAINT "fk_cod_reunion" FOREIGN KEY ("cod_reunion") REFERENCES "sgrt"."tsgrtreuniones" ("cod_reunion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcompromisos" ADD CONSTRAINT "fk_cod_chat" FOREIGN KEY ("cod_chat") REFERENCES "sgrt"."tsgrtchat" ("cod_chat") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgrt"."tsgrtcompromisos" ADD CONSTRAINT "fk_cod_reunion" FOREIGN KEY ("cod_reunion") REFERENCES "sgrt"."tsgrtreuniones" ("cod_reunion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcorreo" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcorreo" ADD CONSTRAINT "fk_cod_depto" FOREIGN KEY ("cod_depto") REFERENCES "sgrt"."tsgrtdepartamento" ("cod_depto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcorreo" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcorreo" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtcorreo" ADD CONSTRAINT "fk_cod_usuario" FOREIGN KEY ("cod_usuario") REFERENCES "sgco"."tsgcousuarios" ("cod_usuario") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdatossolicitud" ADD CONSTRAINT "fk_cod_edosolicitud" FOREIGN KEY ("cod_edosolicitud") REFERENCES "sgrt"."tsgrtedosolicitudes" ("cod_edosolicitud") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdatossolicitud" ADD CONSTRAINT "fk_cod_elemento" FOREIGN KEY ("cod_elemento") REFERENCES "sgrt"."tsgrtelementos" ("cod_elemento") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdatossolicitud" ADD CONSTRAINT "fk_cod_solicitud" FOREIGN KEY ("cod_solicitud") REFERENCES "sgrt"."tsgrtsolicitudservicios" ("cod_solicitud") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdepartamento" ADD CONSTRAINT "fk_cod_correo" FOREIGN KEY ("cod_correo") REFERENCES "sgrt"."tsgrtcorreo" ("cod_correo") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdepartamento" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdepartamento" ADD CONSTRAINT "fk_cod_plantillacorreo" FOREIGN KEY ("cod_plantillacorreo") REFERENCES "sgrt"."tsgrtplantillacorreos" ("cod_plantillacorreo") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdepartamento" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtdepartamento" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtfaq" ADD CONSTRAINT "fk_cod_categoria" FOREIGN KEY ("cod_categoriafaq") REFERENCES "sgrt"."tsgrtcategoriafaq" ("cod_categoriafaq") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtfaq" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtfaq" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtgrupo" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtgrupo" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtgrupo" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtinvitados" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtinvitados" ADD CONSTRAINT "fk_cod_reunion" FOREIGN KEY ("cod_reunion") REFERENCES "sgrt"."tsgrtreuniones" ("cod_reunion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtlugares" ADD CONSTRAINT "fk_cod_ciudad" FOREIGN KEY ("cod_ciudad") REFERENCES "sgrt"."tsgrtciudades" ("cod_ciudad") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtmsjticket" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtmsjticket" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtmsjticket" ADD CONSTRAINT "fk_cod_ticket" FOREIGN KEY ("cod_ticket") REFERENCES "sgrt"."tsgrtticket" ("cod_ticket") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtmsjticket" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtnota" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtnota" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtnota" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtnota" ADD CONSTRAINT "fk_cod_ticket" FOREIGN KEY ("cod_ticket") REFERENCES "sgrt"."tsgrtticket" ("cod_ticket") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtnota" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtplantillacorreos" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtplantillacorreos" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtplantillacorreos" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtprioridad" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtprioridad" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtprioridad" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtresppredefinida" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtresppredefinida" ADD CONSTRAINT "fk_cod_depto" FOREIGN KEY ("cod_depto") REFERENCES "sgrt"."tsgrtdepartamento" ("cod_depto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtresppredefinida" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtresppredefinida" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtrespuesta" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtrespuesta" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtrespuesta" ADD CONSTRAINT "fk_cod_mensaje" FOREIGN KEY ("cod_mensaje") REFERENCES "sgrt"."tsgrtmsjticket" ("cod_mensaje") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtrespuesta" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtrespuesta" ADD CONSTRAINT "fk_cod_ticket" FOREIGN KEY ("cod_ticket") REFERENCES "sgrt"."tsgrtticket" ("cod_ticket") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtrespuesta" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtreuniones" ADD CONSTRAINT "fk_cod_creadorreunion" FOREIGN KEY ("cod_creadorreunion") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtreuniones" ADD CONSTRAINT "fk_cod_lugar" FOREIGN KEY ("cod_lugar") REFERENCES "sgrt"."tsgrtlugares" ("cod_lugar") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtreuniones" ADD CONSTRAINT "fk_cod_responsable" FOREIGN KEY ("cod_responsable") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtreuniones" ADD CONSTRAINT "fk_cod_reunionanterior" FOREIGN KEY ("cod_proximareunion") REFERENCES "sgrt"."tsgrtreuniones" ("cod_reunion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtsolicitudservicios" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtsolicitudservicios" ADD CONSTRAINT "fk_cod_servicio" FOREIGN KEY ("cod_servicio") REFERENCES "sgrt"."tsgrtservicios" ("cod_servicio") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtsolicitudservicios" ADD CONSTRAINT "fk_cod_ticket" FOREIGN KEY ("cod_ticket") REFERENCES "sgrt"."tsgrtticket" ("cod_ticket") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtticket" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtticket" ADD CONSTRAINT "fk_cod_depto" FOREIGN KEY ("cod_depto") REFERENCES "sgrt"."tsgrtdepartamento" ("cod_depto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtticket" ADD CONSTRAINT "fk_cod_empleado" FOREIGN KEY ("cod_empleado") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtticket" ADD CONSTRAINT "fk_cod_prioridad" FOREIGN KEY ("cod_prioridad") REFERENCES "sgrt"."tsgrtprioridad" ("cod_prioridad") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtticket" ADD CONSTRAINT "fk_cod_sistemasuite" FOREIGN KEY ("cod_sistemasuite") REFERENCES "sgco"."tsgcosistemas" ("cod_sistema") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtticket" ADD CONSTRAINT "fk_cod_topico" FOREIGN KEY ("cod_topico") REFERENCES "sgrt"."tsgrtayudatopico" ("cod_topico") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgrt"."tsgrtticket" ADD CONSTRAINT "fk_cod_ultactualizacionpor" FOREIGN KEY ("cod_ultactualizacionpor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatasignaciones" ADD CONSTRAINT "fk_cod_cliente" FOREIGN KEY ("cod_cliente") REFERENCES "sgrh"."tsgrhclientes" ("cod_cliente") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatasignaciones" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatasignaciones" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatasignaciones" ADD CONSTRAINT "fk_cod_perfil" FOREIGN KEY ("cod_perfil") REFERENCES "sgrh"."tsgrhperfiles" ("cod_perfil") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatasignaciones" ADD CONSTRAINT "fk_cod_prospecto" FOREIGN KEY ("cod_prospecto") REFERENCES "sisat"."tsisatprospectos" ("cod_prospecto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcandidatos" ADD CONSTRAINT "fk_cod_perfil" FOREIGN KEY ("cod_perfil") REFERENCES "sgrh"."tsgrhperfiles" ("cod_perfil") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaaceptacion" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaaceptacion" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_ape" FOREIGN KEY ("cod_ape") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_cliente" FOREIGN KEY ("cod_cliente") REFERENCES "sgrh"."tsgrhclientes" ("cod_cliente") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_gpy" FOREIGN KEY ("cod_gpy") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_perfil" FOREIGN KEY ("cod_perfil") REFERENCES "sgrh"."tsgrhperfiles" ("cod_perfil") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_prospecto" FOREIGN KEY ("cod_prospecto") REFERENCES "sisat"."tsisatprospectos" ("cod_prospecto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_rhat" FOREIGN KEY ("cod_rhta") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcartaasignacion" ADD CONSTRAINT "fk_cod_rys" FOREIGN KEY ("cod_rys") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcotizaciones" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcotizaciones" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcotizaciones" ADD CONSTRAINT "fk_cod_puesto" FOREIGN KEY ("cod_puesto") REFERENCES "sgrh"."tsgrhpuestos" ("cod_puesto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatcursosycerticados" ADD CONSTRAINT "fk_cod_prospecto" FOREIGN KEY ("cod_prospecto") REFERENCES "sisat"."tsisatprospectos" ("cod_prospecto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatentrevistas" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatentrevistas" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatenviocorreos" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatenviocorreos" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatescolaridad" ADD CONSTRAINT "fk_cod_prospecto" FOREIGN KEY ("cod_prospecto") REFERENCES "sisat"."tsisatprospectos" ("cod_prospecto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatexperienciaslaborales" ADD CONSTRAINT "fk_cod_prospecto" FOREIGN KEY ("cod_prospecto") REFERENCES "sisat"."tsisatprospectos" ("cod_prospecto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatfirmas" ADD CONSTRAINT "fk_cod_autoriza" FOREIGN KEY ("cod_autoriza") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatfirmas" ADD CONSTRAINT "fk_cod_contratacion" FOREIGN KEY ("cod_contratacion") REFERENCES "sgrh"."tsgrhcontrataciones" ("cod_contratacion") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatfirmas" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatfirmas" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatfirmas" ADD CONSTRAINT "fk_cod_puestoautoriza" FOREIGN KEY ("cod_puestoautoriza") REFERENCES "sgrh"."tsgrhpuestos" ("cod_puesto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatfirmas" ADD CONSTRAINT "fk_cod_puestosolicita" FOREIGN KEY ("cod_puestosolicita") REFERENCES "sgrh"."tsgrhpuestos" ("cod_puesto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatfirmas" ADD CONSTRAINT "fk_cod_solicita" FOREIGN KEY ("cod_solicita") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatidiomas" ADD CONSTRAINT "fk_cod_prospecto" FOREIGN KEY ("cod_prospecto") REFERENCES "sisat"."tsisatprospectos" ("cod_prospecto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatordenservicio" ADD CONSTRAINT "fk_cod_ciudad" FOREIGN KEY ("cod_ciudad") REFERENCES "sgrt"."tsgrtciudades" ("cod_ciudad") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatordenservicio" ADD CONSTRAINT "fk_cod_cliente" FOREIGN KEY ("cod_cliente") REFERENCES "sgrh"."tsgrhclientes" ("cod_cliente") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatordenservicio" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatordenservicio" ADD CONSTRAINT "fk_cod_estadorep" FOREIGN KEY ("cod_estadorep") REFERENCES "sgrt"."tsgrtestados" ("cod_estadorep") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatordenservicio" ADD CONSTRAINT "fk_cod_gpy" FOREIGN KEY ("cod_gpy") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatordenservicio" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatordenservicio" ADD CONSTRAINT "fk_cod_puesto" FOREIGN KEY ("cod_puesto") REFERENCES "sgrh"."tsgrhpuestos" ("cod_puesto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatprospectos" ADD CONSTRAINT "fk_cod_administrador" FOREIGN KEY ("cod_administrador") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatprospectos" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatprospectos" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatproyectos" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatproyectos" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatproyectos" ADD CONSTRAINT "fk_cod_perfil" FOREIGN KEY ("cod_perfil") REFERENCES "sgrh"."tsgrhperfiles" ("cod_perfil") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatproyectos" ADD CONSTRAINT "fk_cod_prospecto" FOREIGN KEY ("cod_prospecto") REFERENCES "sisat"."tsisatprospectos" ("cod_prospecto") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatvacantes" ADD CONSTRAINT "fk_cod_creadopor" FOREIGN KEY ("cod_creadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sisat"."tsisatvacantes" ADD CONSTRAINT "fk_cod_modificadopor" FOREIGN KEY ("cod_modificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "sgnom"."tsgnomempleados" ADD CONSTRAINT "fk_tsgnomempleados_tsgrhempleados_1" FOREIGN KEY ("cod_empleado_fk") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomempleados" ADD CONSTRAINT "fk_tsgnomempleados_tsgrhempleados_2" FOREIGN KEY ("aud_codcreadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomempleados" ADD CONSTRAINT "fk_tsgnomempleados_tsgrhempleados_3" FOREIGN KEY ("aud_codmodificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "fk_tsgnomincidencia_tsgrhempleados_1" FOREIGN KEY ("cod_empreporta_fk") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "fk_tsgnomincidencia_tsgrhempleados_2" FOREIGN KEY ("cod_empautoriza_fk") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomincidencia" ADD CONSTRAINT "fk_tsgnomincidencia_tsgrhempleados_3" FOREIGN KEY ("aud_codmodificadopor") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "fk_tsgnommanterceros_tsgrhempleados_1" FOREIGN KEY ("aud_ucrea") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnommanterceros" ADD CONSTRAINT "fk_tsgnommanterceros_tsgrhempleados_2" FOREIGN KEY ("aud_uactualizacion") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "fk_tsgnomconcepto_tsgrhempleados_1" FOREIGN KEY ("aud_ualta") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomconcepto" ADD CONSTRAINT "fk_tsgnomconcepto_tsgrhempleados_2" FOREIGN KEY ("aud_uactualizacion") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "fk_tsgnomcabecera_tsgrhempleados_1" FOREIGN KEY ("aud_ucrea") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");
ALTER TABLE "sgnom"."tsgnomcabecera" ADD CONSTRAINT "fk_tsgnomcabecera_tsgrhempleados_2" FOREIGN KEY ("aud_uactualiza") REFERENCES "sgrh"."tsgrhempleados" ("cod_empleado");

