/*
 Navicat PostgreSQL Data Transfer

 Source Server         : Nomina
 Source Server Type    : PostgreSQL
 Source Server Version : 110002
 Source Host           : localhost:5432
 Source Catalog        : nomina
 Source Schema         : sgnom

 Target Server Type    : PostgreSQL
 Target Server Version : 110002
 File Encoding         : 65001

 Date: 10/04/2019 22:31:18
*/


-- ----------------------------
-- Table structure for TSGNOMALGUINALDO
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMALGUINALDO";
CREATE TABLE "sgnom"."TSGNOMALGUINALDO" (
  "COD_AGUINALDOID" int4 NOT NULL,
  "IMP_AGUINALDO" numeric(10,2) NOT NULL,
  "COD_TIPOAGUINALDO" char(1) COLLATE "pg_catalog"."default" NOT NULL,
  "COD_EMPQUINCENAID_FK" int4 NOT NULL,
  "BOL_ESTATUS" bool NOT NULL,
  "XML_DESGLOCE" xml
)
;
ALTER TABLE "sgnom"."TSGNOMALGUINALDO" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMALGUINALDO" IS 'COD_TIPOAGUINALDO

(

i = imss,

h = honorarios

)';

-- ----------------------------
-- Table structure for TSGNOMARGUMENTO
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMARGUMENTO";
CREATE TABLE "sgnom"."TSGNOMARGUMENTO" (
  "COD_ARGUMENTOID" int4 NOT NULL,
  "COD_NBARGUMENTO" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "COD_CLAVEARG" varchar(5) COLLATE "pg_catalog"."default" NOT NULL,
  "IMP_VALORCONST" numeric(10,2),
  "DES_FUNCIONBD" varchar(60) COLLATE "pg_catalog"."default",
  "BOL_ESTATUS" bool NOT NULL,
  "TXT_DESCRIPCION" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "sgnom"."TSGNOMARGUMENTO" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMBITACORA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMBITACORA";
CREATE TABLE "sgnom"."TSGNOMBITACORA" (
  "COD_BITACORAID" int4 NOT NULL,
  "XML_BITACORA" xml NOT NULL,
  "COD_TABLAID_FK" int4 NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMBITACORA" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMCABECERA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCABECERA";
CREATE TABLE "sgnom"."TSGNOMCABECERA" (
  "COD_CABECERAID" int4 NOT NULL,
  "COD_NBNOMINA" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "FEC_CREACION" date NOT NULL,
  "FEC_EJECUCION" date NOT NULL,
  "FEC_CIERRE" date NOT NULL,
  "IMP_TOTPERCEPCION" numeric(10,2) NOT NULL,
  "IMP_TOTDEDUCCION" numeric(10,2) NOT NULL,
  "IMP_TOTALEMP" numeric(10,2) NOT NULL,
  "COD_QUINCENAID_FK" int4 NOT NULL,
  "COD_TIPONOMINAID_FK" int4 NOT NULL,
  "COD_ESTATUSNOMID_FK" int4 NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMCABECERA" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMCABECERAHT
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCABECERAHT";
CREATE TABLE "sgnom"."TSGNOMCABECERAHT" (
  "COD_CABECERAID" int4 NOT NULL,
  "COD_NBNOMINA" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "FEC_CREACION" date NOT NULL,
  "FEC_EJECUCION" date NOT NULL,
  "FEC_CIERRE" date NOT NULL,
  "IMP_TOTPERCEPCION" numeric(10,2) NOT NULL,
  "IMP_TOTDEDUCCION" numeric(10,2) NOT NULL,
  "IMP_TOTALEMP" numeric(10,2) NOT NULL,
  "COD_QUINCENAID_FK" int4 NOT NULL,
  "COD_TIPONOMINAID_FK" int4 NOT NULL,
  "COD_ESTATUSNOMID_FK" int4 NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMCABECERAHT" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMCALCULO
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCALCULO";
CREATE TABLE "sgnom"."TSGNOMCALCULO" (
  "COD_CALCULOID" int4 NOT NULL,
  "COD_TPCALCULO" varchar(25) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMCALCULO" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMCALCULO" IS 'BOL_ESTATUS

(

activo, inactivo

)';

-- ----------------------------
-- Table structure for TSGNOMCATINCIDENCIA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCATINCIDENCIA";
CREATE TABLE "sgnom"."TSGNOMCATINCIDENCIA" (
  "COD_CATINCIDENCIAID" int4 NOT NULL,
  "COD_CLAVEINCIDENCIA" varchar(5) COLLATE "pg_catalog"."default",
  "COD_NBINCIDENCIA" varchar(20) COLLATE "pg_catalog"."default",
  "COD_PERFILINCIDENCIA" varchar(25) COLLATE "pg_catalog"."default",
  "BOL_ESTATUS" bool,
  "COD_TIPOINCIDENCIA" char(1) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "sgnom"."TSGNOMCATINCIDENCIA" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMCATINCIDENCIA" IS 'COD_TIPOINCIDENCIA

(

1 = HORAS

2 = DIAS

3 = ACTIVIDAD

)';

-- ----------------------------
-- Table structure for TSGNOMCLASIFICADOR
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCLASIFICADOR";
CREATE TABLE "sgnom"."TSGNOMCLASIFICADOR" (
  "COD_CLASIFICADORID" int4 NOT NULL,
  "COD_TPCLASIFICADOR" varchar(20) COLLATE "pg_catalog"."default",
  "BOL_ESTATUS" bool
)
;
ALTER TABLE "sgnom"."TSGNOMCLASIFICADOR" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMCNCPTOQUINC
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCNCPTOQUINC";
CREATE TABLE "sgnom"."TSGNOMCNCPTOQUINC" (
  "COD_CNCPTOQUINCID" int4 NOT NULL,
  "COD_EMPQUINCENAID_FK" int4 NOT NULL,
  "COD_CONCEPTOID_FK" int4 NOT NULL,
  "IMP_CONCEPTO" numeric(10,2) NOT NULL,
  "IMP_GRAVADO" numeric(10,2),
  "IMP_EXENTO" numeric(10,2),
  "XML_DESGLOCE" xml
)
;
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINC" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMCNCPTOQUINCHT
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCNCPTOQUINCHT";
CREATE TABLE "sgnom"."TSGNOMCNCPTOQUINCHT" (
  "COD_CNCPTOQUINCHTID" int4 NOT NULL,
  "COD_EMPQUINCENAID_FK" int4 NOT NULL,
  "COD_CONCEPTOID_FK" int4 NOT NULL,
  "IMP_CONCEPTO" numeric(10,2) NOT NULL,
  "IMP_GRAVADO" numeric(10,2),
  "IMP_EXENTO" numeric(10,2),
  "XML_DESGLOCE" xml
)
;
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINCHT" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMCONCEPTO
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCONCEPTO";
CREATE TABLE "sgnom"."TSGNOMCONCEPTO" (
  "COD_CONCEPTOID" int4 NOT NULL,
  "COD_NBCONCEPTO" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "COD_CLAVECONCEPTO" varchar(4) COLLATE "pg_catalog"."default" NOT NULL,
  "CNU_PRIORICALCULO" int4 NOT NULL,
  "CNU_ARTICULO" int4 NOT NULL,
  "BOL_ESTATUS" bool NOT NULL,
  "COD_FORMULAID_FK" int4 NOT NULL,
  "COD_TIPOCONCEPTOID_FK" int4 NOT NULL,
  "COD_CALCULOID_FK" int4 NOT NULL,
  "COD_CONCEPTOSATID_FK" int4 NOT NULL,
  "COD_FRECUENCIAPAGO" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "COD_PARTIDAPREP" varchar(4) COLLATE "pg_catalog"."default" NOT NULL,
  "CNU_CUENTACONTABLE" int4 NOT NULL,
  "COD_GRAVADO" char(1) COLLATE "pg_catalog"."default" NOT NULL,
  "COD_EXCENTO" char(1) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_RETROACTIVIDAD" bool NOT NULL,
  "CNU_TOPEEX" int4 NOT NULL,
  "COD_CLASIFICADORID_FK" int4 NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMCONCEPTO" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMCONCEPTO" IS 'BOL_ESTATUS

(

activo, inactivo

)



GRAVADO

(

1 = dias, 

2 = porcentaje, 

3 = no aplica

)



EXCENTO

(

activo, inactivo

)





VALIDAR TOPE_EX';

-- ----------------------------
-- Table structure for TSGNOMCONCEPTOSAT
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCONCEPTOSAT";
CREATE TABLE "sgnom"."TSGNOMCONCEPTOSAT" (
  "COD_CONCEPTOSATID" int4 NOT NULL,
  "DES_CONCEPTOSAT" varchar(51) COLLATE "pg_catalog"."default" NOT NULL,
  "DES_DESCCONCEPTO" varchar(51) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMCONCEPTOSAT" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMCONCEPTOSAT" IS 'BOL_ESTATUS

(

activo, inactivo

)';

-- ----------------------------
-- Table structure for TSGNOMCONFPAGO
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMCONFPAGO";
CREATE TABLE "sgnom"."TSGNOMCONFPAGO" (
  "COD_CONFPAGOID" int4 NOT NULL,
  "BOL_PAGOEMPLEADO" bool,
  "BOL_PAGORH" bool,
  "BOL_PAGOFINANZAS" bool,
  "COD_EMPQUINCENAID_FK" int4
)
;
ALTER TABLE "sgnom"."TSGNOMCONFPAGO" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMCONFPAGO" IS 'BOL_PAGOEMPLEADO

(

confirmado, pendiente 

)



PAGO_RH

(

autorizado, pendiente

)



PAGO_FNZAS

(

autorizado, pendiente

)';

-- ----------------------------
-- Table structure for TSGNOMEJERCICIO
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMEJERCICIO";
CREATE TABLE "sgnom"."TSGNOMEJERCICIO" (
  "COD_EJERCICIOID" int4 NOT NULL,
  "CNU_VALOREJERCICIO" int4 NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMEJERCICIO" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMEMPLEADOS
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMEMPLEADOS";
CREATE TABLE "sgnom"."TSGNOMEMPLEADOS" (
  "COD_EMPLEADOID" int4 NOT NULL,
  "FEC_INGRESO" date NOT NULL,
  "FEC_SALIDA" date,
  "BOL_ESTATUS" bool NOT NULL,
  "COD_EMPLEADO_FK" int4 NOT NULL,
  "IMP_SUELDOIMSS" numeric(10,2),
  "IMP_HONORARIOS" numeric(10,2),
  "COD_TIPOIMSS" char(1) COLLATE "pg_catalog"."default",
  "COD_TIPOHONORARIOS" char(1) COLLATE "pg_catalog"."default",
  "COD_BANCO" varchar(50) COLLATE "pg_catalog"."default",
  "COD_SUCURSAL" int4,
  "COD_CUENTA" int4,
  "COD_CLABE" int4,
  "TXT_DESCRIPCIONBAJA" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "sgnom"."TSGNOMEMPLEADOS" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMEMPLEADOS" IS 'COD_EMPLEADO_FK hace referencia al schema sgrh en la tabla tsgrhempleados';
--COMMENT ON TABLE "sgnom"."TSGNOMEMPLEADOS" IS 'RH_COD_EMPLEADO_FK hace referencia al schema sgrh en la tabla tsgrhempleados';
--se quedo para saber que viene de RH

-- ----------------------------
-- Table structure for TSGNOMEMPQUINCENA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMEMPQUINCENA";
CREATE TABLE "sgnom"."TSGNOMEMPQUINCENA" (
  "COD_EMPQUINCENAID" int4 NOT NULL,
  "COD_EMPLEADOID_FK" int4 NOT NULL,
  "COD_CABECERAID_FK" int4 NOT NULL,
  "IMP_TOTPERCEPCION" numeric(10,2) NOT NULL,
  "IMP_TOTDEDUCCION" numeric(10,2) NOT NULL,
  "IMP_TOTALEMP" numeric(10,2) NOT NULL,
  "BOL_ESTATUSEMP" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENA" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMEMPQUINCENA" IS 'BOL_ESTATUSEMP

(

activo, inactivo

)';

-- ----------------------------
-- Table structure for TSGNOMEMPQUINCENAHT
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMEMPQUINCENAHT";
CREATE TABLE "sgnom"."TSGNOMEMPQUINCENAHT" (
  "COD_EMPQUINCENAHTID" int4 NOT NULL,
  "COD_EMPLEADOID_FK" int4 NOT NULL,
  "COD_CABECERAID_FK" int4 NOT NULL,
  "IMP_TOTPERCEPCION" numeric(10,2) NOT NULL,
  "IMP_TOTDEDUCCION" numeric(10,2) NOT NULL,
  "IMP_TOTALEMP" numeric(10,2) NOT NULL,
  "BOL_ESTATUSEMP" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENAHT" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMESTATUSNOM
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMESTATUSNOM";
CREATE TABLE "sgnom"."TSGNOMESTATUSNOM" (
  "COD_ESTATUSNOMID" int4 NOT NULL,
  "COD_ESTATUSNOMINA" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMESTATUSNOM" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMESTATUSNOM" IS 'COD_ESTATUSNOMID 

ESTATUS

(

abierta,

calculada,

revision,

validada,

cerrada

)



nota: en caso de no ser validada entra en revision y posteriormente calcular nuevamente 

hasta ser validada';

-- ----------------------------
-- Table structure for TSGNOMFORMULA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMFORMULA";
CREATE TABLE "sgnom"."TSGNOMFORMULA" (
  "COD_FORMULAID" int4 NOT NULL,
  "DES_NBFORMULA" varchar(60) COLLATE "pg_catalog"."default" NOT NULL,
  "DES_FORMULA" varchar(250) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMFORMULA" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMFORMULA" IS 'BOL_ESTATUS

(

activo, inactivo

)';

-- ----------------------------
-- Table structure for TSGNOMFUNCION
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMFUNCION";
CREATE TABLE "sgnom"."TSGNOMFUNCION" (
  "COD_FUNCIONID" int4 NOT NULL,
  "COD_NBFUNCION" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMFUNCION" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMHISTTABLA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMHISTTABLA";
CREATE TABLE "sgnom"."TSGNOMHISTTABLA" (
  "COD_TABLAID" int4 NOT NULL,
  "COD_NBTABLA" varchar(18) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMHISTTABLA" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMINCIDENCIA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMINCIDENCIA";
CREATE TABLE "sgnom"."TSGNOMINCIDENCIA" (
  "COD_INCIDENCIAID" int4 NOT NULL,
  "COD_CATINCIDENCIAID_FK" int4 NOT NULL,
  "CNU_CANTIDAD" int2,
  "DES_ACTIVIDAD" varchar(100) COLLATE "pg_catalog"."default",
  "TXT_COMENTARIOS" text COLLATE "pg_catalog"."default",
  "COD_EMPREPORTA_FK" int4,
  "COD_EMPAUTORIZA_FK" int4,
  "IMP_MONTO" numeric(10,2),
  "XML_DETCANTIDAD" xml,
  "BOL_ESTATUS" bool,
  "COD_QUINCENAID_FK" int4 NOT NULL,
  "BOL_VALIDACION" bool
)
;
ALTER TABLE "sgnom"."TSGNOMINCIDENCIA" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMINCIDENCIA" IS 'BOL_ESTATUS: (activo, inactivo) (1, 0)

VALIDACION: (validar, denegar) (1, 0) 



';

-- ----------------------------
-- Table structure for TSGNOMMANTERCEROS
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMMANTERCEROS";
CREATE TABLE "sgnom"."TSGNOMMANTERCEROS" (
  "COD_MANTERCEROSID" int4 NOT NULL,
  "COD_CONCEPTOID_FK" int4,
  "IMP_MONTO" numeric(10,2),
  "COD_QUINCENAINICIO_FK" int4,
  "COD_QUINCENAFIN_FK" int4,
  "COD_EMPLEADOID_FK" int4,
  "COD_FRECUENCIAPAGO" varchar(20) COLLATE "pg_catalog"."default",
  "BOL_ESTATUS" bool
)
;
ALTER TABLE "sgnom"."TSGNOMMANTERCEROS" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMQUINCENA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMQUINCENA";
CREATE TABLE "sgnom"."TSGNOMQUINCENA" (
  "COD_QUINCENAID" int4 NOT NULL,
  "DES_QUINCENA" varchar(70) COLLATE "pg_catalog"."default" NOT NULL,
  "FEC_INICIO" date NOT NULL,
  "FEC_FIN" date NOT NULL,
  "FEC_PAGO" date NOT NULL,
  "FEC_DISPERSION" date NOT NULL,
  "CNU_NUMQUINCENA" int4 NOT NULL,
  "COD_EJERCICIOID_FK" int4 NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMQUINCENA" OWNER TO "suite";

-- ----------------------------
-- Table structure for TSGNOMTIPOCONCEPTO
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMTIPOCONCEPTO";
CREATE TABLE "sgnom"."TSGNOMTIPOCONCEPTO" (
  "COD_TIPOCONCEPTOID" int4 NOT NULL,
  "COD_TIPOCONCEPTO" varchar(25) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMTIPOCONCEPTO" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMTIPOCONCEPTO" IS 'BOL_ESTATUS

(

activo, inactivo

)';

-- ----------------------------
-- Table structure for TSGNOMTIPONOMINA
-- ----------------------------
DROP TABLE IF EXISTS "sgnom"."TSGNOMTIPONOMINA";
CREATE TABLE "sgnom"."TSGNOMTIPONOMINA" (
  "COD_TIPONOMINAID" int4 NOT NULL,
  "COD_NOMINA" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "BOL_ESTATUS" bool NOT NULL
)
;
ALTER TABLE "sgnom"."TSGNOMTIPONOMINA" OWNER TO "suite";
COMMENT ON TABLE "sgnom"."TSGNOMTIPONOMINA" IS 'BOL_ESTATUS (activa, inactiva)';

-- ----------------------------
-- Primary Key structure for table TSGNOMALGUINALDO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMALGUINALDO" ADD CONSTRAINT
 "NOM_AGUINALDO_ID" 
PRIMARY KEY ("COD_AGUINALDOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMARGUMENTO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMARGUMENTO" ADD CONSTRAINT
 "NOM_CAT_ARGUMENTO_ID" 
PRIMARY KEY ("COD_ARGUMENTOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMBITACORA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMBITACORA" ADD CONSTRAINT
 "NOM_BITACORA_ID" 
PRIMARY KEY ("COD_BITACORAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCABECERA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCABECERA" ADD CONSTRAINT
 "NOM_CABECERA_ID" 
PRIMARY KEY ("COD_CABECERAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCABECERAHT
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCABECERAHT" ADD CONSTRAINT
 "NOM_CABECERA_COPIA_ID" 
PRIMARY KEY ("COD_CABECERAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCALCULO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCALCULO" ADD CONSTRAINT
 "NOM_CAT_TIPO_CALCULO_ID" 
PRIMARY KEY ("COD_CALCULOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCATINCIDENCIA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCATINCIDENCIA" ADD CONSTRAINT
 "CAT_INCIDENCIA_ID" 
PRIMARY KEY ("COD_CATINCIDENCIAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCLASIFICADOR
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCLASIFICADOR" ADD CONSTRAINT
 "NOM_CAT_TIPO_CLASIFICADOR_ID" 
PRIMARY KEY ("COD_CLASIFICADORID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCNCPTOQUINC
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINC" ADD CONSTRAINT
 "NOM_CONCEPTOS_QUINCENA_ID" 
PRIMARY KEY ("COD_CNCPTOQUINCID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCNCPTOQUINCHT
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINCHT" ADD CONSTRAINT
 "NOM_CONCEPTOS_QUINCENA_COPIA_ID" 
PRIMARY KEY ("COD_CNCPTOQUINCHTID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCONCEPTO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCONCEPTO" ADD CONSTRAINT
 "NOM_CAT_CONCEPTO_ID" 
PRIMARY KEY ("COD_CONCEPTOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCONCEPTOSAT
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCONCEPTOSAT" ADD CONSTRAINT
 "NOM_CAT_CONCEPTO_SAT_ID" 
PRIMARY KEY ("COD_CONCEPTOSATID");

-- ----------------------------
-- Primary Key structure for table TSGNOMCONFPAGO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCONFPAGO" ADD CONSTRAINT
 "NOM_CONF_PAGO_pkey" 
PRIMARY KEY ("COD_CONFPAGOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMEJERCICIO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMEJERCICIO" ADD CONSTRAINT
 "NOM_CAT_EJERCICIO_ID" 
PRIMARY KEY ("COD_EJERCICIOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMEMPLEADOS
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMEMPLEADOS" ADD CONSTRAINT
 "NOM_EMPLEADO_ID" 
PRIMARY KEY ("COD_EMPLEADOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMEMPQUINCENA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENA" ADD CONSTRAINT
 "NOM_EMPLEADO_QUINCENA_ID" 
PRIMARY KEY ("COD_EMPQUINCENAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMEMPQUINCENAHT
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENAHT" ADD CONSTRAINT
 "NOM_EMPLEADO_QUINCENA_COPIA_ID" 
PRIMARY KEY ("COD_EMPQUINCENAHTID");

-- ----------------------------
-- Primary Key structure for table TSGNOMESTATUSNOM
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMESTATUSNOM" ADD CONSTRAINT
 "NOM_CAT_ESTATUS_NOMINA_ID" 
PRIMARY KEY ("COD_ESTATUSNOMID");

-- ----------------------------
-- Primary Key structure for table TSGNOMFORMULA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMFORMULA" ADD CONSTRAINT
 "NOM_CAT_FORMULA_ID" 
PRIMARY KEY ("COD_FORMULAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMFUNCION
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMFUNCION" ADD CONSTRAINT
 "NOM_CAT_FUNCION_ID" 
PRIMARY KEY ("COD_FUNCIONID");

-- ----------------------------
-- Primary Key structure for table TSGNOMHISTTABLA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMHISTTABLA" ADD CONSTRAINT
 "NOM_CAT_TABLA_ID" 
PRIMARY KEY ("COD_TABLAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMINCIDENCIA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMINCIDENCIA" ADD CONSTRAINT
 "NOM_INCIDENCIA_ID" 
PRIMARY KEY ("COD_INCIDENCIAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMMANTERCEROS
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMMANTERCEROS" ADD CONSTRAINT
 "NOM_MANUALES_TERCEROS_pkey" 
PRIMARY KEY ("COD_MANTERCEROSID");

-- ----------------------------
-- Primary Key structure for table TSGNOMQUINCENA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMQUINCENA" ADD CONSTRAINT
 "NOM_CAT_QUINCENA_ID" 
PRIMARY KEY ("COD_QUINCENAID");

-- ----------------------------
-- Primary Key structure for table TSGNOMTIPOCONCEPTO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMTIPOCONCEPTO" ADD CONSTRAINT
 "NOM_CAT_TIPO_CONEPTO_ID" 
PRIMARY KEY ("COD_TIPOCONCEPTOID");

-- ----------------------------
-- Primary Key structure for table TSGNOMTIPONOMINA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMTIPONOMINA" ADD CONSTRAINT
 "NOM_CAT_TIPO_NOMINA_ID" 
PRIMARY KEY ("COD_TIPONOMINAID");

-- ----------------------------
-- Foreign Keys structure for table TSGNOMALGUINALDO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMALGUINALDO" ADD CONSTRAINT
 "NOM_EMPLEADOS_QUINCENA_FK_AGUINALDOS"
 FOREIGN KEY ("COD_EMPQUINCENAID_FK") 
 REFERENCES "sgnom"."TSGNOMEMPQUINCENA" ("COD_EMPQUINCENAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMBITACORA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMBITACORA" ADD CONSTRAINT
 "NOM_CAT_TABLA_ID_FK_NOM_CAT_TABLAS"
 FOREIGN KEY ("COD_TABLAID_FK") 
 REFERENCES "sgnom"."TSGNOMHISTTABLA" ("COD_TABLAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMCABECERA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCABECERA" ADD CONSTRAINT
 "NOM_CAT_ESTATUS_NOMINA_ID_FK_CABECERAS"
 FOREIGN KEY ("COD_ESTATUSNOMID_FK") 
 REFERENCES "sgnom"."TSGNOMESTATUSNOM" ("COD_ESTATUSNOMID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCABECERA" ADD CONSTRAINT
 "NOM_CAT_QUINCENA_ID_FK_CABECERAS"
 FOREIGN KEY ("COD_QUINCENAID_FK") 
 REFERENCES "sgnom"."TSGNOMQUINCENA" ("COD_QUINCENAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCABECERA" ADD CONSTRAINT
 "NOM_CAT_TIPO_NOMINA_ID_FK_CABECERAS"
 FOREIGN KEY ("COD_TIPONOMINAID_FK") 
 REFERENCES "sgnom"."TSGNOMTIPONOMINA" ("COD_TIPONOMINAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMCABECERAHT
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCABECERAHT" ADD CONSTRAINT
 "NOM_CAT_ESTATUS_NOMINA_ID_COPIA_FK_CABECERAS"
 FOREIGN KEY ("COD_ESTATUSNOMID_FK") 
 REFERENCES "sgnom"."TSGNOMESTATUSNOM" ("COD_ESTATUSNOMID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCABECERAHT" ADD CONSTRAINT
 "NOM_CAT_QUINCENA_ID_COPIA_FK_CABECERAS"
 FOREIGN KEY ("COD_QUINCENAID_FK") 
 REFERENCES "sgnom"."TSGNOMQUINCENA" ("COD_QUINCENAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCABECERAHT" ADD CONSTRAINT
 "NOM_CAT_TIPO_NOMINA_ID_COPIA_FK_CABECERAS"
 FOREIGN KEY ("COD_TIPONOMINAID_FK") 
 REFERENCES "sgnom"."TSGNOMTIPONOMINA" ("COD_TIPONOMINAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMCNCPTOQUINC
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINC" ADD CONSTRAINT
 "CONCEPTO_QUINCENA_ID_FK_CONCEPTOS_QUINCENA"
 FOREIGN KEY ("COD_CONCEPTOID_FK") 
 REFERENCES "sgnom"."TSGNOMCONCEPTO" ("COD_CONCEPTOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINC" ADD CONSTRAINT
 "EMPLEADO_CONCEPTO_ID_FK_CONCEPTOS_QUINCENA"
 FOREIGN KEY ("COD_EMPQUINCENAID_FK") 
 REFERENCES "sgnom"."TSGNOMEMPQUINCENA" ("COD_EMPQUINCENAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMCNCPTOQUINCHT
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINCHT" ADD CONSTRAINT
 "CONCEPTO_QUINCENA_ID_COPIA_FK_CONCEPTOS_QUINCENA"
 FOREIGN KEY ("COD_CONCEPTOID_FK") 
 REFERENCES "sgnom"."TSGNOMCONCEPTO" ("COD_CONCEPTOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCNCPTOQUINCHT" ADD CONSTRAINT
 "EMPLEADO_CONCEPTO_ID_COPIA_FK_CONCEPTOS_QUINCENA"
 FOREIGN KEY ("COD_EMPQUINCENAID_FK") 
 REFERENCES "sgnom"."TSGNOMEMPQUINCENA" ("COD_EMPQUINCENAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMCONCEPTO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCONCEPTO" ADD CONSTRAINT
 "NOM_CAT_CLASIFICADOR_ID_FK_CAT_CONCEPTOS"
 FOREIGN KEY ("COD_CLASIFICADORID_FK") 
 REFERENCES "sgnom"."TSGNOMCLASIFICADOR" ("COD_CLASIFICADORID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCONCEPTO" ADD CONSTRAINT
 "NOM_CONCEPTO_SAT_ID_FK_CAT_CONCEPTOS"
 FOREIGN KEY ("COD_CONCEPTOSATID_FK") 
 REFERENCES "sgnom"."TSGNOMCONCEPTOSAT" ("COD_CONCEPTOSATID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCONCEPTO" ADD CONSTRAINT
 "NOM_FORMULA_ID_FK_CAT_CONCEPTOS"
 FOREIGN KEY ("COD_FORMULAID_FK") 
 REFERENCES "sgnom"."TSGNOMFORMULA" ("COD_FORMULAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCONCEPTO" ADD CONSTRAINT
 "NOM_TIPO_CALCULO_ID_FK_CAT_CONCEPTOS"
 FOREIGN KEY ("COD_CALCULOID_FK") 
 REFERENCES "sgnom"."TSGNOMCALCULO" ("COD_CALCULOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMCONCEPTO" ADD CONSTRAINT
 "NOM_TIPO_CONCEPTO_ID_FK_CAT_CONCEPTOS"
 FOREIGN KEY ("COD_TIPOCONCEPTOID_FK") 
 REFERENCES "sgnom"."TSGNOMTIPOCONCEPTO" ("COD_TIPOCONCEPTOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMCONFPAGO
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMCONFPAGO" ADD CONSTRAINT
 "NOM_EMPLEADOS_QUINCENA_FK_CONF_PAGO"
 FOREIGN KEY ("COD_EMPQUINCENAID_FK") 
 REFERENCES "sgnom"."TSGNOMEMPQUINCENA" ("COD_EMPQUINCENAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMEMPQUINCENA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENA" ADD CONSTRAINT
 "NOM_CABECERA_ID_FK_EMPLEADOS_QUINCENA"
 FOREIGN KEY ("COD_CABECERAID_FK") 
 REFERENCES "sgnom"."TSGNOMCABECERA" ("COD_CABECERAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENA" ADD CONSTRAINT
 "NOM_EMPLEADO_QUINCENA_ID_FK_EMPLEADOS_QUINCENA"
 FOREIGN KEY ("COD_EMPLEADOID_FK") 
 REFERENCES "sgnom"."TSGNOMEMPLEADOS" ("COD_EMPLEADOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMEMPQUINCENAHT
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENAHT" ADD CONSTRAINT
 "NOM_CABECERA_ID_FK_EMPLEADOS_QUINCENA_COPIA"
 FOREIGN KEY ("COD_CABECERAID_FK") 
 REFERENCES "sgnom"."TSGNOMCABECERA" ("COD_CABECERAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMEMPQUINCENAHT" ADD CONSTRAINT
 "NOM_EMPLEADO_QUINCENA_ID_FK_EMPLEADOS_QUINCENA_COPIA"
 FOREIGN KEY ("COD_EMPLEADOID_FK") 
 REFERENCES "sgnom"."TSGNOMEMPLEADOS" ("COD_EMPLEADOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMINCIDENCIA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMINCIDENCIA" ADD CONSTRAINT
 "NOM_CAT_INCIDENCIA_ID_FK_INCIDENCIAS"
 FOREIGN KEY ("COD_CATINCIDENCIAID_FK") 
 REFERENCES "sgnom"."TSGNOMCATINCIDENCIA" ("COD_CATINCIDENCIAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMINCIDENCIA" ADD CONSTRAINT
 "NOM_CAT_QUINCENA_ID_FK_INCIDENCIAS"
 FOREIGN KEY ("COD_QUINCENAID_FK") 
 REFERENCES "sgnom"."TSGNOMQUINCENA" ("COD_QUINCENAID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMINCIDENCIA" ADD CONSTRAINT
 "NOM_EMP_AUTORIZA_FK_INCIDENCIAS"
 FOREIGN KEY ("COD_EMPAUTORIZA_FK") 
 REFERENCES "sgnom"."TSGNOMEMPLEADOS" ("COD_EMPLEADOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE "sgnom"."TSGNOMINCIDENCIA" ADD CONSTRAINT
 "NOM_EMP_REPORTA_FK_INCIDENCIAS"
 FOREIGN KEY ("COD_EMPREPORTA_FK") 
 REFERENCES "sgnom"."TSGNOMEMPLEADOS" ("COD_EMPLEADOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMMANTERCEROS
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMMANTERCEROS" ADD CONSTRAINT
 "NOM_CAT_CONCEPTOS_FK_MANUALES_TERCEROS"
 FOREIGN KEY ("COD_CONCEPTOID_FK") 
 REFERENCES "sgnom"."TSGNOMCONCEPTO" ("COD_CONCEPTOID") 
 ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."TSGNOMMANTERCEROS" ADD CONSTRAINT
 "NOM_CAT_QUINCENAS_FK_MANUALES_TERCEROS_FIN"
 FOREIGN KEY ("COD_QUINCENAFIN_FK") 
 REFERENCES "sgnom"."TSGNOMQUINCENA" ("COD_QUINCENAID") 
 ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."TSGNOMMANTERCEROS" ADD CONSTRAINT
 "NOM_CAT_QUINCENAS_FK_MANUALES_TERCEROS_INICIO"
 FOREIGN KEY ("COD_QUINCENAINICIO_FK") 
 REFERENCES "sgnom"."TSGNOMQUINCENA" ("COD_QUINCENAID") 
 ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "sgnom"."TSGNOMMANTERCEROS" ADD CONSTRAINT
 "NOM_EMPLEADOS_FK_MANUALES_TERCEROS"
 FOREIGN KEY ("COD_EMPLEADOID_FK") 
 REFERENCES "sgnom"."TSGNOMEMPLEADOS" ("COD_EMPLEADOID") 
 ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table TSGNOMQUINCENA
-- ----------------------------
ALTER TABLE "sgnom"."TSGNOMQUINCENA" ADD CONSTRAINT
 "NOM_CAT_EJERCICIO_ID_FK_CAT_QUINCENAS"
 FOREIGN KEY ("COD_EJERCICIOID_FK") 
 REFERENCES "sgnom"."TSGNOMEJERCICIO" ("COD_EJERCICIOID") 
 ON DELETE NO ACTION ON UPDATE CASCADE;
